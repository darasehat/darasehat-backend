<?php

namespace App\Modules\Mobile\UserProfile\Queries;

use App\Models\UserProfile;

class UserProfileEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new UserProfile());
    }
}