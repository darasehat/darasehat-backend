<?php

namespace App\Modules\Mobile\UserProfile\Queries;

use App\Modules\Mobile\UserProfile\Queries\UserProfileEntity;
use Auth;

class UserProfileQuery extends UserProfileEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('name AS layName', 'id AS layId')
        ->where('id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [myProfileQuery description]
     * @return [type] [description]
     */
    public function myProfileQuery()
    {
        $query = $this->newEntity()
        ->select(
            'usrprf_name AS layName',
            'usrprf_home_address AS layAddress',
            'usrprf_phone_no AS layPhone',
            'usrprf_school AS laySchool',
            'adm1_id',
            'adm2_id',
            'adm3_id',
            'adm4_id',
            )
        ->where('usr_id','=',Auth::user()->id)
        ->first();
        return $query;
    }
}