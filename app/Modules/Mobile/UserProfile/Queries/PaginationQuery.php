<?php

namespace App\Modules\Mobile\UserProfile\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('usrprf_id AS layId','usrprf_name AS layName')
        ->where('is_active','=',1)
        ->paginate($rowsPerPage);
    }
}