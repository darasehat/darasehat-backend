<?php

namespace App\Modules\Mobile\UserProfile\Logics;

use App\Modules\Mobile\UserProfile\Queries\UserProfileField;
use App\Modules\Mobile\UserProfile\Queries\UserProfileQuery;
use App\Modules\Mobile\UserProfile\Queries\UserProfileDataManagerQuery;

class UserProfileDataManagerLogic extends UserProfileDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new UserProfileField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new UserProfileQuery())->setEntity());
	}

	/**
	 * untuk data register pertama kali tanpa adanya token login
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new UserProfileField())->setField($request);
        return $this->storeDataSaveFirst($datetime, $datarequest, (new UserProfileQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new UserProfileField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new UserProfileQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
		return $this->deleteData($id, $datetime, $datarequest, (new UserProfileQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new UserProfileQuery())->setEntity());
	}
}
