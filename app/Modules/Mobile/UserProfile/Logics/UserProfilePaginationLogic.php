<?php

namespace App\Modules\Mobile\UserProfile\Logics;
use App\Modules\Mobile\UserProfile\Queries\UserProfileQuery;

class UserProfilePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new UserProfileQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
