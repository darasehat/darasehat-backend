<?php

namespace App\Modules\Mobile\News\Logics;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Modules\Mobile\News\Logics\NewsDataManagerLogic;
use App\Modules\Mobile\News\Logics\NewsPaginationLogic;
use App\Modules\Mobile\News\Queries\NewsQuery;

// use App\Modules\Mobile\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\Mobile\NewsPackage\Logics\NewsPackageLogic;

class NewsLogic
{
    /**
     * [getPaginationData description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    // public function getPaginationData($request)
    // {
    //     $result = (new NewsPaginationLogic())->getPaginationData($request);
    //     return [
    //         'data' => $result,
    //         'lastRecord' => $request->rowsPerPage + $request->lastRecord
    //     ];

    // }

    /**
     * [doShow description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function doShow()
    {
        $result = (new NewsQuery())->showQuery();
        if($result)
        {
            return [
                "layId" => $result->layId,
                "layDate" => $result->layDate,
                "layTime" => $result->layTime,
                "layNextDate" => '-',
                "layNextTime" => '-'
            ];
        }
        else
        {
            return [
                "layId" => 0,
                "layDate" => '-',
                "layTime" => '-',
                "layNextDate" => '-',
                "layNextTime" => '-'
            ];
        }
    }
}
