<?php

namespace App\Modules\Mobile\News\Logics;

use App\Modules\Mobile\News\Queries\NewsField;
use App\Modules\Mobile\News\Queries\NewsQuery;
use App\Modules\Mobile\News\Queries\NewsDataManagerQuery;

class NewsDataManagerLogic extends NewsDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NewsField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new NewsQuery())->setEntity());
	}
}
