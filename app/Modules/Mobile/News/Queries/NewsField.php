<?php

namespace App\Modules\Mobile\News\Queries;

use App\Queries\General\FieldMap;

class NewsField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layTitle' => 'news_title',
            'layDescription' => 'news_description',
            'layPicUrl' => 'news_pic_url'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}