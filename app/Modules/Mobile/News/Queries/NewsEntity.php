<?php

namespace App\Modules\Mobile\News\Queries;

use App\Models\News;

class NewsEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new News());
    }
}