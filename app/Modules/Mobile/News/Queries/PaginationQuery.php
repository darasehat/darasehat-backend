<?php

namespace App\Modules\Mobile\News\Queries;
use Auth;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5, $request)
    {
        $select = $this->setSelect($request);
        $query = $entity->select($select)
        ->where('news.is_active','=',1);
        // $query = $this->processSearch($query, $request->search);
        // return $query->paginate($rowsPerPage);
        $query = $query->offset($request->lastRecord)->limit($rowsPerPage)
        ->orderBy('createdtime','DESC');
        return $query->get();
    }

    /**
     * 
     */
    private function setSelect($request)
    {
        $select = [
            'news.news_id AS layId',
            'news_title AS layTitle',
            'news_description AS layDescription',
            'news_pic_url AS layPicUrl',
        ];
        // if((!$request->layCmpId) )
        // {
        //     array_push($select, 'cmp_name AS layCompany');
        // }
        // if((!$request->layBrndId) )
        // {
        //     array_push($select, 'brnd_name AS layBrand');
        // }
        // if((!$request->layStoreId) )
        // {
        //     array_push($select, 'store_name AS layStore');
        // }
        return $select;
    }

    /**
     * 
     */
    private function setLevel($query, $request)
    {
        // if($request->layCmpId)
        // {
        //     $query = $query->where('news.cmp_id','=',$request->layCmpId);
        // }
        // if($request->layBrndId)
        // {
        //     $query = $query->where('news.brnd_id','=',$request->layBrndId);            
        // }
        // if($request->layStoreId)
        // {
        //     $query = $query->where('news.store_id','=',$request->layStoreId);            
        // }

        $searchParam = json_decode($request->search);
        // if (!empty($searchParam->layName))
        // {
        //     $query = $query->where('user_profiles.usrprf_name','=',$searchParam->layName);
        // }
        // if (!empty($searchParam->layEmail))
        // {
        //     $query = $query->where('users.email','=',$searchParam->layEmail);
        // }
        // if (!empty($searchParam->layPhone))
        // {
        //     $query = $query->where('user_profiles.usrprf_phone_no','=',$searchParam->layPhone);
        // }
        return $query;
    }

    private function processSearch($query, $search)
    {
        $searchParam = json_decode($search);
        $query->where(function ($queryGroup) use ($searchParam) {
            if (!empty($searchParam->layName)) {
                $queryGroup = $queryGroup->where('usrprf_name','like',"%{$searchParam->layName}%");
            }
            if (!empty($searchParam->layEmail)) {
                $queryGroup = $queryGroup->where('users.email','like',"%{$searchParam->layEmail}%");
            }
            if (!empty($searchParam->layPhone)) {
                $queryGroup = $queryGroup->where('usrprf_phone_no','like',"%{$searchParam->layPhone}%");
            }
        });
        return $query;
    }

    // private function processSearchActivePackage($query, $search)
    // {
    //     $date=date('Y-m-d');
    //     $searchParam = json_decode($search);
    //     if (!empty($searchParam->layPackageStatus)) {
    //         $query = $query->leftJoin('gym_member_packages','gym_member_packages.gymbr_id','=','news.gymbr_id');
    //         if($searchParam->layPackageStatus == 'active')
    //         {
    //             $query = $query->where('gymmbrpkg_en_date','>=',$date);
    //         }
    //         if($searchParam->layPackageStatus == 'inactive')
    //         {
    //             $query = $query->where('gymmbrpkg_en_date','<',$date);
    //         }
    //         $query = $query->distinct('news.gymbr_id');
    //     }
    //     return $query;
    // }

}