<?php

namespace App\Modules\Mobile\News\Queries;

use Auth;
use DB;
use App\Modules\Mobile\News\Queries\NewsEntity;

class NewsQuery extends NewsEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * 
     */
    // private function setLevel($query, $request)
    // {
    //     if($request->layCmpId)
    //     {
    //         $query = $query->where('news.cmp_id','=',$request->layCmpId);
    //     }
    //     if($request->layBrndId)
    //     {
    //         $query = $query->where('news.brnd_id','=',$request->layBrndId);            
    //     }
    //     if($request->layStoreId)
    //     {
    //         $query = $query->where('news.store_id','=',$request->layStoreId);            
    //     }
    //     return $query;
    // }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5, $request)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage, $request);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery()
    {
        $query = $this->setEntity()
        ->select(
            'news_id AS layId',
            'news_date AS layDate',
            'news_time AS layTime',
            'news_datetime AS layDatetime',
        )
        ->where('news.is_active','=',1)
        ->orderBy('news_datetime','DESC')
        ->first();
        return $query;
    }
}