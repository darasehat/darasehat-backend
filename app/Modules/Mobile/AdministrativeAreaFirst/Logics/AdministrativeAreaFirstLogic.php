<?php

namespace App\Modules\Mobile\AdministrativeAreaFirst\Logics;

use App\Modules\Mobile\AdministrativeAreaFirst\Queries\AdministrativeAreaFirstQuery;

class AdministrativeAreaFirstLogic
{
	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new AdministrativeAreaFirstQuery())->selectListQuery();
	}
}
