<?php

namespace App\Modules\Mobile\AdministrativeAreaFirst\Queries;

use App\Modules\Mobile\AdministrativeAreaFirst\Queries\AdministrativeAreaFirstEntity;

class AdministrativeAreaFirstQuery extends AdministrativeAreaFirstEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('adm1_name AS layName', 'adm1_id AS layId')
        ->where('is_active','=','1')
        ->orderBy('adm1_name','asc')
        ->get();
        return $query;
    }
}