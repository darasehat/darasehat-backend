<?php

namespace App\Modules\Mobile\User\Queries;

use App\Models\UserMobile;

class UserEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new UserMobile());
    }
}