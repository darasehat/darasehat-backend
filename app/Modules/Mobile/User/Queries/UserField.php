<?php

namespace App\Modules\Mobile\User\Queries;

use App\Queries\General\FieldMap;

class UserField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layName' => 'name',
            'layEmail' => 'email',
            'email_verified_at' => 'email_verified_at',
            'layPassword' => 'password',
            'remember_token' => 'remember_token',
            'layToken' => 'api_token'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        $data['password'] = bcrypt($data['password']);
        return $data;
    }

    public function setFieldLogin($request)
    {
        $fieldmap = [
            'remember_token' => 'remember_token',
            'layToken' => 'api_token'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    /**
     * [setFieldPassword description]
     * @param [type] $request [description]
     */
    public function setFieldPassword($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layPassword' => 'password',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        $data['password'] = bcrypt($data['password']);

        return $data;
    }

    /**
     * [setFieldData description]
     * @param [type] $request [description]
     */
    public function setFieldData($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layName' => 'name',
            'layEmail' => 'email',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);

        return $data;
    }
}