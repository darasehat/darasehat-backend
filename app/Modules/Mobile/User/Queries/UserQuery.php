<?php

namespace App\Modules\Mobile\User\Queries;

use App\Modules\Mobile\User\Queries\UserEntity;

class UserQuery extends UserEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName', 'email AS layEmail')
        ->where('id','=',$id)
        ->where('is_active','=',1)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName', 'email AS layEmail', 'is_active AS active')
        ->where('id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [loginQuery description]
     * @param  [type] $email  [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function loginQuery($email)
    {
        $query = $this->setEntity()
        ->select('id AS layId','name AS layName', 'email AS layEmail', 'api_token AS layToken')
        ->where('email','=',$email)
        ->first();
        return $query;        
    }
}