<?php

namespace App\Modules\Mobile\User\Logics;

use Auth;
use App\Modules\Mobile\User\Logics\UserLogic;
use App\Modules\Mobile\UserProfile\Logics\UserProfileLogic;

class UserProcessRegister
{
	/**
	 * [doRegister description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$user = (new UserLogic())->doStoreFirst($request, $datetime);
        
        Auth::attempt($request->only($request->layEmail, $request->layPassword));
        Auth::guard('apitoken');

		$request->request->add(['layUsrId' => $user->id]);
		$profile = (new UserProfileLogic())->doStoreFirst($request, $datetime);

		$user->assignRole('end_user');
		
        return ['user' => $user, 'userProfile' => $profile];
	}
}
