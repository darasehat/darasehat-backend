<?php

namespace App\Modules\Mobile\User\Logics;

use Auth;
use App\Modules\Mobile\User\Queries\UserField;
use App\Modules\Mobile\User\Queries\UserQuery;
use App\Modules\Mobile\User\Queries\UserDataManagerQuery;

class UserDataManagerLogic extends UserDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new UserField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new UserQuery())->setEntity());
	}
	
	/**
	 * [storeFirstSave description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new UserField())->setField($request);
        return $this->storeDataSaveFirst($datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
        $datarequest = (new UserField())->setFieldData($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * 
	 */
	public function updateSaveLogin($id, $request, $datetime)
	{
        $datarequest = (new UserField())->setFieldLogin($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * [updatePasswordSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updatePasswordSave($id, $request, $datetime)
	{
        $datarequest = (new UserField())->setFieldPassword($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new UserField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new UserQuery())->setEntity());
	}
}
