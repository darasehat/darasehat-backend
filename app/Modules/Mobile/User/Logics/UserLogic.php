<?php

namespace App\Modules\Mobile\User\Logics;

use Illuminate\Support\Str;
use App\Modules\Mobile\User\Logics\UserDataManagerLogic;
use App\Modules\Mobile\User\Logics\UserPaginationLogic;
use App\Modules\Mobile\User\Queries\UserQuery;
use App\Modules\Mobile\UserProfile\Logics\UserProfileLogic;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\Neighborhood\Logics\NeighborhoodLogic;
use App\Modules\Mobile\Sakuta\NeighborhoodDetail\Logics\NeighborhoodDetailLogic;

class UserLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new UserPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
    	$request->request->add(['layToken' => Str::random(255)]);
		return (new UserDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doStoreFirst description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStoreFirst($request, $datetime)
	{
    	$request->request->add(['layToken' => Str::random(255)]);
		return (new UserDataManagerLogic())->storeFirstSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new UserDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doUpdatePassword description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdatePassword($id, $request, $datetime)
	{
		return (new UserDataManagerLogic())->updatePasswordSave($id, $request, $datetime);
	}
	
	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new UserQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new UserQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new UserDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doLogin description]
	 * @param  [type] $email  [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function doLogin($email)
	{
		return (new UserQuery())->loginQuery($email);
	}

	/**
	 * [updateAPIToken description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateAPIToken($id, $request, $datetime)
	{
    	$request->request->add(['layToken' => Str::random(255)]);
		return (new UserDataManagerLogic())->updateSaveLogin($id, $request, $datetime); 
	}

	/**
	 * [doRegisterNeighborhood description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	// public function doSakutaRegisterNeighborhood($request, $datetime)
	// {
  //   	$request->request->add(['layFirstName' => $request->layName]);
	// 	$user = $this->doStoreFirst($request, $datetime);
    	
  //   	$request->request->add(['layUsrId' => $user->id]);
	// 	$profile = (new UserProfileLogic())->doStoreFirst($request, $datetime);
    	
  //   	$request->request->add(['layName' => $request->layNeiName]);
	// 	$neighborhood = (new NeighborhoodLogic())->doStoreFirst($request, $datetime);
    	
  //   	$request->request->add(['layNeiId' => $neighborhood->nei_id]);
	// 	$neighborhood = (new NeighborhoodDetailLogic())->doStoreFirst($request, $datetime);

	// 	$citizen = (new CitizenLogic())->doStoreFirst($request, $datetime);

	// 	return ['user' => $user, 'userProfile' => $profile, 'neighborhood' => $neighborhood];
	// }
}
