<?php

namespace App\Modules\Mobile\AdministrativeAreaThird\Logics;

use App\Modules\Mobile\AdministrativeAreaThird\Queries\AdministrativeAreaThirdField;
use App\Modules\Mobile\AdministrativeAreaThird\Queries\AdministrativeAreaThirdQuery;
use App\Modules\Mobile\AdministrativeAreaThird\Queries\AdministrativeAreaThirdDataManagerQuery;

class AdministrativeAreaThirdDataManagerLogic extends AdministrativeAreaThirdDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new AdministrativeAreaThirdField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new AdministrativeAreaThirdQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new AdministrativeAreaThirdField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new AdministrativeAreaThirdQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new AdministrativeAreaThirdField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new AdministrativeAreaThirdQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new AdministrativeAreaThirdQuery())->setEntity());
	}
}
