<?php

namespace App\Modules\Mobile\AdministrativeAreaThird\Logics;

use App\Modules\Mobile\AdministrativeAreaThird\Queries\AdministrativeAreaThirdQuery;

class AdministrativeAreaThirdLogic
{
	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList($adm2)
	{
		return (new AdministrativeAreaThirdQuery())->selectListQuery($adm2);
	}
}
