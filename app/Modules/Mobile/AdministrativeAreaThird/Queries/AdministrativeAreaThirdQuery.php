<?php

namespace App\Modules\Mobile\AdministrativeAreaThird\Queries;

use App\Modules\Mobile\AdministrativeAreaThird\Queries\AdministrativeAreaThirdEntity;

class AdministrativeAreaThirdQuery extends AdministrativeAreaThirdEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery($adm2)
    {
        $query = $this->setEntity()
        ->select('adm3_name AS layName', 'adm3_id AS layId')
        ->where('is_active','=','1')
        ->where('adm2_id','=',$adm2)
        ->orderBy('adm3_name','asc')
        ->get();
        return $query;
    }
}