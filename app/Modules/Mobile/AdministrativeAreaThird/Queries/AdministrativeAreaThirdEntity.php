<?php

namespace App\Modules\Mobile\AdministrativeAreaThird\Queries;

use App\Models\AdministrativeAreaThird;

class AdministrativeAreaThirdEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new AdministrativeAreaThird());
    }
}