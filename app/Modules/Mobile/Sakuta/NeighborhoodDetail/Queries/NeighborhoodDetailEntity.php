<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodDetail\Queries;

use App\Models\Sakuta\NeighborhoodDetail;

class NeighborhoodDetailEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new NeighborhoodDetail());
    }
}