<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodDetail\Queries;

use App\Modules\Mobile\Sakuta\NeighborhoodDetail\Queries\NeighborhoodDetailEntity;

class NeighborhoodDetailQuery extends NeighborhoodDetailEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('neidet_rw AS layName','nei_rt AS layRt','nei_rw AS layRw','neicat_name AS layCategory','adm1_name AS layProvince','adm2_name AS layCity','adm3_name AS layCamat','adm4_name AS layVillage')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','gu_neighborhoods.adm1_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','gu_neighborhoods.adm2_id')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','gu_neighborhoods.adm3_id')
        ->leftJoin('administrative_area_fourths','administrative_area_fourths.adm4_id','=','gu_neighborhoods.adm4_id')
        ->leftJoin('gu_neighborhood_categories','gu_neighborhood_categories.neicat_id','=','gu_neighborhoods.neicat_id')
        ->where('nei_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('neidet_rw AS layName','nei_rt AS layRt','nei_rw AS layRw','neicat_id AS layCategory','adm1_id AS layProvince','adm2_id AS layCity','adm3_id AS layCamat','adm4_id AS layVillage')
        ->where('nei_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery($adm1, $adm2, $adm3, $adm4)
    {
        $query = $this->setEntity()
        ->select('neidet_id AS layId','neidet_rw AS layRw','adm1_id AS layAdm1Id','adm2_id AS layAdm2Id','adm3_id AS layAdm3Id','adm4_id AS layAdm4Id')
        ->where('is_active','=','1')
        ->where('adm1_id','=',$adm1)
        ->where('adm2_id','=',$adm2)
        ->where('adm3_id','=',$adm3)
        ->where('adm4_id','=',$adm4)
        ->get();
        return $query;
    }

    /**
     * [rtrwQuery description]
     * @param  [type] $rt [description]
     * @param  [type] $rw [description]
     * @return [type]     [description]
     */
    public function rwQuery($adm1, $adm2, $adm3, $adm4, $rw)
    {
        $query = $this->setEntity()
        ->select('neidet_id')
        ->where('adm1_id','=',$adm1)
        ->where('adm2_id','=',$adm2)
        ->where('adm3_id','=',$adm3)
        ->where('adm4_id','=',$adm4)
        ->where('neidet_rw','=',$rw)
        ->where('is_active','=','1')
        ->first();
        return $query;        
    }
}