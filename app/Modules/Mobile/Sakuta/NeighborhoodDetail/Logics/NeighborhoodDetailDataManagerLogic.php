<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodDetail\Logics;

use App\Modules\Mobile\Sakuta\NeighborhoodDetail\Queries\NeighborhoodDetailField;
use App\Modules\Mobile\Sakuta\NeighborhoodDetail\Queries\NeighborhoodDetailQuery;
use App\Modules\Mobile\Sakuta\NeighborhoodDetail\Queries\NeighborhoodDetailDataManagerQuery;

class NeighborhoodDetailDataManagerLogic extends NeighborhoodDetailDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodDetailField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new NeighborhoodDetailQuery())->setEntity());
	}

	/**
	 * [storeFirstSave description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodDetailField())->setField($request);
        return $this->storeDataSaveFirst($datetime, $datarequest, (new NeighborhoodDetailQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodDetailField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new NeighborhoodDetailQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new NeighborhoodDetailField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new NeighborhoodDetailQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new NeighborhoodDetailQuery())->setEntity());
	}
}
