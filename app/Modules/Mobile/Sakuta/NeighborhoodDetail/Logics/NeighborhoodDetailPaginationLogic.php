<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodDetail\Logics;
use App\Modules\Mobile\Sakuta\NeighborhoodDetail\Queries\NeighborhoodDetailQuery;

class NeighborhoodDetailPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new NeighborhoodDetailQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
