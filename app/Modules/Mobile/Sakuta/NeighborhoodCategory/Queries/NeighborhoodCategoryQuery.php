<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodCategory\Queries;

use App\Modules\Mobile\Sakuta\NeighborhoodCategory\Queries\NeighborhoodCategoryEntity;

class NeighborhoodCategoryQuery extends NeighborhoodCategoryEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('neicat_name AS layName')
        ->where('neicat_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('neicat_name AS layName')
        ->where('neicat_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('neicat_name AS layName', 'neicat_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }
}