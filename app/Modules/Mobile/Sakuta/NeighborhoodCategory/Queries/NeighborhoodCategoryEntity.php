<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodCategory\Queries;

use App\Models\Sakuta\NeighborhoodCategory;

class NeighborhoodCategoryEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new NeighborhoodCategory());
    }
}