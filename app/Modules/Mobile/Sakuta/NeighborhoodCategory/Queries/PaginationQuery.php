<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodCategory\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('neicat_id AS layId','neicat_name AS layName')
        ->where('is_active','=',1)
        ->paginate($rowsPerPage);
    }
}