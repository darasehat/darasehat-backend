<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodCategory\Logics;

use App\Modules\Mobile\Sakuta\NeighborhoodCategory\Queries\NeighborhoodCategoryField;
use App\Modules\Mobile\Sakuta\NeighborhoodCategory\Queries\NeighborhoodCategoryQuery;
use App\Modules\Mobile\Sakuta\NeighborhoodCategory\Queries\NeighborhoodCategoryDataManagerQuery;

class NeighborhoodCategoryDataManagerLogic extends NeighborhoodCategoryDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodCategoryField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new NeighborhoodCategoryQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodCategoryField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new NeighborhoodCategoryQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new NeighborhoodCategoryField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new NeighborhoodCategoryQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new NeighborhoodCategoryQuery())->setEntity());
	}
}
