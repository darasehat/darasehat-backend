<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodCategory\Logics;
use App\Modules\Mobile\Sakuta\NeighborhoodCategory\Queries\NeighborhoodCategoryQuery;

class NeighborhoodCategoryPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new NeighborhoodCategoryQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
