<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodRequest\Logics;

use App\Modules\Mobile\Sakuta\NeighborhoodRequest\Queries\NeighborhoodRequestField;
use App\Modules\Mobile\Sakuta\NeighborhoodRequest\Queries\NeighborhoodRequestQuery;
use App\Modules\Mobile\Sakuta\NeighborhoodRequest\Queries\NeighborhoodRequestDataManagerQuery;

class NeighborhoodRequestDataManagerLogic extends NeighborhoodRequestDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodRequestField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new NeighborhoodRequestQuery())->setEntity());
	}

	/**
	 * [storeFirstSave description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodRequestField())->setField($request);
        return $this->storeDataSaveFirst($datetime, $datarequest, (new NeighborhoodRequestQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodRequestField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new NeighborhoodRequestQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new NeighborhoodRequestField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new NeighborhoodRequestQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new NeighborhoodRequestQuery())->setEntity());
	}
}
