<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodRequest\Logics;

use App\Modules\Mobile\Sakuta\NeighborhoodRequest\Logics\NeighborhoodRequestDataManagerLogic;
use App\Modules\Mobile\Sakuta\NeighborhoodRequest\Logics\NeighborhoodRequestPaginationLogic;
use App\Modules\Mobile\Sakuta\NeighborhoodRequest\Queries\NeighborhoodRequestQuery;

class NeighborhoodRequestLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new NeighborhoodRequestPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [getMyPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getMyPaginationData($request)
	{
		return (new NeighborhoodRequestPaginationLogic())->getMyPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new NeighborhoodRequestDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doStoreFirst description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStoreFirst($request, $datetime)
	{
		return (new NeighborhoodRequestDataManagerLogic())->storeFirstSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new NeighborhoodRequestDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new NeighborhoodRequestQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new NeighborhoodRequestQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new NeighborhoodRequestDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList($adm1, $adm2, $adm3, $adm4)
	{
		return (new NeighborhoodRequestQuery())->selectListQuery($adm1, $adm2, $adm3, $adm4);
	}

	/**
	 * [getRecord description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new NeighborhoodRequestQuery())->recordQuery($id, $select);
	}

	public function isUserHasRequest($userid)
	{
		$result = (new NeighborhoodRequestQuery())->isUserHasRequestQuery($userid);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
