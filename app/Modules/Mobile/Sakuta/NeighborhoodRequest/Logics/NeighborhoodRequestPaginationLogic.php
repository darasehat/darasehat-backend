<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodRequest\Logics;
use App\Modules\Mobile\Sakuta\NeighborhoodRequest\Queries\NeighborhoodRequestQuery;

class NeighborhoodRequestPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new NeighborhoodRequestQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}

	/**
	 * [getMyPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getMyPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new NeighborhoodRequestQuery())->getMyPaginationQuery($rowsPerPage);
    	return $result;
	}
}
