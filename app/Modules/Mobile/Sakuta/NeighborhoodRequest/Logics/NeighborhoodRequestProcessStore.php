<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodRequest\Logics;

use Auth;
use App\Modules\Mobile\Sakuta\NeighborhoodRequest\Logics\NeighborhoodRequestLogic;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;

class NeighborhoodRequestProcessStore
{
	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['citzen_id','usrprf_id','usr_id']);
		$request->request->add(['layCitzenId' => $citizen->citzen_id]);
		$request->request->add(['layUsrId' => $citizen->usr_id]);
		$request->request->add(['layUsrprfId' => $citizen->usrprf_id]);
		
		$result =  (new NeighborhoodRequestLogic())->doStore($request, $datetime);
		return [
			"status" => true,
			"code" => "auth",
			"data" => [
				"adm1_id" => $result->adm1_id,
				"adm2_id" => $result->adm2_id,
				"adm3_id" => $result->adm3_id,
				"adm4_id" => $result->adm4_id,
				"neicat_id" => $result->neicat_id,
				"neireq_name" => $result->neireq_name,
				"neireq_rt" => $result->neireq_rt,
				"neireq_rw" => $result->neireq_rw,
				"citzen_id" => $result->citzen_id,
				"usr_id" => $result->usr_id,
				"usrprf_id" => $result->usrprf_id,
				"neidet_id" => $result->neidet_id,
				"neisdet_id" => $result->neisdet_id,
				"neireq_owner_flag" => $result->neireq_owner_flag,
				"neireq_stay_flag" => $result->neireq_stay_flag,
				"neireq_rent_flag" => $result->neireq_rent_flag,
				"neireq_house_no" => $result->neireq_house_no
			],
			'message' => 'Data berhasil disimpan, tunggu konfirmasi dari tim kami dalam 2x24 jam',
		];
	}
}
