<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodRequest\Queries;

Use Auth;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('neireq_id AS layId','neireq_name AS layName','neireq_rt AS layRt','neireq_rw AS layRw','neicat_name AS layCategory','adm1_name AS layProvince','adm2_name AS layCity','adm3_name AS layCamat','adm4_name AS layVillage','neireq_house_no AS layNo','neireq_owner_flag AS layOwner','neireq_stay_flag AS layStay','neireq_rent_flag AS layRent')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','gu_neighborhood_requests.adm1_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','gu_neighborhood_requests.adm2_id')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','gu_neighborhood_requests.adm3_id')
        ->leftJoin('administrative_area_fourths','administrative_area_fourths.adm4_id','=','gu_neighborhood_requests.adm4_id')
        ->leftJoin('gu_neighborhood_categories','gu_neighborhood_categories.neicat_id','=','gu_neighborhood_requests.neicat_id')
        ->where('gu_neighborhood_requests.is_active','=',1)
        ->paginate($rowsPerPage);
    }

    public function getMyPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('neireq_id AS layId','neireq_name AS layName','neireq_rt AS layRt','neireq_rw AS layRw','neicat_name AS layCategory','adm1_name AS layProvince','adm2_name AS layCity','adm3_name AS layCamat','adm4_name AS layVillage','neireq_house_no AS layNo','neireq_owner_flag AS layOwner','neireq_stay_flag AS layStay','neireq_rent_flag AS layRent')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','gu_neighborhood_requests.adm1_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','gu_neighborhood_requests.adm2_id')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','gu_neighborhood_requests.adm3_id')
        ->leftJoin('administrative_area_fourths','administrative_area_fourths.adm4_id','=','gu_neighborhood_requests.adm4_id')
        ->leftJoin('gu_neighborhood_categories','gu_neighborhood_categories.neicat_id','=','gu_neighborhood_requests.neicat_id')
        ->where('gu_neighborhood_requests.is_active','=',1)
        ->where('gu_neighborhood_requests.usr_id','=',Auth::id())
        ->paginate($rowsPerPage);
    }

}