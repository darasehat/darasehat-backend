<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodRequest\Queries;

use App\Queries\General\FieldMap;

class NeighborhoodRequestField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layAdm1Id' => 'adm1_id',
            'layAdm2Id' => 'adm2_id',
            'layAdm3Id' => 'adm3_id',
            'layAdm4Id' => 'adm4_id',
            'layNeicatId' => 'neicat_id',
            'layCitzenId' => 'citzen_id',
            'layUsrId' => 'usr_id',
            'layUsrprfId' => 'usrprf_id',
            'layName' => 'neireq_name',
            'layRt' => 'neireq_rt',
            'layRw' => 'neireq_rw',
            'layIsApproved' => 'is_approved',
            'layNeiId' => 'nei_id',
            'layNeidetId' => 'neidet_id',
            'layNeisdetId' => 'neisdet_id',
            'layOwnerFlag' => 'neireq_owner_flag',
            'layStayFlag' => 'neireq_stay_flag',
            'layRentFlag' => 'neireq_rent_flag',
            'layHouseNo' => 'neireq_house_no',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}