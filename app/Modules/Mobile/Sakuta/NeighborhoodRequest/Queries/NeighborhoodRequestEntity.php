<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodRequest\Queries;

use App\Models\Sakuta\NeighborhoodRequest;

class NeighborhoodRequestEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new NeighborhoodRequest());
    }
}