<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Logics;
use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Queries\NeighborhoodSubdetailQuery;

class NeighborhoodSubdetailPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new NeighborhoodSubdetailQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
