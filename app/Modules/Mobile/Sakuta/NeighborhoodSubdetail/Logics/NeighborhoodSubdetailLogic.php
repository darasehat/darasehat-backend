<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Logics;

use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Logics\NeighborhoodSubdetailDataManagerLogic;
use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Logics\NeighborhoodSubdetailPaginationLogic;
use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Queries\NeighborhoodSubdetailQuery;

class NeighborhoodSubdetailLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new NeighborhoodSubdetailPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new NeighborhoodSubdetailDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doStoreFirst description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStoreFirst($request, $datetime)
	{
		return (new NeighborhoodSubdetailDataManagerLogic())->storeFirstSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new NeighborhoodSubdetailDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new NeighborhoodSubdetailQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new NeighborhoodSubdetailQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new NeighborhoodSubdetailDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList($adm1, $adm2, $adm3, $adm4, $neidet)
	{
		return (new NeighborhoodSubdetailQuery())->selectListQuery($adm1, $adm2, $adm3, $adm4, $neidet);
	}

	public function getRt($adm1, $adm2, $adm3, $adm4, $rt)
	{
		return (new NeighborhoodSubdetailQuery())->rtQuery($adm1, $adm2, $adm3, $adm4, $rt);
	}

	/**
	 * [getRtInsideRwList description]
	 * @param  [type] $rw [description]
	 * @param  [type] $rt [description]
	 * @return [type]     [description]
	 */
	public function getRtInsideRwList($rw, $rt)
	{
		return (new NeighborhoodSubdetailQuery())->rtInsideRwListQuery($rw, $rt);
	}
}
