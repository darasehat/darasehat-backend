<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Logics;

use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Queries\NeighborhoodSubdetailField;
use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Queries\NeighborhoodSubdetailQuery;
use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Queries\NeighborhoodSubdetailDataManagerQuery;

class NeighborhoodSubdetailDataManagerLogic extends NeighborhoodSubdetailDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodSubdetailField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new NeighborhoodSubdetailQuery())->setEntity());
	}

	/**
	 * [storeFirstSave description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodSubdetailField())->setField($request);
        return $this->storeDataSaveFirst($datetime, $datarequest, (new NeighborhoodSubdetailQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodSubdetailField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new NeighborhoodSubdetailQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new NeighborhoodSubdetailField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new NeighborhoodSubdetailQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new NeighborhoodSubdetailQuery())->setEntity());
	}
}
