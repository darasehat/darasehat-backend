<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Validations;

use Auth;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Logics\NeighborhoodSubdetailLogic;
// use App\Modules\Mobile\Sakuta\Citizen\Queries\CitizenValidationQuery;

class NeighborhoodSubdetailValidation
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function validate($request)
	{
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
		$errors = [];
		$member = $this->isRtrw($request);
		if($result['errors'])
		{
	        $iserror = true;
	        array_push($errors, $result['errors']);
		}
		if($iserror)
		{
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
	}

	/**
	 * untuk user RW yang input data harus di area RT dia
	 * @param  [type]  $request [description]
	 * @return boolean          [description]
	 */
	public function isMyRt($request)
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
		if($citizen->citzen_isrw)
		{
			if($request->layNeisdetId)
			{
				$result = (new NeighborhoodSubdetailLogic())->getRtInsideRwList($citizen->neidet_id, $request->layNeisdetId);
				if($result->isEmpty())
				{
					$errors['layNeisdetId'][0] = 'RT tidak berada di wilayah RW!';
					return [
						'message' => 'The given data was invalid.',
						'errors' => $errors
					];
				}
			}
			else
			{
				$errors['layNeisdetId'][0] = 'RT harus diisi!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors
				];
			}
		}
		return false;
	}
}
