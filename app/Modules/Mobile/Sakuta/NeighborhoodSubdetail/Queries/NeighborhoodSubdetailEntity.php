<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Queries;

use App\Models\Sakuta\NeighborhoodSubdetail;

class NeighborhoodSubdetailEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new NeighborhoodSubdetail());
    }
}