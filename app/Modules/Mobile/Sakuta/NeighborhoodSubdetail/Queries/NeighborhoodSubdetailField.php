<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Queries;

use App\Queries\General\FieldMap;

class NeighborhoodSubdetailField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layAdm1Id' => 'adm1_id',
            'layAdm2Id' => 'adm2_id',
            'layAdm3Id' => 'adm3_id',
            'layAdm4Id' => 'adm4_id',
            'layNeidetId' => 'neidet_id',
            'layRt' => 'neisdet_rt',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}