<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Queries;

use App\Queries\General\FieldMap;

class NeighborhoodAuthorityField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layParentId' => 'parent_id',
            'layNeiId' => 'nei_id',
            'layNeidetId' => 'neidet_id',
            'layName' => 'neiaut_name',
            'layDescription' => 'neiaut_description'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}