<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('nei_id AS layId','nei_name AS layName','nei_rt AS layRt','nei_rw AS layRw','neicat_name AS layCategory','adm1_name AS layProvince','adm2_name AS layCity','adm3_name AS layCamat','adm4_name AS layVillage')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','gu_neighborhoods.adm1_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','gu_neighborhoods.adm2_id')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','gu_neighborhoods.adm3_id')
        ->leftJoin('administrative_area_fourths','administrative_area_fourths.adm4_id','=','gu_neighborhoods.adm4_id')
        ->leftJoin('gu_neighborhood_categories','gu_neighborhood_categories.neicat_id','=','gu_neighborhoods.neicat_id')
        ->where('gu_neighborhoods.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}