<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Queries;

use App\Models\Sakuta\NeighborhoodAuthority;

class NeighborhoodAuthorityEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new NeighborhoodAuthority());
    }
}