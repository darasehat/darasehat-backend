<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Logics;
use App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Queries\NeighborhoodAuthorityQuery;

class NeighborhoodAuthorityPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new NeighborhoodAuthorityQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
