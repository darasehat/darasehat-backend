<?php

namespace App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Logics;

use App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Queries\NeighborhoodAuthorityField;
use App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Queries\NeighborhoodAuthorityQuery;
use App\Modules\Mobile\Sakuta\NeighborhoodAuthority\Queries\NeighborhoodAuthorityDataManagerQuery;

class NeighborhoodAuthorityDataManagerLogic extends NeighborhoodAuthorityDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodAuthorityField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new NeighborhoodAuthorityQuery())->setEntity());
	}

	/**
	 * [storeFirstSave description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodAuthorityField())->setField($request);
        return $this->storeDataSaveFirst($datetime, $datarequest, (new NeighborhoodAuthorityQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodAuthorityField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new NeighborhoodAuthorityQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new NeighborhoodAuthorityField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new NeighborhoodAuthorityQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new NeighborhoodAuthorityQuery())->setEntity());
	}
}
