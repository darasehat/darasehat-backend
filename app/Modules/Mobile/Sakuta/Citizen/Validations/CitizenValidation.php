<?php

namespace App\Modules\Mobile\Sakuta\Citizen\Validations;

use Auth;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\Citizen\Queries\CitizenValidationQuery;

class CitizenValidation
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function validate($request)
	{
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
		$errors = [];
		$member = $this->isRtrw($request);
		if($result['errors'])
		{
	        $iserror = true;
	        array_push($errors, $result['errors']);
		}
		if($iserror)
		{
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
	}

	/**
	 * jika proses hanya boleh dilakukan oleh pejabat RTRW saja
	 * @return boolean [description]
	 */
	public function isRtrw()
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
		if((!$citizen->citzen_isrt) && (!$citizen->citzen_isrw))
		{
			$errors['layCitzenId'][0] = 'Data khusus RW dan RT saja!';
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors,
				'data' => $citizen
			];
		}

		return false;
	}

	public function isHostInRw($host)
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
		if($citizen->citzen_isrw==1)
		{
			$citizen = (new CitizenLogic())->getRwCitizen($host, $citizen->neidet_id, ['nei_id','neidet_id','neisdet_id','citzen_isrw','citzen_isrt']);
			if(!$citizen)
			{
				$errors['layCitzenId'][0] = 'Bukan warga RW sini!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];
			}
		}
		if($citizen->citzen_isrt==1)
		{
			$citizen = (new CitizenLogic())->getRtCitizen($host, $citizen->neidet_id, $citizen->neisdet_id,['nei_id','neidet_id','neisdet_id','citzen_isrw','citzen_isrt']);
			if(!$citizen)
			{
				$errors['layCitzenId'][0] = 'Bukan warga RT sini!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];			
			}
		}
	}
}
