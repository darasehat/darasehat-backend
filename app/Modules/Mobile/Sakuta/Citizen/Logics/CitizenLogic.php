<?php

namespace App\Modules\Mobile\Sakuta\Citizen\Logics;

use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenDataManagerLogic;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenPaginationLogic;
use App\Modules\Mobile\Sakuta\Citizen\Queries\CitizenQuery;

class CitizenLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new CitizenPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new CitizenDataManagerLogic())->storeSave($request, $datetime);
	}
	
	/**
	 * [doStoreFirst description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStoreFirst($request, $datetime)
	{
		return (new CitizenDataManagerLogic())->storeFirstSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new CitizenDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new CitizenQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new CitizenQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new CitizenDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new CitizenQuery())->selectListQuery();
	}

	/**
	 * [getUser description]
	 * @param  [type] $userid     [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getUser($userid, $select)
	{
		return (new CitizenQuery())->userQuery($userid, $select);
	}

	/**
	 * [list siapa aja RT dan RW yang ada di lingkungan]
	 * @param  [type] $neiid    [description]
	 * @param  [type] $neidetid [description]
	 * @return [type]           [description]
	 */
	public function getRtrws($neiid, $neidetid)
	{
		return (new CitizenQuery())->rtrwQuery($neiid, $neidetid);
	}

	/**
	 * ambil data citizen berdasarkan citizen id
	 * @param  [type] $citzenid [description]
	 * @param  [type] $select   [description]
	 * @return [type]           [description]
	 */
	public function getCitizen($citzenid, $select)
	{
		return (new CitizenQuery())->citizenQuery($citzenid, $select);
	}

	/**
	 * [getRwCitizen description]
	 * @param  [type] $citzenid [description]
	 * @param  [type] $neidetid [description]
	 * @return [type]           [description]
	 */
	public function getRwCitizen($citzenid, $neidetid, $select)
	{
		return (new CitizenQuery())->rwCitizenQuery($citzenid, $neidetid, $select);
	}

	/**
	 * [getRtCitizen description]
	 * @param  [type] $citzenid  [description]
	 * @param  [type] $neidetid  [description]
	 * @param  [type] $neisdetid [description]
	 * @param  [type] $select    [description]
	 * @return [type]            [description]
	 */
	public function getRtCitizen($citzenid, $neidetid, $neisdetid, $select)
	{
		return (new CitizenQuery())->rtCitizenQuery($citzenid, $neidetid, $neisdetid, $select);		
	}
}
