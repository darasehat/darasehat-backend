<?php

namespace App\Modules\Mobile\Sakuta\Citizen\Logics;

use App\Modules\Mobile\Sakuta\Citizen\Queries\CitizenField;
use App\Modules\Mobile\Sakuta\Citizen\Queries\CitizenQuery;
use App\Modules\Mobile\Sakuta\Citizen\Queries\CitizenDataManagerQuery;

class CitizenDataManagerLogic extends CitizenDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CitizenField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new CitizenQuery())->setEntity());
	}

	/**
	 * [storeFirstSave description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CitizenField())->setField($request);
        return $this->storeDataSaveFirst($datetime, $datarequest, (new CitizenQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CitizenField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new CitizenQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new CitizenField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new CitizenQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new CitizenQuery())->setEntity());
	}
}
