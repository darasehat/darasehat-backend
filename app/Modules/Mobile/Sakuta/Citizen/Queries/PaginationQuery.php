<?php

namespace App\Modules\Mobile\Sakuta\Citizen\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('citzen_id AS layId','citzen_name AS layName','nei_name AS layNei','house_no AS layHouse','citzen_authority AS layAuthority','citzen_resident_status AS layResidentStatus')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_citizens.nei_id')
        ->leftJoin('gu_houses','gu_houses.house_id','=','gu_citizens.house_id')
        ->where('gu_citizens.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}