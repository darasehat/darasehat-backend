<?php

namespace App\Modules\Mobile\Sakuta\Citizen\Queries;

use App\Queries\General\FieldMap;

class CitizenField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layNeiId' => 'nei_id',
            'layNeidetId' => 'neidet_id',
            'layNeisdetId' => 'neisdet_id',
            'layHouseId' => 'house_id',
            'layName' => 'citzen_name',
            'layOwnerFlag' => 'citzen_owner_flag',
            // 'layStayFlag' => 'citzen_stay_flag',
            // 'layRentFlag' => 'citzen_rent_flag',
            'layAuthority' => 'citzen_authority',
            'layResidentStatus' => 'citzen_resident_status',
            'layUsrId' => 'usr_id',
            'layUsrprfId' => 'usrprf_id',
            'layVerified' => 'citzen_verified',
            'layVerifiedby' => 'citzen_verifiedby',
            'layIsrt' => 'citzen_isrt',
            'layIsrw' => 'citzen_isrw'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}