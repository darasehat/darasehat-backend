<?php

namespace App\Modules\Mobile\Sakuta\Citizen\Queries;

use App\Modules\Mobile\Sakuta\Citizen\Queries\CitizenEntity;

class CitizenQuery extends CitizenEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('citzen_id AS layId','citzen_name AS layName','nei_name AS layNei','house_no AS layHouse','citzen_authority AS layAuthority','citzen_resident_status AS layResidentStatus')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_citizens.nei_id')
        ->leftJoin('gu_houses','gu_houses.house_id','=','gu_citizens.house_id')
        ->where('gu_citizens.citzen_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('citzen_id AS layId','citzen_name AS layName','nei_id AS layNei','house_id AS layHouse','citzen_authority AS layAuthority','citzen_resident_status AS layResidentStatus')
        ->where('citzen_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('citzen_name AS layName', 'citzen_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [userQuery description]
     * @param  [type] $userid [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function userQuery($userid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('usr_id','=', $userid)
        ->where('is_active','=',1)
        ->first();
        return $query;
    }

    /**
     * []
     * @param  [type] $neiid    [description]
     * @param  [type] $neidetid [description]
     * @return [type]           [description]
     */
    public function rtrwQuery($neiid, $neidetid)
    {
        $query = $this->setEntity()
        ->select('citzen_id','citzen_isrt','citzen_isrw')
        ->where('nei_id','=',$neiid)
        ->where('neidet_id','=',$neidetid)
        ->where('is_active','=',1)
        ->where(function($query){
            $query->orWhere('citzen_isrt','=',1)
            ->orWhere('citzen_isrw','=',1);
        })
        ->get();
        return $query;
    }

    /**
     * ambil data citizen berdasarkan citizen id
     * @param  [type] $citzenid [description]
     * @param  [type] $select   [description]
     * @return [type]           [description]
     */
    public function citizenQuery($citzenid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('citzen_id','=', $citzenid)
        ->where('is_active','=',1)
        ->first();
        return $query;        
    }

    /**
     * [rwCitizenQuery description]
     * @param  [type] $citzenid [description]
     * @param  [type] $nesdetid [description]
     * @return [type]           [description]
     */
    public function rwCitizenQuery($citzenid, $neidetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('citzen_id','=', $citzenid)
        ->where('neidet_id','=', $neidetid)
        ->where('is_active','=',1)
        ->first();
        return $query;
    }

    public function rtCitizenQuery($citzenid, $neidetid, $neisdetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('citzen_id','=', $citzenid)
        ->where('neidet_id','=', $neidetid)
        ->where('neisdet_id','=', $neisdetid)
        ->where('is_active','=',1)
        ->first();
        return $query;
    }
}