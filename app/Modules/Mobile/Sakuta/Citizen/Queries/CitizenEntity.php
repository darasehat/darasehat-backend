<?php

namespace App\Modules\Mobile\Sakuta\Citizen\Queries;

use App\Models\Sakuta\Citizen;

class CitizenEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Citizen());
    }
}