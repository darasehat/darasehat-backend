<?php

namespace App\Modules\Mobile\Sakuta\News\Queries;

use App\Modules\Mobile\Sakuta\News\Queries\NewsEntity;

class NewsQuery extends NewsEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5, $isrw, $isrt, $neidetid, $neisdetid)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage, $isrw, $isrt, $neidetid, $neisdetid);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id, $isrw, $isrt, $neidetid, $neisdetid)
    {
        $query = $this->setEntity()
        ->select('news_id AS layId','nei_name AS layNei','news_title AS layTitle','news_short AS layShort','news_description AS layDescription','news_publish_datetime AS layPublishDateTime')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_news.nei_id')
        ->where('news_id','=',$id);
        if($isrw==1 || ($isrw==0 && $isrt==0))
        {
            $query = $query->where('gu_news.neidet_id','=',$neidetid);
        }
        if($isrt==1 || ($isrw==0 && $isrt==0))
        {
            $query = $query->where('gu_news.neisdet_id','=',$neisdetid);
        }
        return $query->first();
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('news_id AS layId','nei_id AS layNei','news_title AS layTitle','news_short AS layShort','news_description AS layDescription','news_publish_date AS layPublishDate','news_publish_time AS layPublishTime')
        ->where('news_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('news_name AS layName', 'news_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [inRwRecordQuery description]
     * @param  [type] $neidetid [description]
     * @param  [type] $newsid    [description]
     * @param  [type] $select   [description]
     * @return [type]           [description]
     */
    public function inRwRecordQuery($newsid, $neidetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('news_id','=',$newsid)
        ->where('neidet_id','=',$neidetid)
        ->first();
        return $query;
    }

    /**
     * [inRtRecordQuery description]
     * @param  [type] $neisdetid [description]
     * @param  [type] $newsid   [description]
     * @param  [type] $select    [description]
     * @return [type]            [description]
     */
    public function inRtRecordQuery($newsid, $neisdetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('news_id','=',$newsid)
        ->where('neisdet_id','=',$neisdetid)
        ->first();
        return $query;
    }
}