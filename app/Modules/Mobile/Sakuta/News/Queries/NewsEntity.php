<?php

namespace App\Modules\Mobile\Sakuta\News\Queries;

use App\Models\Sakuta\News;

class NewsEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new News());
    }
}