<?php

namespace App\Modules\Mobile\Sakuta\News\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5, $isrw, $isrt, $neidetid, $neisdetid)
    {
        $result = $entity->select('news_id AS layId','news_title AS layTitle','news_publish_datetime AS layPublishDateTime','news_short AS layShort','news_publish_date AS layPublishDate','news_publish_time AS layPublishTime')
        ->where('gu_news.is_active','=',1);
				if($isrw==1 || ($isrw==0 && $isrt==0))
				{
						$result = $result->where('gu_news.neidet_id','=',$neidetid);
				}
				if($isrt==1 || ($isrw==0 && $isrt==0))
				{
						$result = $result->where('gu_news.neisdet_id','=',$neisdetid);
				}
				return $result->paginate($rowsPerPage);
    }
}