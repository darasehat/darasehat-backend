<?php

namespace App\Modules\Mobile\Sakuta\News\Queries;

use App\Queries\General\FieldMap;

class NewsField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layNeiId' => 'nei_id',
            'layNeidetId' => 'neidet_id',
            'layNeisdetId' => 'neisdet_id',
            'layTitle' => 'news_title',
            'layShort' => 'news_short',
            'layDescription' => 'news_description',
            'layPublishDate' => 'news_publish_date',
            'layPublishTime' => 'news_publish_time',
            'layPublishDatetime' => 'news_publish_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setUpdateField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layTitle' => 'news_title',
            'layShort' => 'news_short',
            'layDescription' => 'news_description',
            'layPublishDate' => 'news_publish_date',
            'layPublishTime' => 'news_publish_time',
            'layPublishDatetime' => 'news_publish_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setDeleteField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}