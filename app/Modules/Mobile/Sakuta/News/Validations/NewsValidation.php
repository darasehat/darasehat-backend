<?php

namespace App\Modules\Mobile\Sakuta\News\Validations;

use Auth;
use App\Modules\Mobile\Sakuta\News\Logics\NewsLogic;
// use App\Modules\Mobile\Sakuta\News\Queries\NewsValidationQuery;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;

class NewsValidation
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function validate($request)
	{
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
		$errors = [];
		$member = $this->isDataOwner($request);
		if($result['errors'])
		{
	        $iserror = true;
	        array_push($errors, $result['errors']);
		}
		if($iserror)
		{
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
	}

	/**
	 * jika proses hanya boleh dilakukan oleh pejabat RTRW saja
	 * @return boolean [description]
	 */
	public function isDataOwner($newsid)
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
		if($citizen->citzen_isrw)
		{
			$news = (new NewsLogic())->getInRwRecord($newsid, $citizen->neidet_id, ['news_id']);
			if(!$news)
			{
				$errors['layActId'][0] = 'Data bukan milik RW!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];
			}
		}
		if($citizen->citzen_isrt)
		{
			$news = (new NewsLogic())->getInRtRecord($newsid, $citizen->neisdet_id, ['news_id']);
			if(!$news)
			{
				$errors['layActId'][0] = 'Data bukan milik RT!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];
			}
		}
		return false;
	}
}
