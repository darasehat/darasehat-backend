<?php

namespace App\Modules\Mobile\Sakuta\News\Logics;

use Auth;

use App\Modules\Mobile\Sakuta\News\Logics\NewsDataManagerLogic;
use App\Modules\Mobile\Sakuta\News\Logics\NewsPaginationLogic;
use App\Modules\Mobile\Sakuta\News\Queries\NewsQuery;

use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\CitizenHouse\Logics\CitizenHouseLogic;

class NewsLogic
{
	private $isrw;
	private $isrt;
	private $neidetid;
	private $neisdetid;

	public function citizenStatus()
	{
		return (new CitizenLogic())->getUser(Auth::id(),['citzen_id','nei_id','neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
	}

	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$citizen = $this->citizenStatus();
		$this->isrw = $citizen->citzen_isrw;
		$this->isrt = $citizen->citzen_isrt;
		$this->neidetid = $citizen->neidet_id;
		$this->neisdetid = $citizen->neisdet_id;
		if($request->layMyPersonalFlag == 1 && $request->layHouseId)
		{
			$this->isrw = 0;
			$this->isrt = 0;
		}
		// jika ingin melihat data rumah yang lain
		if($this->isrw == 0 && $this->isrt == 0 && $request->layHouseId)
		{
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseRecord($citizen->citzen_id,$request->layHouseId,['house_owner_flag','house_stay_flag','house_rent_flag','gu_houses.neidet_id AS layNeidetId','gu_houses.neisdet_id AS layNeisdetId']);
			$this->neidetid = $citizenhouse->layNeidetId;
			$this->neisdetid = $citizenhouse->layNeisdetId;			
		}
		return (new NewsPaginationLogic())->getPaginationData($request, $this->isrw, $this->isrt, $this->neidetid, $this->neisdetid);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$citizen = $this->citizenStatus();
    	$request->request->add(['layNeiId' => $citizen->nei_id]);
		if($citizen->citzen_isrw==1)
		{
	    	$request->request->add(['layNeidetId' => $citizen->neidet_id]);
		}
		if($citizen->citzen_isrt==1 || ($citizen->citzen_isrw==0 && $citizen->citzen_isrt==0))
		{
	    	$request->request->add(['layNeidetId' => $citizen->neidet_id]);
	    	$request->request->add(['layNeisdetId' => $citizen->neisdet_id]);
		}
		// $dateTimeFormat = date('Y-m-d', strtotime($request->layPublishDate));
		$dateTimeFormat = \Carbon\Carbon::parse($request->layPublishDate)->format('Y-m-d');
		$request->request->add(['layPublishDate' => $dateTimeFormat]);
		$request->request->add(['layPublishDatetime' => $dateTimeFormat.' '.$request->layPublishTime]);
		return (new NewsDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
    	$request->request->add(['layPublishDatetime' => $request->layPublishDate.' '.$request->layPublishTime]);
		return (new NewsDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id, $layMyPersonalFlag, $layHouseId)
	{
		$citizen = $this->citizenStatus();
		$this->isrw = $citizen->citzen_isrw;
		$this->isrt = $citizen->citzen_isrt;
		$this->neidetid = $citizen->neidet_id;
		$this->neisdetid = $citizen->neisdet_id;

		if($layMyPersonalFlag == 1 && $layMyPersonalFlag)
		{
			$this->isrw = 0;
			$this->isrt = 0;
		}
		// jika ingin melihat data rumah yang lain
		if($this->isrw == 0 && $this->isrt == 0 && $layMyPersonalFlag)
		{
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseRecord($citizen->citzen_id,$layHouseId,['house_owner_flag','house_stay_flag','house_rent_flag','gu_houses.neidet_id AS layNeidetId','gu_houses.neisdet_id AS layNeisdetId']);
			$this->neidetid = $citizenhouse->layNeidetId;
			$this->neisdetid = $citizenhouse->layNeisdetId;
		}
		return (new NewsQuery())->showQuery($id, $this->isrw, $this->isrt, $this->neidetid, $this->neisdetid);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new NewsQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new NewsDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		// return (new NewsQuery())->selectListQuery();
	}

	/**
	 * ambil data berdasarkan ID
	 * @param  [type] $id     [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new NewsQuery())->recordQuery($id, $select);
	}

	/**
	 * data aktivitas yang ada di lingkungan RW
	 * @param  [type] $neidetid [description]
	 * @param  [type] $newsid    [description]
	 * @param  [type] $select   [description]
	 * @return [type]           [description]
	 */
	public function getInRwRecord($newsid, $neidetid, $select)
	{
		return (new NewsQuery())->inRwRecordQuery($newsid, $neidetid, $select);
	}

	/**
	 * Data aktivitas yang ada di lingkungan RT
	 * @param  [type] $neisdetid [description]
	 * @param  [type] $newsid     [description]
	 * @param  [type] $select    [description]
	 * @return [type]            [description]
	 */
	public function getInRtRecord($newsid, $neisdetid, $select)
	{
		return (new NewsQuery())->inRtRecordQuery($newsid, $neisdetid, $select);
	}
}
