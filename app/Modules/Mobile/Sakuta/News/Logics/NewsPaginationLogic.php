<?php

namespace App\Modules\Mobile\Sakuta\News\Logics;
use App\Modules\Mobile\Sakuta\News\Queries\NewsQuery;

class NewsPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request, $isrw, $isrt, $neidetid, $neisdetid)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 50;
		}
		$result = (new NewsQuery())->getPaginationQuery($rowsPerPage, $isrw, $isrt, $neidetid, $neisdetid);
    	return $result;
	}
}
