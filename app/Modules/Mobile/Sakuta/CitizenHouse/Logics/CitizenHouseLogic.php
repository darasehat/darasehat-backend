<?php

namespace App\Modules\Mobile\Sakuta\CitizenHouse\Logics;

use Auth;
use App\Modules\Mobile\Sakuta\CitizenHouse\Logics\CitizenHouseDataManagerLogic;
use App\Modules\Mobile\Sakuta\CitizenHouse\Logics\CitizenHousePaginationLogic;
use App\Modules\Mobile\Sakuta\CitizenHouse\Queries\CitizenHouseQuery;

class CitizenHouseLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new CitizenHousePaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new CitizenHouseDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new CitizenHouseDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new CitizenHouseQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new CitizenHouseQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new CitizenHouseDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doDeleteMyHouse description]
	 * @param  [type] $houseid  [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDeleteMyHouse($houseid, $request, $datetime)
	{
		return (new CitizenHouseDataManagerLogic())->deleteMyHouse($houseid, $request, $datetime);		
	}
	
	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new CitizenHouseQuery())->selectListQuery();
	}

	public function getMyHouseList($citzenid, $select)
	{
		return (new CitizenHouseQuery())->myHouseListQuery($citzenid, $select);
	}

	public function getMyHouseRecord($citzenid, $houseid, $select)
	{
		return (new CitizenHouseQuery())->myHouseRecordQuery($citzenid, $houseid, $select);
	}

	/**
	 * cek jika user punya rumah
	 */
	public function isUserHasHouse($userid)
	{
		$result = (new CitizenHouseQuery())->isUserHasHouseQuery($userid);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
