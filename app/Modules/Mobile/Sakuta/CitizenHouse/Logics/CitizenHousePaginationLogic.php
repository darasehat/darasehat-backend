<?php

namespace App\Modules\Mobile\Sakuta\CitizenHouse\Logics;
use App\Modules\Mobile\Sakuta\CitizenHouse\Queries\CitizenHouseQuery;

class CitizenHousePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new CitizenHouseQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
