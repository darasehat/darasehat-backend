<?php

namespace App\Modules\Mobile\Sakuta\CitizenHouse\Logics;

use Auth;
use DB;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\CitizenHouse\Queries\CitizenHouseField;
use App\Modules\Mobile\Sakuta\CitizenHouse\Queries\CitizenHouseQuery;
use App\Modules\Mobile\Sakuta\CitizenHouse\Queries\CitizenHouseDataManagerQuery;

class CitizenHouseDataManagerLogic extends CitizenHouseDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CitizenHouseField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new CitizenHouseQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CitizenHouseField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new CitizenHouseQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
		$datarequest = (new CitizenHouseField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new CitizenHouseQuery())->setEntity());
	}

	/**
	 * [deleteMyHouse description]
	 * @param  [type] $houseid  [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function deleteMyHouse($houseid, $request, $datetime)
	{
		$result = (new CitizenLogic())->getUser(Auth::id(),['citzen_id']);
		$entity = (new CitizenHouseQuery())->setEntity()->getTable();
		return DB::table($entity)
			->where('citzen_id','=',$result->citzen_id)
			->where('house_id','=',$houseid)
            ->update(['is_active' => 0]);
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new CitizenHouseQuery())->setEntity());
	}
}
