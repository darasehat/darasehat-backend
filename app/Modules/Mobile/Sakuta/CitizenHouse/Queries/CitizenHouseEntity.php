<?php

namespace App\Modules\Mobile\Sakuta\CitizenHouse\Queries;

use App\Models\Sakuta\CitizenHouse;

class CitizenHouseEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new CitizenHouse());
    }
}