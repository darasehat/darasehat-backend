<?php

namespace App\Modules\Mobile\Sakuta\CitizenHouse\Queries;

use App\Queries\General\FieldMap;

class CitizenHouseField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCitzenId' => 'citzen_id',
            'layHouseId' => 'house_id',
            'layOwnerFlag' => 'house_owner_flag',
            'layStayFlag' => 'house_stay_flag',
            'layRentFlag' => 'house_rent_flag',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}