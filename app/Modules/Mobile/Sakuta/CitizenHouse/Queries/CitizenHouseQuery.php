<?php

namespace App\Modules\Mobile\Sakuta\CitizenHouse\Queries;

use DB;
use App\Modules\Mobile\Sakuta\CitizenHouse\Queries\CitizenHouseEntity;

class CitizenHouseQuery extends CitizenHouseEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('citzen_id AS layId','house_no AS layHouseNo','nei_name AS layNeiName','neidet_rt AS layRt','neidet_rw AS layRw')
        ->leftJoin('gu_houses','gu_houses.house_id','=','gu_citizen_houses.house_id')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_houses.nei_id')
        ->leftJoin('gu_neighborhood_details','gu_neighborhood_details.neidet_id','=','gu_houses.neidet_id')
        ->where('gu_houses.is_active','=',1)
        ->where('gu_citizens.is_active','=',1)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        // $query = $this->setEntity()
        // ->select('Citizenhouse_no AS layNo','nei_id AS layNei')
        // ->where('Citizenhouse_id','=',$id)
        // ->first();
        // return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        // $query = $this->setEntity()
        // ->select('Citizenhouse_no AS layName', 'Citizenhouse_id AS layCode')
        // ->where('is_active','=','1')
        // ->get();
        // return $query;
    }

    public function myHouseListQuery($citzenid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=',1)
        ->where('citzen_id','=',$citzenid)
        ->get();
        return $query;
    }

    public function myHouseRecordQuery($citzenid, $houseid, $select)
    {
        // print_r($citzenid);
        // die();
                // $entity = $this->setEntity()->getTable();
        // $query = DB::table($entity)
        $query = $this->setEntity()
        ->select($select)
        ->leftJoin('gu_houses','gu_houses.house_id','=','gu_citizen_houses.house_id')
        ->where('gu_citizen_houses.is_active','=',1)
        ->where('gu_citizen_houses.citzen_id',$citzenid)
        ->where('gu_citizen_houses.house_id',$houseid)
        ->first();
        return $query;
    }

    public function isUserHasHouseQuery($userid)
    {
        return $this->setEntity()
        ->select('gu_citizens.citzen_id AS layId')
        ->leftJoin('gu_citizens','gu_citizens.house_id','=','gu_citizen_houses.house_id')
        ->where('gu_citizens.is_active','=','1')
        ->where('usr_id','=',$userid)
        ->first();
    }
}