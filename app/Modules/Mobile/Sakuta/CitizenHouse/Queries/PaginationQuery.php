<?php

namespace App\Modules\Mobile\Sakuta\CitizenHouse\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('citzen_id AS layId','house_no AS layHouseNo','nei_name AS layNeiName','neidet_rt AS layRt','neidet_rw AS layRw')
        ->leftJoin('gu_houses','gu_houses.house_id','=','gu_citizen_houses.house_id')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_houses.nei_id')
        ->leftJoin('gu_neighborhood_details','gu_neighborhood_details.neidet_id','=','gu_houses.neidet_id')
        ->where('gu_houses.is_active','=',1)
        ->where('gu_citizens.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}