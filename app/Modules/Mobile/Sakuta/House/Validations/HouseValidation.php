<?php

namespace App\Modules\Mobile\Sakuta\House\Validations;

use Auth;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\House\Logics\HouseLogic;

class HouseValidation
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function validate($request)
	{
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
		$errors = [];
		$result = $this->isAreaHouse($request);
		if($result['errors'])
		{
	        $iserror = true;
	        array_push($errors, $result['errors']);
		}
		if($iserror)
		{
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
	}

	public function isAreaHouse($houseid)
	{
		// $houseid="";
		// if($request->layHouseId)
		// {
		// 	$houseid = $request->layHouseId;
		// }
		// else if($request)
		// {
		// 	$houseid = $request;
		// }
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
		if($citizen->citzen_isrw)
		{
			$house = (new HouseLogic())->getHouseInRwRecord($citizen->neidet_id, $houseid,['house_id']);
		}
		else if($citizen->citzen_isrt)
		{
			$house = (new HouseLogic())->getHouseInRtRecord($citizen->neisdet_id, $houseid,['house_id']);
		}
		if(!$house)
		{
			$errors['layHouseId'][0] = 'Rumah ini tidak ada di daerah mu!';
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
		return false;
	}
}
