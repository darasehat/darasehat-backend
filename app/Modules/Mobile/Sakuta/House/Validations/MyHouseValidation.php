<?php

namespace App\Modules\Mobile\Sakuta\House\Validations;

use Auth;
use App\Modules\Mobile\Sakuta\CitizenHouse\Logics\CitizenHouseLogic;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;

class MyHouseValidation
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function validate($request)
	{
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
		$errors = [];
		$result = $this->houseInTable($request);
		if($result)
		{
	        $iserror = true;
			$errors['layHouseId'][0] = 'Rumah kamu sudah terdaftar, terima kasih';
		}
		if($iserror)
		{
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
	}

	private function houseInTable($request)
	{
		if($request->layHouseId)
		{
			$citizen = (new CitizenLogic())->getUser(Auth::id(),['citzen_id']);
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseList($citizen->citzen_id,['citzen_id','house_id','house_owner_flag','house_stay_flag','house_rent_flag']);
			foreach ($citizenhouse as $key => $value) {
				if($value->house_id == $request->layHouseId)
				{
					return true;
				}
			}
		}
		return false;
	}

	public function isMyHouse($request)
	{
		if($request->layHouseId)
		{
			$citizen = (new CitizenLogic())->getUser(Auth::id(),['citzen_id']);
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseRecord($citizen->citzen_id,$request->layHouseId,['gu_citizen_houses.citzen_id','gu_citizen_houses.house_id','house_owner_flag','house_stay_flag','house_rent_flag']);
			if(!$citizenhouse)
			{
				$errors['layHouseId'][0] = 'Ini bukan rumah kamu!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors
				];
			}
		}
		return false;
	}

	public function isMyHouseId($houseid)
	{
		if($houseid)
		{
			$citizen = (new CitizenLogic())->getUser(Auth::id(),['citzen_id']);
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseRecord($citizen->citzen_id,$houseid,['gu_citizen_houses.citzen_id','gu_citizen_houses.house_id','house_owner_flag','house_stay_flag','house_rent_flag']);
			if(!$citizenhouse)
			{
				$errors['layHouseId'][0] = 'Ini bukan rumah kamu!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors
				];
			}
		}
		return false;
	}
}
