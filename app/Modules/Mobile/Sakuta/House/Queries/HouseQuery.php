<?php

namespace App\Modules\Mobile\Sakuta\House\Queries;

use Auth;
use App\Modules\Mobile\Sakuta\House\Queries\HouseEntity;

class HouseQuery extends HouseEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationRwQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationRwQuery($neidetid, $rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationRwQuery($neidetid, $this->setEntity(), $rowsPerPage);
    }

    /**
     * [getPaginationRtQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationRtQuery($neisdetid, $rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationRtQuery($neisdetid, $this->setEntity(), $rowsPerPage);
    }

    /**
     * [getMyPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getMyPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getMyPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('house_id AS layId','house_no AS layNo','nei_name AS layNei')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_houses.nei_id')
        ->where('gu_houses.house_id','=',$id)
        ->first();
        return $query;
    }

    public function showMyHouseQuery($id)
    {
        $query = $this->setEntity()
        ->select('gu_houses.house_id AS layId','house_no AS layNo','house_owner_flag AS layOwner','house_stay_flag AS layStay','neisdet_rt AS layRt','neidet_rw AS layRw','nei_name AS layName')
        ->leftJoin('gu_citizen_houses','gu_citizen_houses.house_id','=','gu_houses.house_id')
        ->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_citizen_houses.citzen_id')
        ->leftJoin('gu_neighborhood_details','gu_neighborhood_details.neidet_id','=','gu_houses.neidet_id')
        ->leftJoin('gu_neighborhood_subdetails','gu_neighborhood_subdetails.neisdet_id','=','gu_houses.neisdet_id')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_houses.nei_id')
        ->where('gu_houses.house_id','=',$id)
        ->where('gu_houses.is_active','=',1)
        ->where('gu_citizens.usr_id','=',Auth::id())
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('house_no AS layNo')
        ->where('house_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editMyHouseQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editMyHouseQuery($id)
    {
        $query = $this->setEntity()
        ->select('house_no AS layNo','nei_id AS layNeiId','neidet_id AS layNeidetId','neisdet_id AS layNeisdetId')
        ->where('house_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * select list rumah hanya oleh RT
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('house_no AS layName', 'house_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * select list rumah yang hanya dimiliki oleh user
     * @return [type] [description]
     */
    public function myHouseSelectListQuery()
    {
        $query = $this->setEntity()
        ->select('house_no AS layName', 'house_id AS layCode')
        ->leftJoin('gu_citizen_houses','gu_citizen_houses.house_id','=','gu_houses.house_id')
        ->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_citizen_houses.citzen_id')
        ->where('gu_houses.is_active','=','1')
        ->where('gu_citizens.usr_id','=',Auth::id())
        ->get();
        return $query;
    }

    /**
     * ambil data berdasarkan house id
     * @param  [type] $houseid [description]
     * @param  [type] $select  [description]
     * @return [type]          [description]
     */
    public function recordQuery($houseid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('house_id','=',$houseid)
        ->first();
        return $query;
    }

    /**
     * [houseInRwRecordQuery description]
     * @param  [type] $neidetid [description]
     * @param  [type] $houseid  [description]
     * @param  [type] $select   [description]
     * @return [type]           [description]
     */
    public function houseInRwRecordQuery($neidetid, $houseid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('house_id','=',$houseid)
        ->where('neidet_id','=',$neidetid)
        ->first();
        return $query;
    }

    /**
     * [houseInRtRecordQuery description]
     * @param  [type] $neisdetid [description]
     * @param  [type] $houseid   [description]
     * @param  [type] $select    [description]
     * @return [type]            [description]
     */
    public function houseInRtRecordQuery($neisdetid, $houseid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('house_id','=',$houseid)
        ->where('neisdet_id','=',$neisdetid)
        ->first();
        return $query;
    }
}