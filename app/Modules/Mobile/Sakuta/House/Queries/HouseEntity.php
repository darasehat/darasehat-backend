<?php

namespace App\Modules\Mobile\Sakuta\House\Queries;

use App\Models\Sakuta\House;

class HouseEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new House());
    }
}