<?php

namespace App\Modules\Mobile\Sakuta\House\Queries;

use App\Queries\General\FieldMap;

class HouseField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layNeiId' => 'nei_id',
            'layNeidetId' => 'neidet_id',
            'layNeisdetId' => 'neisdet_id',
            'layNo' => 'house_no',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}