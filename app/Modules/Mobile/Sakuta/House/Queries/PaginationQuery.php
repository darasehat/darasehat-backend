<?php

namespace App\Modules\Mobile\Sakuta\House\Queries;
use Auth;

class PaginationQuery
{
    public function getPaginationRwQuery($neidetid, $entity, $rowsPerPage = 5)
    {
        return $entity->select('house_id AS layId','house_no AS layNo','nei_name AS layNei','neidet_rw AS layRw','neisdet_rt AS layRt')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_houses.nei_id')
        ->leftJoin('gu_neighborhood_details','gu_neighborhood_details.neidet_id','=','gu_houses.neidet_id')
        ->leftJoin('gu_neighborhood_subdetails','gu_neighborhood_subdetails.neisdet_id','=','gu_houses.neisdet_id')
        ->where('gu_houses.is_active','=',1)
        ->where('gu_houses.neidet_id','=',$neidetid)
        ->paginate($rowsPerPage);
    }

    public function getPaginationRtQuery($neisdetid, $entity, $rowsPerPage = 5)
    {
        return $entity->select('house_id AS layId','house_no AS layNo','nei_name AS layNei','neidet_rw AS layRw','neisdet_rt AS layRt')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_houses.nei_id')
        ->leftJoin('gu_neighborhood_details','gu_neighborhood_details.neidet_id','=','gu_houses.neidet_id')
        ->leftJoin('gu_neighborhood_subdetails','gu_neighborhood_subdetails.neisdet_id','=','gu_houses.neisdet_id')
        ->where('gu_houses.neisdet_id','=',$neisdetid)
        ->paginate($rowsPerPage);
    }

    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
    }

    public function getMyPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('gu_houses.house_id AS layId','house_no AS layNo','house_owner_flag AS layOwner','house_stay_flag AS layStay','neisdet_rt AS layRt','neidet_rw AS layRw','nei_name AS layName')
        ->leftJoin('gu_citizen_houses','gu_citizen_houses.house_id','=','gu_houses.house_id')
        ->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_citizen_houses.citzen_id')
        ->leftJoin('gu_neighborhood_details','gu_neighborhood_details.neidet_id','=','gu_houses.neidet_id')
        ->leftJoin('gu_neighborhood_subdetails','gu_neighborhood_subdetails.neisdet_id','=','gu_houses.neisdet_id')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_houses.nei_id')
        ->where('gu_houses.is_active','=',1)
        ->where('gu_citizen_houses.is_active','=',1)
        // ->where('gu_citizen_houses.house_stay_flag','=',1)
        ->where('gu_citizens.usr_id','=',Auth::id())
        ->paginate($rowsPerPage);
    }
}