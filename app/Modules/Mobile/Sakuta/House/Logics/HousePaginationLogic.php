<?php

namespace App\Modules\Mobile\Sakuta\House\Logics;

use Auth;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\House\Queries\HouseQuery;

class HousePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		if($citizen->citzen_isrw)
		{
			$result = (new HouseQuery())->getPaginationRwQuery($citizen->neidet_id, $rowsPerPage);
		}
		else if($citizen->citzen_isrt)
		{
			$result = (new HouseQuery())->getPaginationRtQuery($citizen->neisdet_id, $rowsPerPage);
		}
    	return $result;
	}

	/**
	 * [getMyPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getMyPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new HouseQuery())->getMyPaginationQuery($rowsPerPage);
    	return $result;
	}
}
