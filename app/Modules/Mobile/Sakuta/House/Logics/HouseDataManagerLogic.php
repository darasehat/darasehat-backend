<?php

namespace App\Modules\Mobile\Sakuta\House\Logics;

use Auth;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\House\Queries\HouseField;
use App\Modules\Mobile\Sakuta\House\Queries\HouseQuery;
use App\Modules\Mobile\Sakuta\House\Queries\HouseDataManagerQuery;

class HouseDataManagerLogic extends HouseDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * nei_id, neidet_id dan neisdet_id bisa dimana aja, karena rumah tidak hanya disatu daerah, bisa jadi ada didaerah lain yang jauh letaknya.
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['nei_id','neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
    	// $request->request->add(['layNeiId' => $citizen->nei_id]);
    	// $request->request->add(['layNeidetId' => $citizen->neidet_id]);
		$request->request->add(['is_active' => 1]);
		$datarequest = (new HouseField())->setField($request);
		return $this->storeDataSave($datetime, $datarequest, (new HouseQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new HouseField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new HouseQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new HouseField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new HouseQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new HouseQuery())->setEntity());
	}
}
