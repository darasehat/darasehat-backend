<?php

namespace App\Modules\Mobile\Sakuta\House\Logics;

use Auth;
use App\Modules\Mobile\Sakuta\House\Logics\HouseDataManagerLogic;
use App\Modules\Mobile\Sakuta\House\Logics\HousePaginationLogic;
use App\Modules\Mobile\Sakuta\House\Queries\HouseQuery;

class HouseLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new HousePaginationLogic())->getPaginationData($request);
	}

	/**
	 * [getMyPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getMyPaginationData($request)
	{
		return (new HousePaginationLogic())->getMyPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new HouseDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new HouseDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new HouseQuery())->showQuery($id);
	}

	/**
	 * [doShowMyHouse description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShowMyHouse($id)
	{
		return (new HouseQuery())->showMyHouseQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new HouseQuery())->editQuery($id);
	}

	/**
	 * [doEditMyHouse description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEditMyHouse($id)
	{
		return (new HouseQuery())->editMyHouseQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new HouseDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new HouseQuery())->selectListQuery();
	}

	/**
	 * [doMyHouseSelectList description]
	 * @return [type] [description]
	 */
	public function doMyHouseSelectList()
	{
		return (new HouseQuery())->myHouseSelectListQuery();
	}

	/**
	 * [getRecord description]
	 * @param  [type] $houseid [description]
	 * @param  [type] $select  [description]
	 * @return [type]          [description]
	 */
	public function getRecord($houseid, $select)
	{
		return (new HouseQuery())->recordQuery($houseid, $select);
	}

	/**
	 * [getHouseInRwRecord description]
	 * @param  [type] $neidetid [description]
	 * @param  [type] $houseid  [description]
	 * @return [type]           [description]
	 */
	public function getHouseInRwRecord($neidetid, $houseid, $select)
	{
		return (new HouseQuery())->houseInRwRecordQuery($neidetid, $houseid, $select);
	}

	/**
	 * query data rumah di area RW
	 * @param  [type] $neisdetid [description]
	 * @param  [type] $houseid   [description]
	 * @return [type]            [description]
	 */
	public function getHouseInRtRecord($neisdetid, $houseid, $select)
	{
		return (new HouseQuery())->houseInRtRecordQuery($neisdetid, $houseid, $select);
	}
}
