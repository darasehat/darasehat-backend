<?php

namespace App\Modules\Mobile\Sakuta\House\Logics;

use Auth;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\CitizenHouse\Logics\CitizenHouseLogic;
use App\Modules\Mobile\Sakuta\House\Logics\HouseLogic;
use App\Modules\Mobile\Sakuta\Neighborhood\Logics\NeighborhoodLogic;
use App\Modules\Mobile\Sakuta\NeighborhoodDetail\Logics\NeighborhoodDetailLogic;
use App\Modules\Mobile\Sakuta\NeighborhoodSubdetail\Logics\NeighborhoodSubdetailLogic;

class MyHouseLogic
{
	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		// validasi citizen ada atau tidak
		$citizen = $this->getCitizenUser();
		$request->merge(['layCitzenId' => $citizen->citzen_id]);
		$request->request->add(['layCitzenId' => $citizen->citzen_id]);
		// cek apakah rumah pertama?
		$citizenhouse = (new CitizenHouseLogic())->getMyHouseList($citizen->citzen_id,['citzen_id','house_id','house_owner_flag','house_stay_flag','house_rent_flag']);
		if($citizenhouse->isEmpty())
		{
			//jika rumah pertama
			$result = $this->myFirstHouse($request, $datetime);
			return [
				'status' => true,
				'data' => $result,
			];
		}
		else
		{
			//jika rumah tambahan
			$result = $this->otherHouse($request, $datetime);
			return [
				'status' => true,
				'data' => $result,
			];
		}
	}

	/**
	 * [getCitizenUser description]
	 * @return [type] [description]
	 */
	private function getCitizenUser()
	{
		return (new CitizenLogic())->getUser(Auth::id(),['citzen_id']);
	}

	/**
	 * [myFirstHouse description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	private function myFirstHouse($request, $datetime)
	{
		// validasi adm harus sesuai parent child
		// validasi RTRW harus sesuai parent child
		// validasi jika sudah ada RTRW dilingkungan, maka otomatis menjadi warga
		// jika owner flag true tidak bisa rent flag true
		// jika rent flag true maka stay flag otomatis true
		if(!$request->layHouseId)
		{
			/**
			 * set neighborhood
			 */
			// kalau bukan neiID maka bikin baru
			$result = $this->neighborhood($request, $datetime);
			$request->request = $result->request;
			/**
			 * set neighborhood detail
			 */
			// cek nomor RW
			$result = $this->neighborhoodDetail($request, $datetime);
			$request->request = $result->request;
			/**
			 * set neighborhood subdetail
			 */
			// cek nomor RT
			$result = $this->neighborhoodSubdetail($request, $datetime);
			$request->request = $result->request;
			/**
			 * set house
			 */
			// set house
			$result = $this->house($request, $datetime);
			$request->request = $result['request']->request;
			$house = $result['house'];
		}
		else
		{
			$house = (new HouseLogic())->getRecord($request->layHouseId, ['house_no','nei_id','neidet_id']);
		}
		/**
		 * set Citizen House
		 */
		$result = $this->citizenHouse($request, $datetime);		
		$request->request = $result['request']->request;
		$citizenhouse = $result['citizenhouse'];
		/**
		 * set Citizen
		 */
		$result = $this->citizen($request, $datetime);
		$request->request = $result['request']->request;
		$citizen = $result['citizen'];

		return $house;
	}

	/**
	 * [otherHouse description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 * bisa terjadi 1 rumah ada 2 pemilik, sehingga harus dibuat fungsi untuk menyelesaikan masalah tersebut
	 */
	private function otherHouse($request, $datetime)
	{
		/**
		 * set house
		 */
		if(!$request->layHouseId)
		{
			/**
			 * set neighborhood
			 */
			// kalau bukan neiID maka bikin baru
			$result = $this->neighborhood($request, $datetime);
			$request->request = $result->request;
			/**
			 * set neighborhood detail
			 */
			// cek nomor RW
			$result = $this->neighborhoodDetail($request, $datetime);
			$request->request = $result->request;
			/**
			 * set neighborhood subdetail
			 */
			// cek nomor RT
			$result = $this->neighborhoodSubdetail($request, $datetime);
			$request->request = $result->request;
			/**
			 * set house
			 */
			// set house
			$result = $this->house($request, $datetime);
			$request->request = $result['request']->request;
			$house = $result['house'];
		}
		else
		{
			$house = (new HouseLogic())->getRecord($request->layHouseId, ['house_no','nei_id','neidet_id']);
		}
		/**
		 * set Citizen
		 */
		// $result = $this->citizen($request, $datetime);
		// $request->request = $result['request']->request;
		// $citizen = $result['citizen'];
		/**
		 * set Citizen House
		 */
		$result = $this->citizenHouse($request, $datetime);		
		$request->request = $result['request']->request;
		$citizenhouse = $result['citizenhouse'];

		return $house;
	}
	
	/**
	 * [neighborhood description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	private function neighborhood($request, $datetime)
	{
		$neighborhood = "";
		if(!$request->layNeiId)
		{
			$request->request->add(['layName' => $request->layNeiName]);
			$neighborhood = (new NeighborhoodLogic())->doStore($request, $datetime);
			$request->request->add(['layNeiId' => $neighborhood->nei_id]);
			$request->request->remove('layName');
		}
		return $request;
	}

	/**
	 * [neighborhoodDetail description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	private function neighborhoodDetail($request, $datetime)
	{
		if(!$request->layNeidetId)
		{
			$result = (new NeighborhoodDetailLogic())->getRw($request->layAdm1Id, $request->layAdm2Id, $request->layAdm3Id, $request->layAdm4Id, $request->layRw);
			if(!$result)
			{
				$result = (new NeighborhoodDetailLogic())->doStore($request, $datetime);
			}
			$request->request->add(['layNeidetId' => $result->neidet_id]);
		}
		return $request;
	}

	/**
	 * [neighborhoodSubdetail description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	private function neighborhoodSubdetail($request, $datetime)
	{
		if(!$request->layNeisdetId)
		{
			$result = (new NeighborhoodSubdetailLogic())->getRt($request->layAdm1Id, $request->layAdm2Id, $request->layAdm3Id, $request->layAdm4Id, $request->layRt);
			if(!$result)
			{
				$result = (new NeighborhoodSubdetailLogic())->doStore($request, $datetime);
			}
			$request->request->add(['layNeisdetId' => $result->neisdet_id]);
		}
		return $request;
	}

	/**
	 * [house description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	private function house($request, $datetime)
	{
		$house = (new HouseLogic())->doStore($request, $datetime);
		$request->request->add(['layHouseId' => $house->house_id]);
		return ['request' => $request, 'house' => $house];
	}

	/**
	 * [citizen description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	private function citizen($request, $datetime)
	{
		$flag = false;
		$citizen = null;
		//user mengaku RT atau RW, maka harus di cek dulu apakah RT atau RW sudah ada yang menjabat
		$result = $this->setIsRtRwFlag($request);
		$request->request = $result->request;
		// if($request->layIsrt || $request->layIsrw)
		// {
		// 	// cek posisi pejabat RT RW
		// 	// $rtrws = (new CitizenLogic())->getRtrws($request->layNeiId, $request->layNeidetId);
		// 	// foreach ($rtrws as $key => $value) {
		// 	// 	if($value->citzen_isrw || $value->citzen_isrt)
		// 	// 	{
		// 	// 		$flag = true;
		// 	// 	}
		// 	// }
		// 	// if(!$flag)
		// 	// {
		// 		(new CitizenLogic())->doUpdate($citizen->citzen_id, $request, $datetime);
		// 	// }
		// }
		if(($request->layOwnerFlag && $request->layStayFlag) || ($request->layIsrt || $request->layIsrw) || ($request->layRentFlag && $request->layStayFlag) || $request->layStayFlag)
		{
			$citizen = (new CitizenLogic())->doUpdate($request->layCitzenId, $request, $datetime);
		}
		return ['request' => $request, 'citizen' => $citizen];
	}

	/**
	 * [setIsRtRwFlag description]
	 * @param [type] $request [description]
	 */
	private function setIsRtRwFlag($request)
	{
		$rtrws = (new CitizenLogic())->getRtrws($request->layNeiId, $request->layNeidetId);
		foreach ($rtrws as $key => $value) {
			if($value->citzen_isrw)
			{
				$request->request->add(['layIsrw' => false]);
			}
			if($value->citzen_isrt)
			{
				$request->request->add(['layIsrt' => false]);
			}
		}
		return $request;
	}

	/**
	 * [citizenHouse description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	private function citizenHouse($request, $datetime)
	{
		$citizenhouse = (new CitizenHouseLogic())->doStore($request, $datetime);
		return ['request' => $request, 'citizenhouse' => $citizenhouse];
	}
}
