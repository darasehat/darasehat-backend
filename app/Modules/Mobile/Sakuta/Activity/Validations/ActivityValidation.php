<?php

namespace App\Modules\Mobile\Sakuta\Activity\Validations;

use Auth;
use App\Modules\Mobile\Sakuta\Activity\Logics\ActivityLogic;
// use App\Modules\Mobile\Sakuta\Activity\Queries\ActivityValidationQuery;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;

class ActivityValidation
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function validate($request)
	{
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
		$errors = [];
		$member = $this->isDataOwner($request);
		if($result['errors'])
		{
	        $iserror = true;
	        array_push($errors, $result['errors']);
		}
		if($iserror)
		{
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
	}

	/**
	 * jika proses hanya boleh dilakukan oleh pejabat RTRW saja
	 * @return boolean [description]
	 */
	public function isDataOwner($activityid)
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
		if($citizen->citzen_isrw)
		{
			$activity = (new ActivityLogic())->getInRwRecord($activityid, $citizen->neidet_id, ['act_id']);
			if(!$activity)
			{
				$errors['layActId'][0] = 'Data bukan milik RW!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];
			}
		}
		if($citizen->citzen_isrt)
		{
			$activity = (new ActivityLogic())->getInRtRecord($activityid, $citizen->neisdet_id, ['act_id']);
			if(!$activity)
			{
				$errors['layActId'][0] = 'Data bukan milik RT!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];
			}
		}
		return false;
	}
}
