<?php

namespace App\Modules\Mobile\Sakuta\Activity\Logics;

use App\Modules\Mobile\Sakuta\Activity\Queries\ActivityQuery;

class ActivityPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request, $isrw, $isrt, $neidetid, $neisdetid)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new ActivityQuery())->getPaginationQuery($rowsPerPage, $isrw, $isrt, $neidetid, $neisdetid);
    	return $result;
	}
}
