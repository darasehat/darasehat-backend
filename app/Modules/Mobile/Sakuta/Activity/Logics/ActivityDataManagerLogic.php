<?php

namespace App\Modules\Mobile\Sakuta\Activity\Logics;

use App\Modules\Mobile\Sakuta\Activity\Queries\ActivityField;
use App\Modules\Mobile\Sakuta\Activity\Queries\ActivityQuery;
use App\Modules\Mobile\Sakuta\Activity\Queries\ActivityDataManagerQuery;

class ActivityDataManagerLogic extends ActivityDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new ActivityField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new ActivityQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new ActivityField())->setUpdateField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new ActivityQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new ActivityField())->setDeleteField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new ActivityQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new ActivityQuery())->setEntity());
	}
}
