<?php

namespace App\Modules\Mobile\Sakuta\Activity\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5, $isrw, $isrt, $neidetid, $neisdetid)
    {
        $entity = $entity->select('act_id AS layId','act_name AS layName','act_description AS layDescription','act_publish_datetime AS layPublishDatetime')
    	->where('gu_activities.is_active','=',1);
		if($isrw==1 || ($isrw==0 && $isrt==0))
		{
	    	$entity = $entity->where('gu_activities.neidet_id','=',$neidetid);
		}
		if($isrt==1 || ($isrw==0 && $isrt==0))
		{
	    	$entity = $entity->where('gu_activities.neisdet_id','=',$neisdetid);
		}
		return $entity->paginate($rowsPerPage);
    }
}