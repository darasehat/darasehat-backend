<?php

namespace App\Modules\Mobile\Sakuta\Activity\Queries;

use App\Models\Sakuta\Activity;

class ActivityEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Activity());
    }
}