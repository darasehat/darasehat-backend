<?php

namespace App\Modules\Mobile\Sakuta\Activity\Queries;

use App\Queries\General\FieldMap;

class ActivityField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layNeiId' => 'nei_id',
            'layNeidetId' => 'neidet_id',
            'layNeisdetId' => 'neisdet_id',
            'layName' => 'act_name',
            'layDescription' => 'act_description',
            'layPublishDate' => 'act_publish_date',
            'layPublishTime' => 'act_publish_time',
            'layPublishDatetime' => 'act_publish_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setUpdateField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layName' => 'act_name',
            'layDescription' => 'act_description',
            'layPublishDate' => 'act_publish_date',
            'layPublishTime' => 'act_publish_time',
            'layPublishDatetime' => 'act_publish_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setDeleteField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}