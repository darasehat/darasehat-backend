<?php

namespace App\Modules\Mobile\Sakuta\Activity\Queries;

use App\Modules\Mobile\Sakuta\Activity\Queries\ActivityEntity;

class ActivityQuery extends ActivityEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5, $isrw, $isrt, $neidetid, $neisdetid)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage, $isrw, $isrt, $neidetid, $neisdetid);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id, $isrw, $isrt, $neidetid, $neisdetid)
    {
        $query = $this->setEntity()
        ->select('act_id AS layId','act_name AS layName','act_description AS layDescription','act_publish_datetime AS layPublishDatetime')
        // ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_activities.nei_id')
        ->where('act_id','=',$id);
        if($isrw==1 || ($isrw==0 && $isrt==0))
        {
            $query = $query->where('gu_activities.neidet_id','=',$neidetid);
        }
        if($isrt==1 || ($isrw==0 && $isrt==0))
        {
            $query = $query->where('gu_activities.neisdet_id','=',$neisdetid);
        }
        return $query->first();
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('act_name AS layName','nei_id AS layNei','act_description AS layDescription','act_publish_date AS layPublishDate','act_publish_time AS layPublishTime')
        ->where('act_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('act_name AS layName', 'act_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [inRwRecordQuery description]
     * @param  [type] $neidetid [description]
     * @param  [type] $actid    [description]
     * @param  [type] $select   [description]
     * @return [type]           [description]
     */
    public function inRwRecordQuery($actid, $neidetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('act_id','=',$actid)
        ->where('neidet_id','=',$neidetid)
        ->first();
        return $query;
    }

    /**
     * [inRtRecordQuery description]
     * @param  [type] $neisdetid [description]
     * @param  [type] $actid   [description]
     * @param  [type] $select    [description]
     * @return [type]            [description]
     */
    public function inRtRecordQuery($actid, $neisdetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('act_id','=',$actid)
        ->where('neisdet_id','=',$neisdetid)
        ->first();
        return $query;
    }
}