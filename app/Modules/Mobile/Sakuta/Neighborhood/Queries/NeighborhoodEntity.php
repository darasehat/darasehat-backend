<?php

namespace App\Modules\Mobile\Sakuta\Neighborhood\Queries;

use App\Models\Sakuta\Neighborhood;

class NeighborhoodEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Neighborhood());
    }
}