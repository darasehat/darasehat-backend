<?php

namespace App\Modules\Mobile\Sakuta\Neighborhood\Queries;

use App\Queries\General\FieldMap;

class NeighborhoodField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layAdm1Id' => 'adm1_id',
            'layAdm2Id' => 'adm2_id',
            'layAdm3Id' => 'adm3_id',
            'layAdm4Id' => 'adm4_id',
            'layNeicatId' => 'neicat_id',
            'layName' => 'nei_name',
            'layVerified' => 'nei_verified',
            'layVerifiedby' => 'nei_verifiedby',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}