<?php

namespace App\Modules\Mobile\Sakuta\Neighborhood\Logics;
use App\Modules\Mobile\Sakuta\Neighborhood\Queries\NeighborhoodQuery;

class NeighborhoodPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new NeighborhoodQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
