<?php

namespace App\Modules\Mobile\Sakuta\Neighborhood\Logics;

use App\Modules\Mobile\Sakuta\Neighborhood\Queries\NeighborhoodField;
use App\Modules\Mobile\Sakuta\Neighborhood\Queries\NeighborhoodQuery;
use App\Modules\Mobile\Sakuta\Neighborhood\Queries\NeighborhoodDataManagerQuery;

class NeighborhoodDataManagerLogic extends NeighborhoodDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new NeighborhoodQuery())->setEntity());
	}

	/**
	 * [storeFirstSave description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodField())->setField($request);
        return $this->storeDataSaveFirst($datetime, $datarequest, (new NeighborhoodQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NeighborhoodField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new NeighborhoodQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new NeighborhoodField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new NeighborhoodQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new NeighborhoodQuery())->setEntity());
	}
}
