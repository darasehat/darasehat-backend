<?php

namespace App\Modules\Mobile\Sakuta\Neighborhood\Logics;

use App\Modules\Mobile\Sakuta\Neighborhood\Logics\NeighborhoodDataManagerLogic;
use App\Modules\Mobile\Sakuta\Neighborhood\Logics\NeighborhoodPaginationLogic;
use App\Modules\Mobile\Sakuta\Neighborhood\Queries\NeighborhoodQuery;

class NeighborhoodLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new NeighborhoodPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new NeighborhoodDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doStoreFirst description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStoreFirst($request, $datetime)
	{
		return (new NeighborhoodDataManagerLogic())->storeFirstSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new NeighborhoodDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new NeighborhoodQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new NeighborhoodQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new NeighborhoodDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList($adm1, $adm2, $adm3, $adm4)
	{
		return (new NeighborhoodQuery())->selectListQuery($adm1, $adm2, $adm3, $adm4);
	}

	/**
	 * [getRecord description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new NeighborhoodQuery())->recordQuery($id, $select);
	}
}
