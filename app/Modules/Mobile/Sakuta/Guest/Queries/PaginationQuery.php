<?php

namespace App\Modules\Mobile\Sakuta\Guest\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5, $isrw, $isrt, $neidetid, $neisdetid, $citzenid)
    {
        $entity = $entity->select('guest_id AS layId','guest_name AS layName','guest_phone AS layPhone','citzen_name AS layHost','guest_in_datetime AS layInDatetime','guest_out_datetime AS layOutDatetime')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_guests.nei_id')
        ->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_guests.guest_host')
        ->where('gu_guests.is_active','=',1);
		if($isrw==1 || ($isrw==0 && $isrt==0))
		{
	    	$entity = $entity->where('gu_guests.neidet_id','=',$neidetid);
		}
		if($isrt==1 || ($isrw==0 && $isrt==0))
		{
	    	$entity = $entity->where('gu_guests.neisdet_id','=',$neisdetid);
		}
		if($isrw==0 && $isrt==0)
		{
			$entity = $entity->where('gu_guests.guest_host','=',$citzenid);
		}

        return $entity->paginate($rowsPerPage);
    }
}