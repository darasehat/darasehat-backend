<?php

namespace App\Modules\Mobile\Sakuta\Guest\Queries;

use App\Modules\Mobile\Sakuta\Guest\Queries\GuestEntity;

class GuestQuery extends GuestEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5, $isrw, $isrt, $neidetid, $neisdetid, $citzenid)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage, $isrw, $isrt, $neidetid, $neisdetid, $citzenid);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id, $isrw, $isrt, $neidetid, $neisdetid, $citzenid)
    {
        $query = $this->setEntity()
        ->select('nei_name AS layNei','guest_name AS layName','guest_phone AS layPhone','citzen_name AS layHost','guest_in_datetime AS layDatetimein','guest_out_datetime AS layDatetimeOut')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_guests.nei_id')
        ->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_guests.guest_host')
        ->where('guest_id','=',$id);
        if($isrw==1 || ($isrw==0 && $isrt==0))
        {
            $query = $query->where('gu_guests.neidet_id','=',$neidetid);
        }
        if($isrt==1 || ($isrw==0 && $isrt==0))
        {
            $query = $query->where('gu_guests.neisdet_id','=',$neisdetid);
        }
        if($isrw==0 && $isrt==0)
        {
            $query = $query->where('gu_guests.guest_host','=',$citzenid);
        }
        $query = $query->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('nei_id AS layNei','guest_name AS layName','guest_phone AS layPhone','guest_host AS layHost','guest_in_date AS layDateIn','guest_in_time AS layTimeIn','guest_out_date AS layDateOut','guest_out_time AS layTimeOut')
        ->where('guest_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('guest_name AS layName', 'guest_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * data tamu se RW
     * @param  [type] $guestid  [description]
     * @param  [type] $neidetid [description]
     * @param  [type] $select   [description]
     * @return [type]           [description]
     */
    public function inRwRecordQuery($guestid, $neidetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('guest_id','=',$guestid)
        ->where('neidet_id','=',$neidetid)
        ->first();
        return $query;
    }

    /**
     * tamu seRT
     * @param  [type] $guestid   [description]
     * @param  [type] $neisdetid [description]
     * @param  [type] $select    [description]
     * @return [type]            [description]
     */
    public function inRtRecordQuery($guestid, $neisdetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('guest_id','=',$guestid)
        ->where('neisdet_id','=',$neisdetid)
        ->first();
        return $query;
    }

    /**
     * tamu saya
     * @param  [type] $guestid [description]
     * @param  [type] $hostid  [description]
     * @param  [type] $select  [description]
     * @return [type]          [description]
     */
    public function isMyGuestRecordQuery($guestid, $hostid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('guest_id','=',$guestid)
        ->where('guest_host','=',$hostid)
        ->first();
        return $query;        
    }
}