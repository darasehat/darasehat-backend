<?php

namespace App\Modules\Mobile\Sakuta\Guest\Queries;

use App\Models\Sakuta\Guest;

class GuestEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Guest());
    }
}