<?php

namespace App\Modules\Mobile\Sakuta\Guest\Queries;

use App\Queries\General\FieldMap;

class GuestField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layNeiId' => 'nei_id',
            'layNeidetId' => 'neidet_id',
            'layNeisdetId' => 'neisdet_id',
            'layName' => 'guest_name',
            'layPhone' => 'guest_phone',
            'layHost' => 'guest_host',
            'layInDate' => 'guest_in_date',
            'layInTime' => 'guest_in_time',
            'layInDatetime' => 'guest_in_datetime',
            'layOutDate' => 'guest_out_date',
            'layOutTime' => 'guest_out_time',
            'layOutDatetime' => 'guest_out_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setUpdateField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layName' => 'guest_name',
            'layPhone' => 'guest_phone',
            'layInDate' => 'guest_in_date',
            'layInTime' => 'guest_in_time',
            'layInDatetime' => 'guest_in_datetime',
            'layOutDate' => 'guest_out_date',
            'layOutTime' => 'guest_out_time',
            'layOutDatetime' => 'guest_out_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setUpdateOutField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layOutDate' => 'guest_out_date',
            'layOutTime' => 'guest_out_time',
            'layOutDatetime' => 'guest_out_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setDeleteField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}