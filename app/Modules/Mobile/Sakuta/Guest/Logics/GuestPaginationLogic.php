<?php

namespace App\Modules\Mobile\Sakuta\Guest\Logics;
use App\Modules\Mobile\Sakuta\Guest\Queries\GuestQuery;

class GuestPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request, $isrw, $isrt, $neidetid, $neisdetid, $citzenid)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new GuestQuery())->getPaginationQuery($rowsPerPage, $isrw, $isrt, $neidetid, $neisdetid, $citzenid);
    	return $result;
	}
}
