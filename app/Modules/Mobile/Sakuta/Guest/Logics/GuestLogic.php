<?php

namespace App\Modules\Mobile\Sakuta\Guest\Logics;

use Auth;

use App\Modules\Mobile\Sakuta\Guest\Logics\GuestDataManagerLogic;
use App\Modules\Mobile\Sakuta\Guest\Logics\GuestPaginationLogic;
use App\Modules\Mobile\Sakuta\Guest\Queries\GuestQuery;

use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\CitizenHouse\Logics\CitizenHouseLogic;

class GuestLogic
{
	private $isrw;
	private $isrt;
	private $neidetid;
	private $neisdetid;

	public function citizenStatus()
	{
		return (new CitizenLogic())->getUser(Auth::id(),['citzen_id','nei_id','neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
	}

	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$citizen = $this->citizenStatus();
		$this->isrw = $citizen->citzen_isrw;
		$this->isrt = $citizen->citzen_isrt;
		$this->neidetid = $citizen->neidet_id;
		$this->neisdetid = $citizen->neisdet_id;

		if($request->layMyPersonalFlag == 1 && $request->layHouseId)
		{
			$this->isrw = 0;
			$this->isrt = 0;
		}
		if($this->isrw == 0 && $this->isrt == 0 && $request->layHouseId)
		{
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseRecord($citizen->citzen_id,$request->layHouseId,['house_owner_flag','house_stay_flag','house_rent_flag','gu_houses.neidet_id AS layNeidetId','gu_houses.neisdet_id AS layNeisdetId']);
			$this->neidetid = $citizenhouse->layNeidetId;
			$this->neisdetid = $citizenhouse->layNeisdetId;
		}
		return (new GuestPaginationLogic())->getPaginationData($request, $this->isrw, $this->isrt, $this->neidetid, $this->neisdetid, $citizen->citzen_id);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$citizen = (new CitizenLogic())->getCitizen($request->layHost, ['nei_id','neidet_id','neisdet_id','citzen_isrw','citzen_isrt']);
    	$request->request->add(['layNeiId' => $citizen->nei_id]);
    	$request->request->add(['layNeidetId' => $citizen->neidet_id]);
    	$request->request->add(['layNeisdetId' => $citizen->neisdet_id]);

		if($request->layInDate)
		{
	    	$request->request->add(['layInDatetime' => $request->layInDate.' '.$request->layInTime]);
		}
		if($request->layOutDate)
		{
	    	$request->request->add(['layOutDatetime' => $request->layOutDate.' '.$request->layOutTime]);
		}
		return (new GuestDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		if($request->layInDate)
		{
	    	$request->request->add(['layInDatetime' => $request->layInDate.' '.$request->layInTime]);
		}
		if($request->layOutDate)
		{
	    	$request->request->add(['layOutDatetime' => $request->layOutDate.' '.$request->layOutTime]);
		}
		return (new GuestDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doUpdateOut description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdateOut($id, $request, $date, $time)
	{
    	$request->request->add(['layOutDate' => $date]);
	   	$request->request->add(['layOutTime' => $time]);
	   	$request->request->add(['layOutDatetime' => $date.' '.$time]);
		return (new GuestDataManagerLogic())->updateOutSave($id, $request, $request->layOutDatetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id, $layMyPersonalFlag, $houseid)
	{
		$citizen = $this->citizenStatus();
		$this->isrw = $citizen->citzen_isrw;
		$this->isrt = $citizen->citzen_isrt;
		$this->neidetid = $citizen->neidet_id;
		$this->neisdetid = $citizen->neisdet_id;

		if($layMyPersonalFlag == 1 && $houseid)
		{
			$this->isrw = 0;
			$this->isrt = 0;
		}
		// jika ingin melihat data rumah yang lain 
		if($this->isrw == 0 && $this->isrt == 0 && $houseid)
		{
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseRecord($citizen->citzen_id,$houseid,['house_owner_flag','house_stay_flag','house_rent_flag','gu_houses.neidet_id AS layNeidetId','gu_houses.neisdet_id AS layNeisdetId']);
			$this->neidetid = $citizenhouse->layNeidetId;
			$this->neisdetid = $citizenhouse->layNeisdetId;
		}
		return (new GuestQuery())->showQuery($id, $this->isrw, $this->isrt, $this->neidetid, $this->neisdetid, $citizen->citzen_id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new GuestQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new GuestDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new GuestQuery())->selectListQuery();
	}

	public function getInRwRecord($guestid, $neidetid, $select)
	{
		return (new GuestQuery())->inRwRecordQuery($guestid, $neidetid, $select);
	}

	public function getInRtRecord($guestid, $neisdetid, $select)
	{
		return (new GuestQuery())->inRtRecordQuery($guestid, $neisdetid, $select);
	}

	public function isMyGuestRecord($guestid, $hostid, $select)
	{
		return (new GuestQuery())->isMyGuestRecordQuery($guestid, $hostid, $select);
	}
}
