<?php

namespace App\Modules\Mobile\Sakuta\Guest\Logics;

use App\Modules\Mobile\Sakuta\Guest\Queries\GuestField;
use App\Modules\Mobile\Sakuta\Guest\Queries\GuestQuery;
use App\Modules\Mobile\Sakuta\Guest\Queries\GuestDataManagerQuery;

class GuestDataManagerLogic extends GuestDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new GuestField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new GuestQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new GuestField())->setUpdateField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new GuestQuery())->setEntity());
	}

	/**
	 * [updateOutSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateOutSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new GuestField())->setUpdateOutField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new GuestQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new GuestField())->setDeleteField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new GuestQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new GuestQuery())->setEntity());
	}
}
