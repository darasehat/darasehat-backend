<?php

namespace App\Modules\Mobile\Sakuta\CitizenVehicle\Queries;

use App\Models\Sakuta\CitizenVehicle;

class CitizenVehicleEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new CitizenVehicle());
    }
}