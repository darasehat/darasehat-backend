<?php

namespace App\Modules\Mobile\Sakuta\CitizenVehicle\Queries;

use App\Queries\General\FieldMap;

class CitizenVehicleField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layNeiId' => 'nei_id',
            'layHouseId' => 'house_id',
            'layCitzenId' => 'citzen_id',
            'layVehcatId' => 'vehcat_id',
            'layPlatNo' => 'citzenveh_plat_no',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}