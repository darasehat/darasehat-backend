<?php

namespace App\Modules\Mobile\Sakuta\CitizenVehicle\Queries;

use App\Modules\Mobile\Sakuta\CitizenVehicle\Queries\CitizenVehicleEntity;

class CitizenVehicleQuery extends CitizenVehicleEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('nei_name AS layNei','citzen_name AS layCitizen','vehcat_name AS layVehCat','citzenveh_plat_no AS layPlatNo')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_citizen_vehicles.nei_id')
        ->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_citizen_vehicles.citzen_id')
        ->leftJoin('gu_vehicle_categories','gu_vehicle_categories.vehcat_id','=','gu_citizen_vehicles.vehcat_id')
        ->where('citzenveh_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('nei_id AS layNei','citzen_id AS layCitizen','vehcat_id AS layVehCat','citzenveh_plat_no AS layPlatNo')
        ->where('citzenveh_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('citzenveh_name AS layName', 'citzenveh_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }
}