<?php

namespace App\Modules\Mobile\Sakuta\CitizenVehicle\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('citzenveh_id AS layId','nei_name AS layNei','citzen_name AS layCitizen','vehcat_name AS layVehCat','citzenveh_plat_no AS layPlatNo')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_citizen_vehicles.nei_id')
        ->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_citizen_vehicles.citzen_id')
        ->leftJoin('gu_vehicle_categories','gu_vehicle_categories.vehcat_id','=','gu_citizen_vehicles.vehcat_id')
        ->where('gu_citizen_vehicles.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}