<?php

namespace App\Modules\Mobile\Sakuta\CitizenVehicle\Logics;
use App\Modules\Mobile\Sakuta\CitizenVehicle\Queries\CitizenVehicleQuery;

class CitizenVehiclePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new CitizenVehicleQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
