<?php

namespace App\Modules\Mobile\Sakuta\CitizenVehicle\Logics;

use App\Modules\Mobile\Sakuta\CitizenVehicle\Queries\CitizenVehicleField;
use App\Modules\Mobile\Sakuta\CitizenVehicle\Queries\CitizenVehicleQuery;
use App\Modules\Mobile\Sakuta\CitizenVehicle\Queries\CitizenVehicleDataManagerQuery;

class CitizenVehicleDataManagerLogic extends CitizenVehicleDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CitizenVehicleField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new CitizenVehicleQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CitizenVehicleField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new CitizenVehicleQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new CitizenVehicleField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new CitizenVehicleQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new CitizenVehicleQuery())->setEntity());
	}
}
