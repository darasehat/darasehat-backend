<?php

namespace App\Modules\Mobile\Sakuta\GoodsIn\Queries;

use App\Modules\Mobile\Sakuta\GoodsIn\Queries\GoodsInEntity;

class GoodsInQuery extends GoodsinEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5, $isrw, $isrt, $neidetid, $neisdetid, $citzenid)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage, $isrw, $isrt, $neidetid, $neisdetid, $citzenid);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id, $isrw, $isrt, $neidetid, $neisdetid, $citzenid)
    {
        $query = $this->setEntity()
        ->select('nei_name AS layNei','gugoodsin_name AS layName','citzen_name AS layReceivedBy','gugoodsin_sender AS laySender','gugoodsin_datetime AS layDatetime','gugoodsin_description AS layDescription')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_goods_in.nei_id')
        ->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_goods_in.gugoodsin_received_by')
        ->where('gu_neighborhoods.is_active','=',1)
        ->where('gugoodsin_id','=',$id);
        if($isrw==1 || ($isrw==0 && $isrt==0))
        {
            $query = $query->where('gu_goods_in.neidet_id','=',$neidetid);
        }
        if($isrt==1 || ($isrw==0 && $isrt==0))
        {
            $query = $query->where('gu_goods_in.neisdet_id','=',$neisdetid);
        }
        if($isrw==0 && $isrt==0)
        {
            $query = $query->where('gu_goods_in.gugoodsin_received_by','=',$citzenid);
        }
        $query = $query->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('nei_id AS layNei','gugoodsin_name AS layName','gugoodsin_received_by AS layReceivedBy','gugoodsin_sender AS laySender','gugoodsin_datetime AS layDatetime','gugoodsin_description AS layDescription')
        ->where('gugoodsin_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('gugoodsin_name AS layName', 'gugoodsin_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * data se RW
     * @param  [type] $goodsinid  [description]
     * @param  [type] $neidetid [description]
     * @param  [type] $select   [description]
     * @return [type]           [description]
     */
    public function inRwRecordQuery($goodsinid, $neidetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('gugoodsin_id','=',$goodsinid)
        ->where('neidet_id','=',$neidetid)
        ->first();
        return $query;
    }

    /**
     * data seRT
     * @param  [type] $goodsinid   [description]
     * @param  [type] $neisdetid [description]
     * @param  [type] $select    [description]
     * @return [type]            [description]
     */
    public function inRtRecordQuery($goodsinid, $neisdetid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('gugoodsin_id','=',$goodsinid)
        ->where('neisdet_id','=',$neisdetid)
        ->first();
        return $query;
    }

    /**
     * data saya
     * @param  [type] $goodsinid [description]
     * @param  [type] $receivedby  [description]
     * @param  [type] $select  [description]
     * @return [type]          [description]
     */
    public function isMyGoodsRecordQuery($goodsinid, $receivedby, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('gugoodsin_id','=',$goodsinid)
        ->where('gugoodsin_received_by','=',$receivedby)
        ->first();
        return $query;        
    }
    
}