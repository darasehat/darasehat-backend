<?php

namespace App\Modules\Mobile\Sakuta\GoodsIn\Queries;

use App\Queries\General\FieldMap;

class GoodsInField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layNeiId' => 'nei_id',
            'layNeidetId' => 'neidet_id',
            'layNeisdetId' => 'neisdet_id',
            'layReceivedBy' => 'gugoodsin_received_by',
            'layName' => 'gugoodsin_name',
            'laySender' => 'gugoodsin_sender',
            'layDescription' => 'gugoodsin_description',
            'layDate' => 'gugoodsin_date',
            'layTime' => 'gugoodsin_time',
            'layDatetime' => 'gugoodsin_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setUpdateField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
            'layName' => 'gugoodsin_name',
            'laySender' => 'gugoodsin_sender',
            'layDescription' => 'gugoodsin_description',
            'layDate' => 'gugoodsin_date',
            'layTime' => 'gugoodsin_time',
            'layDatetime' => 'gugoodsin_datetime',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }

    public function setDeleteField($request)
    {
        $fieldmap = [
            'is_active' => 'is_active',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}