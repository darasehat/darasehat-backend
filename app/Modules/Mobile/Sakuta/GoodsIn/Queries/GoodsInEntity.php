<?php

namespace App\Modules\Mobile\Sakuta\GoodsIn\Queries;

use App\Models\Sakuta\GoodsIn;

class GoodsInEntity
{
	protected $primaryKey = 'gugoodsin_id';

	protected $table = 'gu_goods_in';

    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new GoodsIn());
    }
}