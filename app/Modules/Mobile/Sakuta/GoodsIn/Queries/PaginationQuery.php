<?php

namespace App\Modules\Mobile\Sakuta\GoodsIn\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5, $isrw, $isrt, $neidetid, $neisdetid, $citzenid)
    {
        $entity = $entity->select('gugoodsin_id AS layId','nei_name AS layNei','gugoodsin_name AS layName','citzen_name AS layReceiveBy','gugoodsin_sender AS laySender','gugoodsin_datetime AS layDatetime')
        ->leftJoin('gu_neighborhoods','gu_neighborhoods.nei_id','=','gu_goods_in.nei_id')
		->leftJoin('gu_citizens','gu_citizens.citzen_id','=','gu_goods_in.gugoodsin_received_by')
       	->where('gu_goods_in.is_active','=',1);
		if($isrw==1 || ($isrw==0 && $isrt==0))
		{
	    	$entity = $entity->where('gu_goods_in.neidet_id','=',$neidetid);
		}
		if($isrt==1 || ($isrw==0 && $isrt==0))
		{
	    	$entity = $entity->where('gu_goods_in.neisdet_id','=',$neisdetid);
		}
		if($isrw==0 && $isrt==0)
		{
			$entity = $entity->where('gu_goods_in.gugoodsin_received_by','=',$citzenid);
		}
        return $entity->paginate($rowsPerPage);
    }
}