<?php

namespace App\Modules\Mobile\Sakuta\GoodsIn\Logics;
use App\Modules\Mobile\Sakuta\GoodsIn\Queries\GoodsInQuery;

class GoodsInPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request, $isrw, $isrt, $neidetid, $neisdetid, $citzenid)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new GoodsInQuery())->getPaginationQuery($rowsPerPage, $isrw, $isrt, $neidetid, $neisdetid, $citzenid);
    	return $result;
	}
}
