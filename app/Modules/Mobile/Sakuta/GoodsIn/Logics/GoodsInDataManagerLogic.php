<?php

namespace App\Modules\Mobile\Sakuta\GoodsIn\Logics;

use App\Modules\Mobile\Sakuta\GoodsIn\Queries\GoodsInField;
use App\Modules\Mobile\Sakuta\GoodsIn\Queries\GoodsInQuery;
use App\Modules\Mobile\Sakuta\GoodsIn\Queries\GoodsInDataManagerQuery;

class GoodsInDataManagerLogic extends GoodsInDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new GoodsInField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new GoodsInQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new GoodsInField())->setUpdateField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new GoodsInQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new GoodsInField())->setDeleteField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new GoodsInQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new GoodsInQuery())->setEntity());
	}
}
