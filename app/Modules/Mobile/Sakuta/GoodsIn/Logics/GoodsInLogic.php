<?php

namespace App\Modules\Mobile\Sakuta\GoodsIn\Logics;

use Auth;

use App\Modules\Mobile\Sakuta\GoodsIn\Logics\GoodsInDataManagerLogic;
use App\Modules\Mobile\Sakuta\GoodsIn\Logics\GoodsInPaginationLogic;
use App\Modules\Mobile\Sakuta\GoodsIn\Queries\GoodsInQuery;

use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;
use App\Modules\Mobile\Sakuta\CitizenHouse\Logics\CitizenHouseLogic;

class GoodsInLogic
{
	private $isrw;
	private $isrt;
	private $neidetid;
	private $neisdetid;

	public function citizenStatus()
	{
		return (new CitizenLogic())->getUser(Auth::id(),['citzen_id','nei_id','neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
	}

	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$citizen = $this->citizenStatus();
		$this->isrw = $citizen->citzen_isrw;
		$this->isrt = $citizen->citzen_isrt;
		$this->neidetid = $citizen->neidet_id;
		$this->neisdetid = $citizen->neisdet_id;

		if($request->layMyPersonalFlag == 1 && $request->layHouseId)
		{
			$this->isrw = 0;
			$this->isrt = 0;
		}
		// jika ingin melihat data rumah yang lain
		if($this->isrw == 0 && $this->isrt == 0 && $request->layHouseId)
		{
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseRecord($citizen->citzen_id,$request->layHouseId,['house_owner_flag','house_stay_flag','house_rent_flag','gu_houses.neidet_id AS layNeidetId','gu_houses.neisdet_id AS layNeisdetId']);
			$this->neidetid = $citizenhouse->layNeidetId;
			$this->neisdetid = $citizenhouse->layNeisdetId;
		}
		return (new GoodsInPaginationLogic())->getPaginationData($request, $this->isrw, $this->isrt, $this->neidetid, $this->neisdetid, $citizen->citzen_id);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$citizen = $this->citizenStatus();
    	$request->request->add(['layNeiId' => $citizen->nei_id]);
		if($citizen->citzen_isrw==1)
		{
	    	$request->request->add(['layNeidetId' => $citizen->neidet_id]);
		}
		else if($citizen->citzen_isrt==1)
		{
	    	$request->request->add(['layNeidetId' => $citizen->neidet_id]);
	    	$request->request->add(['layNeisdetId' => $citizen->neisdet_id]);
		}
    	$request->request->add(['layDatetime' => $request->layDate.' '.$request->layTime]);
		return (new GoodsInDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
    	$request->request->add(['layDatetime' => $request->layDate.' '.$request->layTime]);
		return (new GoodsInDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id, $layMyPersonalFlag, $houseid)
	{
		$citizen = $this->citizenStatus();
		$this->isrw = $citizen->citzen_isrw;
		$this->isrt = $citizen->citzen_isrt;
		$this->neidetid = $citizen->neidet_id;
		$this->neisdetid = $citizen->neisdet_id;

		if($layMyPersonalFlag == 1 && $houseid)
		{
			$this->isrw = 0;
			$this->isrt = 0;
		}
		// jika ingin melihat data rumah yang lain 
		if($this->isrw == 0 && $this->isrt == 0 && $houseid)
		{
			$citizenhouse = (new CitizenHouseLogic())->getMyHouseRecord($citizen->citzen_id,$houseid,['house_owner_flag','house_stay_flag','house_rent_flag','gu_houses.neidet_id AS layNeidetId','gu_houses.neisdet_id AS layNeisdetId']);
			$this->neidetid = $citizenhouse->layNeidetId;
			$this->neisdetid = $citizenhouse->layNeisdetId;
		}
		return (new GoodsInQuery())->showQuery($id, $this->isrw, $this->isrt, $this->neidetid, $this->neisdetid, $citizen->citzen_id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new GoodsInQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new GoodsInDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		// return (new GoodsInQuery())->selectListQuery();
	}

	/**
	 * [getRecord description]
	 * @param  [type] $id     [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new GoodsInQuery())->recordQuery($id, $select);
	}

	/**
	 * [getInRwRecord description]
	 * @param  [type] $actid    [description]
	 * @param  [type] $neidetid [description]
	 * @param  [type] $select   [description]
	 * @return [type]           [description]
	 */
	public function getInRwRecord($actid, $neidetid, $select)
	{
		return (new GoodsInQuery())->inRwRecordQuery($actid, $neidetid, $select);
	}

	/**
	 * [getInRtRecord description]
	 * @param  [type] $actid     [description]
	 * @param  [type] $neisdetid [description]
	 * @param  [type] $select    [description]
	 * @return [type]            [description]
	 */
	public function getInRtRecord($actid, $neisdetid, $select)
	{
		return (new GoodsInQuery())->inRtRecordQuery($actid, $neisdetid, $select);
	}

	/**
	 * [isMyGoodsRecord description]
	 * @param  [type]  $guestid [description]
	 * @param  [type]  $hostid  [description]
	 * @param  [type]  $select  [description]
	 * @return boolean          [description]
	 */
	public function isMyGoodsRecord($guestid, $hostid, $select)
	{
		return (new GoodsInQuery())->isMyGoodsRecordQuery($guestid, $hostid, $select);
	}
}
