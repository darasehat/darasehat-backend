<?php

namespace App\Modules\Mobile\Sakuta\GoodsIn\Validations;

use Auth;
use App\Modules\Mobile\Sakuta\GoodsIn\Logics\GoodsInLogic;
// use App\Modules\Mobile\Sakuta\GoodsIn\Queries\GoodsInValidationQuery;
use App\Modules\Mobile\Sakuta\Citizen\Logics\CitizenLogic;

class GoodsInValidation
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function validate($request)
	{
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
		$errors = [];
		$member = $this->isDataOwner($request);
		if($result['errors'])
		{
	        $iserror = true;
	        array_push($errors, $result['errors']);
		}
		if($iserror)
		{
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
	}

	/**
	 * jika proses hanya boleh dilakukan oleh pejabat RTRW saja
	 * @return boolean [description]
	 */
	public function isDataOwner($goodsinid)
	{
		$citizen = (new CitizenLogic())->getUser(Auth::id(),['citzen_id','neidet_id','neisdet_id','citzen_isrt','citzen_isrw']);
		if($citizen->citzen_isrw==1)
		{
			$guest = (new GoodsInLogic())->getInRwRecord($goodsinid, $citizen->neidet_id, ['gugoodsin_id']);
			if(!$guest)
			{
				$errors['layGuestId'][0] = 'Data bukan milik RW!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];
			}
		}
		if($citizen->citzen_isrt==1)
		{
			$guest = (new GoodsInLogic())->getInRtRecord($goodsinid, $citizen->neisdet_id, ['gugoodsin_id']);
			if(!$guest)
			{
				$errors['layGuestId'][0] = 'Data bukan milik RT!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];
			}
		}
		if($citizen->citzen_isrw==0 && $citizen->citzen_isrt==0)
		{
			$guest = (new GoodsInLogic())->isMyGoodsRecord($goodsinid, $citizen->citzen_id, ['gugoodsin_id']);
			if(!$guest)
			{
				$errors['layGuestId'][0] = 'Data bukan milik kamu!';
				return [
					'message' => 'The given data was invalid.',
					'errors' => $errors,
				];
			}
		}
		return false;
	}
}
