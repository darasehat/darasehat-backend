<?php

namespace App\Modules\Mobile\AdministrativeAreaSecond\Logics;

use Auth;
use App\Modules\Mobile\AdministrativeAreaSecond\Queries\AdministrativeAreaSecondField;
use App\Modules\Mobile\AdministrativeAreaSecond\Queries\AdministrativeAreaSecondQuery;
use App\Modules\Mobile\AdministrativeAreaSecond\Queries\AdministrativeAreaSecondDataManagerQuery;

class AdministrativeAreaSecondDataManagerLogic extends AdministrativeAreaSecondDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new AdministrativeAreaSecondField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new AdministrativeAreaSecondQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new AdministrativeAreaSecondField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new AdministrativeAreaSecondQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new AdministrativeAreaSecondField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new AdministrativeAreaSecondQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new AdministrativeAreaSecondQuery())->setEntity());
	}
}
