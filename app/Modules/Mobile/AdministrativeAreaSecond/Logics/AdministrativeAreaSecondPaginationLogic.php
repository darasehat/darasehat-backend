<?php

namespace App\Modules\Mobile\AdministrativeAreaSecond\Logics;
use App\Modules\Mobile\AdministrativeAreaSecond\Queries\AdministrativeAreaSecondQuery;

class AdministrativeAreaSecondPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new AdministrativeAreaSecondQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
