<?php

namespace App\Modules\Mobile\AdministrativeAreaSecond\Logics;

use App\Modules\Mobile\AdministrativeAreaSecond\Logics\AdministrativeAreaSecondDataManagerLogic;
use App\Modules\Mobile\AdministrativeAreaSecond\Logics\AdministrativeAreaSecondPaginationLogic;
use App\Modules\Mobile\AdministrativeAreaSecond\Queries\AdministrativeAreaSecondQuery;

class AdministrativeAreaSecondLogic
{
	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList($adm1)
	{
		return (new AdministrativeAreaSecondQuery())->selectListQuery($adm1);		
	}
}
