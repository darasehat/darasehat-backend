<?php

namespace App\Modules\Mobile\AdministrativeAreaSecond\Queries;

use App\Modules\Mobile\AdministrativeAreaSecond\Queries\AdministrativeAreaSecondEntity;

class AdministrativeAreaSecondQuery extends AdministrativeAreaSecondEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery($adm1)
    {
        $query = $this->setEntity()
        ->select('adm2_name AS layName', 'adm2_id AS layId')
        ->where('is_active','=','1')
        ->where('adm1_id','=',$adm1)
        ->orderBy('adm2_name','asc')
        ->get();
        return $query;
    }
}