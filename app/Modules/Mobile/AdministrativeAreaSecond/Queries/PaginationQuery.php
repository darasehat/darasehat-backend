<?php

namespace App\Modules\Mobile\AdministrativeAreaSecond\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('adm2_id AS layId','adm2_name AS layName','adm1_name AS layAdm1')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','administrative_area_seconds.adm1_id')
        ->where('administrative_area_seconds.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}