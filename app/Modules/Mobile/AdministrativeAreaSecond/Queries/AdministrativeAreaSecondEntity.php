<?php

namespace App\Modules\Mobile\AdministrativeAreaSecond\Queries;

use App\Models\AdministrativeAreaSecond;

class AdministrativeAreaSecondEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new AdministrativeAreaSecond());
    }
}