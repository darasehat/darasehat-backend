<?php

namespace App\Modules\Mobile\AdministrativeAreaFourth\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('adm4_id AS layId','adm4_name AS layName','adm1_name AS layProvince','adm2_name AS layCity','adm3_name AS layCamat')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','administrative_area_fourths.adm3_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','administrative_area_thirds.adm2_id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','administrative_area_seconds.adm1_id')
        ->where('administrative_area_fourths.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}