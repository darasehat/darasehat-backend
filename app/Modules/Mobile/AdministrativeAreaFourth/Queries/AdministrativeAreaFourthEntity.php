<?php

namespace App\Modules\Mobile\AdministrativeAreaFourth\Queries;

use App\Models\AdministrativeAreaFourth;

class AdministrativeAreaFourthEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new AdministrativeAreaFourth());
    }
}