<?php

namespace App\Modules\Mobile\AdministrativeAreaFourth\Queries;

use App\Models\AdministrativeAreaFourth;
use App\Queries\General\FieldMap;

class AdministrativeAreaFourthField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layAdm3Id' => 'adm3_id',
            'layName' => 'adm4_name',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}