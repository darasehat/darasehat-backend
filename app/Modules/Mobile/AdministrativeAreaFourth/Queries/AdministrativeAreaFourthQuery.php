<?php

namespace App\Modules\Mobile\AdministrativeAreaFourth\Queries;

use App\Modules\Mobile\AdministrativeAreaFourth\Queries\AdministrativeAreaFourthEntity;

class AdministrativeAreaFourthQuery extends AdministrativeAreaFourthEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery($adm3)
    {
        $query = $this->setEntity()
        ->select('adm4_name AS layName', 'adm4_id AS layId')
        ->where('is_active','=','1')
        ->where('adm3_id','=',$adm3)
        ->orderBy('adm4_name','asc')
        ->get();
        return $query;
    }
}