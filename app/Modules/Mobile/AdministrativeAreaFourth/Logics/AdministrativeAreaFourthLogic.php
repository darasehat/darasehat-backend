<?php

namespace App\Modules\Mobile\AdministrativeAreaFourth\Logics;

use App\Modules\Mobile\AdministrativeAreaFourth\Queries\AdministrativeAreaFourthQuery;

class AdministrativeAreaFourthLogic
{
	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList($adm3)
	{
		return (new AdministrativeAreaFourthQuery())->selectListQuery($adm3);
	}

}
