<?php

namespace App\Modules\Mobile\MyMedicineRecord\Queries;

use App\Models\MedicineRecord;

class MyMedicineRecordEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new MedicineRecord());
    }
}