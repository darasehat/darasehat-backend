<?php

namespace App\Modules\Mobile\MyMedicineRecord\Queries;

use DB;
use App\Modules\Mobile\MyMedicineRecord\Queries\MyMedicineRecordEntity;

class MyMedicineRecordDashboardQuery extends MyMedicineRecordEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * 
     */
    private function setLevel($query, $request)
    {
        if($request->layCmpId)
        {
            $query = $query->where('gym_members.cmp_id','=',$request->layCmpId);
        }
        if($request->layBrndId)
        {
            $query = $query->where('gym_members.brnd_id','=',$request->layBrndId);            
        }
        if($request->layStoreId)
        {
            $query = $query->where('gym_members.store_id','=',$request->layStoreId);            
        }

        $searchParam = json_decode($request->search);
        if (!empty($searchParam->layCmp))
        {
            $query = $query->where('gym_members.cmp_id','=',$searchParam->layCmp);
        }
        if (!empty($searchParam->layBrnd))
        {
            $query = $query->where('gym_members.brnd_id','=',$searchParam->layBrnd);
        }
        if (!empty($searchParam->layStore))
        {
            $query = $query->where('gym_members.store_id','=',$searchParam->layStore);
        }
        return $query;
    }

    /**
     * [getTotalQuery description]
     * @return [type]     [description]
     */
    public function getTotalQuery($request)
    {
        $query = $this->setEntity()
        ->select('gymbr_id AS layId')
        ->where('is_active','=',1);
        $query = $this->setLevel($query, $request);
        return $query->count();
    }

    /**
     * 
     */
    public function getTodayQuery($date, $request)
    {
        $query = $this->setEntity()
        ->select('gymbr_id AS layId')
        ->leftJoin('me_members','me_members.mbr_id','=','gym_members.mbr_id')
        ->where('gym_members.is_active','=',1)
        ->where('mbr_reg_date','=',$date);
        $query = $this->setLevel($query, $request);
        return $query->count();
    }
}