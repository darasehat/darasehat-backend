<?php

namespace App\Modules\Mobile\MyMedicineRecord\Queries;

use App\Queries\General\FieldMap;

class MyMedicineRecordField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layUsrId' => 'usr_id',
            'layUsrprfId' => 'usrprf_id',
            'layDate' => 'medrec_date',
            'layTime' => 'medrec_time',
            'layDatetime' => 'medrec_datetime',
            'layUsrId' => 'usr_id',
            'layAdm1Id' => 'adm1_id',
            'layAdm2Id' => 'adm2_id',
            'layAdm3Id' => 'adm3_id',
            'layAdm4Id' => 'adm4_id'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}