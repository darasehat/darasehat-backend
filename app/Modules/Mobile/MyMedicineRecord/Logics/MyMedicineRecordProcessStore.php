<?php

namespace App\Modules\Mobile\MyMedicineRecord\Logics;

use App\Modules\Mobile\MyMedicineRecord\Logics\MyMedicineRecordLogic;
use App\Modules\Mobile\MyProfile\Logics\MyProfileLogic;

class MyMedicineRecordProcessStore
{
    public function doStore($request, $datetime, $date, $time)
    {
		$request->request->add(['layDate' => $date]);
		$request->request->add(['layTime' => $time]);
        //datetime
		$request->request->add(['layDatetime' => $request->layDate.' '.$request->layTime]);
        //usrprf_id
        $userProfile = (new MyProfileLogic())->doShow();
		$request->request->add(['layUsrprfId' => $userProfile->layId]);
        //adm1
		$request->request->add(['layAdm1Id' => $userProfile->adm1_id]);
        //adm2
		$request->request->add(['layAdm2Id' => $userProfile->adm2_id]);
        //adm3
		$request->request->add(['layAdm3Id' => $userProfile->adm3_id]);
        //adm4
		$request->request->add(['layAdm4Id' => $userProfile->adm4_id]);
        //usr_id
		$request->request->add(['layUsrId' => $userProfile->usr_id]);

        return (new MyMedicineRecordLogic())->doStore($request, $datetime);
    }
}
