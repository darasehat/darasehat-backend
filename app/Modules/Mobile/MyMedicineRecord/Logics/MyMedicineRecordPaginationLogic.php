<?php

namespace App\Modules\Mobile\MyMedicineRecord\Logics;
use App\Modules\Mobile\MyMedicineRecord\Queries\MyMedicineRecordQuery;

class MyMedicineRecordPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new MyMedicineRecordQuery())->getPaginationQuery($rowsPerPage, $request);
		return $result;
	}
}
