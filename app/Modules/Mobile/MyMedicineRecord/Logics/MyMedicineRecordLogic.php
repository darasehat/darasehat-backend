<?php

namespace App\Modules\Mobile\MyMedicineRecord\Logics;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Modules\Mobile\MyMedicineRecord\Logics\MyMedicineRecordDataManagerLogic;
use App\Modules\Mobile\MyMedicineRecord\Logics\MyMedicineRecordPaginationLogic;
use App\Modules\Mobile\MyMedicineRecord\Queries\MyMedicineRecordQuery;

// use App\Modules\Mobile\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\Mobile\MyMedicineRecordPackage\Logics\MyMedicineRecordPackageLogic;

class MyMedicineRecordLogic
{
    /**
     * [getPaginationData description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    public function getPaginationData($request)
    {
        $result = (new MyMedicineRecordPaginationLogic())->getPaginationData($request);
        return [
            'data' => $result,
            'lastRecord' => $result->count() + $request->lastRecord
        ];
    }

    /**
     * [doStore description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doStore($request, $datetime)
    {
        $result = (new MyMedicineRecordDataManagerLogic())->storeSave($request, $datetime);
        $nextDate = $this->getNextDate();
        if($result)
        {
            return [
                "layDate" => $result->medrec_date,
                "layTime" => $result->medrec_time,
                "layNextDate" => $nextDate,
                "layNextTime" => '-'
            ];
        }
        else
        {
            return [
                "layId" => 0,
                "layDate" => '-',
                "layTime" => '-',
                "layNextDate" => $nextDate,
                "layNextTime" => '-'
            ];
        }
    }

    /**
     * [doUpdate description]
     * @param  [type] $id       [description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doUpdate($id, $request, $datetime)
    {
        $result = (new MyMedicineRecordDataManagerLogic())->updateSave($id, $request, $datetime);
        $nextDate = $this->getNextDate();
        if($result)
        {
            return [
                "layId" => $result->medrec_id,
                "layDate" => $result->medrec_date,
                "layTime" => $result->medrec_time,
                "layNextDate" => $nextDate,
                "layNextTime" => '-'
            ];
        }
        else
        {
            return [
                "layId" => 0,
                "layDate" => '-',
                "layTime" => '-',
                "layNextDate" => $nextDate,
                "layNextTime" => '-'
            ];
        }
    }

    /**
     * 
     */
    // public function doUpdateSystem($id, $request, $datetime)
    // {
    //     return (new MyMedicineRecordDataManagerLogic())->updateSaveSystem($id, $request, $datetime);
    // }

    /**
     * [doShow description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function doShow()
    {
        $result = (new MyMedicineRecordQuery())->showQuery();
        $nextDate = $this->getNextDate();
        if($result)
        {
            return [
                "layId" => $result->layId,
                "layDate" => $result->layDate,
                "layTime" => $result->layTime,
                "layNextDate" => $nextDate,
                "layNextTime" => '-'
            ];
        }
        else
        {
            return [
                "layId" => 0,
                "layDate" => '-',
                "layTime" => '-',
                "layNextDate" => $nextDate,
                "layNextTime" => '-'
            ];
        }
    }
    
    /**
     * 
     */
    public function showMineRecord($id)
    {
        return (new MyMedicineRecordQuery())->showMineQuery($id);
    }

    /**
     * 
     */
    public function isDatetimeRecord($date, $time)
    {
        return (new MyMedicineRecordQuery())->isDatetimeRecordQuery($date, $time);
    }

    /**
     * [doEdit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    // public function doEdit($id)
    // {
    //     $result = (new MyMedicineRecordQuery())->editQuery($id);
    //     return $result;
    // }

    /**
     * [doDelete description]
     * @param  [type] $id       [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    // public function doDelete($id, $request, $datetime)
    // {
    //     return (new MyMedicineRecordDataManagerLogic())->delete($id, $request, $datetime);
    // }

    /**
     * [getRecord description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    // public function getRecord($id, $select)
    // {
    //     return (new MyMedicineRecordQuery())->recordQuery($id, $select);
    // }

    public function getNextDate()
    {
        //carbon hari ini
        $today = (new Carbon())->createFromFormat('Y-m-d', date('Y-m-d'));
        $nextDate = $today;
        if($today->isFriday())
        {
            $nextDate = $today->isoFormat('YYYY-MM-DD');
            // return "today";
        }
        else if($today->isSaturday() || $today->isSunday())
        {
            $nextDate = (new Carbon('next friday'))->isoFormat('YYYY-MM-DD');
            // return "today2";
        }
        else if($today->isMonday() || $today->isTuesday() || $today->isWednesday() || $today->isThursday())
        {
            $nextDate = (new Carbon('this friday'))->isoFormat('YYYY-MM-DD');
        }
        else {
            $nextDate = $nextDate->isoFormat('YYYY-MM-DD');
        }
        return $nextDate;
    }
}
