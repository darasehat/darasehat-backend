<?php

namespace App\Modules\Mobile\MyMedicineRecord\Logics;

use App\Modules\Mobile\MyMedicineRecord\Logics\MyMedicineRecordLogic;

class MyMedicineRecordProcessUpdate
{
    public function doUpdate($id, $request, $datetime)
    {
        //datetime
		$request->request->add(['layDatetime' => $request->layDate.' '.$request->layTime]);
        return (new MyMedicineRecordLogic())->doUpdate($id, $request, $datetime);
    }
}
