<?php

namespace App\Modules\Mobile\MyMedicineRecord\Logics;

use App\Modules\Mobile\MyMedicineRecord\Queries\MyMedicineRecordField;
use App\Modules\Mobile\MyMedicineRecord\Queries\MyMedicineRecordQuery;
use App\Modules\Mobile\MyMedicineRecord\Queries\MyMedicineRecordDataManagerQuery;

class MyMedicineRecordDataManagerLogic extends MyMedicineRecordDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new MyMedicineRecordField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new MyMedicineRecordQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
		$request->request->add(['is_active' => 1]);
		$datarequest = (new MyMedicineRecordField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new MyMedicineRecordQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
		$request->request->add(['is_active' => 0]);
		$datarequest = (new MyMedicineRecordField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new MyMedicineRecordQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new MyMedicineRecordQuery())->setEntity());
	}
}
