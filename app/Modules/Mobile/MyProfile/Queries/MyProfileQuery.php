<?php

namespace App\Modules\Mobile\MyProfile\Queries;

use App\Modules\Mobile\MyProfile\Queries\MyProfileEntity;
use Auth;

class MyProfileQuery extends MyProfileEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    // public function getPaginationQuery($rowsPerPage = 5)
    // {
    //     return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    // }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery()
    {
        $query = $this->newEntity()
        ->select(
            'usrprf_id AS layId',
            'usrprf_name AS layName',
            'usrprf_home_address AS layAddress',
            'usrprf_phone_no AS layPhone',
            'usrprf_school AS laySchool',
            'adm1_id',
            'adm2_id',
            'adm3_id',
            'adm4_id',
            'usr_id'
        )
        ->where('usr_id','=',Auth::user()->id)
        ->first();
        return $query;
    }

    /**
     * [myProfileQuery description]
     * @return [type] [description]
     */
    // public function myProfileQuery()
    // {
    //     $query = $this->newEntity()
    //     ->select(
    //         'usrprf_name AS layName',
    //         'usrprf_home_address AS layAddress',
    //         'usrprf_phone_no AS layPhone',
    //         'usrprf_school AS laySchool',
    //         'adm1_id',
    //         'adm2_id',
    //         'adm3_id',
    //         'adm4_id',
    //         )
    //     ->where('usr_id','=',Auth::user()->id)
    //     ->first();
    //     return $query;
    // }
}