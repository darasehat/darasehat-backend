<?php

namespace App\Modules\Mobile\MyProfile\Queries;

use App\Models\UserProfile;

class MyProfileEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new UserProfile());
    }
}