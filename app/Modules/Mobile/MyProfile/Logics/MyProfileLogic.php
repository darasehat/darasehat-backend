<?php

namespace App\Modules\Mobile\MyProfile\Logics;

use App\Modules\Mobile\MyProfile\Logics\MyProfileDataManagerLogic;
use App\Modules\Mobile\MyProfile\Logics\MyProfilePaginationLogic;
use App\Modules\Mobile\MyProfile\Queries\MyProfileQuery;

class MyProfileLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	// public function getPaginationData($request)
	// {
	// 	return (new MyProfilePaginationLogic())->getPaginationData($request);
	// }

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	// public function doStore($request, $datetime)
	// {
	// 	return (new MyProfileDataManagerLogic())->storeSave($request, $datetime);
	// }

	/**
	 * [doStoreFirst description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	// public function doStoreFirst($request, $datetime)
	// {
	// 	return (new MyProfileDataManagerLogic())->storeFirstSave($request, $datetime);
	// }

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new MyProfileDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow()
	{
		return (new MyProfileQuery())->showQuery();
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	// public function doDelete($id, $datetime)
	// {
	// 	return (new MyProfileDataManagerLogic())->delete($id, $datetime);
	// }

	/**
	 * [myProfile description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	// public function myProfile()
	// {
	// 	return (new MyProfileQuery())->myProfileQuery();
	// }
}
