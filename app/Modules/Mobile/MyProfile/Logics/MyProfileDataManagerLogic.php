<?php

namespace App\Modules\Mobile\MyProfile\Logics;

use App\Modules\Mobile\MyProfile\Queries\MyProfileField;
use App\Modules\Mobile\MyProfile\Queries\MyProfileQuery;
use App\Modules\Mobile\MyProfile\Queries\MyProfileDataManagerQuery;

class MyProfileDataManagerLogic extends MyProfileDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new MyProfileField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new MyProfileQuery())->setEntity());
	}

	/**
	 * untuk data register pertama kali tanpa adanya token login
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function storeFirstSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
			$datarequest = (new MyProfileField())->setField($request);
			return $this->storeDataSaveFirst($datetime, $datarequest, (new MyProfileQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
			$datarequest = (new MyProfileField())->setField($request);
			return $this->updateDataSave($id, $datetime, $datarequest, (new MyProfileQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
		return $this->deleteData($id, $datetime, $datarequest, (new MyProfileQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new MyProfileQuery())->setEntity());
	}
}
