<?php

namespace App\Modules\BackOffice\UserProfile\Queries;

use App\Models\UserProfile;

class UserProfileEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new UserProfile());
    }
}