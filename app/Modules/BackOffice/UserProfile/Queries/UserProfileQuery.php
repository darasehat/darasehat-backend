<?php

namespace App\Modules\BackOffice\UserProfile\Queries;

use App\Modules\BackOffice\UserProfile\Queries\UserProfileEntity;
use Auth;

class UserProfileQuery extends UserProfileEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('usrprf_name AS layFirstName','usrprf_home_address AS layHomeAddress','usrprf_phone_no AS layPhoneNo','adm1_name AS layAdm1','adm2_name AS layAdm2','adm3_name AS layAdm3','adm4_name AS layAdm4','usrprf_pic AS layPic','usrprf_date AS layDate','gnd_name AS layGnd','email AS layEmail','usr_id AS layUser')
        ->join('users','users.id','=','user_profiles.usr_id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','user_profiles.adm1_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','user_profiles.adm2_id')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','user_profiles.adm3_id')
        ->leftJoin('administrative_area_fourths','administrative_area_fourths.adm4_id','=','user_profiles.adm4_id')
        ->leftJoin('genders','genders.gnd_id','=','user_profiles.gnd_id')
        ->where('usrprf_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('usrprf_name AS layFirstName','usrprf_home_address AS layHomeAddress','usrprf_phone_no AS layPhoneNo','adm1_id AS layAdm1Id','adm2_id AS layAdm2Id','adm3_id AS layAdm3Id','adm4_id AS layAdm4Id','usrprf_pic AS layPic','usrprf_date AS layDate','gnd_id AS layGndId','email AS layEmail')
        ->join('users','users.id','=','user_profiles.usr_id')
        ->where('usrprf_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [myProfileQuery description]
     * @return [type] [description]
     */
    public function myProfileQuery()
    {
        $query = $this->newEntity()
        ->select('usrprf_name AS layFName','usrprf_home_address AS layAddress')
        ->where('usr_id','=',Auth::user()->id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('email AS layName', 'usrprf_id AS layCode')
        ->join('users','users.id','=','user_profiles.usr_id')
        ->where('user_profiles.is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [recordQuery description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('usrprf_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [userEmailQuery description]
     * @param  [type] $email  [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function userEmailQuery($email, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->join('users','users.id','=','user_profiles.usr_id')
        ->where('user_profiles.is_active','=','1')
        ->where('users.email','=',$email)
        ->first();
        return $query;
    }

    public function userIdQuery($userid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->join('users','users.id','=','user_profiles.usr_id')
        ->where('user_profiles.is_active','=','1')
        ->where('users.id','=',$userid)
        ->first();
        return $query;
    }
}