<?php

namespace App\Modules\BackOffice\UserProfile\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('usrprf_id AS layId','usrprf_name AS layName','usrprf_phone_no AS layPhoneNo','email AS layEmail')
        ->join('users','users.id','=','user_profiles.usr_id')
        ->where('user_profiles.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}