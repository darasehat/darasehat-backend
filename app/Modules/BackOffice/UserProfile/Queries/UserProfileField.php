<?php

namespace App\Modules\BackOffice\UserProfile\Queries;

use App\Queries\General\FieldMap;

class UserProfileField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layUsrId' => 'usr_id',
            'layName' => 'usrprf_name',
            'layHomeAddress' => 'usrprf_home_address',
            'layPhoneNo' => 'usrprf_phone_no',
            'layAdm1Id' => 'adm1_id',
            'layAdm2Id' => 'adm2_id',
            'layAdm3Id' => 'adm3_id',
            'layAdm4Id' => 'adm4_id',
            'layDate' => 'usrprf_date',
            'layTime' => 'usrprf_time',
            'layDatetime' => 'usrprf_datetime',
            'laySchool' => 'usrprf_school',
            'layWorkPlace' => 'usrprf_work_place',
            'layDateofbirth' => 'usrprf_dateofbirth'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}