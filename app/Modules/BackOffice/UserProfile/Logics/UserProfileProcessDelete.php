<?php

namespace App\Modules\BackOffice\UserProfile\Logics;

use App\Modules\BackOffice\UserProfile\Logics\UserProfileLogic;
use App\Modules\BackOffice\UserProfile\Queries\UserProfileQuery;
use App\Modules\BackOffice\User\Logics\UserLogic;

class UserProfileProcessDelete
{
	/**
	 * [doDelete description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($request, $datetime)
	{
		$userProfile = (new UserProfileLogic())->doShow($id);
		(new UserLogic())->doDelete($userProfile->layUser, $request, $datetime);
		return (new UserProfileLogic())->doDelete($request, $datetime);
	}
}
