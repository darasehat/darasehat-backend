<?php

namespace App\Modules\BackOffice\UserProfile\Logics;

use App\Modules\BackOffice\UserProfile\Logics\UserProfileDataManagerLogic;
use App\Modules\BackOffice\UserProfile\Logics\UserProfilePaginationLogic;
use App\Modules\BackOffice\UserProfile\Queries\UserProfileQuery;
// use App\Modules\BackOffice\User\Logics\UserLogic;

class UserProfileLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new UserProfilePaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new UserProfileDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new UserProfileQuery())->editQuery($id);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
    	// $request->request->add(['layDatetime' => $request->layDate.' '.$request->layTime]);
		return (new UserProfileDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new UserProfileQuery())->showQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		// $userProfile = $this->doShow($id);
		// (new UserLogic())->doDelete($userProfile->layUser, $request, $datetime);
		return (new UserProfileDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new UserProfileQuery())->selectListQuery();
	}

	/**
	 * [myProfile description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function myProfile()
	{
		return (new UserProfileQuery())->myProfileQuery();
	}

	/**
	 * [getRecord description]
	 * @param  [type] $id     [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new UserProfileQuery())->recordQuery($id, $select);
	}

	/**
	 * [getUserEmail description]
	 * @param  [type] $email [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getUserEmail($email, $select)
	{
		return (new UserProfileQuery())->userEmailQuery($email, $select);
	}

	public function getUserId($userid, $select)
	{
		return (new UserProfileQuery())->userIdQuery($userid, $select);
	}
}
