<?php

namespace App\Modules\BackOffice\UserProfile\Logics;

use App\Modules\BackOffice\UserProfile\Queries\UserProfileField;
use App\Modules\BackOffice\UserProfile\Queries\UserProfileQuery;
use App\Modules\BackOffice\UserProfile\Queries\UserProfileDataManagerQuery;

class UserProfileDataManagerLogic extends UserProfileDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new UserProfileField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new UserProfileQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new UserProfileField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new UserProfileQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new UserProfileField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new UserProfileQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new UserProfileQuery())->setEntity());
	}
}
