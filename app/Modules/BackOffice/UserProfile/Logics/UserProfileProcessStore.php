<?php

namespace App\Modules\BackOffice\UserProfile\Logics;

use App\Modules\BackOffice\UserProfile\Logics\UserProfileLogic;
use App\Modules\BackOffice\User\Logics\UserLogic;

class UserProfileProcessStore
{
	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStoreRandomPassword($request, $datetime)
	{
		$user = (new UserLogic())->doStoreRandomPassword($request, $datetime);
		$request->request->add(['layUsrId' => $user->id]);
		$userProfile = (new UserProfileLogic())->doStore($request, $datetime);
		return $userProfile;
	}
}
