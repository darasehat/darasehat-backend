<?php

namespace App\Modules\BackOffice\MedicineRecord\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordExportQuery;

// use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;

class MedicineRecordExport implements FromCollection, WithHeadings
{
    protected $request;

    public function __construct($request)
    {
        // $result = (new EmployeeLogic())->getLevelInternal($request);
        // $request = $result['request'];
        $this->request = $request;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
       return (new MedicineRecordExportQuery())->exportQuery($this->request);
        // foreach ($result as $key => $value) {
        //     $result['layBrand'] = $value->layBrand;
        //     $result['layStore'] = $value->layStore;
        //     $result['layName'] = $value->layName;
        //     $result['layAddress'] = $value->layAddress;
        //     $result['layPhone'] = $value->layPhone;
        //     $result['layRegDatetime'] = $value->layRegDatetime;
        //     $result['layEmail'] = $value->layEmail;
        //     $result['layGender'] = $value->layGender;
        //     $result['layBirthday'] = $value->layBirthday;
        // }
        // return $result;
    }

    public function headings():array
    {
        $array = [];
        // $array = $this->setLevel($array, $this->request);
        array_push($array, "Nama");
        array_push($array, "Email");
        array_push($array, "Telepon");
        array_push($array, "Sekolah");
        array_push($array, "Tgl. Lahir");
        array_push($array, "Tgl Minum Obat");
        array_push($array, "Waktu Minum Obat");
        array_push($array, "Provinsi");
        array_push($array, "Kabupaten/Kota");
        array_push($array, "Kecamatan");
        array_push($array, "Kelurahan");
        return $array;
    }

    private function setLevel($array, $request)
    {
        if(!$request->layCmpId)
        {
            array_push($array, "Perusahaan");
            // $query = $query->where('gym_member_packages.cmp_id','=',$request->layCmpId);
        }
        if(!$request->layBrndId)
        {
            array_push($array, "Merek");
            // $query = $query->where('gym_member_packages.brnd_id','=',$request->layBrndId);            
        }
        if(!$request->layStoreId)
        {
            array_push($array, "Cabang");
            // $query = $query->where('gym_member_packages.store_id','=',$request->layStoreId);            
        }
        return $array;
    }
}
