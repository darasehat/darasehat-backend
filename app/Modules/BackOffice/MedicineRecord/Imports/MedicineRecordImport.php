<?php

namespace App\Modules\BackOffice\MedicineRecord\Imports;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordProcessStore;
use App\Modules\BackOffice\UserProfile\Logics\UserProfileProcessStore;


// use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordDataManagerLogic;
// use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordPaginationLogic;
// use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordQuery;

// use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
// use App\Modules\BackOffice\MedicineRecordPackage\Logics\MedicineRecordPackageLogic;

class MedicineRecordImport
{
    public function importData($file)
	{
		$datetime=date('Y-m-d H:i:s');
        
        /**
		 * validasi data
		 */

		$errarr = [];
        $errarr = $this->validateData($file);

		if($errarr)
		{
			return [
                'status' => 'error',
                'data' => $errarr
            ];
		}


		/**
		 * input data security profile
		 */

		if($errarr==null)
		{
            return $this->storeData($file, $datetime);
		}
	}

    private function validateData($file)
    {
		$handle = fopen($file, "r");
		$i = 0;                    
		$linecount = count(file($file));
		$errarr = [];

		while (($fileop = fgetcsv($handle, 4000, ";")) !== false)
		{
			if(!($i==0))
			{
				$nama = $fileop[0];
				$email = $fileop[1];
				$telepon = $fileop[2];
				$sekolah = ($fileop[3]);
				$tgl_lahir = ($fileop[4]);
				$tgl_minum_obat = ($fileop[5]);
				$waktu_minum_obat = ($fileop[6]);
				$provinsi = $fileop[7];
				$kabupaten = $fileop[8];
				$kecamatan = $fileop[9];
				$kelurahan = $fileop[10];

				/**
				 * validasi format email
				 */
                $validator = Validator::make(['email' => $email], [
                    'email' => [
                        'email'
                    ],
                    [
                        'email.email' => 'Harus berformat email '.$i
                    ]
                ]);
                if(count($validator->messages()) > 1)
                {
                    $errarr[$i]['email'] = $validator->messages();
                }
                
				/**
				 * validasi format tgl lahir
				 */
                $validator = Validator::make(['tgl_lahir' => $tgl_lahir], [
                    'tgl_lahir' => [
                        'date_format:YYYY-MM-DD'
                    ],
                    [
                        'tgl_lahir.date_format' => 'Format tgl lahir harus tahun-bulan-hari (1990-07-30)  '.$i
                    ]
                ]);
                if(count($validator->messages()) > 1)
                {
                    $errarr[$i]['tgl_lahir'] = $validator->messages();
                }

                /**
				 * validasi format tgl minum obat
				 */
                $validator = Validator::make(['tgl_minum_obat' => $tgl_minum_obat], [
                    'tgl_minum_obat' => [
                        'date_format:YYYY-MM-DD'
                    ],
                    [
                        'tgl_minum_obat.date_format' => 'Format tgl lahir harus tahun-bulan-hari (1990-07-30) '.$i
                    ]
                ]);
                if(count($validator->messages()) > 1)
                {
                    $errarr[$i]['tgl_minum_obat'] = $validator->messages();
                }

                /**
                 * 
                 */

                /**
				 * agama
				 */
				// if($agama)
				// {
				// 	$query = (new ReligionLogic())->getRecord(['status' => 'active', 'relig_code' => $agama],['relig_id']);
				// 	$religid = null;
				// 	if($query == null)
				// 	{
				// 		$errarr[$i]['agama'] = 'No. '.$i.' kode agama: '.$agama.' salah';
				// 	}
				// 	else
				// 	{
				// 		$religid = $query->relig_id;										
				// 	}
				// }
			}
			$i++;
		}
		fclose($handle);
        return $errarr;
    }

    private function storeData($file, $datetime)
    {
        $handle = fopen($file, "r");
        $i = 0;
		$linecount = count(file($file));

        while (($fileop = fgetcsv($handle, 4000, ";")) !== false) 
        {
            if(!($i==0))
            {
				$nama = $fileop[0];
				$email = $fileop[1];
				$telepon = $fileop[2];
				$sekolah = ($fileop[3]);
				$tgl_lahir = ($fileop[4]);
				$tgl_minum_obat = ($fileop[5]);
				$waktu_minum_obat = ($fileop[6]);
				$provinsi = $fileop[7];
				$kabupaten = $fileop[8];
				$kecamatan = $fileop[9];
				$kelurahan = $fileop[10];

                /**
                 * input data user
                 */
                $userequest = (new Request());
                $userequest->request->add(['layName' => $nama]);
                $userequest->request->add(['email' => $email]);
                $userequest->request->add(['layPhoneNo' => $telepon]);
                $userequest->request->add(['laySchool' => $sekolah]);
                $userequest->request->add(['layDateofbirth' => $tgl_lahir]);
                $userequest->request->add(['layDate' => $tgl_minum_obat]);
                $userequest->request->add(['layTime' => $waktu_minum_obat]);
                $userequest->request->add(['layAdm1Name' => $provinsi]);
                $userequest->request->add(['layAdm2Name' => $kabupaten]);
                $userequest->request->add(['layAdm3Name' => $kecamatan]);
                $userequest->request->add(['layAdm4Name' => $kelurahan]);
                $userequest->request = $userequest->query;

                // $request->request->add(['layUsrprfId' => $userProfile->usrprf_id]);

                /**
                 * cek jika user ada maka data langsung masuk
                 */

                /**
                 * jika tidak ada email
                 *  maka match dengan nomor telepon
                 *  maka buat user baru gunakan email dan anonym@gerakjemari.com
                 */

                /**
                 * jika telepon sudah ada tidak perlu update
                 */

                 /**
                  * jika tgl lahir sudah ada tidak perlu update
                  */
                // (new MedicineRecordProcessStore())->doStore($request, $datetime);
                // (new UserProfileProcessStore())->doStore($request, $datetime);
            }
            $i++;
        }
        fclose($handle);
        return ['status' => 'success', 'data' => $i-1];
    }
}
