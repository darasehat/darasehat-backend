<?php

namespace App\Modules\BackOffice\MedicineRecord\Queries;

use App\Models\MedicineRecord;

class MedicineRecordEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new MedicineRecord());
    }
}