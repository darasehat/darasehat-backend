<?php

namespace App\Modules\BackOffice\MedicineRecord\Queries;

use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordEntity;

class MedicineRecordExportQuery extends MedicineRecordEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    public function exportQuery($request)
    {
        $select = $this->setSelect($request);
        $query = $this->setEntity()->select($select)
        ->leftJoin('users','users.id','=','medicine_records.usr_id')
        ->leftJoin('user_profiles','user_profiles.usrprf_id','=','medicine_records.usrprf_id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','user_profiles.adm1_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','user_profiles.adm2_id')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','user_profiles.adm3_id')
        ->leftJoin('administrative_area_fourths','administrative_area_fourths.adm4_id','=','user_profiles.adm4_id')
        ->where('medicine_records.is_active','=',1);
        // $query = $this->setLevel($query, $request);
        $query = $this->processSearch($query, $request->search);
        return $query->get();
    }

    /**
     * 
     */
    private function setSelect($request)
    {
        $select = [];
        // if((!$request->layCmpId) )
        // {
        //     array_push($select, 'cmp_name AS layCompany');
        // }
        // if((!$request->layBrndId) )
        // {
        //     array_push($select, 'brnd_name AS layBrand');
        // }
        // if((!$request->layStoreId) )
        // {
        //     array_push($select, 'store_name AS layStore');
        // }
        array_push($select, 'usrprf_name AS layName');
        array_push($select, 'email AS layEmail');
        array_push($select, 'usrprf_phone_no AS layPhone');
        array_push($select, 'usrprf_school AS laySchool');
        array_push($select, 'usrprf_dateofbirth AS layDateofbirth');
        array_push($select, 'medrec_date AS layDate');
        array_push($select, 'medrec_time AS layTime');
        array_push($select, 'adm1_name AS layAdm1');
        array_push($select, 'adm2_name AS layAdm2');
        array_push($select, 'adm3_name AS layAdm3');
        array_push($select, 'adm4_name AS layAdm4');
        return $select;
    }

    /**
     * 
     */
    private function setLevel($query, $request)
    {
        if($request->layCmpId)
        {
            $query = $query->where('medicine_records.cmp_id','=',$request->layCmpId);
        }
        if($request->layBrndId)
        {
            $query = $query->where('medicine_records.brnd_id','=',$request->layBrndId);            
        }
        if($request->layStoreId)
        {
            $query = $query->where('medicine_records.store_id','=',$request->layStoreId);            
        }

        $searchParam = json_decode($request->search);
        if (!empty($searchParam->layCmp))
        {
            $query = $query->where('medicine_records.cmp_id','=',$searchParam->layCmp);
        }
        if (!empty($searchParam->layBrnd))
        {
            $query = $query->where('medicine_records.brnd_id','=',$searchParam->layBrnd);
        }
        if (!empty($searchParam->layStore))
        {
            $query = $query->where('medicine_records.store_id','=',$searchParam->layStore);
        }
        return $query;
    }
    
    private function processSearch($query, $search)
    {
        $searchParam = json_decode($search);
        $query->where(function ($queryGroup) use ($searchParam) {
            if (!empty($searchParam->layName)) {
                $queryGroup = $queryGroup->where('usrprf_name','like',"%{$searchParam->layName}%");
            }
            if (!empty($searchParam->layEmail)) {
                $queryGroup = $queryGroup->where('users.email','like',"%{$searchParam->layEmail}%");
            }
            if (!empty($searchParam->layPhone)) {
                $queryGroup = $queryGroup->where('usrprf_phone_no','like',"%{$searchParam->layPhone}%");
            }
        });
        return $query;
    }
}