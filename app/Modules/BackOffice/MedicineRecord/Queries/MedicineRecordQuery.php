<?php

namespace App\Modules\BackOffice\MedicineRecord\Queries;

use DB;
use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordEntity;

class MedicineRecordQuery extends MedicineRecordEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * 
     */
    private function setLevel($query, $request)
    {
        if($request->layCmpId)
        {
            $query = $query->where('medicine_records.cmp_id','=',$request->layCmpId);
        }
        if($request->layBrndId)
        {
            $query = $query->where('medicine_records.brnd_id','=',$request->layBrndId);            
        }
        if($request->layStoreId)
        {
            $query = $query->where('medicine_records.store_id','=',$request->layStoreId);            
        }
        return $query;
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5, $request)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage, $request);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select(
            'medrec_id AS layId',
            'usrprf_name AS layName',
            'usrprf_home_address AS layAddress',
            'usrprf_phone_no AS layPhone',
            'medrec_datetime AS layDatetime',
            'email AS layEmail',
            'medicine_records.createdby','medicine_records.createdtime AS layCreatedtime',
            'medicine_records.changedby','medicine_records.changedtime AS layChangedtime'
        )
        ->leftJoin('users','users.id','=','medicine_records.usr_id')
        ->leftJoin('user_profiles','user_profiles.usrprf_id','=','medicine_records.usrprf_id')
        ->where('medicine_records.is_active','=',1)
        ->where('medicine_records.medrec_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select(
            'medrec_id AS layId',
            'medrec_date AS layDate',
            'medrec_time AS layTime',
            'email AS layEmail',
        )
        ->leftJoin('users','users.id','=','medicine_records.usr_id')
        ->where('medicine_records.is_active','=',1)
        ->where('medicine_records.medrec_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [recordQuery description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('medrec_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [memberQuery description]
     * @param  [type] $companyid [description]
     * @param  [type] $brandid   [description]
     * @param  [type] $profileid [description]
     * @param  [type] $select    [description]
     * @return [type]            [description]
     */
    public function memberQuery($companyid, $brandid, $profileid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('cmp_id','=',$companyid)
        ->where('brnd_id','=',$brandid)
        ->where('usrprf_id','=',$profileid)
        ->first();
        return $query;
    }

    /**
     * [userProfileQuery description]
     * @param  [type] $usrprfid [description]
     * @param  [type] $select   [description]
     * @return [type]           [description]
     */
    public function userProfileQuery($usrprfid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('usrprf_id','=',$usrprfid)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function expiredPackageMembersQuery($date, $select)
    {
        $query = $this->setEntity()
        ->select($select)        
        ->where('is_active','=','1')
        ->where('medrec_en_pkg_date','<=',$date)
        ->orWhereNull('medrec_en_pkg_date')
        ->get();
        return $query;
    }
}