<?php

namespace App\Modules\BackOffice\MedicineRecord\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5, $request)
    {
        $select = $this->setSelect($request);
        $query = $entity->select($select)
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','medicine_records.adm1_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','medicine_records.adm2_id')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','medicine_records.adm3_id')
        ->leftJoin('administrative_area_fourths','administrative_area_fourths.adm4_id','=','medicine_records.adm4_id')
        ->leftJoin('user_profiles','user_profiles.usrprf_id','=','medicine_records.usrprf_id')
        ->leftJoin('users','users.id','=','user_profiles.usr_id')
        ->where('medicine_records.is_active','=',1);
        // $query = $this->setLevel($query, $request);
        $query = $this->processSearch($query, $request->search);
        // $query = $this->processSearchActivePackage($query, $request->search);
        return $query->paginate($rowsPerPage);
    }

    /**
     * 
     */
    private function setSelect($request)
    {
        $select = [
            'medicine_records.medrec_id AS layId',
            'usrprf_name AS layName',
            'usrprf_phone_no AS layPhone',
            'medrec_datetime AS layRegDate',
            'email AS layEmail',
            'adm1_name AS layAdm1',
            'adm2_name AS layAdm2',
            'adm3_name AS layAdm3',
            'adm4_name AS layAdm4',
        ];
        // if((!$request->layCmpId) )
        // {
        //     array_push($select, 'cmp_name AS layCompany');
        // }
        // if((!$request->layBrndId) )
        // {
        //     array_push($select, 'brnd_name AS layBrand');
        // }
        // if((!$request->layStoreId) )
        // {
        //     array_push($select, 'store_name AS layStore');
        // }
        return $select;
    }

    /**
     * 
     */
    private function setLevel($query, $request)
    {
        // if($request->layCmpId)
        // {
        //     $query = $query->where('medicine_records.cmp_id','=',$request->layCmpId);
        // }
        // if($request->layBrndId)
        // {
        //     $query = $query->where('medicine_records.brnd_id','=',$request->layBrndId);            
        // }
        // if($request->layStoreId)
        // {
        //     $query = $query->where('medicine_records.store_id','=',$request->layStoreId);            
        // }

        $searchParam = json_decode($request->search);
        // if (!empty($searchParam->layName))
        // {
        //     $query = $query->where('user_profiles.usrprf_name','=',$searchParam->layName);
        // }
        // if (!empty($searchParam->layEmail))
        // {
        //     $query = $query->where('users.email','=',$searchParam->layEmail);
        // }
        // if (!empty($searchParam->layPhone))
        // {
        //     $query = $query->where('user_profiles.usrprf_phone_no','=',$searchParam->layPhone);
        // }
        return $query;
    }

    private function processSearch($query, $search)
    {
        $searchParam = json_decode($search);
        $query->where(function ($queryGroup) use ($searchParam) {
            if (!empty($searchParam->layName)) {
                $queryGroup = $queryGroup->where('usrprf_name','like',"%{$searchParam->layName}%");
            }
            if (!empty($searchParam->layEmail)) {
                $queryGroup = $queryGroup->where('users.email','like',"%{$searchParam->layEmail}%");
            }
            if (!empty($searchParam->layPhone)) {
                $queryGroup = $queryGroup->where('usrprf_phone_no','like',"%{$searchParam->layPhone}%");
            }
        });
        return $query;
    }

    // private function processSearchActivePackage($query, $search)
    // {
    //     $date=date('Y-m-d');
    //     $searchParam = json_decode($search);
    //     if (!empty($searchParam->layPackageStatus)) {
    //         $query = $query->leftJoin('gym_member_packages','gym_member_packages.gymbr_id','=','medicine_records.gymbr_id');
    //         if($searchParam->layPackageStatus == 'active')
    //         {
    //             $query = $query->where('gymmbrpkg_en_date','>=',$date);
    //         }
    //         if($searchParam->layPackageStatus == 'inactive')
    //         {
    //             $query = $query->where('gymmbrpkg_en_date','<',$date);
    //         }
    //         $query = $query->distinct('medicine_records.gymbr_id');
    //     }
    //     return $query;
    // }

}