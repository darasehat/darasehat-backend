<?php

namespace App\Modules\BackOffice\MedicineRecord\Logics;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordDataManagerLogic;
use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordPaginationLogic;
use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordQuery;

// use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\MedicineRecordPackage\Logics\MedicineRecordPackageLogic;

class MedicineRecordLogic
{
    /**
     * [getPaginationData description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    public function getPaginationData($request)
    {
		// $result = (new EmployeeLogic())->getLevelInternal($request);
        // $request = $result['request'];
        return (new MedicineRecordPaginationLogic())->getPaginationData($request);
    }

    /**
     * [doStore description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doStore($request, $datetime)
    {
        return (new MedicineRecordDataManagerLogic())->storeSave($request, $datetime);
    }

    /**
     * [doUpdate description]
     * @param  [type] $id       [description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doUpdate($id, $request, $datetime)
    {
        return (new MedicineRecordDataManagerLogic())->updateSave($id, $request, $datetime);
    }

    /**
     * 
     */
    public function doUpdateSystem($id, $request, $datetime)
    {
        return (new MedicineRecordDataManagerLogic())->updateSaveSystem($id, $request, $datetime);
    }

    /**
     * [doShow description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function doShow($id)
    {
        $show = (new MedicineRecordQuery())->showQuery($id);
        $result = [
            'layName' => $show->layName,
            'layAddress' => $show->layAddress,
            'layPhone' => $show->layPhone,
            'layDatetime' => $show->layDatetime,
            'layEmail' => $show->layEmail,
            'layCreatedtime' => $show->layCreatedtime,
            'layChangedtime' => $show->layChangedtime,
            'layCreatedby' => $show->createdBy->name
        ];
        if($show->changedBy)
        {
            $result['layChangedby'] = $show->changedBy->name;
        }
        return $result;
    }

    /**
     * [doEdit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function doEdit($id)
    {
        $result = (new MedicineRecordQuery())->editQuery($id);
        return $result;
    }

    /**
     * [doDelete description]
     * @param  [type] $id       [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doDelete($id, $request, $datetime)
    {
        return (new MedicineRecordDataManagerLogic())->delete($id, $request, $datetime);
    }

    /**
     * [getRecord description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function getRecord($id, $select)
    {
        return (new MedicineRecordQuery())->recordQuery($id, $select);
    }
}
