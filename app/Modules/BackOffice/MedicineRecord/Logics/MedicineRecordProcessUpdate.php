<?php

namespace App\Modules\BackOffice\MedicineRecord\Logics;

use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordLogic;

class MedicineRecordProcessUpdate
{
    public function doUpdate($id, $request, $datetime)
    {
        //datetime
		$request->request->add(['layDatetime' => $request->layDate.' '.$request->layTime]);
        //usr_id
		// $request->request->add(['layUsrId' => $request->layEmail]);
        //usrprf_id
        // $userProfile = (new UserProfileLogic())->getUserId($request->layEmail, ['usrprf_id','adm1_id','adm2_id','adm3_id','adm4_id']);
		// $request->request->add(['layUsrprfId' => $userProfile->usrprf_id]);
        // //adm1
		// $request->request->add(['layAdm1Id' => $userProfile->adm1_id]);
        // //adm2
		// $request->request->add(['layAdm2Id' => $userProfile->adm2_id]);
        // //adm3
		// $request->request->add(['layAdm3Id' => $userProfile->adm3_id]);
        // //adm4
		// $request->request->add(['layAdm4Id' => $userProfile->adm4_id]);

        $medicineRecord = (new MedicineRecordLogic())->doUpdate($id, $request, $datetime);
        return true;
    }
}
