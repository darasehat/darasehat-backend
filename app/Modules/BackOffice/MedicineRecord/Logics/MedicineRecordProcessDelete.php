<?php

namespace App\Modules\BackOffice\MedicineRecord\Logics;

// use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordLogic;
// use App\Modules\BackOffice\Layanesia\UserProfile\Logics\UserProfileLogic;
use App\Modules\BackOffice\Membership\Member\Logics\MemberLogic;
use App\Modules\BackOffice\User\Logics\UserLogic;

class MedicineRecordProcessDelete
{
	public function doDelete($id, $request, $datetime)
	{
		$result = (new MedicineRecordLogic())->doDelete($id, $request, $datetime);
		return true;
	}
}
