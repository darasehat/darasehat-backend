<?php

namespace App\Modules\BackOffice\MedicineRecord\Logics;
use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordQuery;

class MedicineRecordPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new MedicineRecordQuery())->getPaginationQuery($rowsPerPage, $request);
    	return $result;
	}
}
