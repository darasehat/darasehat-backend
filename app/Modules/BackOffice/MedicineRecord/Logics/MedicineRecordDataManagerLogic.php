<?php

namespace App\Modules\BackOffice\MedicineRecord\Logics;

use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordField;
use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordQuery;
use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordDataManagerQuery;

class MedicineRecordDataManagerLogic extends MedicineRecordDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new MedicineRecordField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new MedicineRecordQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
		$request->request->add(['is_active' => 1]);
		$datarequest = (new MedicineRecordField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new MedicineRecordQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
		$request->request->add(['is_active' => 0]);
		$datarequest = (new MedicineRecordField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new MedicineRecordQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new MedicineRecordQuery())->setEntity());
	}
}
