<?php

namespace App\Modules\BackOffice\MedicineRecord\Logics;

use App\Modules\BackOffice\MedicineRecord\Queries\MedicineRecordDashboardQuery;

// use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;

class MedicineRecordDashboardLogic
{
	/**
	 * [getTotal description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getTotal($request)
	{
		// $level = (new EmployeeLogic())->getLevelInternal($request);
		// $request = $level['request'];
		return (new MedicineRecordDashboardQuery())->getTotalQuery($request);
	}

	/**
	 * 
	 */
	public function getToday($date, $request)
	{
		// $level = (new EmployeeLogic())->getLevelInternal($request);
		// $request = $level['request'];
		return (new MedicineRecordDashboardQuery())->getTodayQuery($date, $request);
	}

	/**
	 * 
	 */
	public function getBirthday($request)
	{
		// $level = (new EmployeeLogic())->getLevelInternal($request);
		// $request = $level['request'];
		return (new MedicineRecordDashboardQuery())->getBirthdayQuery($request);
	}
}
