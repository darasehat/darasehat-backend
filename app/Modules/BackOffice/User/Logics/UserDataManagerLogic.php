<?php

namespace App\Modules\BackOffice\User\Logics;

use Auth;
use App\Modules\BackOffice\User\Queries\UserField;
use App\Modules\BackOffice\User\Queries\UserQuery;
use App\Modules\BackOffice\User\Queries\UserDataManagerQuery;

class UserDataManagerLogic extends UserDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
			$datarequest = (new UserField())->setField($request);
			return $this->storeDataSave($datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * 
	 */
	public function storeSaveRandomPassword($request, $datetime)
	{
			$request->request->add(['is_active' => 1]);
			$datarequest = (new UserField())->setFieldRandomPassword($request);
			return $this->storeDataSave($datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
		$datarequest = (new UserField())->setFieldData($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * [updatePasswordSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updatePasswordSave($id, $request, $datetime)
	{
        $datarequest = (new UserField())->setFieldPassword($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new UserField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new UserQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new UserQuery())->setEntity());
	}
}
