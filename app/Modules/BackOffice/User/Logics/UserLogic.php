<?php

namespace App\Modules\BackOffice\User\Logics;

use App\Modules\BackOffice\User\Logics\UserDataManagerLogic;
use App\Modules\BackOffice\User\Logics\UserPaginationLogic;
use App\Modules\BackOffice\User\Queries\UserQuery;
use Str;

class UserLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new UserPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$request->request->add(['layToken' => Str::random(255)]);
		return (new UserDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * 
	 */
	public function doStoreRandomPassword($request, $datetime)
	{
		$request->request->add(['layToken' => Str::random(255)]);
		return (new UserDataManagerLogic())->storeSaveRandomPassword($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new UserDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doUpdatePassword description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdatePassword($id, $request, $datetime)
	{
		return (new UserDataManagerLogic())->updatePasswordSave($id, $request, $datetime);
	}
	
	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new UserQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new UserQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new UserDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doAutoCompleteSelectList description]
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function doAutoCompleteSelectList($query)
	{
		return (new UserQuery())->autoCompleteSelectListQuery($query);
	}

	/**
	 * [getUserEmail description]
	 * @param  [type] $email  [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getUserEmail($email, $select)
	{
		return (new UserQuery)->userEmailQuery($email, $select);
	}

	/**
	 * 
	 */
	public function getRecord($id, $select)
	{
		return (new UserQuery())->recordQuery($id, $select);
	}
}
