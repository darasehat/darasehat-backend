<?php

namespace App\Modules\BackOffice\User\Logics;
use App\Modules\BackOffice\User\Queries\UserQuery;

class UserPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new UserQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
