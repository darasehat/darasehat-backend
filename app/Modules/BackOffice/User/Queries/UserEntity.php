<?php

namespace App\Modules\BackOffice\User\Queries;

use App\Models\UserPassport;

class UserEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new UserPassport());
    }
}