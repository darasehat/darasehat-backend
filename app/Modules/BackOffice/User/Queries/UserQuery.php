<?php

namespace App\Modules\BackOffice\User\Queries;

use App\Modules\BackOffice\User\Queries\UserEntity;
use DB;

class UserQuery extends UserEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName', 'email AS layEmail')
        ->where('id','=',$id)
        ->where('is_active','=',1)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName', 'email AS layEmail', 'is_active AS active')
        ->where('id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [autoCompleteSelectListQuery description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function autoCompleteSelectListQuery($query)
    {
        $query = $this->setEntity()
        ->select(DB::raw("id AS layCode, CONCAT(email,'-',usrprf_name) AS layName"))
        // ->select('email AS layName', 'id AS layCode')
        ->leftJoin('user_profiles','user_profiles.usr_id','=','users.id')
        ->where('users.is_active','=','1')
        ->where('email','like','%'.$query.'%')
        ->get();
        return $query;
    }

    /**
     * [userEmailQuery description]
     * @param  [type] $query  [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function userEmailQuery($query, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        // ->where('users.is_active','=','1')
        ->where('email','=',$query)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('id','=',$id)
        ->first();
        return $query;
    }
}