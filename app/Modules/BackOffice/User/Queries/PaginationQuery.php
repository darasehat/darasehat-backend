<?php

namespace App\Modules\BackOffice\User\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('id AS layId','name AS layName','email AS layEmail')
        ->where('is_active','=',1)
        ->paginate($rowsPerPage);
    }
}