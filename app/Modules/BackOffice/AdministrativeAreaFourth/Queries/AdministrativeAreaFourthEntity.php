<?php

namespace App\Modules\BackOffice\AdministrativeAreaFourth\Queries;

use App\Models\AdministrativeAreaFourth;

class AdministrativeAreaFourthEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new AdministrativeAreaFourth());
    }
}