<?php

namespace App\Modules\BackOffice\AdministrativeAreaFourth\Queries;

use App\Modules\BackOffice\AdministrativeAreaFourth\Queries\AdministrativeAreaFourthEntity;

class AdministrativeAreaFourthQuery extends AdministrativeAreaFourthEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('adm4_name AS layName', 'adm1_name AS layProvince', 'adm2_name AS layCity', 'adm3_name AS layCamat')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','administrative_area_fourths.adm3_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','administrative_area_thirds.adm2_id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','administrative_area_seconds.adm1_id')
        ->where('adm4_id','=',$id)
        ->first();
        return $query;
    }


    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery($adm3)
    {
        $query = $this->setEntity()
        ->select('adm4_name AS layName', 'adm4_id AS layCode')
        ->where('is_active','=','1')
        ->where('adm3_id','=',$adm3)
        ->orderBy('adm4_name')
        ->get();
        return $query;
    }
}