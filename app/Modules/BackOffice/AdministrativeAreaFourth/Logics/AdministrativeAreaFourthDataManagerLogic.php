<?php

namespace App\Modules\BackOffice\AdministrativeAreaFourth\Logics;

use App\Modules\BackOffice\AdministrativeAreaFourth\Queries\AdministrativeAreaFourthField;
use App\Modules\BackOffice\AdministrativeAreaFourth\Queries\AdministrativeAreaFourthQuery;
use App\Modules\BackOffice\AdministrativeAreaFourth\Queries\AdministrativeAreaFourthDataManagerQuery;

class AdministrativeAreaFourthDataManagerLogic extends AdministrativeAreaFourthDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new AdministrativeAreaFourthField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new AdministrativeAreaFourthQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new AdministrativeAreaFourthField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new AdministrativeAreaFourthQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new AdministrativeAreaFourthField())->setField($request);
		return $this->DeleteDataSave($id, $datetime, $datarequest, (new AdministrativeAreaFourthQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new AdministrativeAreaFourthQuery())->setEntity());
	}
}
