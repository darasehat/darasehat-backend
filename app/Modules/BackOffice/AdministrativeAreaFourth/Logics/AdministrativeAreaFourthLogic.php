<?php

namespace App\Modules\BackOffice\AdministrativeAreaFourth\Logics;

use App\Modules\BackOffice\AdministrativeAreaFourth\Logics\AdministrativeAreaFourthDataManagerLogic;
use App\Modules\BackOffice\AdministrativeAreaFourth\Logics\AdministrativeAreaFourthPaginationLogic;
use App\Modules\BackOffice\AdministrativeAreaFourth\Queries\AdministrativeAreaFourthQuery;

class AdministrativeAreaFourthLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new AdministrativeAreaFourthPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new AdministrativeAreaFourthDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new AdministrativeAreaFourthDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new AdministrativeAreaFourthQuery())->showQuery($id);		
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new AdministrativeAreaFourthDataManagerLogic())->delete($id, $request, $datetime);
	}


	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList($adm3)
	{
		return (new AdministrativeAreaFourthQuery())->selectListQuery($adm3);
	}
}
