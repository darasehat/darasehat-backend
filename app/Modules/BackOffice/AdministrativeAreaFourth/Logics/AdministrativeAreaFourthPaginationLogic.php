<?php

namespace App\Modules\BackOffice\AdministrativeAreaFourth\Logics;
use App\Modules\BackOffice\AdministrativeAreaFourth\Queries\AdministrativeAreaFourthQuery;

class AdministrativeAreaFourthPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new AdministrativeAreaFourthQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
