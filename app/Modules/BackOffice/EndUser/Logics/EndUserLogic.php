<?php

namespace App\Modules\BackOffice\EndUser\Logics;

use App\Modules\BackOffice\EndUser\Logics\EndUserDataManagerLogic;
use App\Modules\BackOffice\EndUser\Logics\EndUserPaginationLogic;
use App\Modules\BackOffice\EndUser\Queries\EndUserQuery;
use Str;

class EndUserLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new EndUserPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$request->request->add(['layToken' => Str::random(255)]);
		return (new EndUserDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * 
	 */
	public function doStoreRandomPassword($request, $datetime)
	{
		$request->request->add(['layToken' => Str::random(255)]);
		return (new EndUserDataManagerLogic())->storeSaveRandomPassword($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new EndUserDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doUpdatePassword description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdatePassword($id, $request, $datetime)
	{
		return (new EndUserDataManagerLogic())->updatePasswordSave($id, $request, $datetime);
	}
	
	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new EndUserQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new EndUserQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new EndUserDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doAutoCompleteSelectList description]
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function doAutoCompleteSelectList($query)
	{
		return (new EndUserQuery())->autoCompleteSelectListQuery($query);
	}

	/**
	 * [getEndUserEmail description]
	 * @param  [type] $email  [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getEndUserEmail($email, $select)
	{
		return (new EndUserQuery)->EnduserEmailQuery($email, $select);
	}

	/**
	 * 
	 */
	public function getRecord($id, $select)
	{
		return (new EndUserQuery())->recordQuery($id, $select);
	}
}
