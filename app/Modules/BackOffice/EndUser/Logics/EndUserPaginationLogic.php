<?php

namespace App\Modules\BackOffice\EndUser\Logics;
use App\Modules\BackOffice\EndUser\Queries\EndUserQuery;

class EndUserPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new EndUserQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
