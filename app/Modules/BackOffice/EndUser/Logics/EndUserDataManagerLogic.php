<?php

namespace App\Modules\BackOffice\EndUser\Logics;

use Auth;
use App\Modules\BackOffice\EndUser\Queries\EndUserField;
use App\Modules\BackOffice\EndUser\Queries\EndUserQuery;
use App\Modules\BackOffice\EndUser\Queries\EndUserDataManagerQuery;

class EndUserDataManagerLogic extends EndUserDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
			$datarequest = (new EndUserField())->setField($request);
			return $this->storeDataSave($datetime, $datarequest, (new EndUserQuery())->setEntity());
	}

	/**
	 * 
	 */
	public function storeSaveRandomPassword($request, $datetime)
	{
			$request->request->add(['is_active' => 1]);
			$datarequest = (new EndUserField())->setFieldRandomPassword($request);
			return $this->storeDataSave($datetime, $datarequest, (new EndUserQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
		$datarequest = (new EndUserField())->setFieldData($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new EndUserQuery())->setEntity());
	}

	/**
	 * [updatePasswordSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updatePasswordSave($id, $request, $datetime)
	{
        $datarequest = (new EndUserField())->setFieldPassword($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new EndUserQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new EndUserField())->setFieldData($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new EndUserQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new EndUserQuery())->setEntity());
	}
}
