<?php

namespace App\Modules\BackOffice\EndUser\Logics;

use App\Modules\BackOffice\EndUser\Logics\EndUserLogic;
use App\Modules\BackOffice\UserProfile\Logics\UserProfileLogic;

class EndUserProcessUpdate
{
    public function doUpdate($id, $request, $datetime)
    {
        $endUser = (new EndUserLogic())->doUpdate($id, $request, $datetime);
		$request->request->add(['layUsrId' => $endUser->id]);
        //search userprofile by user_id
        $userProfile = (new UserProfileLogic())->getUserId($endUser->id, ['usrprf_id']);
        if($userProfile)
        {
            (new UserProfileLogic())->doUpdate($userProfile->usrprf_id,$request, $datetime);
        }
        else
        {
            (new UserProfileLogic())->doStore($request, $datetime);
        }
        $endUser->assignRole('end_user');
        return true;
    }
}
