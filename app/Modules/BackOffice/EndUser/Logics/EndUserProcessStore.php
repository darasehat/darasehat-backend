<?php

namespace App\Modules\BackOffice\EndUser\Logics;

use App\Modules\BackOffice\EndUser\Logics\EndUserLogic;
use App\Modules\BackOffice\UserProfile\Logics\UserProfileLogic;

class EndUserProcessStore
{
    public function doStore($request, $datetime)
    {
        $endUser = (new EndUserLogic())->doStore($request, $datetime);
		$request->request->add(['layUsrId' => $endUser->id]);
        (new UserProfileLogic())->doStore($request, $datetime);
        $endUser->assignRole('end_user');
        return true;
    }
}
