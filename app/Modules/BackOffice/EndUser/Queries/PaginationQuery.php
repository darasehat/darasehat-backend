<?php

namespace App\Modules\BackOffice\EndUser\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->role('end_user')->select('id AS layId','name AS layName','email AS layEmail','usrprf_school AS laySchool','usrprf_phone_no AS layPhoneNo')
        ->leftJoin('user_profiles','user_profiles.usr_id','=','users.id')
        ->where('users.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}