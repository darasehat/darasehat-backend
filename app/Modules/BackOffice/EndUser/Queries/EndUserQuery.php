<?php

namespace App\Modules\BackOffice\EndUser\Queries;

use App\Modules\BackOffice\EndUser\Queries\EndUserEntity;
use DB;

class EndUserQuery extends EndUserEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select(
            'name AS layName', 
            'email AS layEmail',
            'usrprf_school AS laySchool',
            'usrprf_home_address AS layHomeAddress',
            'usrprf_phone_no AS layPhoneNo',
            'adm1_name AS layAdm1',
            'adm2_name AS layAdm2',
            'adm3_name AS layAdm3',
            'adm4_name AS layAdm4'
        )
        ->leftJoin('user_profiles','user_profiles.usr_id','=','users.id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','user_profiles.adm1_id')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','user_profiles.adm2_id')
        ->leftJoin('administrative_area_thirds','administrative_area_thirds.adm3_id','=','user_profiles.adm3_id')
        ->leftJoin('administrative_area_fourths','administrative_area_fourths.adm4_id','=','user_profiles.adm4_id')
        ->where('id','=',$id)
        ->where('users.is_active','=',1)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select(
            'name AS layName', 
            'email AS layEmail',
            'usrprf_school AS laySchool',
            'usrprf_home_address AS layHomeAddress',
            'usrprf_phone_no AS layPhoneNo',
            'adm1_id AS layAdm1Id',
            'adm2_id AS layAdm2Id',
            'adm3_id AS layAdm3Id',
            'adm4_id AS layAdm4Id'
        )
        ->leftJoin('user_profiles','user_profiles.usr_id','=','users.id')
        ->where('id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [autoCompleteSelectListQuery description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function autoCompleteSelectListQuery($query)
    {
        $query = $this->setEntity()
        ->select(DB::raw("id AS layCode, CONCAT(email,'-',usrprf_name) AS layName"))
        // ->select('email AS layName', 'id AS layCode')
        ->leftJoin('user_profiles','user_profiles.usr_id','=','users.id')
        ->where('users.is_active','=','1')
        ->where('email','like','%'.$query.'%')
        ->get();
        return $query;
    }

    /**
     * [userEmailQuery description]
     * @param  [type] $query  [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function userEmailQuery($query, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        // ->where('users.is_active','=','1')
        ->where('email','=',$query)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('id','=',$id)
        ->first();
        return $query;
    }
}