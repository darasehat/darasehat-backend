<?php

namespace App\Modules\BackOffice\EndUser\Queries;

use App\Models\UserMobile;

class EndUserEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new UserMobile());
    }
}