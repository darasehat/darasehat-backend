<?php

namespace App\Modules\BackOffice\AdministrativeAreaSecond\Queries;

use App\Modules\BackOffice\AdministrativeAreaSecond\Queries\AdministrativeAreaSecondEntity;

class AdministrativeAreaSecondQuery extends AdministrativeAreaSecondEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('adm2_name AS layName', 'adm1_name AS layAdmLevel1')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','administrative_area_seconds.adm1_id')
        ->where('adm2_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('adm2_name AS layName', 'adm1_id AS layAdm1Id')
        ->where('adm2_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery($adm1)
    {
        $query = $this->setEntity()
        ->select('adm2_name AS layName', 'adm2_id AS layCode')
        ->where('is_active','=','1')
        ->where('adm1_id','=',$adm1)
        ->orderBy('adm2_name')
        ->get();
        return $query;
    }
}