<?php

namespace App\Modules\BackOffice\AdministrativeAreaSecond\Queries;

use App\Models\AdministrativeAreaSecond;

class AdministrativeAreaSecondEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new AdministrativeAreaSecond());
    }
}