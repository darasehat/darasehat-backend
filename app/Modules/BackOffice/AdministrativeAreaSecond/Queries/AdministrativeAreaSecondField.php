<?php

namespace App\Modules\BackOffice\AdministrativeAreaSecond\Queries;

use App\Models\AdministrativeAreaSecond;
use App\Queries\General\FieldMap;

class AdministrativeAreaSecondField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layName' => 'adm2_name',
            'layAdm1Id' => 'adm1_id',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}