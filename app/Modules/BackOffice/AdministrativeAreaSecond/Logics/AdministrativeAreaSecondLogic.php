<?php

namespace App\Modules\BackOffice\AdministrativeAreaSecond\Logics;

use App\Modules\BackOffice\AdministrativeAreaSecond\Logics\AdministrativeAreaSecondDataManagerLogic;
use App\Modules\BackOffice\AdministrativeAreaSecond\Logics\AdministrativeAreaSecondPaginationLogic;
use App\Modules\BackOffice\AdministrativeAreaSecond\Queries\AdministrativeAreaSecondQuery;

class AdministrativeAreaSecondLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new AdministrativeAreaSecondPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new AdministrativeAreaSecondDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new AdministrativeAreaSecondQuery())->editQuery($id);		
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new AdministrativeAreaSecondDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new AdministrativeAreaSecondQuery())->showQuery($id);		
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new AdministrativeAreaSecondDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList($adm1)
	{
		return (new AdministrativeAreaSecondQuery())->selectListQuery($adm1);		
	}
}
