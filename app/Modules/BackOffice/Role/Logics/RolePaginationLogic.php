<?php

namespace App\Modules\BackOffice\Role\Logics;
use App\Modules\BackOffice\Role\Queries\RoleQuery;

class RolePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new RoleQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
