<?php

namespace App\Modules\BackOffice\Role\Logics;

use Auth;
use App\Modules\BackOffice\Role\Queries\RoleField;
use App\Modules\BackOffice\Role\Queries\RoleQuery;
use App\Modules\BackOffice\Role\Queries\RoleDataManagerQuery;

class RoleDataManagerLogic extends RoleDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new RoleField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new RoleQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
        $datarequest = (new RoleField())->setField($request);
		$entity = (new RoleQuery())->setEntity()->find($id);
		$entity->name = $datarequest['name'];
        return $entity->save();
		// return $this->updateDataSave($id, $datetime, $datarequest, (new RoleQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new RoleField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new RoleQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new RoleQuery())->setEntity());
	}
}
