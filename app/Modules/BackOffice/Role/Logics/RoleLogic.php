<?php

namespace App\Modules\BackOffice\Role\Logics;

use App\Modules\BackOffice\Role\Logics\RoleDataManagerLogic;
use App\Modules\BackOffice\Role\Logics\RolePaginationLogic;
use App\Modules\BackOffice\Role\Queries\RoleQuery;
use Spatie\Permission\Models\Role;

class RoleLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new RolePaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
        return Role::create(['name' => $request->layName]);                
		// return (new RoleDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new RoleDataManagerLogic())->updateSave($id, $request, $datetime);
	}
	
	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new RoleQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new RoleQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $datetime)
	{
		return (new RoleDataManagerLogic())->destroy($id);
	}

	/**
	 * 
	 */
	public function getGymSelectList()
	{
		return (new RoleQuery())->gymSelectListQuery();
	}

	/**
	 * 
	 */
	public function getRecord($id)
	{
		return (new RoleQuery())->recordQuery($id);
	}

	/**
	 * 
	 */
	public function getNameRecord($name)
	{
		return (new RoleQuery())->nameRecordQuery($name);
	}
}
