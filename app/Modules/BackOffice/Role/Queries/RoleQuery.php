<?php

namespace App\Modules\BackOffice\Role\Queries;

use App\Modules\BackOffice\Role\Queries\RoleEntity;

class RoleQuery extends RoleEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName')
        ->where('id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName')
        ->where('id','=',$id)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function gymSelectListQuery()
    {
        $query = $this->setEntity()
        ->select('name AS layName', 'id AS layCode')
        ->whereIn('name',['gym_admin','gym_operator'])
        ->get();
        return $query;
    }

    /**
     * 
     */
    public function recordQuery($id)
    {
        $query = $this->setEntity()
        ->select('name AS layName')
        ->where('id','=',$id)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function nameRecordQuery($name)
    {
        $query = $this->setEntity()
        ->select('id')
        ->where('name','=',$name)
        ->first();
        return $query;
    }
}