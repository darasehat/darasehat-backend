<?php

namespace App\Modules\BackOffice\Role\Queries;

use App\Queries\General\FieldMap;

class RoleField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'layName' => 'name',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);

        return $data;
    }
}