<?php

namespace App\Modules\BackOffice\Role\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('id AS layId','name AS layName')
        ->orderby('name', 'asc')
        ->paginate($rowsPerPage);
    }
}