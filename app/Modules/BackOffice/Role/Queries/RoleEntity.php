<?php

namespace App\Modules\BackOffice\Role\Queries;

use App\Models\Role;

class RoleEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Role());
    }
}