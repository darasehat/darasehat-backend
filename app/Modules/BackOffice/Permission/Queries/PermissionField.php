<?php

namespace App\Modules\BackOffice\Permission\Queries;

use App\Queries\General\FieldMap;

class PermissionField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'layName' => 'name',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);

        return $data;
    }
}