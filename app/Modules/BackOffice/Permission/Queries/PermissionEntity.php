<?php

namespace App\Modules\BackOffice\Permission\Queries;

use App\Models\Permission;

class PermissionEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Permission());
    }
}