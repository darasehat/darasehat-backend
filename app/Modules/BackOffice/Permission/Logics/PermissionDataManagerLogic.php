<?php

namespace App\Modules\BackOffice\Permission\Logics;

use Auth;
use App\Modules\BackOffice\Permission\Queries\PermissionField;
use App\Modules\BackOffice\Permission\Queries\PermissionQuery;
use App\Modules\BackOffice\Permission\Queries\PermissionDataManagerQuery;

class PermissionDataManagerLogic extends PermissionDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new PermissionField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new PermissionQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
        $datarequest = (new PermissionField())->setField($request);
		$entity = (new PermissionQuery())->setEntity()->find($id);
		$entity->name = $datarequest['name'];
        return $entity->save();
		// return $this->updateDataSave($id, $datetime, $datarequest, (new PermissionQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new PermissionField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new PermissionQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new PermissionQuery())->setEntity());
	}
}
