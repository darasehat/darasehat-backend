<?php

namespace App\Modules\BackOffice\Permission\Logics;
use App\Modules\BackOffice\Permission\Queries\PermissionQuery;

class PermissionPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new PermissionQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
