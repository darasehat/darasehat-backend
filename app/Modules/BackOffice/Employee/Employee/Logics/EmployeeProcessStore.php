<?php

namespace App\Modules\BackOffice\Employee\Employee\Logics;

use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\Layanesia\UserProfile\Logics\UserProfileLogic;
use App\Modules\BackOffice\User\Logics\UserLogic;
use App\Modules\BackOffice\Role\Logics\RoleLogic;
// use App\Models\UserPassport;

class EmployeeProcessStore
{
    public $user;

    /**
     * [doStore description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doStore($request, $datetime)
    {
		$result = (new EmployeeLogic())->getLevelInternal($request);
		$request = $result['request'];

        $request->request->add(['layDatetime' => $datetime]);
        $request->request->add(['layDate' => $datetime]);
        $request->request->add(['layTime' => $datetime]);
        $request->request->add(['layFirstName' => $request->layName]);
        
        if($request->layEmail)
        {
            //cek user, jika sudah ada dan tidak digunakan di employee maka gunakan user yang ada
            $this->user = (new UserLogic())->getUserEmail($request->layEmail,['id','is_active']);
            // $this->user = UserPassport::select('id')->where('email', $request->layEmail)->first();
            if(!$this->user) //user baru
            {
                //buat akun/user
                $this->user = (new UserLogic())->doStore($request, $datetime);
                $request->request->add(['layUsrId' => $this->user->id]);
                //simpan data profile
                $userProfile = (new UserProfileLogic())->doStore($request, $datetime);
                //set role
                $this->setRole($request);
            }
            else //user lama
            {
                $request->request->add(['layUsrId' => $this->user->id]);
                //update data administratif user
                (new UserLogic())->doUpdate($this->user->id, $request, $datetime);
                //buat data profile jika belum ada
                $userProfile = (new UserProfileLogic())->getUserEmail($request->layEmail, ['usrprf_id']);
                if(!$userProfile)
                {
                    $userProfile = (new UserProfileLogic())->doStore($request, $datetime);
                }
                //set role
                $this->setRole($request);
            }
            $request->request->add(['layUsrprfId' => $userProfile->usrprf_id]);
        }
        //simpan data employee
        $employee = (new EmployeeLogic())->doStore($request, $datetime);
        //return ke api yang memanggil
        if($employee)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function setRole($request)
    {
        $role = (new RoleLogic())->getRecord($request->layRoleId);
        $this->user->syncRoles($role->layName);
    }
}
