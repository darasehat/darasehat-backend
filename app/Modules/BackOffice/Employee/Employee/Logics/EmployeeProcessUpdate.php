<?php

namespace App\Modules\BackOffice\Employee\Employee\Logics;

use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\Layanesia\UserProfile\Logics\UserProfileLogic;
use App\Modules\BackOffice\User\Logics\UserLogic;
use App\Modules\BackOffice\Role\Logics\RoleLogic;
// use App\Models\UserPassport;

class EmployeeProcessUpdate
{
    public $user;
    // public $userPassport;
    /**
     * [doUpdate description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doUpdate($id, $request, $datetime)
    {
        $request->request->add(['layFirstName' => $request->layName]);
        // $this->userPassport = UserPassport::select('id')->where('email', $request->layEmail)->first();

        if(($request->layEmailOld) || $request->layEmailOld != '')//update email
        {
            $this->user = (new UserLogic())->getUserEmail($request->layEmailOld,['id']);
            if($request->layEmail != $request->layEmailOld)//rename email
            {
                (new UserLogic())->doUpdate($this->user->id, $request, $datetime);
                //set role
                $this->setRole($request);
            }
            else //email tetap, update user_profile saja
            {
                //ambil data
                $employee = (new EmployeeLogic())->getRecord($id,['usrprf_id']);
                if($employee->usrprf_id == '' || $employee->usrprf_id == null)
                {
                    $request = $this->storeUser($request, $datetime);
                }
                else
                {
                    (new UserLogic())->doUpdate($this->user->id, $request, $datetime);
                    $userProfile = (new UserProfileLogic())->doUpdate($employee->usrprf_id, $request, $datetime);
                    //set role
                    $this->setRole($request);
                }
            }
        }
        else //email baru
        {
            if($request->layEmail)
            {
                $this->user = (new UserLogic())->getUserEmail($request->layEmail,['id']);
                $request = $this->storeUser($request, $datetime);    
            }
        }

        //simpan data employee
        $employee = (new EmployeeLogic())->doUpdate($id, $request, $datetime);
        //return ke api yang memanggil
        if($employee)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 
     */
    private function storeUser($request, $datetime)
    {
        $request->request->add(['layDatetime' => $datetime]);
        $request->request->add(['layDate' => $datetime]);
        $request->request->add(['layTime' => $datetime]);
        //cek user, jika sudah ada dan tidak digunakan di employee maka gunakan user yang ada
        if(!$this->user) //akun sudah terdaftar
        {
            //buat akun
            $user = (new UserLogic())->doStoreRandomPassword($request, $datetime);
            $request->request->add(['layUsrId' => $user->id]);
            //simpan data profile
            $userProfile = (new UserProfileLogic())->doStore($request, $datetime);
            //set role
            $this->setRole($request);
        }
        else //akun belum terdaftar
        {
            $request->request->add(['layUsrId' => $this->user->id]);
            $userProfile = (new UserProfileLogic())->getUserEmail($request->layEmail, ['usrprf_id']);
            if(!$userProfile)
            {
                $userProfile = (new UserProfileLogic())->doStore($request, $datetime);
            }
            //set role
            $this->setRole($request);
        }
        $request->request->add(['layUsrprfId' => $userProfile->usrprf_id]);
        return $request;
    }

    private function setRole($request)
    {
        $role = (new RoleLogic())->getRecord($request->layRoleId);
        $this->user->syncRoles($role->layName);
    }
}
