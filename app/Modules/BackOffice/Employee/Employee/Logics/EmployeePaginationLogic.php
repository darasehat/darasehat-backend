<?php

namespace App\Modules\BackOffice\Employee\Employee\Logics;
use App\Modules\BackOffice\Employee\Employee\Queries\PaginationQuery;

class EmployeePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new PaginationQuery())->getPaginationQuery($rowsPerPage, $request);
    	return $result;
	}
}
