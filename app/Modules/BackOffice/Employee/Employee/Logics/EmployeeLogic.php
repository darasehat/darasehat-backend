<?php

namespace App\Modules\BackOffice\Employee\Employee\Logics;

use Auth;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeDataManagerLogic;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeePaginationLogic;
use App\Modules\BackOffice\Employee\Employee\Queries\EmployeeQuery;

use App\Modules\BackOffice\User\Logics\UserLogic;
use App\Modules\BackOffice\Role\Logics\RoleLogic;

class EmployeeLogic
{
    /**
     * [getPaginationData description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    public function getPaginationData($request)
    {
		$result = $this->getLevelInternal($request);
        $request = $result['request'];
        return (new EmployeePaginationLogic())->getPaginationData($request);
    }

    /**
     * [doStore description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doStore($request, $datetime)
    {
        // $merchant = (new MerchantLogic)->getRecord($request->layBrndId, ['cmp_id']);
    // 	$request->request->add(['layCmpId' => $merchant->cmp_id]);
        return (new EmployeeDataManagerLogic())->storeSave($request, $datetime);
    }

    /**
     * [doEdit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function doEdit($id)
    {
        $employee = (new EmployeeQuery())->editQuery($id);
        $result = [
            'layCompany' => $employee->layCompany,
            'layBrand' => $employee->layBrand,
            'layStore' => $employee->layStore,
            'layName' => $employee->layName,
            'layHomeAddress' => $employee->layHomeAddress,
            'layPhoneNo' => $employee->layPhoneNo,
            'layGenderId' => $employee->layGenderId,
            'layEnDate' => $employee->layEnDate,
            'layEmployeeRole' => $employee->layEmployeeRole,
            'layUserId' => $employee->layUserId,
            'layUserProfile' => $employee->layUserProfile
        ];
        if($employee->layUserId)
        {
            $user = (new UserLogic())->getRecord($employee->layUserId, ['id','email AS layEmail']);
            $result['layEmail'] = $user->layEmail;
            $roles = $user->getRoleNames();
            foreach ($roles as $key => $value) {
                $result['layRole'] = $value;
            }
            if(array_key_exists('layRole', $result))
            {
                $role = (new RoleLogic())->getNameRecord($result['layRole']);
                $result['layRole'] = $role->id;
            }
        }
        return $result;
    }

    /**
     * [doUpdate description]
     * @param  [type] $id       [description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doUpdate($id, $request, $datetime)
    {
        // $merchant = (new MerchantLogic)->getRecord($request->layBrndId, ['cmp_id']);
        // $request->request->add(['layCmpId' => $merchant->cmp_id]);
        return (new EmployeeDataManagerLogic())->updateSave($id, $request, $datetime);
    }

    /**
     * [doShow description]
     * @return [type] [description]
     */
    public function doShow($id)
    {
        $result = (new EmployeeQuery())->showQuery($id);
		if($result->createdBy)
		{
    		$result->layCreatedby = $result->createdBy->name;
        }
        if($result->changedBy)
		{
			$result->layChangedby = $result->changedBy->name;
        }
        return $result;
    }

    /**
     * [doDelete description]
     * @param  [type] $id       [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doDelete($id, $request, $datetime)
    {
        return (new EmployeeDataManagerLogic())->delete($id, $request, $datetime);
    }

    /**
     * [doSelectList description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function doSelectList()
    {
        return (new EmployeeQuery())->selectListQuery();
    }

    /**
     * [getRecord description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function getRecord($id, $select)
    {
        return (new EmployeeQuery())->recordQuery($id, $select);
    }

    /**
     * 
     */
    public function getEmailEmployeeList($email, $select)
    {
        return (new EmployeeQuery())->emailEmployeeListQuery($email, $select);
    }

    /**
     * 
     */
    public function getUserRecord($userid, $select)
    {
        return (new EmployeeQuery())->userRecordQuery($userid, $select);
    }

    /**
     * 
     */
    public function getLevel()
    {
        // $array = [];
        $level = -1;
        $emp = $this->getUserRecord(Auth::user()->id, ['cmp_id','brnd_id','store_id']);
        if($emp)
        {
            if($emp->cmp_id && $emp->cmp_id != null)
            {
                //bound ke company maka; level 1 (employee company)
                $level = 1;
            }
            if(($emp->cmp_id && $emp->cmp_id != null) && ($emp->brnd_id && $emp->brnd_id != null))
            {
                //bound ke company dan brand; level 2 (employee brand)
                $level = 2;
            }
            if(($emp->cmp_id && $emp->cmp_id != null) && ($emp->brnd_id && $emp->brnd_id != null) && ($emp->store_id && $emp->store_id != null))
            {
                //bound ke company, brand, dan store; level 3 (employee store)
                $level = 3;
            }
            // $array['company'] = $emp->cmp_id;
            // $array['brand'] = $emp->brnd_id;
            // $array['store'] = $emp->store_id;
        }
        //admin mantap
        if(Auth::user()->superadmin_flag == 1)
        {
            //bound ke mana saja selama dia superadmin = 1
            $level = 0;
        }

        // $array['level'] = $level;

        // return $array;
        return [
            'level' => $level
        ];
    }

    /**
     * 
     */
    public function getLevelInternal($request = null)
    {
        $array = [];
        $level = -1;
        $emp = $this->getUserRecord(Auth::user()->id, ['cmp_id','brnd_id','store_id']);
        if($emp)
        {
            if($emp->cmp_id && $emp->cmp_id != null)
            {
                //bound ke company maka; level 1 (employee company)
                $level = 1;
                if($request)
                {
                    $request->request->add(['layCmpId' => $emp->cmp_id]);
                    $request->merge(['layCmpId' => $emp->cmp_id]);    
                }
                else
                {
                    $array['layCmpId'] = $emp->cmp_id;
                }
            }
            if(($emp->cmp_id && $emp->cmp_id != null) && ($emp->brnd_id && $emp->brnd_id != null))
            {
                //bound ke company dan brand; level 2 (employee brand)
                $level = 2;
                if($request)
                {
                    $request->request->add(['layCmpId' => $emp->cmp_id]);
                    $request->merge(['layCmpId' => $emp->cmp_id]);
                    $request->request->add(['layBrndId' => $emp->brnd_id]);
                    $request->merge(['layBrndId' => $emp->brnd_id]);
                }
                else
                {
                    $array['layCmpId'] = $emp->cmp_id;
                    $array['layBrndId'] = $emp->brnd_id;    
                }
            }
            if(($emp->cmp_id && $emp->cmp_id != null) && ($emp->brnd_id && $emp->brnd_id != null) && ($emp->store_id && $emp->store_id != null))
            {
                //bound ke company, brand, dan store; level 3 (employee store)
                $level = 3;
                if($request)
                {
                    $request->request->add(['layCmpId' => $emp->cmp_id]);
                    $request->merge(['layCmpId' => $emp->cmp_id]);
                    $request->request->add(['layBrndId' => $emp->brnd_id]);
                    $request->merge(['layBrndId' => $emp->brnd_id]);
                    $request->request->add(['layStoreId' => $emp->store_id]);
                    $request->merge(['layStoreId' => $emp->store_id]);    
                }
                else
                {
                    $array['layCmpId'] = $emp->cmp_id;
                    $array['layBrndId'] = $emp->brnd_id;
                    $array['layStoreId'] = $emp->store_id;
                }
            }
        }
        //admin mantap
        if(Auth::user()->superadmin_flag == 1)
        {
            //bound ke mana saja selama dia superadmin = 1
            $level = 0;
        }

        if($request)
        {
            return [
                'level' => $level,
                'request' => $request
            ];    
        }
        else
        {
            return [
                'level' => $level,
                'request' => $array
            ];
        }
    }
}
