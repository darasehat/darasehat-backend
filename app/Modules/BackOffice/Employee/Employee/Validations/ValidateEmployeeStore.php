<?php

namespace App\Modules\BackOffice\Employee\Employee\Validations;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests\BackOffice\Employee\StoreEmployeeRequest;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\User\Logics\UserLogic;

class ValidateEmployeeStore
{
    /**
     * 
     */
    public function validate($request)
    {
        $this->formRequest = $request->all();
        if($request->has('layEmail'))
        {
            Validator::make(
                $request->all(),
                [
                    'layEmail' => [
                        'sometimes',
                        function ($attribute, $value, $fail) {
                            $employee = (new EmployeeLogic())->getEmailEmployeeList($value, ['usr_id']);
                            if ($employee->count()>0) //existing
                            {
                                $fail('Email sudah digunakan oleh karyawan lain');
                            }
                            else //baru
                            {
                                if(!array_key_exists('layPassword', $this->formRequest))
                                {
                                    $this->formRequest['layPassword'] = '';
                                }
                            }
                        },
                    ],
                    (new StoreEmployeeRequest())->messages(),
                    (new StoreEmployeeRequest())->attributes()
                ]
            )->validate();
        }
        if(array_key_exists('layPassword', $this->formRequest))
        {
            if(!$request->layPassword)
            {
                $request->request->add(['layPassword' => '']);
                $request->merge(['layPassword' => '']);    
            }
            $user = (new UserLogic())->getUserEmail($this->formRequest['layEmail'], ['id']);
            if(!$user)
            {
                Validator::make(
                    $request->all(),
                    [
                        'layPassword' => [
                            // 'sometimes',
                            'required',
                            'min:7',
                            'max:255',
                        ],
                    ],
                    (new StoreEmployeeRequest())->messages(),
                    (new StoreEmployeeRequest())->attributes()
                )->validate();    
            }
        }
    }
}
