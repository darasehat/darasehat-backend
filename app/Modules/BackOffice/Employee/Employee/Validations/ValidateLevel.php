<?php

namespace App\Modules\BackOffice\Employee\Employee\Validations;

use Illuminate\Support\Facades\Validator;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\Layanesia\Brand\Logics\BrandLogic;
use App\Modules\BackOffice\Layanesia\Store\Logics\StoreLogic;

class ValidateLevel
{
    public $data;
    /**
     * validate($name, $request)
     * 1. $name(string):
     *      1. company
     *      2. brand
     *      3. store
     *      4. store_store
     *          Harus dikombinasi dengan $request
     * 2. $request(request object)
     *      1. Jika tidak diisi
     *          maka hanya akan mengecek level user di perusahaan
     *      2. Jika diisi
     *          maka akan memproses validasi sesuai inputan dan level user di perusahaan
     */
    public function validate($name, $request=null)
    {
        if($request)
        {
            $employee = (new EmployeeLogic())->getLevelInternal($request);
            $level = $employee['level'];
            $request = $employee['request'];
            $this->data = $request->all();
            if($level == 0)
            {
                if($name=="store_store")
                {
                    Validator::make(
                        $request->all(),
                        [
                            'layCmpId' => [
                                'required',
                                'exists:companies,cmp_id',
                            ],
                            'layBrndId' => [
                                'required',
                                'exists:brands,brnd_id',
                                function ($attribute, $value, $fail) {
                                    //harus terkait dengan company
                                    $result = (new BrandLogic())->getCompanyRecord($value, $this->data['layCmpId'], ['brnd_id']);
                                    if(!$result)
                                    {
                                        $fail('Merek/Brand bukan milik perusahaan');
                                    }
                                },
                            ],
                        ]
                    )->validate();    
                }
                else
                {
                    Validator::make(
                        $request->all(),
                        [
                            'layCmpId' => [
                                'required',
                                'exists:companies,cmp_id',
                            ],
                            'layBrndId' => [
                                'required',
                                'exists:brands,brnd_id',
                                function ($attribute, $value, $fail) {
                                    //harus terkait dengan company
                                    $result = (new BrandLogic())->getCompanyRecord($value, $this->data['layCmpId'], ['brnd_id']);
                                    if(!$result)
                                    {
                                        $fail('Merek/Brand bukan milik perusahaan');
                                    }
                                },
                            ],
                            'layStoreId' => [
                                'required',
                                'exists:stores,store_id',
                                function ($attribute, $value, $fail) {
                                    //harus terkait dengan company dan brand
                                    $result = (new StoreLogic())->getBrandRecord($value, $this->data['layBrndId'], $this->data['layCmpId'], ['store_id']);
                                    if(!$result)
                                    {
                                        $fail('Cabang/Toko bukan milik perusahaan');
                                    }
                                },
                            ],
                        ]
                    )->validate();
                }
            }
            else if($level == 1)
            {
                Validator::make(
                    $request->all(),
                    [
                        'layBrndId' => [
                            'required',
                            'exists:brands,brnd_id',
                            function ($attribute, $value, $fail) {
                                //harus terkait dengan company
                                $result = (new BrandLogic())->getCompanyRecord($value, $this->data['layCmpId'], ['brnd_id']);
                                if(!$result)
                                {
                                    $fail('Merek/Brand bukan milik perusahaan');
                                }
                            },
                        ],
                        'layStoreId' => [
                            'required',
                            'exists:stores,store_id',
                            function ($attribute, $value, $fail) {
                                //harus terkait dengan company dan brand
                                $result = (new StoreLogic())->getBrandRecord($value, $this->data['layBrndId'], $this->data['layCmpId'], ['store_id']);
                                if(!$result)
                                {
                                    $fail('Cabang/Toko bukan milik perusahaan');
                                }
                            },
                        ],
                    ]
                )->validate();
            }
            else if($level == 2)
            {
                if($name=="employe_role")
                {
                }
                else
                {
                    Validator::make(
                        $request->all(),
                        [
                            'layStoreId' => [
                                'required',
                                'exists:stores,store_id',
                                function ($attribute, $value, $fail) {
                                    //harus terkait dengan company dan brand
                                    $result = (new StoreLogic())->getBrandRecord($value, $this->data['layBrndId'], $this->data['layCmpId'], ['store_id']);
                                    if(!$result)
                                    {
                                        $fail('Cabang/Toko bukan milik perusahaan');
                                    }
                                },
                            ],
                        ]
                    )->validate();
                }
            }
            else if($level == -1)
            {
                Validator::make(
                    ['level' => ''],
                    [
                        'level' => [
                            function ($attribute, $value, $fail) {
                                $fail('Level tidak cukup ');
                            },
                        ],
                    ]
                )->validate();
            }
            return $request;
        }
        else{
            $this->data = (new EmployeeLogic())->getLevelInternal();
            Validator::make(
                ['level' => $name],
                [
                    'level' => [
                        function ($attribute, $value, $fail) {
                            $level = $this->data['level'];
                            $error = false;
                            switch ($value) {
                                case "all":
                                    if($level == 0) //return none
                                    {
                            
                                    }
                                    else
                                    {
                                        $error = true;
                                    }
                                    break;
                                
                                case "company":
                                    if($level == 0 || $level == 1) //return cmp
                                    {
                            
                                    }
                                    else
                                    {
                                        $error = true;
                                    }
                                    break;
                    
                                case "brand":
                                    if($level == 0 || $level == 1 || $level == 2) //return cmp, brnd
                                    {
                            
                                    }
                                    else
                                    {
                                        $error = true;
                                    }
                                    break;
                    
                                case "store":
                                    if($level == 0 || $level == 1 || $level == 2 || $level == 3) //return cmp, brnd, store
                                    {
                            
                                    }
                                    else
                                    {
                                        $error = true;
                                    }
                                    break;

                                default:
                                    # code...
                                    break;
                            }                
                            if($error)
                            {
                                $fail('Level tidak cukup ');
                            }
                        },
                    ],
                ]
            )->validate();
            return $this->data['request'];
        }
    }
}
