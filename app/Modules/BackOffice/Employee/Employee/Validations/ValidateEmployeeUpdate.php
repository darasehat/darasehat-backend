<?php

namespace App\Modules\BackOffice\Employee\Employee\Validations;

use Illuminate\Support\Facades\Validator;
use App\Http\Requests\BackOffice\Employee\UpdateEmployeeRequest;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\User\Logics\UserLogic;

class ValidateEmployeeUpdate
{
    /**
     * 
     */
    public function validate($request)
    {
        $this->formRequest = $request->all();
        if($request->has('layEmail'))
        {
            Validator::make(
                $request->all(),
                [
                    'layEmail' => [
                        'sometimes',
                        function ($attribute, $value, $fail) {
                            
                            if(array_key_exists('layEmailOld', $this->formRequest))//update email
                            {
                                if($this->formRequest['layEmailOld'] != $value) //email diganti
                                {
                                    $employee = (new EmployeeLogic())->getEmailEmployeeList($value, ['usr_id']);
                                    if ($employee->count()>0)
                                    {
                                        $fail((new UpdateEmployeeRequest())->attributes()[$attribute].' sudah digunakan oleh karyawan lain');
                                    }
                                    else //baru
                                    {
                                        // if(!array_key_exists('layPassword', $this->formRequest))
                                        // {
                                        //     $this->formRequest['layPassword'] = '';
                                        // }
                                    }
                                    $user = (new UserLogic())->getUserEmail($this->formRequest['layEmail'], ['id']);
                                    if($user->id)
                                    {
                                        $fail((new UpdateEmployeeRequest())->attributes()[$attribute].' sudah terdaftar, silahkan gunakan email lain!');
                                    }
                                }
                            }
                            else //email baru
                            {
                                $employee = (new EmployeeLogic())->getEmailEmployeeList($value, ['usr_id']);
                                if ($employee->count()>0)
                                {
                                    $fail((new UpdateEmployeeRequest())->attributes()[$attribute].' sudah digunakan oleh karyawan lain');
                                }
                                else //baru
                                {
                                    if(!array_key_exists('layPassword', $this->formRequest))
                                    {
                                        $this->formRequest['layPassword'] = '';
                                    }
                                }
                            }
                        },
                    ],
                ],
                (new UpdateEmployeeRequest())->messages(),
                (new UpdateEmployeeRequest())->attributes()
        )->validate();
        }
        if(array_key_exists('layPassword', $this->formRequest))
        {
            if(!$request->layPassword)
            {
                $request->request->add(['layPassword' => '']);
                $request->merge(['layPassword' => '']);    
            }
            $user = (new UserLogic())->getUserEmail($this->formRequest['layEmail'], ['id']);
            if(!$user)
            {
                Validator::make(
                    $request->all(),
                    [
                        'layPassword' => [
                            'required',
                            'min:7',
                            'max:255',
                        ],
                    ],
                    (new UpdateEmployeeRequest())->messages(),
                    (new UpdateEmployeeRequest())->attributes()
                )->validate();    
            }
        }
    }
}
