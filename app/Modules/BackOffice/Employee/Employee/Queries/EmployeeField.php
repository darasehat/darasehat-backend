<?php

namespace App\Modules\BackOffice\Employee\Employee\Queries;

use App\Queries\General\FieldMap;

class EmployeeField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCmpId' => 'cmp_id',
            'layBrndId' => 'brnd_id',
            'layStoreId' => 'store_id',
            'layUsrId' => 'usr_id',
            'layUsrprfId' => 'usrprf_id',
            'layEmproleId' => 'emprole_id',
            'layName' => 'emp_name',
            'layHomeAddress' => 'emp_home_address',
            'layPhoneNo' => 'emp_phone_no',
            'layGndId' => 'gnd_id',
            'layEnDate' => 'emp_end_date',
            'layEmail' => 'emp_email'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}