<?php

namespace App\Modules\BackOffice\Employee\Employee\Queries;

use App\Modules\BackOffice\Employee\Employee\Queries\EmployeeEntity;

class EmployeeQuery extends EmployeeEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }
     */

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select(
            'cmp_name AS layCompany','brnd_name AS layBrand','store_name AS layStore',
            'emp_name AS layName','emp_home_address AS layAddress','emp_phone_no AS layPhoneNo','gnd_name AS layGender','emp_end_date AS layEndDate',
            'emprole_name AS layEmployeeRole','email AS layEmail',
            'emp_employees.createdby','emp_employees.createdtime AS layCreatedtime',
            'emp_employees.changedby','emp_employees.changedtime AS layChangedtime'
        )
        ->leftJoin('companies','companies.cmp_id','=','emp_employees.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','emp_employees.brnd_id')
        ->leftJoin('stores','stores.store_id','=','emp_employees.store_id')
        ->leftJoin('genders','genders.gnd_id','=','emp_employees.gnd_id')
        ->leftJoin('emp_employee_roles','emp_employee_roles.emprole_id','=','emp_employees.emprole_id')
        ->leftJoin('users','users.id','=','emp_employees.usr_id')
        ->where('emp_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select(
            'cmp_id AS layCompany',
            'brnd_id AS layBrand',
            'store_id AS layStore',
            'emp_name AS layName',
            'emp_home_address AS layHomeAddress',
            'emp_phone_no AS layPhoneNo',
            'gnd_id AS layGenderId',
            'emp_end_date AS layEnDate',
            'emprole_id AS layEmployeeRole',
            'usr_id AS layUserId',
            'usrprf_id AS layUserProfile'
        )
        ->where('emp_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('emp_name AS layName', 'emp_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [getRecord description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('emp_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function emailEmployeeListQuery($email, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->leftJoin('users','users.id','=','emp_employees.usr_id')
        ->where('email','=',$email)
        ->get();
        return $query;
    }

    /**
     * 
     */
    public function userRecordQuery($userid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('usr_id','=',$userid)
        ->first();
        return $query;
    }
}