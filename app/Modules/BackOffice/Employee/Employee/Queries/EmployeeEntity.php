<?php

namespace App\Modules\BackOffice\Employee\Employee\Queries;

use App\Models\Employee\Employee;

class EmployeeEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    public function newEntity()
    {
        return (new Employee());
    }
}