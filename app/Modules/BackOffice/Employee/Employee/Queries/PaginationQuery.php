<?php

namespace App\Modules\BackOffice\Employee\Employee\Queries;

use DB;
use App\Modules\BackOffice\Employee\Employee\Queries\EmployeeEntity;

class PaginationQuery extends EmployeeEntity
{
    /**
     * 
     */
    public function getPaginationQuery($rowsPerPage, $request)
    {
        $select = $this->setSelect($request);
        $query = $this->newEntity()
        ->select($select)
        ->leftJoin('companies','companies.cmp_id','=','emp_employees.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','emp_employees.brnd_id')
        ->leftJoin('stores','stores.store_id','=','emp_employees.store_id')
        ->leftJoin('emp_employee_roles','emp_employee_roles.emprole_id','=','emp_employees.emprole_id')
        ->leftJoin('genders','genders.gnd_id','=','emp_employees.gnd_id')
        ->leftJoin('users','users.id','=','emp_employees.usr_id')
        ->where('emp_employees.is_active','=',1)
        ->orderby('emp_employees.emp_end_date', 'desc');
        $query = $this->setLevel($query, $request);
        $query = $this->processSearch($query, $request->search);
        return $query->paginate($rowsPerPage);
    }

    /**
     * 
     */
    private function setSelect($request)
    {
        $select = [
            'emp_id AS layId',
            'emp_name AS layName',
            'emp_phone_no AS layPhoneNo',
            'gnd_name AS layGender',
            'emprole_name AS layEmployeeRole',
            'usr_id AS layUser',
            'email AS layEmail',
            'emp_end_date AS layEndDate',
            DB::raw('(CASE WHEN emp_end_date < DATE(NOW()) THEN "Selesai" ELSE "Bekerja" END) AS layWork')            
        ];
        if((!$request->layCmpId) )
        {
            array_push($select, 'cmp_name AS layCompany');
        }
        if((!$request->layBrndId) )
        {
            array_push($select, 'brnd_name AS layBrand');
        }
        if((!$request->layStoreId) )
        {
            array_push($select, 'store_name AS layStore');
        }
        return $select;
    }

    /**
     * 
     */
    private function setLevel($query, $request)
    {
        if($request->layCmpId)
        {
            $query = $query->where('emp_employees.cmp_id','=',$request->layCmpId);
        }
        if($request->layBrndId)
        {
            $query = $query->where('emp_employees.brnd_id','=',$request->layBrndId);            
        }
        if($request->layStoreId)
        {
            $query = $query->where('emp_employees.store_id','=',$request->layStoreId);            
        }

        $searchParam = json_decode($request->search);
        if (!empty($searchParam->layCmp))
        {
            $query = $query->where('emp_employees.cmp_id','=',$searchParam->layCmp);
        }
        if (!empty($searchParam->layBrnd))
        {
            $query = $query->where('emp_employees.brnd_id','=',$searchParam->layBrnd);
        }
        if (!empty($searchParam->layStore))
        {
            $query = $query->where('emp_employees.store_id','=',$searchParam->layStore);
        }
        return $query;
    }

    /**
     * 
     */
    private function processSearch($query, $search)
    {
        $searchParam = json_decode($search);
        $query->where(function ($queryGroup) use ($searchParam) {
            if (!empty($searchParam->layName)) {
                $queryGroup = $queryGroup->orWhere('emp_name','like',"%{$searchParam->layName}%");
            }
            if (!empty($searchParam->layEndate)) {
                $queryGroup = $queryGroup->orWhere('emp_end_date','like',"%{$searchParam->layEmail}%");
            }
        });
        return $query;
    }
}