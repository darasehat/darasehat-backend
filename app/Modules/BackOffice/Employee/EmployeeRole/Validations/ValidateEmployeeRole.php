<?php

namespace App\Modules\BackOffice\Employee\EmployeeRole\Validations;

use App\Modules\BackOffice\Employee\EmployeeRole\Logics\EmployeeRoleLogic;

class ValidateEmployeeRole
{
    /**
     * 
     */
    public function validate($request)
    {
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
        $errors = [];
        // Data tidak ditemukan
        $result = $this->checkEmployeeRole($request->layRoleId);
        if(!$result['status'])
        {
            $iserror = true;
        }
        $errors['layEmproleId'][0] = $result['message'];
        // return $errors;
        if($iserror)
        {
            return [
                'message' => 'Data tidak ditemukan',
                'errors' => $errors
            ];
        }
    }

    private function checkEmployeeRole($emprole_id)
    {
        $employeeRole = (new EmployeeRoleLogic())->getRecord($emprole_id, ['emprole_id']);
        if($employeeRole)
        {
            return ['status' => true, 'message' => 'Data ditemukan'];
        }
        else
        {
            return ['status' => false, 'message' => 'Data tidak ditemukan'];
        }
    }
}
