<?php

namespace App\Modules\BackOffice\Employee\EmployeeRole\Queries;

use App\Queries\General\FieldMap;

class EmployeeRoleField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCmpId' => 'cmp_id',
            'layName' => 'emprole_name',
            'layDescription' => 'emprole_description',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}