<?php

namespace App\Modules\BackOffice\Employee\EmployeeRole\Queries;

use App\Modules\BackOffice\Employee\EmployeeRole\Queries\EmployeeRoleEntity;

class EmployeeRoleQuery extends EmployeeRoleEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select(
            'emprole_id AS layId',
            'cmp_name AS layCompany',
            'emprole_name AS layName',
            'emprole_description AS layDescription'
        )
        ->leftJoin('companies','companies.cmp_id','=','emp_employee_roles.cmp_id')
        ->where('emp_employee_roles.is_active','=','1')
        ->where('emprole_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select(
            'emprole_id AS layId',
            'cmp_id AS layCompany',
            'emprole_name AS layName',
            'emprole_description AS layDescription'
        )
        ->where('is_active','=','1')
        ->where('emprole_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('emprole_name AS layName', 'emprole_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [recordQuery description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('emprole_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListBrandQuery description]
     * @param  [type] $brandid [description]
     * @return [type]          [description]
     */
    public function selectListBrandQuery()
    {
        $query = $this->setEntity()
        ->select('emprole_name AS layName', 'emprole_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }
}