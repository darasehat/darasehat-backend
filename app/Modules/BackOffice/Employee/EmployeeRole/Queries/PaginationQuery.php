<?php

namespace App\Modules\BackOffice\Employee\EmployeeRole\Queries;

use App\Modules\BackOffice\Employee\EmployeeRole\Queries\EmployeeRoleEntity;

class PaginationQuery extends EmployeeRoleEntity
{
    public function getPaginationQuery($rowsPerPage, $request)
    {
        $select = $this->setSelect($request);
        $query = $this->newEntity()
        ->select($select)
        ->leftJoin('companies','companies.cmp_id','=','emp_employee_roles.cmp_id')
        ->where('emp_employee_roles.is_active','=',1);
        $query = $this->setLevel($query, $request);
        return $query->paginate($rowsPerPage);
    }

    /**
     * 
     */
    private function setSelect($request)
    {
        $select = [
            'emprole_id AS layId',
            'cmp_name AS layCompany',
            'emprole_name AS layName'
        ];
        if((!$request->layCmpId) )
        {
            array_push($select, 'cmp_name AS layCompany');
        }
        // if((!$request->layBrndId) )
        // {
        //     array_push($select, 'brnd_name AS layBrand');
        // }
        // if((!$request->layStoreId) )
        // {
        //     array_push($select, 'store_name AS layStore');
        // }
        return $select;
    }

    /**
     * 
     */
    private function setLevel($query, $request)
    {
        if($request->layCmpId)
        {
            $query = $query->where('emp_employee_roles.cmp_id','=',$request->layCmpId);
        }
        // if($request->layBrndId)
        // {
        //     $query = $query->where('emp_employee_roles.brnd_id','=',$request->layBrndId);            
        // }
        // if($request->layStoreId)
        // {
        //     $query = $query->where('emp_employee_roles.store_id','=',$request->layStoreId);            
        // }

        $searchParam = json_decode($request->search);
        if (!empty($searchParam->layCmp))
        {
            $query = $query->where('emp_employee_roles.cmp_id','=',$searchParam->layCmp);
        }
        // if (!empty($searchParam->layBrnd))
        // {
        //     $query = $query->where('emp_employee_roles.brnd_id','=',$searchParam->layBrnd);
        // }
        // if (!empty($searchParam->layStore))
        // {
        //     $query = $query->where('emp_employee_roles.store_id','=',$searchParam->layStore);
        // }
        return $query;
    }
}