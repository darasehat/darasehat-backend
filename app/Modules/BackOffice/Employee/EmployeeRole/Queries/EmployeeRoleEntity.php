<?php

namespace App\Modules\BackOffice\Employee\EmployeeRole\Queries;

use App\Models\Employee\EmployeeRole;

class EmployeeRoleEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    public function newEntity()
    {
        return (new EmployeeRole());
    }
}