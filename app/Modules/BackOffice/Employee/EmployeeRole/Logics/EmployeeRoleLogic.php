<?php

namespace App\Modules\BackOffice\Employee\EmployeeRole\Logics;

use App\Modules\BackOffice\Employee\EmployeeRole\Logics\EmployeeRoleDataManagerLogic;
use App\Modules\BackOffice\Employee\EmployeeRole\Logics\EmployeeRolePaginationLogic;
use App\Modules\BackOffice\Employee\EmployeeRole\Queries\EmployeeRoleQuery;
use App\Modules\BackOffice\Layanesia\Brand\Logics\BrandLogic;

use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;

class EmployeeRoleLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$result = (new EmployeeLogic())->getLevelInternal($request);
		$request = $result['request'];
		return (new EmployeeRolePaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		// $brand = (new BrandLogic)->getRecord($request->layBrndId, ['cmp_id']);
		// $request->request->add(['layCmpId' => $brand->cmp_id]);
		return (new EmployeeRoleDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new EmployeeRoleQuery())->editQuery($id);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		// $brand = (new BrandLogic)->getRecord($request->layBrndId, ['cmp_id']);
    // 	$request->request->add(['layCmpId' => $brand->cmp_id]);
		return (new EmployeeRoleDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new EmployeeRoleQuery())->showQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new EmployeeRoleDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doSelectList()
	{
		return (new EmployeeRoleQuery())->selectListQuery();
	}

	/**
	 * [getRecord description]
	 * @param  [type] $id     [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new EmployeeRoleQuery())->recordQuery($id, $select);
	}

	/**
	 * [doSelectListBrand description]
	 * @param  [type] $brandid [description]
	 * @return [type]          [description]
	 */
	public function doSelectListBrand()
	{
		return (new EmployeeRoleQuery())->selectListBrandQuery();
	}

}
