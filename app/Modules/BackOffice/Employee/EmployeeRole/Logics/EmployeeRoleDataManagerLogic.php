<?php

namespace App\Modules\BackOffice\Employee\EmployeeRole\Logics;

use App\Modules\BackOffice\Employee\EmployeeRole\Queries\EmployeeRoleField;
use App\Modules\BackOffice\Employee\EmployeeRole\Queries\EmployeeRoleQuery;
use App\Modules\BackOffice\Employee\EmployeeRole\Queries\EmployeeRoleDataManagerQuery;

class EmployeeRoleDataManagerLogic extends EmployeeRoleDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new EmployeeRoleField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new EmployeeRoleQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new EmployeeRoleField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new EmployeeRoleQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new EmployeeRoleField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new EmployeeRoleQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new EmployeeRoleQuery())->setEntity());
	}
}
