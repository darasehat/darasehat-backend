<?php

namespace App\Modules\BackOffice\Layanesia\Gender\Logics;

use App\Modules\BackOffice\Layanesia\Gender\Queries\GenderField;
use App\Modules\BackOffice\Layanesia\Gender\Queries\GenderQuery;
use App\Modules\BackOffice\Layanesia\Gender\Queries\GenderDataManagerQuery;

class GenderDataManagerLogic extends GenderDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new GenderField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new GenderQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new GenderField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new GenderQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new GenderField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new GenderQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new GenderQuery())->setEntity());
	}
}
