<?php

namespace App\Modules\BackOffice\Layanesia\Gender\Logics;
use App\Modules\BackOffice\Layanesia\Gender\Queries\GenderQuery;

class GenderPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new GenderQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
