<?php

namespace App\Modules\BackOffice\Layanesia\Gender\Queries;

use App\Modules\BackOffice\Layanesia\Gender\Queries\GenderEntity;

class GenderQuery extends GenderEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('gnd_code AS layCode', 'gnd_name AS layName')
        ->where('gnd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('gnd_code AS layCode', 'gnd_name AS layName')
        ->where('gnd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('gnd_name AS layName', 'gnd_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }
}