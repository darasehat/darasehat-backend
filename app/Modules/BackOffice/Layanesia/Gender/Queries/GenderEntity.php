<?php

namespace App\Modules\BackOffice\Layanesia\Gender\Queries;

use App\Models\Layanesia\Gender;

class GenderEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Gender());
    }
}