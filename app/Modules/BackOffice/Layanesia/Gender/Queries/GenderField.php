<?php

namespace App\Modules\BackOffice\Layanesia\Gender\Queries;

use App\Queries\General\FieldMap;

class GenderField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCode' => 'gnd_code',
            'layName' => 'gnd_name',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}