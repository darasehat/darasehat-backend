<?php

namespace App\Modules\BackOffice\Layanesia\Gender\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('gnd_id AS layId','gnd_code AS layCode','gnd_name AS layName')
        ->where('is_active','=',1)
        ->paginate($rowsPerPage);
    }
}