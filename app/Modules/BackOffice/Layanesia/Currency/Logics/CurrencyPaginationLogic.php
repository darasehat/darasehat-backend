<?php

namespace App\Modules\BackOffice\Layanesia\Currency\Logics;
use App\Modules\BackOffice\Layanesia\Currency\Queries\CurrencyQuery;

class CurrencyPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new CurrencyQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
