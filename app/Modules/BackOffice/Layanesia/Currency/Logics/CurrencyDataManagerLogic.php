<?php

namespace App\Modules\BackOffice\Layanesia\Currency\Logics;

use Auth;
use App\Modules\BackOffice\Layanesia\Currency\Queries\CurrencyField;
use App\Modules\BackOffice\Layanesia\Currency\Queries\CurrencyQuery;
use App\Modules\BackOffice\Layanesia\Currency\Queries\CurrencyDataManagerQuery;

class CurrencyDataManagerLogic extends CurrencyDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CurrencyField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new CurrencyQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CurrencyField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new CurrencyQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new CurrencyField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new CurrencyQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new CurrencyQuery())->setEntity());
	}
}
