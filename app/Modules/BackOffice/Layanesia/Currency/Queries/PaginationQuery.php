<?php

namespace App\Modules\BackOffice\Layanesia\Currency\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('ccy_id AS layId', 'ccy_code AS layCode', 'ccy_name AS layName', 'cou_name AS layCountry')
        ->leftJoin('countries','countries.cou_id','=','currencies.cou_id')
        ->where('currencies.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}