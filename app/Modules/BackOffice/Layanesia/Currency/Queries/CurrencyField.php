<?php

namespace App\Modules\BackOffice\Layanesia\Currency\Queries;

use App\Queries\General\FieldMap;

class CurrencyField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCouId' => 'cou_id',
            'layCode' => 'ccy_code',
            'layName' => 'ccy_name',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}