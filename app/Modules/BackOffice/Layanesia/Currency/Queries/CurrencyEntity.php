<?php

namespace App\Modules\BackOffice\Layanesia\Currency\Queries;

use App\Models\Layanesia\Currency;

class CurrencyEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Currency());
    }
}