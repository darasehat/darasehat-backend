<?php

namespace App\Modules\BackOffice\Layanesia\Currency\Queries;

use App\Modules\BackOffice\Layanesia\Currency\Queries\CurrencyEntity;

class CurrencyQuery extends CurrencyEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('ccy_code AS layCode', 'ccy_name AS layName', 'cou_id AS layCountry')
        ->where('ccy_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('ccy_code AS layCode', 'ccy_name AS layName', 'cou_name AS layCountry')
        ->leftJoin('countries','countries.cou_id','=','currencies.cou_id')
        ->where('ccy_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('ccy_name AS layName', 'ccy_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }
}