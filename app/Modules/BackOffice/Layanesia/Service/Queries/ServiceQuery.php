<?php

namespace App\Modules\BackOffice\Layanesia\Service\Queries;

use App\Modules\BackOffice\Layanesia\Service\Queries\ServiceEntity;
use Auth;

class ServiceQuery extends ServiceEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('srv_id AS layId','srv_name AS layName','srv_code AS layCode','srv_merchant_flag AS layMerchantFlag')
        ->where('srv_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('srv_id AS layId','srv_name AS layName','srv_code AS layCode','srv_merchant_flag AS layMerchantFlag')
        ->where('srv_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('srv_name AS layName', 'srv_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [codeQuery description]
     * @param  [type] $srvcode [description]
     * @param  [type] $select  [description]
     * @return [type]          [description]
     */
    public function codeQuery($srvcode, $select)
    {
        $query = $this->newEntity()
        ->select($select)
        ->where('srv_code','=',$srvcode)
        ->first();
        return $query;
    }
}