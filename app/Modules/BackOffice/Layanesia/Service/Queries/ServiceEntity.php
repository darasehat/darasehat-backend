<?php

namespace App\Modules\BackOffice\Layanesia\Service\Queries;

use App\Models\Layanesia\Service;

class ServiceEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Service());
    }
}