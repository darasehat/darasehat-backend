<?php

namespace App\Modules\BackOffice\Layanesia\Service\Queries;

use App\Queries\General\FieldMap;

class ServiceField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCode' => 'srv_code',
            'layName' => 'srv_name',
            'layMerchantFlag' => 'srv_merchant_flag',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}