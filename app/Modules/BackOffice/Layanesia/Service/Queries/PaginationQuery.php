<?php

namespace App\Modules\BackOffice\Layanesia\Service\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('srv_id AS layId','srv_name AS layName','srv_code AS layCode','srv_merchant_flag AS layMerchantFlag')
        ->where('is_active','=',1)
        ->paginate($rowsPerPage);
    }
}