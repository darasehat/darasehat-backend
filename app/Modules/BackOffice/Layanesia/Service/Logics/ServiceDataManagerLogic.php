<?php

namespace App\Modules\BackOffice\Layanesia\Service\Logics;

use App\Modules\BackOffice\Layanesia\Service\Queries\ServiceField;
use App\Modules\BackOffice\Layanesia\Service\Queries\ServiceQuery;
use App\Modules\BackOffice\Layanesia\Service\Queries\ServiceDataManagerQuery;

class ServiceDataManagerLogic extends ServiceDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new ServiceField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new ServiceQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new ServiceField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new ServiceQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new ServiceField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new ServiceQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new ServiceQuery())->setEntity());
	}
}
