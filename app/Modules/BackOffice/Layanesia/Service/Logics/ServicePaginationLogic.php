<?php

namespace App\Modules\BackOffice\Layanesia\Service\Logics;
use App\Modules\BackOffice\Layanesia\Service\Queries\ServiceQuery;

class ServicePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new ServiceQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
