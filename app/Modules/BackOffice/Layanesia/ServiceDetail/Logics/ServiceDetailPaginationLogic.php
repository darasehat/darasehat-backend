<?php

namespace App\Modules\BackOffice\Layanesia\ServiceDetail\Logics;
use App\Modules\BackOffice\Layanesia\ServiceDetail\Queries\ServiceDetailQuery;

class ServiceDetailPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new ServiceDetailQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
