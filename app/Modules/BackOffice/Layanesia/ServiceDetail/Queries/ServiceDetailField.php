<?php

namespace App\Modules\BackOffice\Layanesia\ServiceDetail\Queries;

use App\Queries\General\FieldMap;

class ServiceDetailField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCode' => 'srvdtl_code',
            'layName' => 'srvdtl_name',
            'layDescription' => 'srvdtl_description',
            'laySrvId' => 'srv_id',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}