<?php

namespace App\Modules\BackOffice\Layanesia\ServiceDetail\Queries;

use App\Models\Layanesia\ServiceDetail;

class ServiceDetailEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new ServiceDetail());
    }
}