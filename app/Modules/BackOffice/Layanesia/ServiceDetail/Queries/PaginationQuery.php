<?php

namespace App\Modules\BackOffice\Layanesia\ServiceDetail\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('srvdtl_id AS layId','srvdtl_name AS layName','srvdtl_code AS layCode','srv_name AS layService')
        ->leftJoin('services','services.srv_id','=','service_details.srv_id')
        ->where('service_details.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}