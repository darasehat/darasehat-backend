<?php

namespace App\Modules\BackOffice\Layanesia\ServiceDetail\Queries;

use App\Modules\BackOffice\Layanesia\ServiceDetail\Queries\ServiceDetailEntity;

class ServiceDetailQuery extends ServiceDetailEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('srvdtl_id AS layId','srvdtl_name AS layName','srvdtl_code AS layCode','srvdtl_description AS layDescription','srv_name AS layService')
        ->leftJoin('services','services.srv_id','=','service_details.srv_id')
        ->where('srvdtl_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('srvdtl_id AS layId','srvdtl_name AS layName','srvdtl_code AS layCode','srv_id AS layServiceId')
        ->where('srvdtl_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('srvdtl_name AS layName', 'srvdtl_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [getRecord description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function getRecord($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('srvdtl_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [codeQuery description]
     * @param  [type] $srvcode [description]
     * @param  [type] $select  [description]
     * @return [type]          [description]
     */
    public function codeQuery($srvcode, $select)
    {
        $query = $this->newEntity()
        ->select($select)
        ->where('srvdtl_code','=',$srvcode)
        ->first();
        return $query;
    }
}