<?php

namespace App\Modules\BackOffice\Layanesia\Brand\Queries;

use App\Models\Layanesia\Brand;

class BrandEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    public function newEntity()
    {
        return (new Brand());
    }
}