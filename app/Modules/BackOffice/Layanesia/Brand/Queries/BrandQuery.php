<?php

namespace App\Modules\BackOffice\Layanesia\Brand\Queries;

use App\Modules\BackOffice\Layanesia\Brand\Queries\BrandEntity;

class BrandQuery extends BrandEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('brnd_name AS layName','cmp_name AS layCompany','brnd_open_flag AS layOpenFlag','brnd_join_date AS layJoinDate','brnd_address AS layAddress','brnd_description AS layDescription')
        ->leftJoin('companies','companies.cmp_id','=','brands.cmp_id')
        ->where('brnd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('brnd_id AS layId','brnd_name AS layName','cmp_id AS layCompany','brnd_open_flag AS layOpenFlag','brnd_join_date AS layJoinDate','brnd_address AS layAddress','brnd_description AS layDescription')
        ->where('brnd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('brnd_name AS layName', 'brnd_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [recordQuery description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('brnd_id','=', $id)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function companySelectListQuery($cmpid)
    {
        $query = $this->setEntity()
        ->select('brnd_name AS layName', 'brnd_id AS layCode')
        ->where('is_active','=','1')
        ->where('cmp_id','=',$cmpid)
        ->get();
        return $query;
    }

    public function companyRecordQuery($id, $cmpid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('brnd_id','=',$id)
        ->where('cmp_id','=',$cmpid)
        ->first();
        return $query;
    }
}