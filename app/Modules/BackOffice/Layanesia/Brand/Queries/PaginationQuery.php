<?php

namespace App\Modules\BackOffice\Layanesia\Brand\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('brnd_id AS layId','brnd_name AS layName','cmp_name AS layCompany','brnd_open_flag AS layOpenFlag','brnd_join_date AS layJoinDate')
        ->leftJoin('companies','companies.cmp_id','=','brands.cmp_id')
        ->where('brands.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}