<?php

namespace App\Modules\BackOffice\Layanesia\Brand\Queries;

use App\Queries\General\FieldMap;

class BrandField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layOpenFlag' => 'brnd_open_flag',
            'layName' => 'brnd_name',
            'layAddress' => 'brnd_address',
            'layDescription' => 'brnd_description',
            'layJoinDate' => 'brnd_join_date',
            'layLat' => 'brnd_lat',
            'layLon' => 'brnd_lon',
            'layCmpId' => 'cmp_id',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}