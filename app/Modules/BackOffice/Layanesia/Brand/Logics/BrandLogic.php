<?php

namespace App\Modules\BackOffice\Layanesia\Brand\Logics;

use App\Modules\BackOffice\Layanesia\Brand\Logics\BrandDataManagerLogic;
use App\Modules\BackOffice\Layanesia\Brand\Logics\BrandPaginationLogic;
use App\Modules\BackOffice\Layanesia\Brand\Queries\BrandQuery;

class BrandLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new BrandPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new BrandDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new BrandQuery())->editQuery($id);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new BrandDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new BrandQuery())->showQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new BrandDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [getSelectList description]
	 * @return [type] [description]
	 */
	public function getSelectList()
	{
		return (new BrandQuery())->selectListQuery();
	}

	/**
	 * [getRecord description]
	 * @param  [type] $id     [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new BrandQuery())->recordQuery($id, $select);
	}

	/**
	 * 
	 */
	public function getCompanySelectList($cmpid)
	{
		return (new BrandQuery())->companySelectListQuery($cmpid);
	}

	/**
	 * 
	 */
	public function getCompanyRecord($id, $cmpid, $select)
	{
		return (new BrandQuery())->companyRecordQuery($id, $cmpid, $select);
	}
}
