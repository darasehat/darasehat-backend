<?php

namespace App\Modules\BackOffice\Layanesia\Brand\Logics;
use App\Modules\BackOffice\Layanesia\Brand\Queries\BrandQuery;

class BrandPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new BrandQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
