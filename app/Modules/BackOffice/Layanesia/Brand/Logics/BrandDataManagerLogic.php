<?php

namespace App\Modules\BackOffice\Layanesia\Brand\Logics;

use App\Modules\BackOffice\Layanesia\Brand\Queries\BrandField;
use App\Modules\BackOffice\Layanesia\Brand\Queries\BrandQuery;
use App\Modules\BackOffice\Layanesia\Brand\Queries\BrandDataManagerQuery;

class BrandDataManagerLogic extends BrandDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new BrandField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new BrandQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new BrandField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new BrandQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new BrandField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new BrandQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new BrandQuery())->setEntity());
	}
}
