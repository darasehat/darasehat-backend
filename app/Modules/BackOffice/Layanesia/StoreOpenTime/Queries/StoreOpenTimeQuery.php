<?php

namespace App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries;

use App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries\StoreOpenTimeEntity;

class StoreOpenTimeQuery extends StoreOpenTimeEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('cmp_name AS layCompany','brnd_name AS layMerchant','store_name AS layStore','storeopn_day AS layDay','storeopn_open AS layOpen','storeopn_close AS layClose')
        ->leftJoin('companies','companies.cmp_id','=','store_open_times.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','store_open_times.brnd_id')
        ->leftJoin('stores','stores.store_id','=','store_open_times.store_id')
        ->where('storeopn_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('store_id AS layStore','storeopn_day AS layDay','storeopn_open AS layOpen','storeopn_close AS layClose')
        ->where('storeopn_id','=',$id)
        ->first();
        return $query;
    }
}