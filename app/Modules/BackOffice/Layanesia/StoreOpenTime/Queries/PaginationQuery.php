<?php

namespace App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('storeopn_id AS layId','cmp_name AS layCompany','store_name AS layStore','brnd_name AS layMerchant','storeopn_day AS layDay','storeopn_open AS layOpen','storeopn_close AS layClose')
        ->leftJoin('companies','companies.cmp_id','=','store_open_times.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','store_open_times.brnd_id')
        ->leftJoin('stores','stores.store_id','=','store_open_times.store_id')
        ->where('store_open_times.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}