<?php

namespace App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries;

use App\Queries\General\FieldMap;

class StoreOpenTimeField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCmpId' => 'cmp_id',
            'layBrndId' => 'brnd_id',
            'layStoreId' => 'store_id',
            'layDay' => 'storeopn_day',
            'layOpen' => 'storeopn_open',
            'layClose' => 'storeopn_close',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}