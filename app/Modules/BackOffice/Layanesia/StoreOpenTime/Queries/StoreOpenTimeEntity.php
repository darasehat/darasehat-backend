<?php

namespace App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries;

use App\Models\Layanesia\StoreOpenTime;

class StoreOpenTimeEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    public function newEntity()
    {
        return (new StoreOpenTime());
    }
}