<?php

namespace App\Modules\BackOffice\Layanesia\StoreOpenTime\Logics;

use App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries\StoreOpenTimeField;
use App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries\StoreOpenTimeQuery;
use App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries\StoreOpenTimeDataManagerQuery;

class StoreOpenTimeDataManagerLogic extends StoreOpenTimeDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new StoreOpenTimeField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new StoreOpenTimeQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new StoreOpenTimeField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new StoreOpenTimeQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new StoreOpenTimeField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new StoreOpenTimeQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new StoreOpenTimeQuery())->setEntity());
	}
}
