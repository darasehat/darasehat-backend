<?php

namespace App\Modules\BackOffice\Layanesia\StoreOpenTime\Logics;

use App\Modules\BackOffice\Layanesia\StoreOpenTime\Logics\StoreOpenTimeDataManagerLogic;
use App\Modules\BackOffice\Layanesia\StoreOpenTime\Logics\StoreOpenTimePaginationLogic;
use App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries\StoreOpenTimeQuery;
use App\Modules\BackOffice\Layanesia\Store\Logics\StoreLogic;

class StoreOpenTimeLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new StoreOpenTimePaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$store = (new StoreLogic())->getRecord($request->layStoreId, ['cmp_id','brnd_id']);
    	$request->request->add(['layCmpId' => $store->cmp_id]);
    	$request->request->add(['layBrndId' => $store->brnd_id]);
		return (new StoreOpenTimeDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new StoreOpenTimeQuery())->editQuery($id);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		$store = (new StoreLogic())->getRecord($request->layStoreId, ['cmp_id','brnd_id']);
    	$request->request->add(['layCmpId' => $store->cmp_id]);
    	$request->request->add(['layBrndId' => $store->brnd_id]);
		return (new StoreOpenTimeDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new StoreOpenTimeQuery())->showQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new StoreOpenTimeDataManagerLogic())->delete($id, $request, $datetime);
	}
}
