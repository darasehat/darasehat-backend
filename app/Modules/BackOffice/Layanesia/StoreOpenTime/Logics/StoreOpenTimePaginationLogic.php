<?php

namespace App\Modules\BackOffice\Layanesia\StoreOpenTime\Logics;
use App\Modules\BackOffice\Layanesia\StoreOpenTime\Queries\StoreOpenTimeQuery;

class StoreOpenTimePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new StoreOpenTimeQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
