<?php

namespace App\Modules\BackOffice\Layanesia\Country\Queries;

use App\Models\Layanesia\Country;

class CountryEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Country());
    }
}