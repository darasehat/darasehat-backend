<?php

namespace App\Modules\BackOffice\Layanesia\Country\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('cou_id AS layId','cou_code AS layCode','cou_name AS layName')
        ->where('is_active','=',1)
        ->paginate($rowsPerPage);
    }
}