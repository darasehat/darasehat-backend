<?php

namespace App\Modules\BackOffice\Layanesia\Country\Logics;
use App\Modules\BackOffice\Layanesia\Country\Queries\CountryQuery;

class CountryPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new CountryQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
