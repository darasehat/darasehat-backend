<?php

namespace App\Modules\BackOffice\Layanesia\Country\Logics;

use App\Modules\BackOffice\Layanesia\Country\Queries\CountryField;
use App\Modules\BackOffice\Layanesia\Country\Queries\CountryQuery;
use App\Modules\BackOffice\Layanesia\Country\Queries\CountryDataManagerQuery;

class CountryDataManagerLogic extends CountryDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CountryField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new CountryQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CountryField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new CountryQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new CountryField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new CountryQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new CountryQuery())->setEntity());
	}
}
