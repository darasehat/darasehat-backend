<?php

namespace App\Modules\BackOffice\Layanesia\Store\Logics;

use App\Modules\BackOffice\Layanesia\Store\Logics\StoreDataManagerLogic;
use App\Modules\BackOffice\Layanesia\Store\Logics\StorePaginationLogic;
use App\Modules\BackOffice\Layanesia\Store\Queries\StoreQuery;
use App\Modules\BackOffice\Layanesia\Merchant\Logics\MerchantLogic;
use App\Modules\BackOffice\Layanesia\Service\Logics\ServiceLogic;

class StoreLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new StorePaginationLogic())->getPaginationData($request);
	}

	/**
	 * [getPaginationServiceData description]
	 * @param  [type] $request [description]
	 * @param  [type] $srvcode [description]
	 * @return [type]          [description]
	 */
	public function getPaginationServiceData($request, $srvcode)
	{
		$service = (new ServiceLogic())->getCode($srvcode, ['srv_id']);
		return (new StorePaginationLogic())->getPaginationServiceData($request, $service->srv_id);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$merchant = (new MerchantLogic)->getRecord($request->layBrndId, ['cmp_id']);
    	$request->request->add(['layCmpId' => $merchant->cmp_id]);
		return (new StoreDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new StoreQuery())->editQuery($id);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		$merchant = (new MerchantLogic)->getRecord($request->layBrndId, ['cmp_id']);
    	$request->request->add(['layCmpId' => $merchant->cmp_id]);
		return (new StoreDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new StoreQuery())->showQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new StoreDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doSelectList()
	{
		return (new StoreQuery())->selectListQuery();
	}

	/**
	 * [getRecord description]
	 * @param  [type] $id     [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new StoreQuery())->recordQuery($id, $select);
	}

	/**
	 * 
	 */
	public function getBrandRecord($id, $brandid, $cmpid, $select)
	{
		return (new StoreQuery())->brandRecordQuery($id, $brandid, $cmpid, $select);
	}

	/**
	 * 
	 */
	public function getBrandSelectList($brndid)
	{
		return (new StoreQuery())->brandSelectListQuery($brndid);
	}
}
