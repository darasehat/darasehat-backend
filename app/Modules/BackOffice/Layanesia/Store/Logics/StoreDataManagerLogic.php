<?php

namespace App\Modules\BackOffice\Layanesia\Store\Logics;

use App\Modules\BackOffice\Layanesia\Store\Queries\StoreField;
use App\Modules\BackOffice\Layanesia\Store\Queries\StoreQuery;
use App\Modules\BackOffice\Layanesia\Store\Queries\StoreDataManagerQuery;

class StoreDataManagerLogic extends StoreDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new StoreField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new StoreQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new StoreField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new StoreQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new StoreField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new StoreQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new StoreQuery())->setEntity());
	}
}
