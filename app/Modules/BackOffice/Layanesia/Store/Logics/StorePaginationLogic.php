<?php

namespace App\Modules\BackOffice\Layanesia\Store\Logics;
use App\Modules\BackOffice\Layanesia\Store\Queries\PaginationQuery;

class StorePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new PaginationQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}

	/**
	 * [getPaginationServiceData description]
	 * @param  [type] $request [description]
	 * @param  [type] $srvid [description]
	 * @return [type]          [description]
	 */
	public function getPaginationServiceData($request, $srvid)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new PaginationQuery())->getPaginationServiceQuery($rowsPerPage, $srvid);
    	return $result;
	}
}
