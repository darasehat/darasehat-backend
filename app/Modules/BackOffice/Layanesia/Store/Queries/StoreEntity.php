<?php

namespace App\Modules\BackOffice\Layanesia\Store\Queries;

use App\Models\Layanesia\Store;

class StoreEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    public function newEntity()
    {
        return (new Store());
    }
}