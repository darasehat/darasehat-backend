<?php

namespace App\Modules\BackOffice\Layanesia\Store\Queries;

use App\Modules\BackOffice\Layanesia\Store\Queries\StoreEntity;

class StoreQuery extends StoreEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }
     */

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('cmp_name AS layCompany','brnd_name AS layBrand','store_name AS layName','store_open_flag AS layOpenFlag','store_open_since AS layOpenSince','store_address AS layAddress','store_description AS layDescription')
        ->leftJoin('companies','companies.cmp_id','=','stores.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','stores.brnd_id')
        ->where('store_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select(
            'cmp_id AS layCompany',
            'brnd_id AS layBrand',
            'store_id AS layStore',
            'store_name AS layName',
            'store_open_flag AS layOpenFlag',
            'store_open_since AS layOpenSince',
            'store_address AS layAddress',
            'store_description AS layDescription'
        )
        ->where('store_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('store_name AS layName', 'store_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [recordQuery description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('store_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function brandRecordQuery($id, $brandid, $cmpid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=',1)
        ->where('store_id','=',$id)
        ->where('brnd_id','=',$brandid)
        ->where('cmp_id','=',$cmpid)
        ->first();
        return $query;
    }

    /**
     * 
     */
    public function brandSelectListQuery($brndid)
    {
        $query = $this->setEntity()
        ->select('store_name AS layName', 'store_id AS layCode')
        ->where('is_active','=','1')
        ->where('brnd_id','=',$brndid)
        ->get();
        return $query;
    }
}