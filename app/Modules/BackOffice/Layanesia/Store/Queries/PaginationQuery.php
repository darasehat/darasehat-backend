<?php

namespace App\Modules\BackOffice\Layanesia\Store\Queries;

use App\Modules\BackOffice\Layanesia\Store\Queries\StoreEntity;

class PaginationQuery extends StoreEntity
{
    public function getPaginationQuery($rowsPerPage)
    {
        return $this->newEntity()->select('store_id AS layId','cmp_name AS layCompany','brnd_name AS layMerchant','store_name AS layName','store_open_flag AS layOpenFlag','store_open_since AS layOpenSince')
        ->leftJoin('companies','companies.cmp_id','=','stores.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','stores.brnd_id')
        ->where('stores.is_active','=',1)
        ->paginate($rowsPerPage);
    }

    public function getPaginationServiceQuery($rowsPerPage, $srvid)
    {
        return $this->newEntity()->select('store_id AS layId','cmp_name AS layCompany','brnd_name AS layMerchant','store_name AS layName','store_open_flag AS layOpenFlag','store_open_since AS layOpenSince')
        ->leftJoin('companies','companies.cmp_id','=','stores.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','stores.brnd_id')
        ->leftJoin('store_services','store_services.srv_id','=','stores.srv_id')
        ->where('stores.is_active','=',1)
        ->where('srv_id','=',$srvid)
        ->paginate($rowsPerPage);    	
    }
}