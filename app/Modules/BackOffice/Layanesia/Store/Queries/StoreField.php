<?php

namespace App\Modules\BackOffice\Layanesia\Store\Queries;

use App\Queries\General\FieldMap;

class StoreField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layOpenFlag' => 'store_open_flag',
            'layName' => 'store_name',
            'layAddress' => 'store_address',
            'layDescription' => 'store_description',
            'layOpenSince' => 'store_open_since',
            'layLat' => 'store_lat',
            'layLon' => 'store_lon',
            'layCmpId' => 'cmp_id',
            'layBrndId' => 'brnd_id'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}