<?php

namespace App\Modules\BackOffice\Layanesia\StoreService\Logics;
use App\Modules\BackOffice\Layanesia\StoreService\Queries\StoreServiceQuery;

class StoreServicePaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new StoreServiceQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
