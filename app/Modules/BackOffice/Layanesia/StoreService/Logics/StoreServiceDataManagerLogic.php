<?php

namespace App\Modules\BackOffice\Layanesia\StoreService\Logics;

use App\Modules\BackOffice\Layanesia\StoreService\Queries\StoreServiceField;
use App\Modules\BackOffice\Layanesia\StoreService\Queries\StoreServiceQuery;
use App\Modules\BackOffice\Layanesia\StoreService\Queries\StoreServiceDataManagerQuery;

class StoreServiceDataManagerLogic extends StoreServiceDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeServiceSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeServiceSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new StoreServiceField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new StoreServiceQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new StoreServiceField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new StoreServiceQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new StoreServiceField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new StoreServiceQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new StoreServiceQuery())->setEntity());
	}
}
