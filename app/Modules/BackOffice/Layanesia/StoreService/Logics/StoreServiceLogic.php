<?php

namespace App\Modules\BackOffice\Layanesia\StoreService\Logics;

use App\Modules\BackOffice\Layanesia\StoreService\Logics\StoreServiceDataManagerLogic;
use App\Modules\BackOffice\Layanesia\StoreService\Logics\StoreServicePaginationLogic;
use App\Modules\BackOffice\Layanesia\StoreService\Queries\StoreServiceQuery;
use App\Modules\BackOffice\Layanesia\Store\Logics\StoreLogic;
use App\Modules\BackOffice\Layanesia\ServiceDetail\Logics\ServiceDetailLogic;

class StoreServiceLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new StoreServicePaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStoreService description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		// return ['layStoreId' => $request->layStoreId, 'laySrvdtlId' => $request->laySrvdtlId];
		$store = (new StoreLogic())->getRecord($request->layStoreId, ['cmp_id','brnd_id']);		
		$servicedetail = (new ServiceDetailLogic())->getRecord($request->laySrvdtlId, ['srv_id']);
    	$request->request->add(['layCmpId' => $store->cmp_id]);
    	$request->request->add(['layBrndId' => $store->brnd_id]);
    	$request->request->add(['laySrvId' => $servicedetail->srv_id]);
		return (new StoreServiceDataManagerLogic())->storeServiceSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new StoreServiceQuery())->editQuery($id);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		$store = (new StoreLogic())->getRecord($request->layStoreId, ['cmp_id','brnd_id']);
		$servicedetail = (new ServiceDetailLogic())->getRecord($request->laySrvdtlId, ['srv_id']);
    	$request->request->add(['layCmpId' => $store->cmp_id]);
    	$request->request->add(['layBrndId' => $store->brnd_id]);
    	$request->request->add(['laySrvId' => $servicedetail->srv_id]);
		return (new StoreServiceDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new StoreServiceQuery())->showQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new StoreServiceDataManagerLogic())->delete($id, $request, $datetime);
	}
}
