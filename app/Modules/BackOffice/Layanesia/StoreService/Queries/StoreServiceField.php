<?php

namespace App\Modules\BackOffice\Layanesia\StoreService\Queries;

use App\Queries\General\FieldMap;

class StoreServiceField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCmpId' => 'cmp_id',
            'layBrndId' => 'brnd_id',
            'layStoreId' => 'store_id',
            'laySrvId' => 'srv_id',
            'laySrvdtlId' => 'srvdtl_id',
            'layStdate' => 'storesrv_stdate',
            'layEndate' => 'storesrv_endate',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}