<?php

namespace App\Modules\BackOffice\Layanesia\StoreService\Queries;

use App\Modules\BackOffice\Layanesia\StoreService\Queries\StoreServiceEntity;

class StoreServiceQuery extends StoreServiceEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('storesrv_id AS layId','store_name AS layStore','srv_name AS layService','srvdtl_name AS layDetail','storesrv_stdate AS layStdate','storesrv_endate AS layEndate')
        ->leftJoin('stores','stores.store_id','=','store_services.store_id')
        ->leftJoin('service_details','service_details.srvdtl_id','=','store_services.srvdtl_id')
        ->leftJoin('services','services.srv_id','=','store_services.srv_id')
        ->where('storesrv_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('storesrv_id AS layId','store_id AS layStore','srv_id AS layService','srvdtl_id AS layDetail','storesrv_stdate AS layStdate','storesrv_endate AS layEndate')
        ->where('storesrv_id','=',$id)
        ->first();
        return $query;
    }
}