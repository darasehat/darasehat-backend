<?php

namespace App\Modules\BackOffice\Layanesia\StoreService\Queries;

use App\Models\Layanesia\StoreService;

class StoreServiceEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    public function newEntity()
    {
        return (new StoreService());
    }
}