<?php

namespace App\Modules\BackOffice\Layanesia\StoreService\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('storesrv_id AS layId','store_name AS layStore','srv_name AS layService','srvdtl_name AS layDetail','storesrv_stdate AS layStdate','storesrv_endate AS layEndate')
        ->leftJoin('stores','stores.store_id','=','store_services.store_id')
        ->leftJoin('service_details','service_details.srvdtl_id','=','store_services.srvdtl_id')
        ->leftJoin('services','services.srv_id','=','store_services.srv_id')
        ->where('store_services.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}