<?php

namespace App\Modules\BackOffice\Layanesia\Company\Logics;

use App\Modules\BackOffice\Layanesia\Company\Logics\CompanyDataManagerLogic;
use App\Modules\BackOffice\Layanesia\Company\Logics\CompanyPaginationLogic;
use App\Modules\BackOffice\Layanesia\Company\Queries\CompanyQuery;

class CompanyLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new CompanyPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new CompanyDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new CompanyQuery())->editQuery($id);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new CompanyDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @return [type] [description]
	 */
	public function doShow($id)
	{
		return (new CompanyQuery())->showQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new CompanyDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new CompanyQuery())->selectListQuery();
	}
}
