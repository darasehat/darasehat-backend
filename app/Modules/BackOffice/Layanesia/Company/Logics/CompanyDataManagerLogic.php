<?php

namespace App\Modules\BackOffice\Layanesia\Company\Logics;

use App\Modules\BackOffice\Layanesia\Company\Queries\CompanyField;
use App\Modules\BackOffice\Layanesia\Company\Queries\CompanyQuery;
use App\Modules\BackOffice\Layanesia\Company\Queries\CompanyDataManagerQuery;

class CompanyDataManagerLogic extends CompanyDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CompanyField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new CompanyQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new CompanyField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new CompanyQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new CompanyField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new CompanyQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new CompanyQuery())->setEntity());
	}
}
