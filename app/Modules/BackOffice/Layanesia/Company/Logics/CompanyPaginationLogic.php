<?php

namespace App\Modules\BackOffice\Layanesia\Company\Logics;
use App\Modules\BackOffice\Layanesia\Company\Queries\CompanyQuery;

class CompanyPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new CompanyQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
