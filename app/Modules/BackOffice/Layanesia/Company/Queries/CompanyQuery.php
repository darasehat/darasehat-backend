<?php

namespace App\Modules\BackOffice\Layanesia\Company\Queries;

use App\Modules\BackOffice\Layanesia\Company\Queries\CompanyEntity;

class CompanyQuery extends CompanyEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('cmp_id AS layId','cmp_name AS layName','cmp_logo AS layLogo','cmp_address AS layAddress','cmp_join_date AS layJoinDate','cou_name AS layCountry','adm1_name AS layProvince','adm2_name AS layCity')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','companies.adm2_id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','companies.adm1_id')
        ->leftJoin('countries','countries.cou_id','=','companies.cou_id')
        ->where('cmp_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('cmp_id AS layId','cmp_name AS layName','cmp_logo AS layLogo','cmp_address AS layAddress','cmp_join_date AS layJoinDate','cou_id AS layCountry','adm1_id AS layProvince','adm2_id AS layCity')
        ->where('cmp_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('cmp_name AS layName', 'cmp_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }
}