<?php

namespace App\Modules\BackOffice\Layanesia\Company\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('cmp_id AS layId','cmp_name AS layName','cmp_join_date AS layJoinDate','cou_name AS layCountry','adm1_name AS layProvince','adm2_name AS layCity')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','companies.adm2_id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','companies.adm1_id')
        ->leftJoin('countries','countries.cou_id','=','companies.cou_id')
        ->where('companies.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}