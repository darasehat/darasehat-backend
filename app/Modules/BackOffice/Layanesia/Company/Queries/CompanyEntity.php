<?php

namespace App\Modules\BackOffice\Layanesia\Company\Queries;

use App\Models\Layanesia\Company;

class CompanyEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Company());
    }
}