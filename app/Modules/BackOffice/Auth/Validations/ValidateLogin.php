<?php

namespace App\Modules\BackOffice\Auth\Validations;

use Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\BackOffice\Auth\LoginRequest;
// use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
// use App\Modules\BackOffice\User\Logics\UserLogic;

class ValidateLogin
{
    // public $formRequest;
    // public $user;
    // public $resp = '';

    /**
     * 
     */
    public function validate($request, $user)
    {

        $this->formRequest = $request->all();
        $this->formLogin = $request->only('email', 'password');
        $this->user = $user;
        Validator::make(
            $request->all(),
            [
                'email' => [
                    'required',
                    // 'email',
                    // 'max:100'.
                    function ($attribute, $value, $fail) {
                        // if(!Auth::attempt($this->formLogin))
                        // {
                        //     $fail('Email atau password salah');
                        // }
                        if(!$this->user)
                        {
                            $fail('Email tidak terdaftar');
                        }
                        // if(!$this->resp)
                        // {
                        //     $fail('Email atau password salah');
                        // }
                        // $employee = (new EmployeeLogic())->getEmailEmployeeList($value, ['usr_id']);
                        // if ($employee->count()>0) //existing
                        // {
                        //     $fail('Email sudah digunakan oleh karyawan lain');
                        // }
                        // else //baru
                        // {
                        //     if(!array_key_exists('layPassword', $this->formRequest))
                        //     {
                        //         $this->formRequest['layPassword'] = '';
                        //     }
                        // }
                    },
                ],
                // (new LoginRequest())->messages(),
                // (new LoginRequest())->attributes()
            ]
        )->validate();
        // if(array_key_exists('layPassword', $this->formRequest))
        // {
        //     if(!$request->layPassword)
        //     {
        //         $request->request->add(['layPassword' => '']);
        //         $request->merge(['layPassword' => '']);    
        //     }
        //     $user = (new UserLogic())->getUserEmail($this->formRequest['email'], ['id']);
        //     if(!$user)
        //     {
        //         Validator::make(
        //             $request->all(),
        //             [
        //                 'layPassword' => [
        //                     // 'sometimes',
        //                     'required',
        //                     'min:7',
        //                     'max:255',
        //                 ],
        //             ],
        //             (new LoginRequest())->messages(),
        //             (new LoginRequest())->attributes()
        //         )->validate();    
        //     }
        // }
    }
}
