<?php

namespace App\Modules\BackOffice\Operator\Logics;

use App\Modules\BackOffice\Operator\Logics\OperatorLogic;
use App\Modules\BackOffice\UserProfile\Logics\UserProfileLogic;

class OperatorProcessUpdate
{
    public function doUpdate($id, $request, $datetime)
    {
        $operator = (new OperatorLogic())->doUpdate($id, $request, $datetime);
		$request->request->add(['layUsrId' => $operator->id]);
        //search userprofile by user_id
        $userProfile = (new UserProfileLogic())->getUserId($operator->id, ['usrprf_id']);
        if($userProfile)
        {
            (new UserProfileLogic())->doUpdate($userProfile->usrprf_id,$request, $datetime);
        }
        else
        {
            (new UserProfileLogic())->doStore($request, $datetime);
        }
        $operator->assignRole('user_operator');
        return true;
    }
}
