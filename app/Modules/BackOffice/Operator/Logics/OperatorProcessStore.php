<?php

namespace App\Modules\BackOffice\Operator\Logics;

use App\Modules\BackOffice\Operator\Logics\OperatorLogic;
use App\Modules\BackOffice\UserProfile\Logics\UserProfileLogic;

class OperatorProcessStore
{
    public function doStore($request, $datetime)
    {
        $operator = (new OperatorLogic())->doStore($request, $datetime);
		$request->request->add(['layUsrId' => $operator->id]);
        (new UserProfileLogic())->doStore($request, $datetime);
        $operator->assignRole('user_operator');
        return true;
    }
}
