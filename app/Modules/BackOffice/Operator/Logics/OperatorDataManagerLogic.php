<?php

namespace App\Modules\BackOffice\Operator\Logics;

use Auth;
use App\Modules\BackOffice\Operator\Queries\OperatorField;
use App\Modules\BackOffice\Operator\Queries\OperatorQuery;
use App\Modules\BackOffice\Operator\Queries\OperatorDataManagerQuery;

class OperatorDataManagerLogic extends OperatorDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
			$datarequest = (new OperatorField())->setField($request);
			return $this->storeDataSave($datetime, $datarequest, (new OperatorQuery())->setEntity());
	}

	/**
	 * 
	 */
	public function storeSaveRandomPassword($request, $datetime)
	{
			$request->request->add(['is_active' => 1]);
			$datarequest = (new OperatorField())->setFieldRandomPassword($request);
			return $this->storeDataSave($datetime, $datarequest, (new OperatorQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
		$datarequest = (new OperatorField())->setFieldData($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new OperatorQuery())->setEntity());
	}

	/**
	 * [updatePasswordSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updatePasswordSave($id, $request, $datetime)
	{
		$datarequest = (new OperatorField())->setFieldPassword($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new OperatorQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
		$request->request->add(['is_active' => 0]);
		$datarequest = (new OperatorField())->setFieldData($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new OperatorQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new OperatorQuery())->setEntity());
	}
}
