<?php

namespace App\Modules\BackOffice\Operator\Logics;
use App\Modules\BackOffice\Operator\Queries\OperatorQuery;

class OperatorPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new OperatorQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
