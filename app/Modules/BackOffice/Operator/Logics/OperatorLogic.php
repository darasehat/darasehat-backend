<?php

namespace App\Modules\BackOffice\Operator\Logics;

use App\Modules\BackOffice\Operator\Logics\OperatorDataManagerLogic;
use App\Modules\BackOffice\Operator\Logics\OperatorPaginationLogic;
use App\Modules\BackOffice\Operator\Queries\OperatorQuery;
use Str;

class OperatorLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new OperatorPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$request->request->add(['layToken' => Str::random(255)]);
		return (new OperatorDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * 
	 */
	public function doStoreRandomPassword($request, $datetime)
	{
		$request->request->add(['layToken' => Str::random(255)]);
		return (new OperatorDataManagerLogic())->storeSaveRandomPassword($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new OperatorDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doUpdatePassword description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdatePassword($id, $request, $datetime)
	{
		return (new OperatorDataManagerLogic())->updatePasswordSave($id, $request, $datetime);
	}
	
	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new OperatorQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new OperatorQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new OperatorDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doAutoCompleteSelectList description]
	 * @param  [type] $query [description]
	 * @return [type]        [description]
	 */
	public function doAutoCompleteSelectList($query)
	{
		return (new OperatorQuery())->autoCompleteSelectListQuery($query);
	}

	/**
	 * [getOperatorEmail description]
	 * @param  [type] $email  [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getOperatorEmail($email, $select)
	{
		return (new OperatorQuery)->OperatorEmailQuery($email, $select);
	}

	/**
	 * 
	 */
	public function getRecord($id, $select)
	{
		return (new OperatorQuery())->recordQuery($id, $select);
	}
}
