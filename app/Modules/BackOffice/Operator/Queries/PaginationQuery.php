<?php

namespace App\Modules\BackOffice\Operator\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->role('user_operator')->select('id AS layId','name AS layName','email AS layEmail','usrprf_work_place AS layWorkPlace','usrprf_phone_no AS layPhoneNo')
        ->leftJoin('user_profiles','user_profiles.usr_id','=','users.id')
        ->where('users.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}