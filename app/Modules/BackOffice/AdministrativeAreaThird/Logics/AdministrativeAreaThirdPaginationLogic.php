<?php

namespace App\Modules\BackOffice\AdministrativeAreaThird\Logics;
use App\Modules\BackOffice\AdministrativeAreaThird\Queries\AdministrativeAreaThirdQuery;

class AdministrativeAreaThirdPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new AdministrativeAreaThirdQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
