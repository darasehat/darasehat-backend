<?php

namespace App\Modules\BackOffice\Layanesia\AdministrativeAreaThird\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('adm3_id AS layId','adm3_name AS layName','adm1_name AS layProvince','adm2_name AS layCity')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','administrative_area_thirds.adm2_id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','administrative_area_seconds.adm1_id')
        ->where('administrative_area_thirds.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}