<?php

namespace App\Modules\BackOffice\AdministrativeAreaThird\Queries;

use App\Modules\BackOffice\AdministrativeAreaThird\Queries\AdministrativeAreaThirdEntity;

class AdministrativeAreaThirdQuery extends AdministrativeAreaThirdEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('adm3_name AS layName', 'adm1_name AS layProvince', 'adm2_name AS layCity')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','administrative_area_thirds.adm2_id')
        ->leftJoin('administrative_area_firsts','administrative_area_firsts.adm1_id','=','administrative_area_seconds.adm1_id')
        ->where('adm3_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('adm3_name AS layName', 'adm1_id AS layProvince', 'administrative_area_thirds.adm2_id AS layCity')
        ->leftJoin('administrative_area_seconds','administrative_area_seconds.adm2_id','=','administrative_area_thirds.adm2_id')
        ->where('adm3_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery($adm2)
    {
        $query = $this->setEntity()
        ->select('adm3_name AS layName', 'adm3_id AS layCode')
        ->where('is_active','=','1')
        ->where('adm2_id','=',$adm2)
        ->orderBy('adm3_name')
        ->get();
        return $query;
    }
}