<?php

namespace App\Modules\BackOffice\AdministrativeAreaThird\Queries;

use App\Queries\General\FieldMap;

class AdministrativeAreaThirdField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layAdm2Id' => 'adm2_id',
            'layName' => 'adm3_name',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}