<?php

namespace App\Modules\BackOffice\AdministrativeAreaFirst\Queries;

use App\Models\AdministrativeAreaFirst;

class AdministrativeAreaFirstEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new AdministrativeAreaFirst());
    }
}