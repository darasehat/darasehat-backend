<?php

namespace App\Modules\BackOffice\AdministrativeAreaFirst\Queries;

use App\Modules\BackOffice\AdministrativeAreaFirst\Queries\AdministrativeAreaFirstEntity;

class AdministrativeAreaFirstQuery extends AdministrativeAreaFirstEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select('adm1_name AS layName', 'adm1_id AS layId')
        ->where('adm1_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select('adm1_name AS layName')
        ->where('adm1_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('adm1_name AS layName', 'adm1_id AS layCode')
        ->where('is_active','=','1')
        ->orderBy('adm1_name')
        ->get();
        return $query;
    }
}