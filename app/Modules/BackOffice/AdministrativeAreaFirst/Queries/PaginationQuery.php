<?php

namespace App\Modules\BackOffice\AdministrativeAreaFirst\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('adm1_id AS layId','adm1_name AS layName')
        ->where('administrative_area_firsts.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}