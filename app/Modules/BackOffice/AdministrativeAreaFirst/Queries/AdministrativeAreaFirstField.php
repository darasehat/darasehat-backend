<?php

namespace App\Modules\BackOffice\AdministrativeAreaFirst\Queries;

use App\Queries\General\FieldMap;

class AdministrativeAreaFirstField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layName' => 'adm1_name'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}