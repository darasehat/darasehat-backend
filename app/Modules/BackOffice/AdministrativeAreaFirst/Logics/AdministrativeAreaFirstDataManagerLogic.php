<?php

namespace App\Modules\BackOffice\AdministrativeAreaFirst\Logics;

use Auth;
use App\Modules\BackOffice\AdministrativeAreaFirst\Queries\AdministrativeAreaFirstField;
use App\Modules\BackOffice\AdministrativeAreaFirst\Queries\AdministrativeAreaFirstQuery;
use App\Modules\BackOffice\AdministrativeAreaFirst\Queries\AdministrativeAreaFirstDataManagerQuery;

class AdministrativeAreaFirstDataManagerLogic extends AdministrativeAreaFirstDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new AdministrativeAreaFirstField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new AdministrativeAreaFirstQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new AdministrativeAreaFirstField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new AdministrativeAreaFirstQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new AdministrativeAreaFirstField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new AdministrativeAreaFirstQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new AdministrativeAreaFirstQuery())->setEntity());
	}
}
