<?php

namespace App\Modules\BackOffice\AdministrativeAreaFirst\Logics;

use App\Modules\BackOffice\AdministrativeAreaFirst\Logics\AdministrativeAreaFirstDataManagerLogic;
use App\Modules\BackOffice\AdministrativeAreaFirst\Logics\AdministrativeAreaFirstPaginationLogic;
use App\Modules\BackOffice\AdministrativeAreaFirst\Queries\AdministrativeAreaFirstQuery;

class AdministrativeAreaFirstLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new AdministrativeAreaFirstPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		return (new AdministrativeAreaFirstDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new AdministrativeAreaFirstQuery())->editQuery($id);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new AdministrativeAreaFirstDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new AdministrativeAreaFirstQuery())->showQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new AdministrativeAreaFirstDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new AdministrativeAreaFirstQuery())->selectListQuery();
	}
}
