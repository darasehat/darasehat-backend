<?php

namespace App\Modules\BackOffice\AdministrativeAreaFirst\Logics;
use App\Modules\BackOffice\AdministrativeAreaFirst\Queries\AdministrativeAreaFirstQuery;

class AdministrativeAreaFirstPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new AdministrativeAreaFirstQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
