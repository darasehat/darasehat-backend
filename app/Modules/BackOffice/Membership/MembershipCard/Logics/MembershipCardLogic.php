<?php

namespace App\Modules\BackOffice\Membership\MembershipCard\Logics;

use App\Modules\BackOffice\Membership\MembershipCard\Logics\MembershipCardDataManagerLogic;
use App\Modules\BackOffice\Membership\MembershipCard\Logics\MembershipCardPaginationLogic;
use App\Modules\BackOffice\Membership\MembershipCard\Queries\MembershipCardQuery;

use App\Modules\BackOffice\Layanesia\Brand\Logics\BrandLogic;

class MembershipCardLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new MembershipCardPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		// $brand = (new BrandLogic)->getRecord($request->layBrndId, ['cmp_id']);
    // 	$request->request->add(['layCmpId' => $brand->cmp_id]);
		return (new MembershipCardDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		// $brand = (new BrandLogic)->getRecord($request->layBrndId, ['cmp_id']);
    // 	$request->request->add(['layCmpId' => $brand->cmp_id]);
		return (new MembershipCardDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new MembershipCardQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new MembershipCardQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new MembershipCardDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new MembershipCardQuery())->selectListQuery();
	}
}
