<?php

namespace App\Modules\BackOffice\Membership\MembershipCard\Logics;
use App\Modules\BackOffice\Membership\MembershipCard\Queries\MembershipCardQuery;

class MembershipCardPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new MembershipCardQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
