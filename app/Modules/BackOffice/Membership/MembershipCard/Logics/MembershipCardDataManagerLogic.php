<?php

namespace App\Modules\BackOffice\Membership\MembershipCard\Logics;

use App\Modules\BackOffice\Membership\MembershipCard\Queries\MembershipCardField;
use App\Modules\BackOffice\Membership\MembershipCard\Queries\MembershipCardQuery;
use App\Modules\BackOffice\Membership\MembershipCard\Queries\MembershipCardDataManagerQuery;

class MembershipCardDataManagerLogic extends MembershipCardDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new MembershipCardField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new MembershipCardQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new MembershipCardField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new MembershipCardQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new MembershipCardField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new MembershipCardQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new MembershipCardQuery())->setEntity());
	}
}
