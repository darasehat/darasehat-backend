<?php

namespace App\Modules\BackOffice\Membership\MembershipCard\Queries;

use App\Models\Membership\MembershipCard;

class MembershipCardEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new MembershipCard());
    }
}