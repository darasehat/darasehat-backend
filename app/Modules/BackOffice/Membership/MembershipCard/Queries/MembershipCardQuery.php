<?php

namespace App\Modules\BackOffice\Membership\MembershipCard\Queries;

use App\Modules\BackOffice\Membership\MembershipCard\Queries\MembershipCardEntity;

class MembershipCardQuery extends MembershipCardEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('cmp_name AS layCompany','brnd_name AS layMerchant','mbrscrd_name AS layName','mbrscrd_price AS layPrice')
        ->leftJoin('companies','companies.cmp_id','=','me_membership_cards.cmp_id')
        ->leftJoin('brands','brands.cmp_id','=','me_membership_cards.cmp_id')
        ->where('mbrscrd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('cmp_id AS layCompany','brnd_id AS layMerchant','mbrscrd_name AS layName','mbrscrd_price AS layPrice')
        ->where('mbrscrd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('mbrscrd_name AS layName', 'mbrscrd_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }
}