<?php

namespace App\Modules\BackOffice\Membership\MembershipCard\Queries;

use App\Queries\General\FieldMap;

class MembershipCardField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCmpId' => 'cmp_id',
            'layBrndId' => 'brnd_id',
            'layName' => 'mbrscrd_name',
            'layDescription' => 'mbrscrd_description',
            'layPrice' => 'mbrscrd_price',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}