<?php

namespace App\Modules\BackOffice\Membership\MembershipCard\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('mbrscrd_id AS layId','cmp_name AS layCompany','brnd_name AS layMerchant','mbrscrd_name AS layName','mbrscrd_price AS layPrice')
        ->leftJoin('companies','companies.cmp_id','=','me_membership_cards.cmp_id')
        ->leftJoin('brands','brands.cmp_id','=','me_membership_cards.cmp_id')
        ->where('me_membership_cards.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}