<?php

namespace App\Modules\BackOffice\Membership\Member\Queries;

use App\Modules\BackOffice\Membership\Member\Queries\MemberEntity;

class AlreadyMemberQuery extends MemberEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    public function alreadyMemberQuery($cmpid, $merid, $usrprfid)
    {
        $query = $this->setEntity()
        ->select('mbr_id','store_name')
        ->leftJoin('stores','stores.store_id','=','me_members.store_id')
        ->where('me_members.cmp_id','=',$cmpid)
        ->where('me_members.brnd_id','=',$merid)
        ->where('me_members.usrprf_id','=',$usrprfid)
        ->where('me_members.is_active','=',1)
        ->first();
        return $query;
    }
}