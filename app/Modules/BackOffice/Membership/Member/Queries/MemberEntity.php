<?php

namespace App\Modules\BackOffice\Membership\Member\Queries;

use App\Models\Membership\Member;

class MemberEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new Member());
    }
}