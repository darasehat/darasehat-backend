<?php

namespace App\Modules\BackOffice\Membership\Member\Queries;

use App\Modules\BackOffice\Membership\Member\Queries\MemberEntity;
use App\Modules\BackOffice\Membership\Member\Queries\PaginationQuery;

class MemberQuery extends MemberEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('mbr_id AS layId','cmp_name AS layCompany','brnd_name AS layMerchant','store_name AS layStore','mbr_no AS layNo','mbr_name AS layName','mbr_address AS layAddress','mbr_phone AS layPhone','mbr_pic AS layPic','mbr_reg_date AS layRegDate','email AS layEmail')
        ->leftJoin('companies','companies.cmp_id','=','me_members.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','me_members.brnd_id')
        ->leftJoin('stores','stores.store_id','=','me_members.store_id')
        ->leftJoin('users','users.id','=','me_members.usr_id')
        ->where('mbr_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('mbr_id AS layId','cmp_id AS layCompany','brnd_id AS layMerchant','store_id AS layStore','mbr_no AS layNo','mbr_name AS layName','mbr_address AS layAddress','mbr_phone AS layPhone','mbr_pic AS layPic','mbr_reg_date AS layRegDate','usrprf_id AS layUsrprf')
        ->where('mbr_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('mbr_name AS layName', 'mbr_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [recordQuery description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('mbr_id','=',$id)
        ->first();
        return $query;        
    }

    /**
     * [memberQuery description]
     * @param  [type] $companyid [description]
     * @param  [type] $brandid   [description]
     * @param  [type] $profileid [description]
     * @param  [type] $select    [description]
     * @return [type]            [description]
     */
    public function memberQuery($companyid, $brandid, $profileid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('cmp_id','=',$companyid)
        ->where('brnd_id','=',$brandid)
        ->where('usrprf_id','=',$profileid)
        ->first();
        return $query;
    }
}