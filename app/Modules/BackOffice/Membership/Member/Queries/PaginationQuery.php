<?php

namespace App\Modules\BackOffice\Membership\Member\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('mbr_id AS layId','cmp_name AS layCompany','brnd_name AS layMerchant','store_name AS layStore','mbr_no AS layNo','mbr_name AS layName','mbr_address AS layAddress','mbr_phone AS layPhone','mbr_pic AS layPic','mbr_reg_date AS layRegDate','email AS layEmail')
        ->leftJoin('companies','companies.cmp_id','=','me_members.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','me_members.brnd_id')
        ->leftJoin('stores','stores.store_id','=','me_members.store_id')
        ->leftJoin('users','users.id','=','me_members.usr_id')
        ->where('me_members.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}