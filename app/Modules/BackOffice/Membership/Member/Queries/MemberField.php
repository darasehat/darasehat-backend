<?php

namespace App\Modules\BackOffice\Membership\Member\Queries;

use App\Queries\General\FieldMap;

class MemberField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCmpId' => 'cmp_id',
            'layBrndId' => 'brnd_id',
            'layStoreId' => 'store_id',
            'layUsrId' => 'usr_id',
            'layUsrprfId' => 'usrprf_id',
            'layNo' => 'mbr_no',
            'layName' => 'mbr_name',
            'layAddress' => 'mbr_address',
            'layPhone' => 'mbr_phone',
            'layPic' => 'mbr_pic',
            'layRegDate' => 'mbr_reg_date',
            'layRegTime' => 'mbr_reg_time',
            'layRegDatetime' => 'mbr_reg_datetime',
            'layBirthDate' => 'mbr_birth_date',
            'layGndId' => 'gnd_id'
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}