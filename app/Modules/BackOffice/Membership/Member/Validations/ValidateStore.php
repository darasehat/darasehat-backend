<?php

namespace App\Modules\BackOffice\Membership\Member\Validations;

use App\Modules\BackOffice\Membership\Member\Logics\MemberLogic;
use App\Modules\BackOffice\Membership\Member\Queries\AlreadyMemberQuery;
use App\Modules\BackOffice\Layanesia\UserProfile\Logics\UserProfileLogic;

class ValidateStore
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function validate($request)
	{
        // message: "The given data was invalid.", errors: {layEmail: ["Email sudah terdaftar"]}
        $iserror = false;
		$errors = [];
		$member = $this->alreadyMember($request);
		if($member)
		{
	        $iserror = true;
			$errors['layUsrprfId'][0] = 'Sudah terdaftar di '.$member->store_name;
		}
		if($iserror)
		{
			return [
				'message' => 'The given data was invalid.',
				'errors' => $errors
			];
		}
	}

	private function alreadyMember($request)
	{
		return (new AlreadyMemberQuery())->alreadyMemberQuery($request->layCmpId, $request->layBrndId, $request->layUsrprfId);
	}
}
