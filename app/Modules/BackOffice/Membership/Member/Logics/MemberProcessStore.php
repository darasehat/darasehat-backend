<?php

namespace App\Modules\BackOffice\Membership\Member\Logics;

use App\Modules\BackOffice\Membership\Member\Logics\MemberDataManagerLogic;
use App\Modules\BackOffice\Membership\Member\Logics\MemberPaginationLogic;
use App\Modules\BackOffice\Membership\Member\Queries\MemberQuery;

use App\Modules\BackOffice\Layanesia\Brand\Logics\BrandLogic;
use App\Modules\BackOffice\Layanesia\UserProfile\Logics\UserProfileLogic;

class MemberProcessStore
{
	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		$brand = (new BrandLogic)->getRecord($request->layBrndId, ['cmp_id']);
    $request->request->add(['layCmpId' => $brand->cmp_id]);
		$profile = (new UserProfileLogic())->getRecord($request->layUsrprfId,['usr_id']);
    $request->request->add(['layUsrId' => $profile->usr_id]);
		return (new MemberLogic())->doStore($request, $datetime);
	}
}
