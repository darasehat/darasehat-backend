<?php

namespace App\Modules\BackOffice\Membership\Member\Logics;
use App\Modules\BackOffice\Membership\Member\Queries\MemberQuery;

class MemberPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new MemberQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
