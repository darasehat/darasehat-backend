<?php

namespace App\Modules\BackOffice\Membership\Member\Logics;

use App\Modules\BackOffice\Membership\Member\Queries\MemberField;
use App\Modules\BackOffice\Membership\Member\Queries\MemberQuery;
use App\Modules\BackOffice\Membership\Member\Queries\MemberDataManagerQuery;

class MemberDataManagerLogic extends MemberDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function storeSave($request, $datetime)
    {
        $request->request->add(['is_active' => 1]);
        $datarequest = (new MemberField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new MemberQuery())->setEntity());
    }

    /**
     * [updateSave description]
     * @param  [type] $id       [description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function updateSave($id, $request, $datetime)
    {
        $request->request->add(['is_active' => 1]);
        $datarequest = (new MemberField())->setField($request);
        return $this->updateDataSave($id, $datetime, $datarequest, (new MemberQuery())->setEntity());
    }

    /**
     * [delete description]
     * @param  [type] $id       [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function delete($id, $request, $datetime)
    {
        $request->request->add(['is_active' => 0]);
        $datarequest = (new MemberField())->setField($request);
        return $this->deleteDataSave($id, $datetime, $datarequest, (new MemberQuery())->setEntity());
    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function destroy($id)
    {
        return $this->destroyData($id, (new MemberQuery())->setEntity());
    }
}
