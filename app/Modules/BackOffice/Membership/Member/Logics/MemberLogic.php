<?php

namespace App\Modules\BackOffice\Membership\Member\Logics;

use App\Modules\BackOffice\Membership\Member\Logics\MemberDataManagerLogic;
use App\Modules\BackOffice\Membership\Member\Logics\MemberPaginationLogic;
use App\Modules\BackOffice\Membership\Member\Queries\MemberQuery;

class MemberLogic
{
    /**
     * [getPaginationData description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    public function getPaginationData($request)
    {
        return (new MemberPaginationLogic())->getPaginationData($request);
    }

    /**
     * [doStore description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doStore($request, $datetime)
    {
        $request->request->add(['layRegDatetime' => $request->layRegDate.' '.$request->layRegTime]);
        return (new MemberDataManagerLogic())->storeSave($request, $datetime);
    }

    /**
     * [doUpdate description]
     * @param  [type] $id       [description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doUpdate($id, $request, $datetime)
    {
        $request->request->add(['layRegDatetime' => $request->layRegDate.' '.$request->layRegTime]);
        return (new MemberDataManagerLogic())->updateSave($id, $request, $datetime);
    }

    /**
     * [doShow description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function doShow($id)
    {
        return (new MemberQuery())->showQuery($id);
    }

    /**
     * [doEdit description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function doEdit($id)
    {
        return (new MemberQuery())->editQuery($id);
    }

    /**
     * [doDelete description]
     * @param  [type] $id       [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
    public function doDelete($id, $request, $datetime)
    {
        return (new MemberDataManagerLogic())->delete($id, $request, $datetime);
    }

    /**
     * [doSelectList description]
     * @return [type] [description]
     */
    public function doSelectList()
    {
        return (new MemberQuery())->selectListQuery();
    }

    /**
     * [getRecord description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function getRecord($id, $select)
    {
        return (new MemberQuery())->recordQuery($id, $select);
    }

    /**
     * [getBrandMember description]
     * @param  [type] $company [description]
     * @param  [type] $brand   [description]
     * @param  [type] $profile [description]
     * @return [type]          [description]
     */
    public function getBrandMember($company, $brand, $profile, $select)
    {
        return (new MemberQuery())->memberQuery($company, $brand, $profile, $select);
    }
}
