<?php

namespace App\Modules\BackOffice\Membership\MemberCard\Logics;

use App\Modules\BackOffice\Membership\MemberCard\Logics\MemberCardDataManagerLogic;
use App\Modules\BackOffice\Membership\MemberCard\Logics\MemberCardPaginationLogic;
use App\Modules\BackOffice\Membership\MemberCard\Queries\MemberCardQuery;

// use App\Modules\BackOffice\Membership\Member\Logics\MemberLogic;
use App\Modules\BackOffice\Membership\Member\Queries\AlreadyMemberQuery;

class MemberCardLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		return (new MemberCardPaginationLogic())->getPaginationData($request);
	}

	/**
	 * [doStore description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doStore($request, $datetime)
	{
		// $member = (new AlreadyMemberQuery())->alreadyMemberQuery($request->layCmpId, $request->layBrndId, $request->layUsrprfId);
		// $request->request->add(['layMbrId' => $member->mbr_id]);
		// $request->request->add(['layRegDatetime' => $request->layRegDate]);
		// if($request->layRegTime)
		// {
		// 	$request->request->add(['layRegDatetime' => $request->layRegDate.' '.$request->layRegTime]);
		// }
		// $request->request->add(['layActiveUntilDatetime' => $request->layActiveUntilDate]);
		// if($request->layActiveUntilTime)
		// {
		// 	$request->request->add(['layActiveUntilDatetime' => $request->layActiveUntilDate.' '.$request->layActiveUntilTime]);
		// }
		return (new MemberCardDataManagerLogic())->storeSave($request, $datetime);
	}

	/**
	 * [doUpdate description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doUpdate($id, $request, $datetime)
	{
		return (new MemberCardDataManagerLogic())->updateSave($id, $request, $datetime);
	}

	/**
	 * [doShow description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doShow($id)
	{
		return (new MemberCardQuery())->showQuery($id);
	}

	/**
	 * [doEdit description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function doEdit($id)
	{
		return (new MemberCardQuery())->editQuery($id);
	}

	/**
	 * [doDelete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function doDelete($id, $request, $datetime)
	{
		return (new MemberCardDataManagerLogic())->delete($id, $request, $datetime);
	}

	/**
	 * [doSelectList description]
	 * @return [type] [description]
	 */
	public function doSelectList()
	{
		return (new MemberCardQuery())->selectListQuery();
	}

	/**
	 * [getRecord description]
	 * @param  [type] $id     [description]
	 * @param  [type] $select [description]
	 * @return [type]         [description]
	 */
	public function getRecord($id, $select)
	{
		return (new MemberCardQuery())->recordQuery($id, $select);
	}

	/**
	 * [getMemberCardRecord description]
	 * @param  [type] $mbrid     [description]
	 * @param  [type] $mbrscrdid [description]
	 * @param  [type] $select    [description]
	 * @return [type]            [description]
	 */
	public function getMemberCardRecord($mbrid, $mbrscrdid, $select)
	{
		return (new MemberCardQuery())->memberCardRecordQuery($mbrid, $mbrscrdid, $select);
	}
}
