<?php

namespace App\Modules\BackOffice\Membership\MemberCard\Logics;

use App\Modules\BackOffice\Membership\MemberCard\Queries\MemberCardField;
use App\Modules\BackOffice\Membership\MemberCard\Queries\MemberCardQuery;
use App\Modules\BackOffice\Membership\MemberCard\Queries\MemberCardDataManagerQuery;

class MemberCardDataManagerLogic extends MemberCardDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new MemberCardField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new MemberCardQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new MemberCardField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new MemberCardQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new MemberCardField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new MemberCardQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new MemberCardQuery())->setEntity());
	}
}
