<?php

namespace App\Modules\BackOffice\Membership\MemberCard\Logics;
use App\Modules\BackOffice\Membership\MemberCard\Queries\MemberCardQuery;

class MemberCardPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new MemberCardQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
