<?php

namespace App\Modules\BackOffice\Membership\MemberCard\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select('mbrcrd_id AS layId','cmp_name AS layCompany','brnd_name AS layMerchant','store_name AS layStore','email AS layEmail','mbrscrd_name AS layCard','mbrcrd_reg_date AS layRegDate','mbrcrd_active_until_date AS layActiveUntilDate','mbrcrd_price AS layPrice')
        ->leftJoin('companies','companies.cmp_id','=','me_member_cards.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','me_member_cards.brnd_id')
        ->leftJoin('stores','stores.store_id','=','me_member_cards.store_id')
        ->leftJoin('me_members','me_members.mbr_id','=','me_member_cards.mbr_id')
        ->leftJoin('users','users.id','=','me_members.usr_id')
        ->leftJoin('me_membership_cards','me_membership_cards.mbrscrd_id','=','me_member_cards.mbrscrd_id')
        ->where('me_member_cards.is_active','=',1)
        ->paginate($rowsPerPage);
    }
}