<?php

namespace App\Modules\BackOffice\Membership\MemberCard\Queries;

use App\Queries\General\FieldMap;

class MemberCardField extends FieldMap
{
    /**
     * [setField description]
     * @param [type] $request [description]
     */
    public function setField($request)
    {
        $fieldmap = [
            'status' => 'status',
            'is_active' => 'is_active',
            'layCmpId' => 'cmp_id',
            'layBrndId' => 'brnd_id',
            'layStoreId' => 'store_id',
            'layMbrId' => 'mbr_id',
            'layMbrscrdId' => 'mbrscrd_id',
            'layRegDate' => 'mbrcrd_reg_date',
            'layRegTime' => 'mbrcrd_reg_time',
            'layRegDatetime' => 'mbrcrd_reg_datetime',
            'layActiveUntilDate' => 'mbrcrd_active_until_date',
            'layActiveUntilTime' => 'mbrcrd_active_until_time',
            'layActiveUntilDatetime' => 'mbrcrd_active_until_datetime',
            'layPrice' => 'mbrcrd_price',
        ];
        $data = [];
        $data = $this->setFieldMap($request->request, $fieldmap);
        return $data;
    }
}