<?php

namespace App\Modules\BackOffice\Membership\MemberCard\Queries;

use App\Models\Membership\MemberCard;

class MemberCardEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new MemberCard());
    }
}