<?php

namespace App\Modules\BackOffice\Membership\MemberCard\Queries;

use App\Modules\BackOffice\Membership\MemberCard\Queries\MemberCardEntity;

class MemberCardQuery extends MemberCardEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->setEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->setEntity()
        ->select('mbrcrd_id AS layId','cmp_name AS layCompany','brnd_name AS layMerchant','store_name AS layStore','email AS layEmail','mbrscrd_name AS layCard','mbrcrd_reg_date AS layRegDate','mbrcrd_active_until_date AS layActiveUntilDate','mbrcrd_price AS layPrice')
        ->leftJoin('companies','companies.cmp_id','=','me_member_cards.cmp_id')
        ->leftJoin('brands','brands.brnd_id','=','me_member_cards.brnd_id')
        ->leftJoin('stores','stores.store_id','=','me_member_cards.store_id')
        ->leftJoin('me_members','me_members.mbr_id','=','me_member_cards.mbr_id')
        ->leftJoin('users','users.id','=','me_members.usr_id')
        ->leftJoin('me_membership_cards','me_membership_cards.mbrscrd_id','=','me_member_cards.mbrscrd_id')
        ->where('mbrcrd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->setEntity()
        ->select('mbrcrd_id AS layId','me_member_cards.cmp_id AS layCompany','me_member_cards.brnd_id AS layMerchant','me_member_cards.store_id AS layStore','mbrscrd_id AS layMbrscrd','mbrcrd_reg_date AS layRegDate','mbrcrd_active_until_date AS layActiveUntilDate','usrprf_id AS layUsrprf','mbrcrd_price AS layPrice')
        ->leftJoin('me_members','me_members.mbr_id','=','me_member_cards.mbr_id')
        ->where('mbrcrd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [selectListQuery description]
     * @return [type] [description]
     */
    public function selectListQuery()
    {
        $query = $this->setEntity()
        ->select('mbrcrd_name AS layName', 'mbrcrd_id AS layCode')
        ->where('is_active','=','1')
        ->get();
        return $query;
    }

    /**
     * [recordQuery description]
     * @param  [type] $id     [description]
     * @param  [type] $select [description]
     * @return [type]         [description]
     */
    public function recordQuery($id, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('mbrcrd_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [memberCardRecordQuery description]
     * @param  [type] $mbrid     [description]
     * @param  [type] $mbrscrdid [description]
     * @param  [type] $select    [description]
     * @return [type]            [description]
     */
    public function memberCardRecordQuery($mbrid, $mbrscrdid, $select)
    {
        $query = $this->setEntity()
        ->select($select)
        ->where('is_active','=','1')
        ->where('mbr_id','=',$mbrid)
        ->where('mbrscrd_id','=',$mbrscrdid)
        ->first();
        return $query;
    }
}