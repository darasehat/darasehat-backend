<?php

namespace App\Modules\BackOffice\News\Queries;

class PaginationQuery
{
    public function getPaginationQuery($entity, $rowsPerPage = 5)
    {
        return $entity->select(
            'news_id AS layId',
            'news_title AS layTitle',
            'news_description AS layDescription',
            'news_pic_url AS layPicUrl'
        )
        ->where('is_active','=',1)
        ->paginate($rowsPerPage);
    }
}