<?php

namespace App\Modules\BackOffice\News\Queries;

use App\Models\News;

class NewsEntity
{
    /**
     * [newEntity description]
     * @return [type] [description]
     */
    protected function newEntity()
    {
        return (new News());
    }
}