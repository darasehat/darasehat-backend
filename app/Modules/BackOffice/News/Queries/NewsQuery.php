<?php

namespace App\Modules\BackOffice\News\Queries;

use App\Modules\BackOffice\News\Queries\NewsEntity;

class NewsQuery extends NewsEntity
{
    /**
     * [setEntity description]
     */
    public function setEntity()
    {
        return $this->newEntity();
    }

    /**
     * [getPaginationQuery description]
     * @param  integer $rowsPerPage [description]
     * @return [type]               [description]
     */
    public function getPaginationQuery($rowsPerPage = 5)
    {
        return (new PaginationQuery())->getPaginationQuery($this->newEntity(), $rowsPerPage);
    }

    /**
     * [showQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function showQuery($id)
    {
        $query = $this->newEntity()
        ->select(
            'news_id AS layId',
            'news_title AS layTitle',
            'news_description AS layDescription',
            'news_pic_url AS layPicUrl'
        )
        ->where('news_id','=',$id)
        ->first();
        return $query;
    }

    /**
     * [editQuery description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function editQuery($id)
    {
        $query = $this->newEntity()
        ->select(
            'news_id AS layId',
            'news_title AS layTitle',
            'news_description AS layDescription',
            'news_pic_url AS layPicUrl'
        )
        ->where('news_id','=',$id)
        ->first();
        return $query;
    }
}