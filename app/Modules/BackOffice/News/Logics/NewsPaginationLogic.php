<?php

namespace App\Modules\BackOffice\News\Logics;
use App\Modules\BackOffice\News\Queries\NewsQuery;

class NewsPaginationLogic
{
	/**
	 * [getPaginationData description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function getPaginationData($request)
	{
		$rowsPerPage = ($request->rowsPerPage);
		if($rowsPerPage == 0) {
			$rowsPerPage = 5;
		}
		$result = (new NewsQuery())->getPaginationQuery($rowsPerPage);
    	return $result;
	}
}
