<?php

namespace App\Modules\BackOffice\News\Logics;

use App\Modules\BackOffice\News\Queries\NewsField;
use App\Modules\BackOffice\News\Queries\NewsQuery;
use App\Modules\BackOffice\News\Queries\NewsDataManagerQuery;

class NewsDataManagerLogic extends NewsDataManagerQuery
{
    public function __construct()
    {
    }

    /**
     * [storeSave description]
     * @param  [type] $request  [description]
     * @param  [type] $datetime [description]
     * @return [type]           [description]
     */
	public function storeSave($request, $datetime)
	{
    	$request->request->add(['is_active' => 1]);
        $datarequest = (new NewsField())->setField($request);
        return $this->storeDataSave($datetime, $datarequest, (new NewsQuery())->setEntity());
	}

	/**
	 * [updateSave description]
	 * @param  [type] $id       [description]
	 * @param  [type] $request  [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function updateSave($id, $request, $datetime)
	{
		$request->request->add(['is_active' => 1]);
		$datarequest = (new NewsField())->setField($request);
		return $this->updateDataSave($id, $datetime, $datarequest, (new NewsQuery())->setEntity());
	}

	/**
	 * [delete description]
	 * @param  [type] $id       [description]
	 * @param  [type] $datetime [description]
	 * @return [type]           [description]
	 */
	public function delete($id, $request, $datetime)
	{
    	$request->request->add(['is_active' => 0]);
        $datarequest = (new NewsField())->setField($request);
		return $this->deleteDataSave($id, $datetime, $datarequest, (new NewsQuery())->setEntity());
	}

	/**
	 * [destroy description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function destroy($id)
	{
		return $this->destroyData($id, (new NewsQuery())->setEntity());
	}
}
