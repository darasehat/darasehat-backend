<?php

namespace App\Modules\BackOffice\News\Logics;

use File;
use Illuminate\Support\Facades\Storage;
use App\Modules\BackOffice\News\Logics\NewsLogic;

class NewsProcessStore
{
    public function doStore($request, $datetime)
    {
        $gymMember = (new NewsLogic())->doStore($request, $datetime);

        // $this->storePicture($request, $gymMember, $member, $datetime);
        return $gymMember;
    }
    
    private function storePicture($request, $gymMember, $member, $datetime)
    {
        if($request->layFiles)
		{
            // $str = str_replace("data:image/*;charset=utf-8;base64,", "", $request->layFiles);
            // data:image/jpeg;base64
            // $base64File = base64_decode($request->layFiles);
            // $name = md5($request->layFiles).".jpeg";
            // Storage::disk('gym_member')->put($name, $base64File);
            if(file_exists(config('filesystems.disks.gym_member.root').'/'.$gymMember->gymbr_id.'/'. $gymMember->gymbr_pic) && ($gymMember->gymbr_pic))
            {
                File::delete(config('filesystems.disks.gym_member.root').'/'.$gymMember->gymbr_id.'/'. $gymMember->gymbr_pic);
            }
            $name = $request->layFiles->hashName();
            Storage::disk('gym_member')->put($gymMember->gymbr_id, $request->layFiles);

            //thumb
            if(file_exists(config('filesystems.disks.gym_member.thumb')).'/'. $gymMember->gymbr_pic)
            {
                File::delete(config('filesystems.disks.gym_member.thumb').'/'. $gymMember->gymbr_pic);
            }
            Storage::disk('gym_member_thumb')->put($gymMember->gymbr_id, $request->layFiles);
            $path = config('filesystems.disks.gym_member_thumb.root').'/'.$gymMember->gymbr_id.'/'. $name;
            \Image::make($request->layFiles)
            ->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path);
            
            $request->request->add(['layPic' => $name]);
            (new NewsLogic())->doUpdate($gymMember->gymbr_id, $request, $datetime);

            return $request;
        }
        else{
            return $request;
        }
    }
}
