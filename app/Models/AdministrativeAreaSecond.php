<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdministrativeAreaSecond extends Model
{
    protected $primaryKey = 'adm2_id';

	public function level1()
	{
        return $this->belongsTo('App\Models\AdministrativeAreaFirst','adm1_id','adm1_id');
    }
}
