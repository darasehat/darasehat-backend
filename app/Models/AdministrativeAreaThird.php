<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdministrativeAreaThird extends Model
{
    protected $primaryKey = 'adm3_id';

	public function area2()
	{
        return $this->belongsTo('App\Models\AdministrativeAreaSecond','adm2_id','adm2_id');
    }
}
