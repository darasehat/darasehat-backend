<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $primaryKey = 'news_id';

    public function createdBy()
    {
        return $this->belongsTo('App\Models\User','createdby','id');
    }

    public function changedBy()
    {
        return $this->belongsTo('App\Models\User','changedby','id');
    }
}
