<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicineRecord extends Model
{
    protected $primaryKey = 'medrec_id';

    /**
     * [user description]
     * @return [type] [description]
     */
	public function user()
	{
        return $this->belongsTo('App\Models\User','usr_id','id');
    }

	public function userProfile()
	{
        return $this->belongsTo('App\Models\UserProfile','usrprf_id','usrprf_id');
    }

    /**
     * [area1 description]
     * @return [type] [description]
     */
	public function area1()
	{
        return $this->belongsTo('App\Models\AdministrativeAreaFirst','adm1_id','adm1_id');
    }

    /**
     * [area2 description]
     * @return [type] [description]
     */
	public function area2()
	{
        return $this->belongsTo('App\Models\AdministrativeAreaSecond','adm2_id','adm2_id');
    }

    /**
     * [area3 description]
     * @return [type] [description]
     */
	public function area3()
	{
        return $this->belongsTo('App\Models\AdministrativeAreaThird','adm3_id','adm3_id');
    }

    /**
     * [area4 description]
     * @return [type] [description]
     */
	public function area4()
	{
        return $this->belongsTo('App\Models\AdministrativeAreaFourth','adm4_id','adm4_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\User','createdby','id');
    }

    public function changedBy()
    {
        return $this->belongsTo('App\Models\User','changedby','id');
    }

}
