<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdministrativeAreaFourth extends Model
{
    protected $primaryKey = 'adm4_id';

	public function level3()
	{
        return $this->belongsTo('App\Models\AdministrativeAreaThird','adm3_id','adm3_id');
    }
}
