<?php

namespace App\Utilities;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProxyRequest
{
    public function grantPasswordToken(string $email, string $password)
    {
        $params = [
            'grant_type' => 'password',
            'username' => $email,
            'password' => $password,
        ];

        return $this->makePostRequest($params);
    }

    public function refreshAccessToken()
    {
        $refreshToken = request()->cookie('refresh_token');

        abort_unless($refreshToken, 403, 'Your refresh token is expired.');

        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken,
        ];

        return $this->makePostRequest($params);
    }

    protected function makePostRequest(array $params)
    {
        $params = array_merge([
            'client_id' => config('services.passport.password_client_id'),
            'client_secret' => config('services.passport.password_client_secret'),
            'scope' => '*',
        ], $params);
        $http = new Client();
        $response = $http->post(url('oauth/token'),['form_params' => $params]);
        // $response = $http->post(url('oauth/token'), [
        //     'form_params' => [
        //         'grant_type' => 'refresh_token',
        //         'refresh_token' => $params['refresh_token'],
        //         'client_id' => config('services.passport.password_client_id'),
        //         'client_secret' => config('services.passport.password_client_secret'),
        //         'scope' => '',
        //     ],
        // ]);
        $resp = json_decode((string) $response->getBody(), true);
        $this->setHttpOnlyCookie($resp['refresh_token']);
        return $resp;

        // $proxy = \Request::create('oauth/token', 'post', $params);
        // $resp = json_decode(app()->handle($proxy)->getContent());
        // $this->setHttpOnlyCookie($resp->refresh_token);
        // return $resp;
    }

    protected function setHttpOnlyCookie(string $refreshToken)
    {
        cookie()->queue(
            'refresh_token',
            $refreshToken,
            14400, // 10 days
            null,
            null,
            false,
            true // httponly
        );
    }
}