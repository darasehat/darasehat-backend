<?php

namespace App\Http\Requests\BackOffice\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function attributes()
    {
        return [
            'email' => 'Email',
            'password' => 'Password'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => ':attribute harus diisi',
            'email.email' => ':attribute harus menggunakan format email yang benar (contoh:email@domain.com)',
            'email.max' => 'Maksimal :max karakter',
            'password.required' => ':attribute harus diisi',
            'password.min' => ':attribute minimal :min',
            'password.max' => ':attribute maksimal :max'
        ];
    }
}
