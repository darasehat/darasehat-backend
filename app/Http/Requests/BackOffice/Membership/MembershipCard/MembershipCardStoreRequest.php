<?php

namespace App\Http\Requests\BackOffice\Membership\MembershipCard;

use Illuminate\Foundation\Http\FormRequest;

class MembershipCardStoreRequest extends FormRequest
{
    /**
     * Determine if the MembershipCard is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layCmpId' => 'required',
            'layBrndId' => 'required',
            'layName' => 'required|max:255',
            'layPrice' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layCmpId' => 'Perusahaan',
            'layBrndId' => 'Pedagang',
            'layName' => 'Nama',
            'layPrice' => 'Harga',
        ];
    }

    public function messages()
    {
        return [
            'layCmpId.required' => ':attribute harus diisi',
            'layBrndId.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layPrice.required' => ':attribute harus diisi',
        ];
    }
}
