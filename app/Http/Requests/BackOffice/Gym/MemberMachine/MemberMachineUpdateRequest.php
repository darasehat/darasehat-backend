<?php

namespace App\Http\Requests\BackOffice\Gym\MemberMachine;

use Illuminate\Foundation\Http\FormRequest;

class MemberMachineUpdateRequest  extends FormRequest
{
    /**
     * Determine if the Member is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layEmail' => 'required|email|unique:App\Models\User,email|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'layEmail' => 'Email',
        ];
    }

    public function messages()
    {
        return [
            'layEmail.required' => ':attribute harus diisi',
            'layEmail.max' => 'Maksimal :max karakter',
        ];
    }
}
