<?php

namespace App\Http\Requests\BackOffice\Gym\GymInvoice;

use Illuminate\Foundation\Http\FormRequest;

class GymInvoiceStoreRequest extends FormRequest
{
    /**
     * Determine if the GymInvoice is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layNo' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'layNo' => 'Quantity',
        ];
    }

    public function messages()
    {
        return [
            'layNo.required' => ':attribute harus diisi',
        ];
    }
}
