<?php

namespace App\Http\Requests\BackOffice\Gym\GymInvoiceSubscription;

use Illuminate\Foundation\Http\FormRequest;

class GymInvoiceSubscriptionStoreRequest extends FormRequest
{
    /**
     * Determine if the GymInvoiceSubscription is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layQuantity' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'layQuantity' => 'Quantity',
        ];
    }

    public function messages()
    {
        return [
            'layQuantity.required' => ':attribute harus diisi',
        ];
    }
}
