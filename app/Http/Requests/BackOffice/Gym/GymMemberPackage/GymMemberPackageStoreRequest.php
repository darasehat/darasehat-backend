<?php

namespace App\Http\Requests\BackOffice\Gym\GymMemberPackage;

use Illuminate\Foundation\Http\FormRequest;

class GymMemberPackageStoreRequest extends FormRequest
{
    /**
     * Determine if the GymMemberPackage is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'layBrndId' => 'required',
            'layGymbrId' => 'required',
            'layGympkgId' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            // 'layBrndId' => 'Merek',
            'layGymbrId' => 'Anggota',
            'layGympkgId' => 'Paket',
        ];
    }

    public function messages()
    {
        return [
            // 'layBrndId.required' => ':attribute harus diisi',
            'layGymbrId.required' => ':attribute harus diisi',
            'layGympkgId.required' => ':attribute harus diisi',
        ];
    }
}
