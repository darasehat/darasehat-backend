<?php

namespace App\Http\Requests\BackOffice\Gym\GymMember;

use Illuminate\Foundation\Http\FormRequest;

class GymMemberUpdateRequest  extends FormRequest
{
    /**
     * Determine if the Member is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->layEmail !== $this->layEmailEdit)
        {
            return [
                'layEmail' => 'required|email|unique:App\Models\User,email|max:255',
                'layName' => 'required|max:255',
            ];
        }
        else
        {
            return [
                'layName' => 'required|max:255'
            ];
        }
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layEmail' => 'Email',
            'layPassword' => 'Kata Kunci',
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
        ];
    }
}
