<?php

namespace App\Http\Requests\BackOffice\Gym\GymMember;

use Illuminate\Foundation\Http\FormRequest;

class GymMemberStoreRequest extends FormRequest
{
    /**
     * Determine if the Member is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(($this->layEmail) && (!$this->layEmailOld))
        {
            return [
                'layEmail' => 'required|email|unique:App\Models\User,email|max:255',
                // 'layPassword' => 'required|min:7|max:255',
                'layName' => 'required|max:255',
            ];
        }
        else if(!($this->layEmail) && ($this->layEmailOld))
        {
            return [
                'layName' => 'required|max:255'
            ];
        }
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layEmail' => 'Email',
            // 'layPassword' => 'Kata Kunci',
        ];
    }

    public function messages()
    {
        return [
            'layEmail.required' => ':attribute harus diisi',
            'layEmail.max' => 'Maksimal :max karakter',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            // 'layPassword.required' => ':attribute harus diisi',
        ];
    }
}
