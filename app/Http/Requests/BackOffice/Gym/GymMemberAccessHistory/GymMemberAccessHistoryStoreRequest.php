<?php

namespace App\Http\Requests\BackOffice\Gym\GymMemberAccessHistory;

use Illuminate\Foundation\Http\FormRequest;

class GymMemberAccessHistoryStoreRequest extends FormRequest
{
    /**
     * Determine if the GymMemberAccessHistory is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'layBrndId' => 'required',
            'layGymbrId' => 'required',
            'layGympkgId' => 'required',
            'layInDate' => 'required|date_format:Y-m-d',
            'layInTime' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            // 'layBrndId' => 'Merek',
            'layGymbrId' => 'Anggota Gym',
            'layGympkgId' => 'Paket',
            'layInDate' => 'Tanggal Masuk',
            'layInTime' => 'Jam Masuk',
        ];
    }

    public function messages()
    {
        return [
            // 'layBrndId.required' => ':attribute harus diisi',
            'layGymbrId.required' => ':attribute harus diisi',
            'layGympkgId.required' => ':attribute harus diisi',
            'layInDate.required' => ':attribute harus diisi',
            'layInTime.required' => ':attribute harus diisi',
        ];
    }
}
