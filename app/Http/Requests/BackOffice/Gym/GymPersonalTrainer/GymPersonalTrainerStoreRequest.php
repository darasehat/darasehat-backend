<?php

namespace App\Http\Requests\BackOffice\Gym\GymPersonalTrainer;

use Illuminate\Foundation\Http\FormRequest;

class GymPersonalTrainerStoreRequest extends FormRequest
{
    /**
     * Determine if the GymPersonalTrainer is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required|max:255'
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
        ];
    }
}
