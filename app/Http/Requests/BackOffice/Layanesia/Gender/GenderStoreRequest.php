<?php

namespace App\Http\Requests\BackOffice\Layanesia\Gender;

use Illuminate\Foundation\Http\FormRequest;

class GenderStoreRequest extends FormRequest
{
    /**
     * Determine if the Gender is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layCode' => 'required|max:4',
            'layName' => 'required|max:30',
        ];
    }

    public function attributes()
    {
        return [
            'layCode' => 'Kode',
            'layName' => 'Nama',
        ];
    }

    public function messages()
    {
        return [
            'layCode.required' => ':attribute harus diisi',
            'layCode.max' => 'Maksimal :max karakter',
            'layCode.unique' => ':attribute sudah terdaftar',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
        ];
    }
}
