<?php

namespace App\Http\Requests\BackOffice\Layanesia\Company;

use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
    /**
     * Determine if the Company is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required|max:255',
            'layAddress' => 'required',
            'layJoinDate' => 'required|date_format:Y-m-d',
            'layCouId' => 'required',
            'layAdm1Id' => 'required',
            'layAdm2Id' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layAddress' => 'Alamat',
            'layJoinDate' => 'Tanggal Gabung',
            'layCouId' => 'Negara',
            'layAdm1Id' => 'Provinsi',
            'layAdm2Id' => 'Kabupaten/ Kota',
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layAddress.required' => ':attribute harus diisi',
            'layJoinDate.required' => ':attribute harus diisi',
            'layJoinDate.date_format' => ':attribute harus berformat YYYY-MM-DD',
            'layCouId.required' => ':attribute harus diisi',
            'layAdm1Id.required' => ':attribute harus diisi',
            'layAdm2Id.required' => ':attribute harus diisi',
        ];
    }
}
