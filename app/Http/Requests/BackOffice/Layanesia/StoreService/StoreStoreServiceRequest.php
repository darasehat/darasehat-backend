<?php

namespace App\Http\Requests\BackOffice\Layanesia\StoreService;

use Illuminate\Foundation\Http\FormRequest;

class StoreStoreServiceRequest extends FormRequest
{
    /**
     * Determine if the StoreService is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layStoreId' => 'required',
            'laySrvdtlId' => 'required',
            'layStdate' => 'required|date_format:Y-m-d',
            'layEndate' => 'required|date_format:Y-m-d',
        ];
    }

    public function attributes()
    {
        return [
            'layStoreId' => 'Toko',
            'laySrvdtlId' => 'Detil Layanan',
            'layStdate' => 'Tanggal Mulai',
            'layEndate' => 'Tanggal Selesai'
        ];
    }

    public function messages()
    {
        return [
            'layStoreId.required' => ':attribute harus diisi',
            'laySrvdtlId.required' => ':attribute harus diisi',
            'layStdate.date_format' => ':attribute harus berformat YYYY-MM-DD',
            'layEndate.date_format' => ':attribute harus berformat YYYY-MM-DD',
        ];
    }
}
