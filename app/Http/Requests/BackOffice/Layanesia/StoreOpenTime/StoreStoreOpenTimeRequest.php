<?php

namespace App\Http\Requests\BackOffice\Layanesia\StoreOpenTime;

use Illuminate\Foundation\Http\FormRequest;

class StoreStoreOpenTimeRequest extends FormRequest
{
    /**
     * Determine if the Store is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layStoreId' => 'required',
            'layDay' => 'required|max:255',
            'layOpen' => 'required',
            'layClose' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layStoreId' => 'Toko',
            'layDay' => 'Hari',
            'layOpen' => 'Waktu Buka',
            'layClose' => 'Waktu Tutup',
        ];
    }

    public function messages()
    {
        return [
            'layStoreId.required' => ':attribute harus diisi',
            'layDay.required' => ':attribute harus diisi',
            'layOpen.required' => ':attribute harus diisi',
            'layClose.required' => ':attribute harus diisi',
        ];
    }
}
