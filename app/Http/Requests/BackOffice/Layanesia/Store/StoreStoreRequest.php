<?php

namespace App\Http\Requests\BackOffice\Layanesia\Store;

use Illuminate\Foundation\Http\FormRequest;

class StoreStoreRequest extends FormRequest
{
    /**
     * Determine if the Store is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layOpenFlag' => 'required',
            'layName' => 'required|max:255',
            'layAddress' => 'required',
            'layOpenSince' => 'required|date_format:Y-m-d',
            'layBrndId' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layOpenFlag' => 'Flag Buka',
            'layName' => 'Nama',
            'layAddress' => 'Alamat',
            'layOpenSince' => 'Buka Sejak',
            'layBrndId' => 'Pedagang',
        ];
    }

    public function messages()
    {
        return [
            'layOpenFlag.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layAddress.required' => ':attribute harus diisi',
            'layOpenSince.required' => ':attribute harus diisi',
            'layOpenSince.date_format' => ':attribute harus berformat YYYY-MM-DD',
            'layBrndId.required' => ':attribute harus diisi',
        ];
    }
}
