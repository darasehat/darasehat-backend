<?php

namespace App\Http\Requests\BackOffice\Layanesia\ServiceDetail;

use Illuminate\Foundation\Http\FormRequest;

class StoreServiceDetailRequest extends FormRequest
{
    /**
     * Determine if the ServiceDetail is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required|max:255',
            'layCode' => 'required|max:255',
            'laySrvId' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layCode' => 'Kode',
            'laySrvId' => 'Service',
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layCode.required' => ':attribute harus diisi',
            'layCode.max' => 'Maksimal :max karakter',
            'laySrvId.required' => ':attribute harus diisi',
        ];
    }
}
