<?php

namespace App\Http\Requests\BackOffice\Layanesia\Brand;

use Illuminate\Foundation\Http\FormRequest;

class StoreBrandRequest extends FormRequest
{
    /**
     * Determine if the Brand is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layOpenFlag' => 'required',
            'layName' => 'required|max:255',
            'layAddress' => 'required',
            'layJoinDate' => 'required|date_format:Y-m-d',
            'layCmpId' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layOpenFlag' => 'Flag Buka',
            'layName' => 'Nama',
            'layAddress' => 'Alamat',
            'layJoinDate' => 'Tanggal Gabung',
            'layCmpId' => 'Perusahaan',
        ];
    }

    public function messages()
    {
        return [
            'layOpenFlag.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layAddress.required' => ':attribute harus diisi',
            'layJoinDate.required' => ':attribute harus diisi',
            'layJoinDate.date_format' => ':attribute harus berformat YYYY-MM-DD',
            'layCmpId.required' => ':attribute harus diisi',
        ];
    }
}
