<?php

namespace App\Http\Requests\BackOffice\Employee;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\User\Logics\UserLogic;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public $employee;
    // public $layPassword;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
        // $rules = [];

        // $this->employee = (new EmployeeLogic())->getEmailEmployeeList($this->layEmail, ['usr_id']);

        // if($this->layEmail)
        // {
        //     $rules['layEmail'] = [
        //         'sometimes',
        //         'required',
        //         'email',
        //         'max:255',
        //         function ($attribute, $value, $fail) {
        //             $attribute = $this->attributes()[$attribute];
        //             // $employee = (new EmployeeLogic())->getEmailEmployeeList($value, ['usr_id']);
        //             if ($this->employee->count()>0) //existing
        //             {
        //                 $fail($attribute.' sudah digunakan oleh karyawan lain');
        //             }
        //             else
        //             {
        //                 // $this->layPassword = '123';
        //             }
        //         },
        //     ];
        //     if ($this->employee->count()>0) //existing
        //     {
        //         // $fail($attribute.' sudah digunakan oleh karyawan lain');
        //     }
        //     else //baru
        //     {
        //         $rules['layPassword'] = [
        //             // 'sometimes',
        //             function ($attribute, $value, $fail) {
        //                 $fail($attribute.' set laypassword 2'.$this->layPassword);
        //                 //jika layEmail ada dan email belum ada di table user maka layPassword harus diisi
        //                 if($this->layEmail)
        //                 {
        //                     $fail($attribute.' untuk akun baru harus diisi');
        //                     $user = (new UserLogic())->getUserEmail($this->layEmail, ['id']);
        //                     if(!$user && $value == '123')
        //                     {
        //                         $attribute = $this->attributes()[$attribute];
        //                         $fail($attribute.' untuk akun baru harus diisi');
        //                     }
        //                 }
        //                 //jika email sudah ada tidak perlu divalidasi
        //             },
        //             'min:7',
        //             'max:255',
        //         ];
        //     }
        // }
        // $rules['layName'] = 'required|max:255';
        // $rules['layEnDate'] = 'required|date';

        // return $rules;
//===============
        // return [
        //     'layName' => 'required|max:255',
        //     'layEmail' => [
        //         'sometimes',
        //         'required',
        //         'email',
        //         'max:255',
        //         function ($attribute, $value, $fail) {
        //             $attribute = $this->attributes()[$attribute];
        //             $employee = (new EmployeeLogic())->getEmailEmployeeList($value, ['usr_id']);
        //             if ($employee->count()>0) //existing
        //             {
        //                 $fail($attribute.' sudah digunakan oleh karyawan lain');
        //             }
        //             else //baru
        //             {
        //                 // $fail($attribute.' set laypassword 1'.$this->layPassword);
        //                 if(!$this->layPassword)
        //                 {
        //                     $data = $this->all();
        //                     $data['layPassword'] = '222';
        //                     $this->getInputSource()->replace($data);
        //                     // $this->layPassword = 'wewew';
        //                     // $this->merge(['layPassword' => '123']);
        //                     // $this->request->add(['layPassword' => '123']);
        //                     // $fail($attribute.' set laypassword 2'.$this->layPassword);
        //                 }
        //             }
        //         },
        //     ],
        //     'layPassword' => [
        //         // 'sometimes',
        //         function ($attribute, $value, $fail) {
        //             $fail($attribute.' set laypassword 2'.$this->layPassword);
        //             //jika layEmail ada dan email belum ada di table user maka layPassword harus diisi
        //             if($this->layEmail)
        //             {
        //                 $fail($attribute.' untuk akun baru harus diisi');
        //                 $user = (new UserLogic())->getUserEmail($this->layEmail, ['id']);
        //                 if(!$user && $value == '123')
        //                 {
        //                     $attribute = $this->attributes()[$attribute];
        //                     $fail($attribute.' untuk akun baru harus diisi');
        //                 }
        //             }
        //             //jika email sudah ada tidak perlu divalidasi
        //         },
        //         'min:7',
        //         'max:255',
        //     ],
        //     'layEnDate' => 'required|date'
        // ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layEmail' => 'Email',
            'layPassword' => 'Password'
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layEmail.required' => ':attribute harus diisi',
            'layEmail.email' => ':attribute harus menggunakan format email yang benar (contoh:email@domain.com)',
            'layEmail.unique' => ':attribute sudah terdaftar',
            'layEmail.max' => 'Maksimal :max karakter',
            'layPassword.required' => ':attribute harus diisi',
            'layPassword.min' => ':attribute minimal :min',
            'layPassword.max' => ':attribute maksimal :max'
        ];
    }
}
