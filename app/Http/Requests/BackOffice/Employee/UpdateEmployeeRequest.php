<?php

namespace App\Http\Requests\BackOffice\Employee;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\User\Logics\UserLogic;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
        // return [
        //     'layName' => 'required|max:255',
        //     'layEmail' => [
        //         'sometimes',
        //         'required',
        //         'email',
        //         'max:255',
        //         function ($attribute, $value, $fail) {
        //             if($this->layEmailOld)//update email
        //             {
        //                 if($this->layEmailOld != $value) //email diganti
        //                 {
        //                     $attribute = $this->attributes()[$attribute];
        //                     $employee = (new EmployeeLogic())->getEmailEmployeeList($value, ['usr_id']);
        //                     if ($employee->count()>0)
        //                     {
        //                         $fail($attribute.' sudah digunakan oleh karyawan lain');
        //                     }
        //                     else //baru
        //                     {
        //                         if(!$this->layPassword)
        //                         {
        //                             $this->layPassword = '';
        //                         }
        //                     }
        //                 }
        //             }
        //             else //email baru
        //             {
        //                 $attribute = $this->attributes()[$attribute];
        //                 $employee = (new EmployeeLogic())->getEmailEmployeeList($value, ['usr_id']);
        //                 if ($employee->count()>0)
        //                 {
        //                     $fail($attribute.' sudah digunakan oleh karyawan lain');
        //                 }
        //                 else //baru
        //                 {
        //                     if(!$this->layPassword)
        //                     {
        //                         $this->layPassword = '';
        //                     }
        //                 }
        //             }
        //         },
        //     ],
        //     'layPassword' => [
        //         'sometimes',
        //         'required',
        //         function ($attribute, $value, $fail) {
        //             //jika layEmail ada dan email belum ada di table user maka layPassword harus diisi
        //             if($this->layEmail)
        //             {
        //                 $user = (new UserLogic())->getUserEmail($this->layEmail, ['id']);
        //                 if(!$user && $value == '')
        //                 {
        //                     $attribute = $this->attributes()[$attribute];
        //                     $fail($attribute.' untuk akun baru harus diisi');
        //                 }
        //             }
        //             //jika email sudah ada tidak perlu divalidasi
        //         },
        //         'min:7',
        //         'max:255',
        //     ],
        //     'layEnDate' => 'required|date'
        // ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layEmail' => 'Email',
            'layPassword' => 'Password'
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layEmail.required' => ':attribute harus diisi',
            'layEmail.email' => ':attribute harus menggunakan format email yang benar (contoh:email@domain.com)',
            'layEmail.unique' => ':attribute sudah terdaftar',
            'layEmail.max' => 'Maksimal :max karakter',
            'layPassword.required' => ':attribute harus diisi',
            'layPassword.min' => ':attribute minimal :min',
            'layPassword.max' => ':attribute maksimal :max'
        ];
    }
}
