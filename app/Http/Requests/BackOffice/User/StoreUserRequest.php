<?php

namespace App\Http\Requests\BackOffice\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required|max:255',
            'layEmail' => 'required|email|unique:App\Models\User,email|max:255',
            'layPassword' => 'required|min:7|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layEmail' => 'Email',
            'layPassword' => 'Kata Kunci',
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layEmail.required' => ':attribute harus diisi',
            'layEmail.email' => ':attribute harus menggunakan format email yang benar (contoh:email@domain.com)',
            'layEmail.unique' => ':attribute sudah terdaftar',
            'layEmail.max' => 'Maksimal :max karakter',
            'layPassword.required' => ':attribute harus diisi',
            'layPassword.min' => ':attribute minimal :min',
            'layPassword.max' => ':attribute maksimal :max'
        ];
    }
}
