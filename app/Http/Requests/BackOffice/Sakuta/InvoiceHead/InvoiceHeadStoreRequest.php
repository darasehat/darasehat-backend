<?php

namespace App\Http\Requests\BackOffice\Sakuta\InvoiceHead;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceHeadStoreRequest extends FormRequest
{
    /**
     * Determine if the InvoiceHead is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layBldmanId' => 'required',
            'layDate' => 'required',
            'layHouseId' => 'required',
            'layNo' => 'required|max:30',
        ];
    }

    public function attributes()
    {
        return [
            'layBldmanId' => 'Manajemen Gedung',
            'layDate' => 'Tanggal Tagihan',
            'layHouseId' => 'Rumah',
            'layNo' => 'Nomor Invoice',
        ];
    }

    public function messages()
    {
        return [
            'layBldmanId.required' => ':attribute harus diisi',
            'layDate.required' => ':attribute harus diisi',
            'layHouseId.required' => ':attribute harus diisi',
            'layNo.required' => ':attribute harus diisi',
            'layNo.max' => 'Maksimal :max karakter',
        ];
    }
}
