<?php

namespace App\Http\Requests\BackOffice\Sakuta\BuildingManagementHouse;

use Illuminate\Foundation\Http\FormRequest;

class BuildingManagementHouseStoreRequest extends FormRequest
{
    /**
     * Determine if the BuildingManagementHouse is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layBldmanId' => 'required',
            'layHouse' => 'required|max:30',
            'layConfirmationCode' => 'required|max:10',
            'layTenantEmail' => 'required|max:50',
            'layTenantPhone' => 'required|max:30'
        ];
    }

    public function attributes()
    {
        return [
            'layBldmanId' => 'Manajemen Gedung',
            'layHouse' => 'Nomor Rumah',
            'layConfirmationCode' => 'Kode Konfirmasi',
            'layTenantEmail' => 'Email',
            'layTenantPhone' => 'Nomor Telepon'
        ];
    }

    public function messages()
    {
        return [
            'layBldmanId.required' => ':attribute harus diisi',
            'layHouse.required' => ':attribute harus diisi',
            'layHouse.max' => 'Maksimal :max karakter',
            'layConfirmationCode.required' => ':attribute harus diisi',
            'layConfirmationCode.max' => 'Maksimal :max karakter',
            'layTenantEmail.required' => ':attribute harus diisi',
            'layTenantEmail.max' => 'Maksimal :max karakter',
            'layTenantPhone.required' => ':attribute harus diisi',
            'layTenantPhone.max' => 'Maksimal :max karakter',
        ];
    }
}
