<?php

namespace App\Http\Requests\BackOffice\Sakuta\CitizenVehicle;

use Illuminate\Foundation\Http\FormRequest;

class CitizenVehicleStoreRequest extends FormRequest
{
    /**
     * Determine if the CitizenVehicle is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layNeiId' => 'required',
            'layCitzenId' => 'required',
            'layVehcatId' => 'required',
            'layPlatNo' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layNeiId' => 'Lingkungan',
            'layCitzenId' => 'Warga',
            'layVehcatId' => 'Jenis Kendaraan',
            'layPlatNo' => 'Nomor Plat:255',
        ];
    }

    public function messages()
    {
        return [
            'layNeiId.required' => ':attribute harus diisi',
            'layCitzenId.required' => ':attribute harus diisi',
            'layVehcatId.required' => ':attribute harus diisi',
            'layPlatNo.required' => ':attribute harus diisi',
            'layPlatNo.max' => 'Maksimal :max karakter',
        ];
    }
}
