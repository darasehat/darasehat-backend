<?php

namespace App\Http\Requests\BackOffice\Sakuta\Goodsin;

use Illuminate\Foundation\Http\FormRequest;

class GoodsinStoreRequest extends FormRequest
{
    /**
     * Determine if the Goodsin is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layNeiId' => 'required',
            'layName' => 'required|max:255',
            'layReceivedBy' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layNeiId' => 'Lingkungan',
            'layName' => 'Nama',
            'layReceivedBy' => 'Penerima',
        ];
    }

    public function messages()
    {
        return [
            'layNeiId.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layReceivedBy.required' => ':attribute harus diisi',
        ];
    }
}
