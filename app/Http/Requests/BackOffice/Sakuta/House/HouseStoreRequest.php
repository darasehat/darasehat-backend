<?php

namespace App\Http\Requests\BackOffice\Sakuta\House;

use Illuminate\Foundation\Http\FormRequest;

class HouseStoreRequest extends FormRequest
{
    /**
     * Determine if the House is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layNeiId' => 'required',
            'layNo' => 'required|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'layNeiId' => 'Lingkungan',
            'layNo' => 'Nama',
        ];
    }

    public function messages()
    {
        return [
            'layNeiId.required' => ':attribute harus diisi',
            'layNo.required' => ':attribute harus diisi',
            'layNo.max' => 'Maksimal :max karakter',
        ];
    }
}
