<?php

namespace App\Http\Requests\BackOffice\Sakuta\Activity;

use Illuminate\Foundation\Http\FormRequest;

class ActivityStoreRequest extends FormRequest
{
    /**
     * Determine if the Country is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required|max:255',
            'layDescription' => 'required',
            'layPublishDate' => 'required|date_format:Y-m-d'
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layDescription' => 'Deskripsi',
            'layPublishDate' => 'Tanggal Terbit',
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layDescription.required' => ':attribute harus diisi',
            'layPublishDate.required' => ':attribute harus diisi',
        ];
    }
}
