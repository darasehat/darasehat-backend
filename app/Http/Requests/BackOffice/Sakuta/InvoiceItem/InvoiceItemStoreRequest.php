<?php

namespace App\Http\Requests\BackOffice\Sakuta\InvoiceItem;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceItemStoreRequest extends FormRequest
{
    /**
     * Determine if the InvoiceItem is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layBldmanId' => 'required',
            'layName' => 'required|max:255',
            'layNominal' => 'required|max:15',
            'layDescription' => 'required',
        ];
      }

    public function attributes()
    {
        return [
            'layBldmanId' => 'Manajemen Gedung',
            'layName' => 'Nama',
            'layNominal' => 'Nominal',
            'layDescription' => 'Keterangan',
        ];
    }

    public function messages()
    {
        return [
            'layBldmanId.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layNominal.required' => ':attribute harus diisi',
            'layNominal.max' => 'Maksimal :max karakter',
            'layDescription.required' => ':attribute harus diisi',
        ];
    }
}
