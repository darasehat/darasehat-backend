<?php

namespace App\Http\Requests\BackOffice\Sakuta\GuestVehicle;

use Illuminate\Foundation\Http\FormRequest;

class GuestVehicleStoreRequest extends FormRequest
{
    /**
     * Determine if the GuestVehicle is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layVehcatId' => 'required',
            'layPlatNo' => 'required|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'layVehcatId' => 'Jenis Kendaraan',
            'layPlatNo' => 'Nomor Plat',
        ];
    }

    public function messages()
    {
        return [
            'layVehcatId.required' => ':attribute harus diisi',
            'layPlatNo.required' => ':attribute harus diisi',
            'layPlatNo.max' => 'Maksimal :max karakter',
        ];
    }
}
