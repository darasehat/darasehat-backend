<?php

namespace App\Http\Requests\BackOffice\Sakuta\Citizen;

use Illuminate\Foundation\Http\FormRequest;

class CitizenStoreRequest extends FormRequest
{
    /**
     * Determine if the Citizen is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layNeiId' => 'required',
            'layHouseId' => 'required',
            'layName' => 'required|max:255',
            'layResidentStatus' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layNeiId' => 'Lingkungan',
            'layHouseId' => 'Rumah',
            'layName' => 'Nama',
            'layResidentStatus' => 'Status Menetap',
        ];
    }

    public function messages()
    {
        return [
            'layNeiId.required' => ':attribute harus diisi',
            'layHouseId.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layResidentStatus.required' => ':attribute harus diisi',
        ];
    }
}
