<?php

namespace App\Http\Requests\BackOffice\Sakuta\Neighborhood;

use Illuminate\Foundation\Http\FormRequest;

class NeighborhoodStoreRequest extends FormRequest
{
    /**
     * Determine if the Neighborhood is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required|max:255',
            'layAdm1Id' => 'required',
            'layAdm2Id' => 'required',
            'layAdm3Id' => 'required',
            'layNeicatId' => 'required',
            'layRt' => 'max:5',
            'layRw' => 'max:5',
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layAdm1Id' => 'Provinsi',
            'layAdm2Id' => 'Kota/Kabupaten',
            'layAdm3Id' => 'Kecamatan',
            'layNeicatId' => 'Jenis Lingkungan',
            'layRt' => 'RT',
            'layRw' => 'RW',
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layAdm1Id.required' => ':attribute harus diisi',
            'layAdm2Id.required' => ':attribute harus diisi',
            'layAdm3Id.required' => ':attribute harus diisi',
            'layNeicatId.required' => ':attribute harus diisi',
            'layRt.max' => 'Maksimal :max karakter',
            'layRw.max' => 'Maksimal :max karakter',
        ];
    }
}
