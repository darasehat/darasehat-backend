<?php

namespace App\Http\Requests\BackOffice\Sakuta\CitizenVehicleTraffic;

use Illuminate\Foundation\Http\FormRequest;

class CitizenVehicleTrafficStoreRequest extends FormRequest
{
    /**
     * Determine if the CitizenVehicleTraffic is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layCitizen' => 'required',
            'layCitzenvehId' => 'required',
            'layNeiId' => 'required',
            'layPlatNo' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'layCitizen' => 'warga',
            'layCitzenvehId' => 'kendaraan warga',
            'layNeiId' => 'lingkungan',
            'layPlatNo' => 'plat nomor'
        ];
    }

    public function messages()
    {
        return [
            'layCitizen.required' => ':attribute harus diisi',
            'layCitzenvehId.required' => ':attribute harus diisi',
            'layNeiId.required' => ':attribute harus diisi',
            'layPlatNo.required' => ':attribute harus diisi',
        ];
    }
}
