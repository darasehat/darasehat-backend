<?php

namespace App\Http\Requests\BackOffice\Sakuta\NeighborhoodSubDetail;

use Illuminate\Foundation\Http\FormRequest;

class NeighborhoodSubDetailStoreRequest extends FormRequest
{
    /**
     * Determine if the NeighborhoodSubDetail is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layAdm1Id' => 'required',
            'layAdm2Id' => 'required',
            'layAdm3Id' => 'required',
            'layAdm4Id' => 'required',
            'layNeidetId' => 'required',
            'layRt' => 'required|max:5',
        ];
    }

    public function attributes()
    {
        return [
            'layAdm1Id' => 'Provinsi',
            'layAdm2Id' => 'Kota/Kabupaten',
            'layAdm3Id' => 'Kecamatan',
            'layAdm4Id' => 'Desa',
            'layNeidetId' => 'Rw',
            'layRt' => 'RW',
        ];
    }

    public function messages()
    {
        return [
            'layAdm1Id.required' => ':attribute harus diisi',
            'layAdm2Id.required' => ':attribute harus diisi',
            'layAdm3Id.required' => ':attribute harus diisi',
            'layAdm4Id.required' => ':attribute harus diisi',
            'layNeidetId.required' => ':attribute harus diisi',
            'layRt.required' => ':attribute harus diisi',
            'layRt.max' => 'Maksimal :max karakter',
        ];
    }
}
