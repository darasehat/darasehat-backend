<?php

namespace App\Http\Requests\BackOffice\Sakuta\NeighborhoodDetail;

use Illuminate\Foundation\Http\FormRequest;

class NeighborhoodDetailStoreRequest extends FormRequest
{
    /**
     * Determine if the NeighborhoodDetail is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layAdm1Id' => 'required',
            'layAdm2Id' => 'required',
            'layAdm3Id' => 'required',
            'layAdm4Id' => 'required',
            'layRw' => 'required|max:5',
        ];
    }

    public function attributes()
    {
        return [
            'layAdm1Id' => 'Provinsi',
            'layAdm2Id' => 'Kota/Kabupaten',
            'layAdm3Id' => 'Kecamatan',
            'layAdm4Id' => 'Desa',
            'layRw' => 'RW',
        ];
    }

    public function messages()
    {
        return [
            'layAdm1Id.required' => ':attribute harus diisi',
            'layAdm2Id.required' => ':attribute harus diisi',
            'layAdm3Id.required' => ':attribute harus diisi',
            'layAdm4Id.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layRw.max' => 'Maksimal :max karakter',
        ];
    }
}
