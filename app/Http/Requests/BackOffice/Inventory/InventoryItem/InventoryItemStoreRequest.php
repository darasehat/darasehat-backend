<?php

namespace App\Http\Requests\BackOffice\Inventory\InventoryItem;

use Illuminate\Foundation\Http\FormRequest;

class InventoryItemStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layItmId' => 'required',
            'layName' => 'required|max:255',
            'layPrice' => 'required',
            'layStock' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layItmId' => 'Item',
            'layName' => 'Nama',
            'layPrice' => 'Harga',
            'layStock' => 'Persediaan',
        ];
    }

    public function messages()
    {
        return [
            'layItmId.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layPrice.required' => ':attribute harus diisi',
            'layStock.required' => ':attribute harus diisi',
        ];
    }
}
