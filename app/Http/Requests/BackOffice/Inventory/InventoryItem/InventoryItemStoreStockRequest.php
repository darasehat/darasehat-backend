<?php

namespace App\Http\Requests\BackOffice\Inventory\InventoryItem;

use Illuminate\Foundation\Http\FormRequest;

class InventoryItemStoreStockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layPrice' => 'required',
            'layStock' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layPrice' => 'Harga',
            'layStock' => 'Persediaan',
        ];
    }

    public function messages()
    {
        return [
            'layPrice.required' => ':attribute harus diisi',
            'layStock.required' => ':attribute harus diisi',
        ];
    }
}
