<?php

namespace App\Http\Requests\BackOffice\Inventory\Item;

use Illuminate\Foundation\Http\FormRequest;

class ItemStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layItmcatId' => 'required',
            'layName' => 'required|max:255',
            'layPrice' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'layItmcatId' => 'Kategori',
            'layName' => 'Nama',
            'layPrice' => 'Harga'
        ];
    }

    public function messages()
    {
        return [
            'layItmcatId.required' => ':attribute harus diisi',
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layPrice.required' => ':attribute harus diisi',
        ];
    }
}
