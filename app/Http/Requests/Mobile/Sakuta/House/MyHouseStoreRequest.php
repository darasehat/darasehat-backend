<?php

namespace App\Http\Requests\Mobile\Sakuta\House;

use Illuminate\Foundation\Http\FormRequest;

class MyHouseStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layAdm1Id' => 'required',
            'layAdm2Id' => 'required',
            'layAdm3Id' => 'required',
            'layAdm4Id' => 'required',
            'layNeiId' => 'required',
            'layNeidetId' => 'required:layRw',
            'layNeisdetId' => 'required:layRt',
            // 'layNeiName' => 'required_without:layNeiId|max:100',
            // 'layRt' => 'required_without:layNeidetId|numeric',
            // 'layRw' => 'required_without:layNeidetId|numeric',
            'layNo' => 'required|max:255',
            // 'layHouseId' => 'required_without:layNo',
        ];
    }

    public function attributes()
    {
        return [
            'layAdm1Id' => 'Provinsi',
            'layAdm2Id' => 'Kota/ Kabupaten',
            'layAdm3Id' => 'Kecamatan',
            'layAdm4Id' => 'Desa',
            'layNeiId' => 'Lingkungan',
            'layNeidetId' => 'RW',
            'layNeisdetId' => 'RT',
            // 'layNeiName' => 'Lingkungan',
            // 'layRt' => 'RT',
            // 'layRw' => 'RW',
            'layNo' => 'Nomor Rumah',
            // 'layHouseId' => 'Nomor Rumah',
        ];
    }

    public function messages()
    {
        return [
            'layAdm1Id.required' => ':attribute harus diisi',
            'layAdm2Id.required' => ':attribute harus diisi',
            'layAdm3Id.required' => ':attribute harus diisi',
            'layAdm4Id.required' => ':attribute harus diisi',
            'layNeiId.required' => ':attribute harus diisi',
            // 'layNeiName.required_without' => ':attribute harus diisi',
            // 'layNeiName.max' => 'Maksimal :max karakter',
            'layNeidetId.required_without' => ':attribute harus diisi',
            'layNeisdetId.required_without' => ':attribute harus diisi',
            // 'layRt.required_without' => ':attribute harus diisi',
            // 'layRt.numeric' => ':attribute harus angka',
            // 'layRw.required_without' => ':attribute harus diisi',
            // 'layRw.numeric' => ':attribute harus angka',
            'layNo.required_without' => ':attribute harus diisi',
            'layNo.max' => 'Maksimal :max karakter',
            // 'layHouseId.required_without' => ':attribute harus diisi',
        ];
    }
}
