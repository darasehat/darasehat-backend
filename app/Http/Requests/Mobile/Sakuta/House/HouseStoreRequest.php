<?php

namespace App\Http\Requests\Mobile\Sakuta\House;

use Illuminate\Foundation\Http\FormRequest;

class HouseStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layHouseNo' => 'required_without:layHouseId|max:255',
            'layHouseId' => 'required_without:layHouseNo',
        ];
    }

    public function attributes()
    {
        return [
            'layHouseNo' => 'Nomor Rumah',
            'layHouseId' => 'Nomor Rumah',
        ];
    }

    public function messages()
    {
        return [
            'layHouseNo.required_without' => ':attribute harus diisi',
            'layHouseNo.max' => 'Maksimal :max karakter',
            'layHouseId.required_without' => ':attribute harus diisi',
        ];
    }
}
