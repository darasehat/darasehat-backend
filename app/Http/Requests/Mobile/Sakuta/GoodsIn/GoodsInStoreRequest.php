<?php

namespace App\Http\Requests\Mobile\Sakuta\GoodsIn;

use Illuminate\Foundation\Http\FormRequest;

class GoodsInStoreRequest extends FormRequest
{
    /**
     * Determine if the GoodsIn is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required|max:255',
            'layReceivedBy' => 'required',
            'laySender' => 'required',
            'layDate' => 'required',
            'layTime' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layReceivedBy' => 'Penerima Barang',
            'laySender' => 'Pengirim barang',
            'layDate' => 'Tanggal Datang',
            'layTime' => 'Jam Datang'
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layReceivedBy.required' => ':attribute harus diisi',
            'laySender.required' => ':attribute harus diisi',
            'layDate.required'  => ':attribute harus diisi',
            'layTime.required' => ':attribute harus diisi'
        ];
    }
}
