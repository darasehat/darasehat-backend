<?php

namespace App\Http\Requests\Mobile\Sakuta\Guest;

use Illuminate\Foundation\Http\FormRequest;

class GuestStoreRequest extends FormRequest
{
    /**
     * Determine if the Guest is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required|max:255',
            'layHost' => 'required',
            'layOutDate' => 'required_without_all:layInDate,layInTime',
            'layOutTime' => 'required_without_all:layInDate,layInTime'
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Nama',
            'layHost' => 'Tuan Rumah',
            'layOutDate' => 'Tanggal Pulang',
            'layOutTime' => 'Jam Pulang'
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layName.max' => 'Maksimal :max karakter',
            'layHost.required' => ':attribute harus diisi',
        ];
    }
}
