<?php

namespace App\Http\Requests\Mobile\Sakuta\Activity;

use Illuminate\Foundation\Http\FormRequest;

class ActivityStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layName' => 'required',
            'layDescription' => 'required',
            'layPublishDate' => 'required:date',
            'layPublishTime' => 'required:time'
        ];
    }

    public function attributes()
    {
        return [
            'layName' => 'Judul Aktivitas',
            'layDescription' => 'Penjelasa Aktivitas',
            'layPublishDate' => 'Tanggal Publikasi',
            'layPublishTime' => 'Waktu Publikasi'
        ];
    }

    public function messages()
    {
        return [
            'layName.required' => ':attribute harus diisi',
            'layDescription.required' => ':attribute harus diisi',
            'layPublishDate.required' => ':attribute harus diisi',
            'layPublishTime.required' => ':attribute harus diisi',
        ];
    }
}
