<?php

namespace App\Http\Requests\Mobile\Sakuta\News;

use Illuminate\Foundation\Http\FormRequest;

class NewsStoreRequest extends FormRequest
{
    /**
     * Determine if the News is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'layNeiId' => 'required',
            'layTitle' => 'required|max:255',
            'layShort' => 'required',
            'layDescription' => 'required',
            'layPublishDate' => 'required|date',
            'layPublishTime' => 'required|date_format:H:i',
        ];
    }

    public function attributes()
    {
        return [
            // 'layNeiId' => 'Lingkungan',
            'layTitle' => 'Judul',
            'layShort' => 'Ringkasan Berita',
            'layDescription' => 'Berita',
            'layPublishDate' => 'Tanggal Terbit',
            'layPublishTime' => 'Waktu Terbit',
        ];
    }

    public function messages()
    {
        return [
            // 'layNeiId.required' => ':attribute harus diisi',
            'layTitle.required' => ':attribute harus diisi',
            'layTitle.max' => 'Maksimal :max karakter',
            'layShort.required' => ':attribute harus diisi',
            'layDescription.required' => ':attribute harus diisi',
            'layPublishDate.required' => ':attribute harus diisi',
            'layPublishDate.date' => ':attribute harus berformat tanggal',
            'layPublishTime.required' => ':attribute harus diisi',
            'layPublishTime.date_format' => ':attribute harus berformat waktu jam dan menit',
        ];
    }
}
