<?php

namespace App\Http\Requests\Mobile\Sakuta\NeighborhoodRequest;

use Illuminate\Foundation\Http\FormRequest;

class NeighborhoodRequestStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'layName' => 'required',
            // 'layRt' => 'required',
            // 'layRw' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            // 'layName' => 'Nama Lingkungan',
            // 'layRt' => 'RT',
            // 'layRw' => 'RW',
        ];
    }

    public function messages()
    {
        return [
            // 'layName.required' => ':attribute harus diisi',
            // 'layRt.required' => ':attribute harus diisi',
            // 'layRw.required' => ':attribute harus diisi',
        ];
    }
}
