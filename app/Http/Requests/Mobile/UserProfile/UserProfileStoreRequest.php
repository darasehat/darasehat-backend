<?php

namespace App\Http\Requests\Mobile\UserProfile;

use Illuminate\Foundation\Http\FormRequest;

class UserProfileStoreRequest extends FormRequest
{
    /**
     * Determine if the Country is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'layAdm1Id' => 'required',
            'layAdm2Id' => 'required',
            'layAdm3Id' => 'required',
            'layAdm4Id' => 'required',
            'layNeicatId' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'layAdm1Id' => 'Provinsi',
            'layAdm2Id' => 'Kabupaten/Kota',
            'layAdm3Id' => 'Kecamatan',
            'layAdm4Id' => 'Desa/Kelurahan',
            'layNeicatId' => 'Jenis Lingkungan',
        ];
    }

    public function messages()
    {
        return [
            'layAdm1Id.required' => ':attribute harus diisi',
            'layAdm2Id.required' => ':attribute harus diisi',
            'layAdm3Id.required' => ':attribute harus diisi',
            'layAdm4Id.required' => ':attribute harus diisi',
            'layNeicatId.required' => ':attribute harus diisi',
        ];
    }
}
