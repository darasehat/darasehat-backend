<?php

namespace App\Http\Controllers\BackOffice\Employee\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests\BackOffice\Employee\StoreEmployeeRequest;
use App\Http\Requests\BackOffice\Employee\UpdateEmployeeRequest;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeProcessStore;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeProcessUpdate;
use App\Modules\BackOffice\Employee\Employee\Validations\ValidateEmployeeStore;
use App\Modules\BackOffice\Employee\Employee\Validations\ValidateEmployeeUpdate;

use App\Modules\BackOffice\Employee\EmployeeRole\Validations\ValidateEmployeeRole;
use App\Modules\BackOffice\User\Logics\UserLogic;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new EmployeeLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    public $formRequest; 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        (new ValidateEmployeeRole())->validate($request);
        (new ValidateEmployeeStore())->validate($request);

        $datetime=date('Y-m-d H:i:s');
        $result = (new EmployeeProcessStore())->doStore($request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new EmployeeLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new EmployeeLogic())->doEdit($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        (new ValidateEmployeeUpdate())->validate($request);

        $datetime=date('Y-m-d H:i:s');
        $result = (new EmployeeProcessUpdate())->doUpdate($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new EmployeeLogic())->doDelete($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    public function selectList()
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new EmployeeLogic())->doSelectList();
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }
}
