<?php

namespace App\Http\Controllers\BackOffice\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use App\Models\UserPassport;
use App\Utilities\ProxyRequest;
use App\Modules\BackOffice\Employee\Employee\Logics\EmployeeLogic;
use App\Modules\BackOffice\Auth\Validations\ValidateLogin;

class AuthController extends Controller
{
    protected $proxy;

    public function __construct(ProxyRequest $proxy)
    {
        $this->proxy = $proxy;
        $this->middleware('guest')->except('logout');
    }

    public function register()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
        ]);

        $resp = $this->proxy->grantPasswordToken(
            $user->email,
            request('password')
        );

        return response([
            'token' => $resp['access_token'],
            'expiresIn' => $resp['expires_in'],
            'message' => 'Your account has been created',
        ], 201);
    }

    public function login(Request $request)
    {
        if(!Auth::attempt($request->only('email', 'password')))
        {
            return response([
                "errors" => [
                    "email" => [
                        "email atau password salah"
                    ]
                ]
                // "status" => false,
                // "code" => 'login01',
                // "errors" => [
                //     "email atau password salah",
                // ],
            ], 200);
        }
        $user = UserPassport::where('email', $request->email)->first();
        // if(!$user)
        // {
        //     return response([
        //         "status" => false,
        //         "code" => 'login02',
        //         "errors" => [
        //             "email atau password salah",
        //         ],
        //     ], 200);
        // }
        // (new ValidateLogin())->validate($request, $user);

        // abort_unless($user, 404, 'This combination does not exists.');
        // abort_unless(
        //     \Hash::check($request->password, $user->password),
        //     403,
        //     'This combination does not exists.'
        // );

        $resp = (new ProxyRequest())
            ->grantPasswordToken($request->email, $request->password);
        if(!$resp)
        {
            return response([
                "status" => false,
                "code" => 'login03',
                "errors" => [
                    "email atau password salah",
                ],
            ], 200);
        }
        /**
         * permission
         */
        $result = $user->getAllPermissions();
        $permission = [];
        foreach ($result as $key => $value) {
            $permission[$key] = $value->name;
        }
        /**
         * employee level
         */
        // $level = (new EmployeeLogic())->getLevel();
        return response([
            'status' => true,
            'token' => $resp['access_token'],
            'permission' => $permission,
            // 'level' => $level,
            'expiresIn' => $resp['expires_in'],
            'message' => 'You have been logged in',
        ], 200)->header('Authorization', $resp['access_token']);
        // }
        // else
        // {
        //     return response([
        //         "status" => false,
        //         "errors" => [
        //             "email atau password salah",
        //         ],
        //     ], 200);
        // }
    }

    public function refreshToken()
    {
        $resp = $this->proxy->refreshAccessToken();

        return response([
            'token' => $resp['access_token'],
            'expiresIn' => $resp['expires_in'],
            'message' => 'Token has been refreshed.',
        ], 200);
    }

    public function logout()
    {
        $token = request()->user()->token();
        $token->delete();

        // remove the httponly cookie
        cookie()->queue(cookie()->forget('refresh_token'));

        return response([
            'message' => 'You have been successfully logged out',
        ], 200);
    }
}
