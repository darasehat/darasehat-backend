<?php

namespace App\Http\Controllers\BackOffice\EndUser;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BackOffice\EndUser\StoreEndUserRequest;
use App\Modules\BackOffice\EndUser\Logics\EndUserLogic;
use App\Modules\BackOffice\EndUser\Logics\EndUserProcessStore;
use App\Modules\BackOffice\EndUser\Logics\EndUserProcessUpdate;

class EndUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new EndUserLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEndUserRequest $request)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new EndUserProcessStore())->doStore($request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new EndUserLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new EndUserLogic())->doEdit($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new EndUserProcessUpdate())->doUpdate($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [updatePassword description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function updatePassword(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new EndUserLogic())->doUpdatePassword($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new EndUserLogic())->doDelete($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [autoCompleteSelectList description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function autoCompleteSelectList($query)
    {
        $result = (new EndUserLogic())->doAutoCompleteSelectList($query);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }
}
