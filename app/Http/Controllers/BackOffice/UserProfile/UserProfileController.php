<?php

namespace App\Http\Controllers\BackOffice\UserProfile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\BackOffice\UserProfile\Logics\UserProfileLogic;
use App\Modules\BackOffice\UserProfile\Logics\UserProfileProcessStore;
use App\Modules\BackOffice\UserProfile\Logics\UserProfileProcessDelete;
use App\Http\Requests\BackOffice\UserProfile\UserProfileStoreRequest;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new UserProfileLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserProfileStoreRequest $request)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new UserProfileProcessStore())->doStore($request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new UserProfileLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new UserProfileLogic())->doEdit($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new UserProfileLogic())->doUpdate($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new UserProfileProcessDelete())->doDelete($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [myProfile description]
     * @return [type] [description]
     */
    public function myProfile()
    {
        $result = (new UserProfileLogic())->myProfile();
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );        
    }

    /**
     * [selectList description]
     * @return [type] [description]
     */
    public function selectList()
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new UserProfileLogic())->doSelectList();
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }
}
