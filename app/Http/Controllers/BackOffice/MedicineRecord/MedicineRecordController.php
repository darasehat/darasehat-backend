<?php

namespace App\Http\Controllers\BackOffice\MedicineRecord;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordLogic;
use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordProcessStore;
use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordProcessUpdate;
use App\Modules\BackOffice\MedicineRecord\Logics\MedicineRecordProcessDelete;
use App\Modules\BackOffice\MedicineRecord\Exports\MedicineRecordExport;
use App\Modules\BackOffice\MedicineRecord\Imports\MedicineRecordImport;
// use App\Http\Requests\BackOffice\Gym\MedicineRecord\MedicineRecordStoreRequest;
// use App\Http\Requests\BackOffice\Gym\MedicineRecord\MedicineRecordUpdateRequest;

use App\Modules\BackOffice\Employee\Employee\Validations\ValidateLevel;

class MedicineRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new MedicineRecordLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result 
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new MedicineRecordProcessStore())->doStore($request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new MedicineRecordLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new MedicineRecordLogic())->doEdit($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new MedicineRecordProcessUpdate())->doUpdate($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new MedicineRecordLogic())->doDelete($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * 
     */
    public function export(Request $request)
    {
        return Excel::download(new MedicineRecordExport($request), 'medicine_report.xlsx');
    }

    /**
     * 
     */
    public function import(Request $request)
    {
        $datetime=date('YmdHis');
        $filext = $request->file('layImportFile')->getClientOriginalExtension();
        if($filext != 'csv')
        {
            return response()->json(
                [
                    "status" => 200,
                    "result" => "Gunakan CSV file"
                ],
                402
            );
        }
        $filename = $request->layImportFile->getClientOriginalName();

        $request->file('layImportFile')->move(base_path() . '/public/medication_report_import/', $datetime.'_'.$filename);

        $imagefile = base_path() . '/public/medication_report_import/'.$datetime.'_'.$filename;

        $result = (new MedicineRecordImport())->importData($imagefile);

        if($result['status'] == 'error')
        {
            return response()->json(
                [
                    "status" => "error",
                    "result" => $result['data']
                ],
                402
            );
        }
        return response()->json(
            [
                "status" => "success",
            ],
            200
        );
    }
}
