<?php

namespace App\Http\Controllers\BackOffice\AdministrativeAreaFirst;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\BackOffice\AdministrativeAreaFirst\Logics\AdministrativeAreaFirstLogic;

class AdministrativeAreaFirstController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new AdministrativeAreaFirstLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFirstLogic())->doStore($request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new AdministrativeAreaFirstLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFirstLogic())->doEdit($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFirstLogic())->doUpdate($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFirstLogic())->doDelete($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    public function selectList()
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFirstLogic())->doSelectList();
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }
}
