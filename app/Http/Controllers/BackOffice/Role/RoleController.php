<?php

namespace App\Http\Controllers\BackOffice\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\BackOffice\Role\Logics\RoleLogic;
use App\Models\Role;
use App\Models\User;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new RoleLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [rolesAddUser description]
     * @param  Request $request [description]
     * @param  Role    $role    [description]
     * @param  User    $user    [description]
     * @return [type]           [description]
     */
    public function rolesAddUser(Request $request, Role $role, User $user)
    {

        $user->assignRole($role);

        return response()->json([
            "message" => $role->name . " Role successfully assigned to User!"
        ], 200);
    }

    /**
     * [rolesRemoveUser description]
     * @param  Request $request [description]
     * @param  Role    $role    [description]
     * @param  User    $user    [description]
     * @return [type]           [description]
     */
    public function rolesRemoveUser(Request $request, Role $role, User $user)
    {
        $user->removeRole($role);

        return response()->json([
            "message" => $role->name . " Role successfully removed from User"
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datetime=date('Y-m-d H:i:s');
        // $result = (new RoleLogic())->doStore($request, $datetime);
        
        return response()->json([
            "message" => $role->name . " Role successfully assigned to User!"
        ], 200);
        
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new RoleLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new RoleLogic())->doEdit($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    public function editPermission($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $availPermissions = (new PermissionLogic())->getPermissionList();
        $assignedPermissions = (new PermissionRoleLogic())->getRoleAssignedPermission($id)->toArray();
        $result = [
            'availPermissions' => $availPermissions,
            'assignedPermissions' => $assignedPermissions
        ];
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    public function editUser($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $availusers = (new UserLogic())->getUserEmailList();
        $assignedusers = (new RoleUserLogic())->getUserAssignedRole($id)->toArray();
        $result = [
            'availusers' => $availusers,
            'assignedusers' => $assignedusers
        ];
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new RoleLogic())->doUpdate($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new RoleLogic())->doDelete($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    public function gymSelectList()
    {
        $result = (new RoleLogic())->getGymSelectList();
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }
}
