<?php

namespace App\Http\Controllers\BackOffice\AdministrativeAreaFourth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\BackOffice\AdministrativeAreaFourth\Logics\AdministrativeAreaFourthLogic;

class AdministrativeAreaFourthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new AdministrativeAreaFourthLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFourthLogic())->doStore($request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new AdministrativeAreaFourthLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFourthLogic())->doEdit($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFourthLogic())->doUpdate($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFourthLogic())->doDelete($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [selectList description]
     * @return [type] [description]
     */
    public function selectList($adm3)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new AdministrativeAreaFourthLogic())->doSelectList($adm3);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }
}
