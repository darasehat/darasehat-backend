<?php

namespace App\Http\Controllers\BackOffice\Store;

use Illuminate\Http\Request;
// use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\Http\Requests\BackOffice\Store\StoreTransactionStockStoreRequest;
use App\Modules\BackOffice\Store\StoreTransactionCategory\Logics\StoreTransactionStockLogic;
// use App\Modules\BackOffice\Package\Logics\StoreTransactionStockProcessDelete;
// use App\Modules\BackOffice\Package\Logics\StoreTransactionStockProcessStore;
// use App\Modules\BackOffice\Package\Logics\StoreTransactionStockProcessUpdate;
// use App\Modules\BackOffice\Package\Exports\StoreTransactionStockExport;

use App\Modules\BackOffice\Employee\Employee\Validations\ValidateLevel;

class StoreTransactionStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new StoreTransactionStockLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransactionStockStoreRequest $request)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new StoreTransactionStockLogic())->doStore($request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new StoreTransactionStockLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function edit($id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new StoreTransactionStockLogic())->doEdit($id, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new StoreTransactionStockLogic())->doUpdate($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        $result = (new StoreTransactionStockLogic())->doDelete($id, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }
}
