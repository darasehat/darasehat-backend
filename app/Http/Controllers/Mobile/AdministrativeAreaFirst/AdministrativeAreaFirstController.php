<?php

namespace App\Http\Controllers\Mobile\AdministrativeAreaFirst;

use App\Http\Controllers\Controller;
use App\Modules\Mobile\AdministrativeAreaFirst\Logics\AdministrativeAreaFirstLogic;

class AdministrativeAreaFirstController extends Controller
{
    /**
     * [selectList description]
     * @return [type] [description]
     */
    public function selectList()
    {
        $result = (new AdministrativeAreaFirstLogic())->doSelectList();
        return response()->json(
            [
                "status" => true,
                "result" => $result
            ],
            200
        );
    }
}
