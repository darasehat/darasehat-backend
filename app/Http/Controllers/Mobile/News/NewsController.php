<?php

namespace App\Http\Controllers\Mobile\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\Mobile\News\Logics\NewsLogic;
use App\Modules\Mobile\News\Logics\NewsPaginationLogic;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new NewsPaginationLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result 
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = (new NewsLogic())->doShow($id);
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }
}
