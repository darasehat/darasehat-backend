<?php

namespace App\Http\Controllers\Mobile\AdministrativeAreaThird;

use App\Http\Controllers\Controller;
use App\Modules\Mobile\AdministrativeAreaThird\Logics\AdministrativeAreaThirdLogic;

class AdministrativeAreaThirdController extends Controller
{
    /**
     * [selectList description]
     * @return [type] [description]
     */
    public function selectList($adm2)
    {
        $result = (new AdministrativeAreaThirdLogic())->doSelectList($adm2);
        return response()->json(
            [
                "status" => true,
                "result" => $result
            ],
            200
        );
    }
}
