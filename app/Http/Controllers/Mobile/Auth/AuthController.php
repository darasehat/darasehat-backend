<?php

namespace App\Http\Controllers\Mobile\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserMobile;

use App\Http\Requests\Mobile\Auth\RegisterNeighborhoodStoreRequest;
use App\Http\Requests\Mobile\Auth\RegisterStoreRequest;
use App\Http\Requests\Mobile\UserProfile\UserProfileStoreRequest;

use App\Modules\Mobile\User\Logics\UserLogic;
use App\Modules\Mobile\UserProfile\Logics\UserProfileLogic;
use App\Modules\Mobile\User\Logics\UserProcessRegister;


class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	// public function registerNeighborhood(RegisterNeighborhoodStoreRequest $request)
	// {
    //     $datetime=date('Y-m-d H:i:s');
	// 	$user = (new UserLogic())->doRegisterNeighborhood($request, $datetime);
	// }

    public function register(RegisterStoreRequest $request)
    {
        $datetime=date('Y-m-d H:i:s');
		$result = (new UserProcessRegister())->doStore($request, $datetime);
	    return response()->json([
            'status' => true,
            'data' => [
                'email' => $result['user']->email,
                'name' => $result['user']->name,
                'dob' => $request->layDateofbirth
            ],
	        'token' => $result['user']->api_token,
	        'message' => 'Your account has been created'
	    ], 200);
    }

    public function login(Request $request)
    {
    	$result = [];
        $datetime = date('Y-m-d H:i:s');
        if(Auth::attempt($request->only('email', 'password')))
        {
            $user = (new UserLogic())->doLogin($request->email);
            $profile = (new UserProfileLogic())->myProfile();
            if($profile)
            {
                $apitoken = (new UserLogic())->updateAPIToken($user->layId, $request, $datetime);
                $result = [
                    "status" => true,
                    "code" => "auth",
                    "data" => [
                        "layName" => $user->layName,
                        "email" => $user->layEmail,
                        "layHomeAddress" => $profile->layAddress,
                        "layPhoneNo" => $profile->layPhone,
                        "laySchool" => $profile->laySchool,
                        "layAdm1" => $profile->area1->adm1_name,
                        "layAdm2" => $profile->area2->adm2_name,
                        "layAdm3" => $profile->area3->adm3_name,
                        "layAdm4" => $profile->area4->adm4_name
                    ],
                    "token" => $apitoken->api_token,
			        'message' => 'You have been logged in',
                ];
            }
            else
            {
                $result = [
                    "status" => false,
                    "errors" => [
                        "user tidak terdaftar atau email salah",
                    ],
                ];
            }
        }
        else
        {
            $result = [
                "status" => false,
                "errors" => [
                    "email atau password salah",
                ],
            ];
        }

	    return response($result, 200);
    }

    // public function refreshTo()
    // {
	   //  $resp = $this->proxy->refreshAccessToken();

	   //  return response([
	   //      'token' => $resp['access_token'],
	   //      'expiresIn' => $resp['expires_in'],
	   //      'message' => 'Token has been refreshed.',
	   //  ], 200);
    // }

    public function logout(Request $request)
    {
        $datetime=date('Y-m-d H:i:s');
    	$userid = Auth::guard('token')->user()->id;
        $apitoken = (new UserLogic())->updateAPIToken($userid, $request, $datetime);
	    return response([
	        'message' => 'You have been successfully logged out',
	    ], 200);
    }

    /**
     * [registerKeamanan description]
     * @param  UserProfileStoreRequest $request [description]
     * @return [type]                           [description]
    public function registerKeamanan(UserProfileStoreRequest $request)
    {
    	$this->register($request);
    	(new UserProfileLogic())->doStore($request, $datetime);

    }
     */

    public function googleLogin(Request $request)
    {
        $googleAuthCode = $request->input('googleAuthCode');
        $accessTokenResponse= Socialite::driver('google')->getAccessTokenResponse($googleAuthCode);
        $accessToken=$accessTokenResponse["access_token"];
        $expiresIn=$accessTokenResponse["expires_in"];
        $idToken=$accessTokenResponse["id_token"];
        $refreshToken=isset($accessTokenResponse["refresh_token"])?$accessTokenResponse["refresh_token"]:"";
        $tokenType=$accessTokenResponse["token_type"];
        $user = Socialite::driver('google')->userFromToken($accessToken);
    }
}
