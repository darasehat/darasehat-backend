<?php

namespace App\Http\Controllers\Mobile\AdministrativeAreaSecond;

use App\Http\Controllers\Controller;
use App\Modules\Mobile\AdministrativeAreaSecond\Logics\AdministrativeAreaSecondLogic;

class AdministrativeAreaSecondController extends Controller
{
    /**
     * [selectList description]
     * @return [type] [description]
     */
    public function selectList($adm1)
    {
        $result = (new AdministrativeAreaSecondLogic())->doSelectList($adm1);
        return response()->json(
            [
                "status" => true,
                "result" => $result
            ],
            200
        );
    }
}
