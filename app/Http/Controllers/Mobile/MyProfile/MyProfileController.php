<?php

namespace App\Http\Controllers\Mobile\MyProfile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Mobile\MyProfile\Logics\MyProfileLogic;
use App\Modules\Mobile\MyProfile\Logics\MyProfileProcessStore;
use App\Modules\Mobile\MyProfile\Logics\MyProfileProcessDelete;
use App\Http\Requests\Mobile\MyProfile\MyProfileStoreRequest;
use App\Modules\Mobile\User\Logics\UserLogic;

class MyProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index(Request $request)
    // {
    //     $result = (new MyProfileLogic())->getPaginationData($request);
    //     return response()->json(
    //         [
    //             "status" => 200,
    //             "result" => $result
    //         ],
    //         200
    //     );
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(MyProfileStoreRequest $request)
    // {
    //     $datetime=date('Y-m-d H:i:s');
    //     $result = (new MyProfileProcessStore())->doStore($request, $datetime);
    //     return response()->json(
    //         [
    //             "status" => 200,
    //             "result" => $result
    //         ],
    //         200
    //     );
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $result = (new MyProfileLogic())->doShow();
        return response()->json(
            [
                "status" => 200,
                "result" => [
                    "layName" => $result->layName,
                    "email" => $result->user->email,
                    "layHomeAddress" => $result->layAddress,
                    "layPhoneNo" => $result->layPhone,
                    "laySchool" => $result->laySchool,
                    "layAdm1" => $result->area1->adm1_name,
                    "layAdm2" => $result->area2->adm2_name,
                    "layAdm3" => $result->area3->adm3_name,
                    "layAdm4" => $result->area4->adm4_name
                ]
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    // public function edit($id)
    // {
    //     $datetime=date('Y-m-d H:i:s');
    //     $result = (new MyProfileLogic())->doEdit($id, $datetime);
    //     return response()->json(
    //         [
    //             "status" => 200,
    //             "result" => $result
    //         ],
    //         200
    //     );
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $datetime=date('Y-m-d H:i:s');
        //ambil data profile untuk mendapatkan ID
        $profile = (new MyProfileLogic())->doShow();
        //update user
        (new UserLogic())->doUpdate($profile->usr_id, $request, $datetime);
        //update profile
        $result = (new MyProfileLogic())->doUpdate($profile->layId, $request, $datetime);
        return response()->json(
            [
                "status" => 200,
                "result" => [
                    "layName" => $result->usrprf_name,
                    "layEmail" => $result->user->email,
                    "layAddress" => $result->usrprf_home_address,
                    "layPhone" => $result->usrprf_phone_no,
                    "laySchool" => $result->usrprf_school,
                    "layAdm1" => $result->area1->adm1_name,
                    "layAdm2" => $result->area2->adm2_name,
                    "layAdm3" => $result->area3->adm3_name,
                    "layAdm4" => $result->area4->adm4_name,
                    "layAdm1Id" => $result->adm1_id,
                    "layAdm2Id" => $result->adm2_id,
                    "layAdm3Id" => $result->adm3_id,
                    "layAdm4Id" => $result->adm4_id
                ]
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function delete(Request $request, $id)
    // {
    //     $datetime=date('Y-m-d H:i:s');
    //     $result = (new MyProfileProcessDelete())->doDelete($id, $request, $datetime);
    //     return response()->json(
    //         [
    //             "status" => 200,
    //             "result" => $result
    //         ],
    //         200
    //     );
    // }

    /**
     * [myProfile description]
     * @return [type] [description]
     */
    // public function myProfile()
    // {
    //     $result = (new MyProfileLogic())->myProfile();
    //     return response()->json(
    //         [
    //             "status" => 200,
    //             "result" => $result
    //         ],
    //         200
    //     );        
    // }

    /**
     * [selectList description]
     * @return [type] [description]
     */
    // public function selectList()
    // {
    //     $datetime=date('Y-m-d H:i:s');
    //     $result = (new MyProfileLogic())->doSelectList();
    //     return response()->json(
    //         [
    //             "status" => 200,
    //             "result" => $result
    //         ],
    //         200
    //     );
    // }
}
