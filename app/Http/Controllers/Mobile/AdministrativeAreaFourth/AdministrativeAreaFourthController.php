<?php

namespace App\Http\Controllers\Mobile\AdministrativeAreaFourth;

use App\Http\Controllers\Controller;
use App\Modules\Mobile\AdministrativeAreaFourth\Logics\AdministrativeAreaFourthLogic;

class AdministrativeAreaFourthController extends Controller
{
    /**
     * [selectList description]
     * @return [type] [description]
     */
    public function selectList($adm3)
    {
        $result = (new AdministrativeAreaFourthLogic())->doSelectList($adm3);
        return response()->json(
            [
                "status" => true,
                "result" => $result
            ],
            200
        );
    }
}
