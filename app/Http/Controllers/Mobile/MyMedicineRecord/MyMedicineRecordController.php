<?php

namespace App\Http\Controllers\Mobile\MyMedicineRecord;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

use App\Modules\Mobile\MyMedicineRecord\Logics\MyMedicineRecordLogic;
use App\Modules\Mobile\MyMedicineRecord\Logics\MyMedicineRecordProcessStore;
use App\Modules\Mobile\MyMedicineRecord\Logics\MyMedicineRecordProcessUpdate;
use App\Modules\Mobile\MyMedicineRecord\Logics\MyMedicineRecordProcessDelete;
use App\Modules\Mobile\MyMedicineRecord\Exports\MyMedicineRecordExport;
// use App\Http\Requests\Mobile\Gym\MyMedicineRecord\MyMedicineRecordStoreRequest;
// use App\Http\Requests\Mobile\Gym\MyMedicineRecord\MyMedicineRecordUpdateRequest;

// use App\Modules\Mobile\Employee\Employee\Validations\ValidateLevel;

class MyMedicineRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result = (new MyMedicineRecordLogic())->getPaginationData($request);
        return response()->json(
            [
                "status" => 200,
                "result" => $result 
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datetime=date('Y-m-d H:i:s');
        $date=date('Y-m-d');
        $time=date('H:i:s');
        // if($request->layDate != '-' && $request->layDate != null)
        // {
        //     $date = $request->layDate;
        // }
        // if($request->layTime != '-' && $request->layTime != null)
        // {
        //     $time = $request->layTime;
        // }
        
        //cek data sudah ada atau belum
        $medrec = (new MyMedicineRecordLogic())->isDatetimeRecord($date, $time);
        if($medrec)
        {
            //update data
            $result = (new MyMedicineRecordProcessUpdate())->doUpdate($medrec->layId, $request, $datetime);
            // $result = [
            //     "layDate" => $result->medrec_date,
            //     "layTime" => $result->medrec_time,
            //     "layNextDate" => '-',
            //     "layNextTime" => '-'
            // ];
        }
        else
        {
            //input data baru
            $result = (new MyMedicineRecordProcessStore())->doStore($request, $datetime, $date, $time);
            // $result = [
            //     "layId" => $result->medrec_id,
            //     "layDate" => $result->medrec_date,
            //     "layTime" => $result->medrec_time,
            //     "layNextDate" => '-',
            //     "layNextTime" => '-'
            // ];
        }

        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $result = (new MyMedicineRecordLogic())->doShow();
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * [edit description]
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    // public function edit($id)
    // {
    //     $datetime=date('Y-m-d H:i:s');
    //     $result = (new MyMedicineRecordLogic())->doEdit($id, $datetime);
    //     return response()->json(
    //         [
    //             "status" => 200,
    //             "result" => $result
    //         ],
    //         200
    //     );
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datetime=date('Y-m-d H:i:s');
        
        if((new MyMedicineRecordLogic())->showMineRecord($id))
        {
            $result = (new MyMedicineRecordProcessUpdate())->doUpdate($id, $request, $datetime);
            $result = true;
        }
        else 
        {
            $result = false;
        }
        return response()->json(
            [
                "status" => 200,
                "result" => $result
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function delete(Request $request, $id)
    // {
    //     $datetime=date('Y-m-d H:i:s');
    //     $result = (new MyMedicineRecordLogic())->doDelete($id, $request, $datetime);
    //     return response()->json(
    //         [
    //             "status" => 200,
    //             "result" => $result
    //         ],
    //         200
    //     );
    // }

    /**
     * 
     */
    // public function export(Request $request)
    // {
    //     return Excel::download(new MyMedicineRecordExport($request), 'medicine_report.xlsx');
    // }
}
