<?php

namespace App\Queries\General;

use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BasicQuery
{  
    /**
     * [storeSaveUserData description]
     * @param  [type] $datetime    [description]
     * @param  [type] $datarequest [description]
     * @return [type]              [description]
     */
    protected function storeDataSaveFirst($datetime, $datarequest, $entity)
    {
        $data = ['mode' => 'store', 'datetime' => $datetime, 'userid' => null, 'datarequest' => $datarequest];
        return $this->saveTable($data, $entity);
    }

    /**
     * [storeSaveData description]
     * @param  [type] $datetime    [description]
     * @param  [type] $datarequest [description]
     * @return [type]              [description]
     */
    protected function storeDataSave($datetime, $datarequest, $entity)
    {
        $data = ['mode' => 'store', 'datetime' => $datetime, 'userid' => Auth::user()->id, 'datarequest' => $datarequest];
        return $this->saveTable($data, $entity);
    }

    /**
     * [updateSaveData description]
     * @param  [type] $datetime    [description]
     * @param  [type] $datarequest [description]
     * @return [type]              [description]
     */
    protected function updateDataSave($id, $datetime, $datarequest, $entity)
    {
        $data = ['mode' => 'update', 'id' => $id, 'datetime' => $datetime, 'userid' => Auth::user()->id, 'datarequest' => $datarequest];
        return $this->saveTable($data, $entity);
    }

    /**
     * [updateDataSaveTrial description]
     * @param  [type] $id          [description]
     * @param  [type] $datetime    [description]
     * @param  [type] $datarequest [description]
     * @param  [type] $entity      [description]
     * @return [type]              [description]
     */
    protected function updateDataSaveTrial($id, $datetime, $datarequest, $entity)
    {
        $data = ['mode' => 'update', 'id' => $id, 'datetime' => $datetime, 'userid' => null, 'datarequest' => $datarequest];
        return $this->saveTable($data, $entity);
    }

    /**
     * [deleteDataSave description]
     * @param  [type] $id          [description]
     * @param  [type] $datetime    [description]
     * @param  [type] $datarequest [description]
     * @param  [type] $entity      [description]
     * @return [type]              [description]
     */
    protected function deleteDataSave($id, $datetime, $datarequest, $entity)
    {
        $data = ['mode' => 'delete', 'id' => $id, 'datetime' => $datetime, 'userid' => Auth::user()->id, 'datarequest' => $datarequest];
        return $this->saveTable($data, $entity);
    }

    /**
     * [destroyData description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    protected function destroyData($id, $entity)
    {
        $data = ['mode' => 'hard', 'id' => $id];
        return $this->destroyModel($id, $entity);
    }

    /**
     * [destroy description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function destroyModel($id, $entity)
    {
        return $entity->destroy($id);
    }

    /**
     * [saveTable description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    private function saveTable($data, $entity)
    {
        if($data['mode'] == 'store')
        {
            $entity->createdby = $data['userid'];
            $entity->createdtime = $data['datetime'];
        }
        elseif($data['mode'] == 'update') 
        {
            $entity = $entity->find($data['id']);
            // $entity = $this->findRecordById($data['id'], null, '*');
            $entity->changedby = $data['userid'];
            $entity->changedtime = $data['datetime'];
        }
        elseif($data['mode'] == 'delete') 
        {
            $entity = $entity->find($data['id']);
            $entity->deletedby = $data['userid'];
            $entity->deletedtime = $data['datetime'];
        }
        else
        {
            return null;
        }
        foreach ($data['datarequest'] as $key => $value)
        {
            $entity->$key = $value;                    
        }
        $entity->save();

        return $entity;
    }
}