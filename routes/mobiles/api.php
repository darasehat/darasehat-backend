<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
  Route::POST('login',
  [
    'as' => 'auth.login',
    'uses' => 'App\Http\Controllers\Mobile\Auth\AuthController@login',
    // 'middleware' => ['permission:event_store'],
  ]);

  // Route::POST('/googlelogin',
  // [
  //   'as' => 'googlelogin',
  //   'uses' => 'App\Http\Controllers\Mobile\Auth\AuthController@googleLogin',
  // ]);

  Route::POST('register',
  [
    'as' => 'auth.register',
    'uses' => 'App\Http\Controllers\Mobile\Auth\AuthController@register',
    // 'middleware' => ['permission:event_store'],
  ]);
  
  //adm1
  Route::prefix('adm1')->group(function () {
    Route::GET('/selectlist',
    [
      'as' => 'administrativeareafirst.selectlist',
      'uses' => 'App\Http\Controllers\Mobile\AdministrativeAreaFirst\AdministrativeAreaFirstController@selectList',
      // 'middleware' => ['permission:administrativeareafirst_show'],
    ]);
  });

  //adm2
  Route::prefix('adm2')->group(function () {
    Route::GET('/selectlist/{adm1}',
    [
      'as' => 'administrativeareasecond.selectlist',
      'uses' => 'App\Http\Controllers\Mobile\AdministrativeAreaSecond\AdministrativeAreaSecondController@selectList',
      // 'middleware' => ['permission:administrativeareasecond_show'],
    ]);
  });

  //adm3
  Route::prefix('adm3')->group(function () {
    Route::GET('/selectlist/{adm2}',
    [
      'as' => 'administrativeareathird.selectlist',
      'uses' => 'App\Http\Controllers\Mobile\AdministrativeAreaThird\AdministrativeAreaThirdController@selectList',
      // 'middleware' => ['permission:administrativeareathird_show'],
    ]);
  });

  //adm4
  Route::prefix('adm4')->group(function () {
    Route::GET('/selectlist/{adm3}',
    [
      'as' => 'administrativeareafourth.selectlist',
      'uses' => 'App\Http\Controllers\Mobile\AdministrativeAreaFourth\AdministrativeAreaFourthController@selectList',
      // 'middleware' => ['permission:administrativeareathird_show'],
    ]);
  });

Route::middleware('auth:apitoken')->group(function () {
    //catatan minum obat
    // Route::GET('medicinerecord',
    // [
    //   'as' => 'medicinerecord.index',
    //   'uses' => 'Mobile\Sakuta\Auth\AuthController@index',
    //   // 'middleware' => ['permission:event_store'],
    // ]);

    // Route::GET('check_property',
    // [
    //   'as' => 'auth.checkProperty',
    //   'uses' => 'Mobile\Sakuta\Auth\AuthController@checkAuthProperty',
    //   // 'middleware' => ['permission:event_store'],
    // ]);
    /*
      "layAdm1Id": 1,
      "layAdm2Id": 1,
      "layAdm3Id": 1,
      "layAdm4Id": 1,
      "layNeicatId": 1,
      "layName": neighborhood,
      "layRt": rt,
      "layRw": rw,
    */
    // Route::prefix('neighborhood/request')->group(function () {
    //   Route::POST('',
    //   [
    //     'as' => 'neighborhoodrequest.store',
    //     'uses' => 'Mobile\Sakuta\NeighborhoodRequest\NeighborhoodRequestController@store',
    //     // 'middleware' => ['permission:event_store'],
    //   ]);
    // });
});

Route::middleware('auth:apitoken')->group(function () {
  Route::prefix('myprofile')->group(function () {
      Route::GET('',
      [
        'as' => 'myprofile.show',
        'uses' => 'App\Http\Controllers\Mobile\MyProfile\MyProfileController@show',
        // 'middleware' => ['permission:myprofile_show'],
      ]);
      Route::PUT('',
      [
        'as' => 'myprofile.update',
        'uses' => 'App\Http\Controllers\Mobile\MyProfile\MyProfileController@update',
        // 'middleware' => ['permission:myprofile_update'],
      ]);
    });

  Route::prefix('news')->group(function () {
      Route::GET('',
      [
        'as' => 'news.index',
        'uses' => 'App\Http\Controllers\Mobile\News\NewsController@index',
        // 'middleware' => ['permission:medicinerecord_index'],
      ]);
  });

  Route::prefix('mymedicinerecord')->group(function () {
      // Route::GET('export',
      // [
      //   'as' => 'medicinerecord.export',
      //   'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@export',
      // //   'middleware' => ['permission:medicinerecord_export'],
      // ]);
      Route::GET('',
      [
        'as' => 'mymedicinerecord.index',
        'uses' => 'App\Http\Controllers\Mobile\MyMedicineRecord\MyMedicineRecordController@index',
        // 'middleware' => ['permission:medicinerecord_index'],
      ]);
      Route::GET('/status',
      [
        'as' => 'mymedicinerecord.show',
        'uses' => 'App\Http\Controllers\Mobile\MyMedicineRecord\MyMedicineRecordController@show',
        // 'middleware' => ['permission:medicinerecord_show'],
      ]);
      Route::POST('',
      [
        'as' => 'mymedicinerecord.store',
        'uses' => 'App\Http\Controllers\Mobile\MyMedicineRecord\MyMedicineRecordController@store',
        // 'middleware' => ['permission:medicinerecord_store'],
      ]);
      // Route::GET('/selectlist',
      // [
      //   'as' => 'mymedicinerecord.selectlist',
      //   'uses' => 'App\Http\Controllers\Mobile\MyMedicineRecord\MyMedicineRecordController@selectList',
      //   // 'middleware' => ['permission:medicinerecord_show'],
      // ]);
      // Route::GET('/{medicinerecord}/edit',
      // [
      //   'as' => 'mymedicinerecord.edit',
      //   'uses' => 'App\Http\Controllers\Mobile\MyMedicineRecord\MyMedicineRecordController@edit',
      //   // 'middleware' => ['permission:medicinerecord_edit'],
      // ]);
      Route::PUT('/{medicinerecord}',
      [
        'as' => 'mymedicinerecord.update',
        'uses' => 'App\Http\Controllers\Mobile\MyMedicineRecord\MyMedicineRecordController@update',
        // 'middleware' => ['permission:medicinerecord_update'],
      ]);
      // Route::DELETE('/{medicinerecord}',
      // [
      //   'as' => 'mymedicinerecord.delete',
      //   'uses' => 'App\Http\Controllers\Mobile\MyMedicineRecord\MedicineRecordController@delete',
      //   // 'middleware' => ['permission:medicinerecord_delete'],
      // ]);
    });
  
}); 

