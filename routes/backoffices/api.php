<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/**
 * back office
 */
Route::middleware('api')->group(function () {
    Route::POST('/login',
      [
        'as' => 'login',
        'uses' => 'App\Http\Controllers\BackOffice\Auth\AuthController@login',
      ]);
  
    Route::middleware('guest')->group(function () {
      Route::POST('register',
      [
        'as' => 'register',
        'uses' => 'App\Http\Controllers\BackOffice\Auth\AuthController@register',
      ]);
      Route::POST('refresh-token',
      [
        'as' => 'refreshToken',
        'uses' => 'App\Http\Controllers\BackOffice\Auth\AuthController@refreshToken',
      ]);
    });
  
    Route::middleware('auth:api')->group(function () {
      Route::POST('logout',
      [
        'as' => 'logout',
        'uses' => 'App\Http\Controllers\BackOffice\Auth\AuthController@logout',
      ]);
    });
  
  
  
  });
  
Route::middleware('auth:api')->group(function () {
    Route::prefix('permission')->group(function () {
      Route::GET('',
      [
        'as' => 'permission.index',
        'uses' => 'App\Http\Controllers\BackOffice\Permission\PermissionController@index',
        // 'middleware' => ['permission:permission_index'],
      ]);
      Route::POST('',
      [
        'as' => 'permission.store',
        'uses' => 'App\Http\Controllers\BackOffice\Permission\PermissionController@store',
        // 'middleware' => ['permission:permission_store'],
      ]);
      Route::GET('/{permission}',
      [
        'as' => 'permission.show',
        'uses' => 'App\Http\Controllers\BackOffice\Permission\PermissionController@show',
        // 'middleware' => ['permission:permission_show'],
      ]);
      Route::GET('/{permission}/edit',
      [
        'as' => 'permission.edit',
        'uses' => 'App\Http\Controllers\BackOffice\Permission\PermissionController@edit',
        // 'middleware' => ['permission:permission_edit'],
      ]);
      Route::PUT('/{permission}',
      [
        'as' => 'permission.update',
        'uses' => 'App\Http\Controllers\BackOffice\Permission\PermissionController@update',
        // 'middleware' => ['permission:permission_update'],
      ]);
      Route::DELETE('/{permission}',
      [
        'as' => 'permission.delete',
        'uses' => 'App\Http\Controllers\BackOffice\Permission\PermissionController@delete',
        // 'middleware' => ['permission:permission_delete'],
      ]);
    });
  
    Route::prefix('role')->group(function () {
      Route::GET('',
      [
        'as' => 'role.index',
        'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@index',
        // 'middleware' => ['permission:role_index'],
      ]);
      // Route::POST('',
      // [
      //   'as' => 'role.store',
      //   'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@store',
      //   // 'middleware' => ['permission:role_store'],
      // ]);
      Route::GET('/{role}',
      [
        'as' => 'role.show',
        'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@show',
        // 'middleware' => ['permission:role_show'],
      ]);
      Route::GET('/{role}/edit',
      [
        'as' => 'role.edit',
        'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@edit',
        // 'middleware' => ['permission:role_edit'],
      ]);
      Route::GET('/{role}/edit/permission',
      [
        'as' => 'role.editpermission',
        'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@editPermission',
        // 'middleware' => ['permission:role_edit'],
      ]);
      Route::GET('/{role}/edit/user',
      [
        'as' => 'role.edituser',
        'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@editUser',
        // 'middleware' => ['permission:role_edit'],
      ]);
      // Route::PUT('/{role}',
      // [
      //   'as' => 'role.update',
      //   'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@update',
      //   // 'middleware' => ['permission:role_update'],
      // ]);
      // Route::DELETE('/{role}',
      // [
      //   'as' => 'role.delete',
      //   'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@delete',
      //   // 'middleware' => ['permission:role_delete'],
      // ]);
  
      // Route::GET('/gym/selectlist',
      // [
      //   'as' => 'role.gymselectlist',
      //   'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@gymSelectList',
      //   // 'middleware' => ['permission:role_gymselectlist'],
      // ]);
      Route::POST('/roles/{role}/assign/{user}',
      [
        'as' => 'role.assign',
        'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@rolesAddUser',
        // 'middleware' => ['permission:role_store'],
      ]);
      Route::POST('/roles/{role}/unassign/{user}',
      [
        'as' => 'role.unassign',
        'uses' => 'App\Http\Controllers\BackOffice\Role\RoleController@rolesRemoveUser',
        // 'middleware' => ['permission:role_store'],
      ]);
    });
  
    Route::prefix('rolepermission')->group(function () {
      // Route::GET('',
      // [
      //   'as' => 'rolepermission.index',
      //   'uses' => 'App\Http\Controllers\BackOffice\RolePermission\RolePermissionController@index',
      //   // 'middleware' => ['permission:permission_index'],
      // ]);
      // Route::POST('',
      // [
      //   'as' => 'rolepermission.store',
      //   'uses' => 'App\Http\Controllers\BackOffice\RolePermission\RolePermissionController@store',
      //   // 'middleware' => ['permission:permission_store'],
      // ]);
      // Route::GET('/{permission}',
      // [
      //   'as' => 'rolepermission.show',
      //   'uses' => 'App\Http\Controllers\BackOffice\RolePermission\RolePermissionController@show',
      //   // 'middleware' => ['permission:permission_show'],
      // ]);
      // Route::GET('/{permission}/edit',
      // [
      //   'as' => 'rolepermission.edit',
      //   'uses' => 'App\Http\Controllers\BackOffice\RolePermission\RolePermissionController@edit',
      //   // 'middleware' => ['permission:permission_edit'],
      // ]);
      // Route::PUT('/{permission}',
      // [
      //   'as' => 'rolepermission.update',
      //   'uses' => 'App\Http\Controllers\BackOffice\RolePermission\RolePermissionController@update',
      //   // 'middleware' => ['permission:permission_update'],
      // ]);
      // Route::DELETE('/{permission}',
      // [
      //   'as' => 'rolepermission.delete',
      //   'uses' => 'App\Http\Controllers\BackOffice\RolePermission\RolePermissionController@delete',
      //   // 'middleware' => ['permission:permission_delete'],
      // ]);
    });
  
    Route::prefix('user')->group(function () {
      Route::GET('',
      [
        'as' => 'user.index',
        'uses' => 'App\Http\Controllers\BackOffice\User\UserController@index',
        // 'middleware' => ['permission:user_index'],
      ]);
      Route::POST('',
      [
        'as' => 'user.store',
        'uses' => 'App\Http\Controllers\BackOffice\User\UserController@store',
        // 'middleware' => ['permission:user_store'],
      ]);
      Route::GET('/{user}',
      [
        'as' => 'user.show',
        'uses' => 'App\Http\Controllers\BackOffice\User\UserController@show',
        // 'middleware' => ['permission:user_show'],
      ]);
      Route::GET('/{user}/edit',
      [
        'as' => 'user.edit',
        'uses' => 'App\Http\Controllers\BackOffice\User\UserController@edit',
        // 'middleware' => ['permission:user_edit'],
      ]);
      Route::PUT('/{user}',
      [
        'as' => 'user.update',
        'uses' => 'App\Http\Controllers\BackOffice\User\UserController@update',
        // 'middleware' => ['permission:user_update'],
      ]);
      Route::PUT('/{user}/password',
      [
        'as' => 'user.update',
        'uses' => 'App\Http\Controllers\BackOffice\User\UserController@updatePassword',
        // 'middleware' => ['permission:user_update'],
      ]);
      Route::DELETE('/{user}',
      [
        'as' => 'user.delete',
        'uses' => 'App\Http\Controllers\BackOffice\User\UserController@delete',
        // 'middleware' => ['permission:user_delete'],
      ]);
      Route::GET('/autocomplete/selectlist/{user}',
      [
        'as' => 'user.edit',
        'uses' => 'App\Http\Controllers\BackOffice\User\UserController@autoCompleteSelectList',
        // 'middleware' => ['permission:user_edit'],
      ]);
    });

    Route::prefix('enduser')->group(function () {
      Route::GET('',
      [
        'as' => 'enduser.index',
        'uses' => 'App\Http\Controllers\BackOffice\EndUser\EndUserController@index',
        // 'middleware' => ['permission:enduser_index'],
      ]);
      Route::POST('',
      [
        'as' => 'enduser.store',
        'uses' => 'App\Http\Controllers\BackOffice\EndUser\EndUserController@store',
        // 'middleware' => ['permission:enduser_store'],
      ]);
      Route::GET('/{enduser}',
      [
        'as' => 'enduser.show',
        'uses' => 'App\Http\Controllers\BackOffice\EndUser\EndUserController@show',
        // 'middleware' => ['permission:enduser_show'],
      ]);
      Route::GET('/{enduser}/edit',
      [
        'as' => 'enduser.edit',
        'uses' => 'App\Http\Controllers\BackOffice\EndUser\EndUserController@edit',
        // 'middleware' => ['permission:enduser_edit'],
      ]);
      Route::PUT('/{enduser}',
      [
        'as' => 'enduser.update',
        'uses' => 'App\Http\Controllers\BackOffice\EndUser\EndUserController@update',
        // 'middleware' => ['permission:enduser_update'],
      ]);
      Route::PUT('/{enduser}/password',
      [
        'as' => 'enduser.update',
        'uses' => 'App\Http\Controllers\BackOffice\EndUser\EndUserController@updatePassword',
        // 'middleware' => ['permission:enduser_update'],
      ]);
      Route::DELETE('/{enduser}',
      [
        'as' => 'enduser.delete',
        'uses' => 'App\Http\Controllers\BackOffice\EndUser\EndUserController@delete',
        // 'middleware' => ['permission:enduser_delete'],
      ]);
      Route::GET('/autocomplete/selectlist/{enduser}',
      [
        'as' => 'enduser.edit',
        'uses' => 'App\Http\Controllers\BackOffice\EndUser\EndUserController@autoCompleteSelectList',
        // 'middleware' => ['permission:user_edit'],
      ]);
    });

    Route::prefix('operator')->group(function () {
      Route::GET('',
      [
        'as' => 'operator.index',
        'uses' => 'App\Http\Controllers\BackOffice\Operator\OperatorController@index',
        // 'middleware' => ['permission:operator_index'],
      ]);
      Route::POST('',
      [
        'as' => 'operator.store',
        'uses' => 'App\Http\Controllers\BackOffice\Operator\OperatorController@store',
        // 'middleware' => ['permission:operator_store'],
      ]);
      Route::GET('/{operator}',
      [
        'as' => 'operator.show',
        'uses' => 'App\Http\Controllers\BackOffice\Operator\OperatorController@show',
        // 'middleware' => ['permission:operator_show'],
      ]);
      Route::GET('/{operator}/edit',
      [
        'as' => 'operator.edit',
        'uses' => 'App\Http\Controllers\BackOffice\Operator\OperatorController@edit',
        // 'middleware' => ['permission:operator_edit'],
      ]);
      Route::PUT('/{operator}',
      [
        'as' => 'operator.update',
        'uses' => 'App\Http\Controllers\BackOffice\Operator\OperatorController@update',
        // 'middleware' => ['permission:operator_update'],
      ]);
      Route::PUT('/{operator}/password',
      [
        'as' => 'operator.update',
        'uses' => 'App\Http\Controllers\BackOffice\Operator\OperatorController@updatePassword',
        // 'middleware' => ['permission:operator_update'],
      ]);
      Route::DELETE('/{operator}',
      [
        'as' => 'operator.delete',
        'uses' => 'App\Http\Controllers\BackOffice\Operator\OperatorController@delete',
        // 'middleware' => ['permission:operator_delete'],
      ]);
      Route::GET('/autocomplete/selectlist/{operator}',
      [
        'as' => 'operator.edit',
        'uses' => 'App\Http\Controllers\BackOffice\Operator\OperatorController@autoCompleteSelectList',
        // 'middleware' => ['permission:operator_edit'],
      ]);
    });

});

Route::middleware('auth:api')->group(function () {
  Route::prefix('administrativefirst')->group(function () {
    Route::GET('',
    [
      'as' => 'administrativeareafirst.index',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFirst\AdministrativeAreaFirstController@index',
      // 'middleware' => ['permission:administrativeareafirst_index'],
    ]);
    Route::POST('',
    [
      'as' => 'administrativeareafirst.store',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFirst\AdministrativeAreaFirstController@store',
      // 'middleware' => ['permission:administrativeareafirst_store'],
    ]);
    Route::GET('/selectlist',
    [
      'as' => 'administrativeareafirst.selectlist',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFirst\AdministrativeAreaFirstController@selectList',
      // 'middleware' => ['permission:administrativeareafirst_show'],
    ]);
    Route::GET('/{administrativeareafirst}',
    [
      'as' => 'administrativeareafirst.show',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFirst\AdministrativeAreaFirstController@show',
      // 'middleware' => ['permission:administrativeareafirst_show'],
    ]);
    Route::GET('/{administrativeareafirst}/edit',
    [
      'as' => 'administrativeareafirst.edit',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFirst\AdministrativeAreaFirstController@edit',
      // 'middleware' => ['permission:administrativeareafirst_edit'],
    ]);
    Route::PUT('/{administrativeareafirst}',
    [
      'as' => 'administrativeareafirst.update',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFirst\AdministrativeAreaFirstController@update',
      // 'middleware' => ['permission:administrativeareafirst_update'],
    ]);
    Route::DELETE('/{administrativeareafirst}',
    [
      'as' => 'administrativeareafirst.delete',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFirst\AdministrativeAreaFirstController@delete',
      // 'middleware' => ['permission:administrativeareafirst_delete'],
    ]);
  });

  Route::prefix('administrativesecond')->group(function () {
    Route::GET('',
    [
      'as' => 'administrativeareasecond.index',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaSecond\AdministrativeAreaSecondController@index',
      // 'middleware' => ['permission:administrativeareasecond_index'],
    ]);
    Route::POST('',
    [
      'as' => 'administrativeareasecond.store',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaSecond\AdministrativeAreaSecondController@store',
      // 'middleware' => ['permission:administrativeareasecond_store'],
    ]);
    Route::GET('/selectlist/{adm1}',
    [
      'as' => 'administrativeareasecond.selectlist',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaSecond\AdministrativeAreaSecondController@selectList',
      // 'middleware' => ['permission:administrativeareasecond_show'],
    ]);
    Route::GET('/{administrativeareasecond}',
    [
      'as' => 'administrativeareasecond.show',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaSecond\AdministrativeAreaSecondController@show',
      // 'middleware' => ['permission:administrativeareasecond_show'],
    ]);
    Route::GET('/{administrativeareasecond}/edit',
    [
      'as' => 'administrativeareasecond.edit',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaSecond\AdministrativeAreaSecondController@edit',
      // 'middleware' => ['permission:administrativeareasecond_edit'],
    ]);
    Route::PUT('/{administrativeareasecond}',
    [
      'as' => 'administrativeareasecond.update',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaSecond\AdministrativeAreaSecondController@update',
      // 'middleware' => ['permission:administrativeareasecond_update'],
    ]);
    Route::DELETE('/{administrativeareasecond}',
    [
      'as' => 'administrativeareasecond.delete',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaSecond\AdministrativeAreaSecondController@delete',
      // 'middleware' => ['permission:administrativeareasecond_delete'],
    ]);
  });

  Route::prefix('administrativethird')->group(function () {
    Route::GET('',
    [
      'as' => 'administrativeareathird.index',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaThird\AdministrativeAreaThirdController@index',
      // 'middleware' => ['permission:administrativeareathird_index'],
    ]);
    Route::POST('',
    [
      'as' => 'administrativeareathird.store',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaThird\AdministrativeAreaThirdController@store',
      // 'middleware' => ['permission:administrativeareathird_store'],
    ]);
    Route::GET('/selectlist/{adm2}',
    [
      'as' => 'administrativeareathird.selectlist',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaThird\AdministrativeAreaThirdController@selectList',
      // 'middleware' => ['permission:administrativeareathird_show'],
    ]);
    Route::GET('/{administrativeareathird}',
    [
      'as' => 'administrativeareathird.show',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaThird\AdministrativeAreaThirdController@show',
      // 'middleware' => ['permission:administrativeareathird_show'],
    ]);
    Route::GET('/{administrativeareathird}/edit',
    [
      'as' => 'administrativeareathird.edit',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaThird\AdministrativeAreaThirdController@edit',
      // 'middleware' => ['permission:administrativeareathird_edit'],
    ]);
    Route::PUT('/{administrativeareathird}',
    [
      'as' => 'administrativeareathird.update',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaThird\AdministrativeAreaThirdController@update',
      // 'middleware' => ['permission:administrativeareathird_update'],
    ]);
    Route::DELETE('/{administrativeareathird}',
    [
      'as' => 'administrativeareathird.delete',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaThird\AdministrativeAreaThirdController@delete',
      // 'middleware' => ['permission:administrativeareathird_delete'],
    ]);
  });

  Route::prefix('administrativefourth')->group(function () {
    Route::GET('',
    [
      'as' => 'administrativeareafourth.index',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFourth\AdministrativeAreaFourthController@index',
      // 'middleware' => ['permission:administrativeareafourth_index'],
    ]);
    Route::POST('',
    [
      'as' => 'administrativeareafourth.store',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFourth\AdministrativeAreaFourthController@store',
      // 'middleware' => ['permission:administrativeareafourth_store'],
    ]);
    Route::GET('/selectlist/{adm3}',
    [
      'as' => 'administrativeareafourth.selectlist',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFourth\AdministrativeAreaFourthController@selectList',
      // 'middleware' => ['permission:administrativeareathird_show'],
    ]);
    Route::GET('/{administrativeareafourth}',
    [
      'as' => 'administrativeareafourth.show',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFourth\AdministrativeAreaFourthController@show',
      // 'middleware' => ['permission:administrativeareafourth_show'],
    ]);
    Route::GET('/{administrativeareafourth}/edit',
    [
      'as' => 'administrativeareafourth.edit',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFourth\AdministrativeAreaFourthController@edit',
      // 'middleware' => ['permission:administrativeareafourth_edit'],
    ]);
    Route::PUT('/{administrativeareafourth}',
    [
      'as' => 'administrativeareafourth.update',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFourth\AdministrativeAreaFourthController@update',
      // 'middleware' => ['permission:administrativeareafourth_update'],
    ]);
    Route::DELETE('/{administrativeareafourth}',
    [
      'as' => 'administrativeareafourth.delete',
      'uses' => 'App\Http\Controllers\BackOffice\AdministrativeAreaFourth\AdministrativeAreaFourthController@delete',
      // 'middleware' => ['permission:administrativeareafourth_delete'],
    ]);
  });
});

Route::middleware('auth:api')->group(function () {
    Route::prefix('userprofile')->group(function () {
        Route::GET('/myprofile',
        [
          'as' => 'userprofile.profile',
          'uses' => 'App\Http\Controllers\BackOffice\UserProfile\UserProfileController@myProfile',
          // 'middleware' => ['permission:user_index'],
        ]);
        Route::GET('',
        [
          'as' => 'userprofile.index',
          'uses' => 'App\Http\Controllers\BackOffice\UserProfile\UserProfileController@index',
          // 'middleware' => ['permission:userprofile_index'],
        ]);
        Route::POST('',
        [
          'as' => 'userprofile.store',
          'uses' => 'App\Http\Controllers\BackOffice\UserProfile\UserProfileController@store',
          // 'middleware' => ['permission:userprofile_store'],
        ]);
        Route::GET('/selectlist',
        [
          'as' => 'store.selectlist',
          'uses' => 'App\Http\Controllers\BackOffice\UserProfile\UserProfileController@selectList',
          // 'middleware' => ['permission:store_show'],
        ]);
        Route::GET('/{userprofile}',
        [
          'as' => 'userprofile.show',
          'uses' => 'App\Http\Controllers\BackOffice\UserProfile\UserProfileController@show',
          // 'middleware' => ['permission:userprofile_show'],
        ]);
        Route::GET('/{userprofile}/edit',
        [
          'as' => 'userprofile.edit',
          'uses' => 'App\Http\Controllers\BackOffice\UserProfile\UserProfileController@edit',
          // 'middleware' => ['permission:userprofile_edit'],
        ]);
        Route::PUT('/{userprofile}',
        [
          'as' => 'userprofile.update',
          'uses' => 'App\Http\Controllers\BackOffice\UserProfile\UserProfileController@update',
          // 'middleware' => ['permission:userprofile_update'],
        ]);
        Route::DELETE('/{userprofile}',
        [
          'as' => 'userprofile.delete',
          'uses' => 'App\Http\Controllers\BackOffice\UserProfile\UserProfileController@delete',
          // 'middleware' => ['permission:userprofile_delete'],
        ]);
      });

    Route::prefix('medicinerecord')->group(function () {
        Route::GET('export',
        [
          'as' => 'medicinerecord.export',
          'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@export',
        //   'middleware' => ['permission:medicinerecord_export'],
        ]);
        Route::POST('import',
        [
          'as' => 'medicinerecord.import',
          'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@import',
        //   'middleware' => ['permission:medicinerecord_import'],
        ]);
        Route::GET('',
        [
          'as' => 'medicinerecord.index',
          'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@index',
          // 'middleware' => ['permission:medicinerecord_index'],
        ]);
        Route::POST('',
        [
          'as' => 'medicinerecord.store',
          'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@store',
          // 'middleware' => ['permission:medicinerecord_store'],
        ]);
        // Route::GET('/selectlist',
        // [
        //   'as' => 'medicinerecord.selectlist',
        //   'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@selectList',
        //   // 'middleware' => ['permission:medicinerecord_show'],
        // ]);
        Route::GET('/{medicinerecord}',
        [
          'as' => 'medicinerecord.show',
          'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@show',
          // 'middleware' => ['permission:medicinerecord_show'],
        ]);
        Route::GET('/{medicinerecord}/edit',
        [
          'as' => 'medicinerecord.edit',
          'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@edit',
          // 'middleware' => ['permission:medicinerecord_edit'],
        ]);
        Route::PUT('/{medicinerecord}',
        [
          'as' => 'medicinerecord.update',
          'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@update',
          // 'middleware' => ['permission:medicinerecord_update'],
        ]);
        Route::DELETE('/{medicinerecord}',
        [
          'as' => 'medicinerecord.delete',
          'uses' => 'App\Http\Controllers\BackOffice\MedicineRecord\MedicineRecordController@delete',
          // 'middleware' => ['permission:medicinerecord_delete'],
        ]);
      });
    Route::prefix('news')->group(function () {
      Route::GET('',
      [
        'as' => 'news.index',
        'uses' => 'App\Http\Controllers\BackOffice\News\NewsController@index',
        // 'middleware' => ['permission:news_index'],
      ]);
      Route::POST('',
      [
        'as' => 'news.store',
        'uses' => 'App\Http\Controllers\BackOffice\News\NewsController@store',
        // 'middleware' => ['permission:news_store'],
      ]);
      Route::GET('/{newsid}',
      [
        'as' => 'news.show',
        'uses' => 'App\Http\Controllers\BackOffice\News\NewsController@show',
        // 'middleware' => ['permission:news_show'],
      ]);
      Route::GET('/{newsid}/edit',
      [
        'as' => 'news.edit',
        'uses' => 'App\Http\Controllers\BackOffice\News\NewsController@edit',
        // 'middleware' => ['permission:news_edit'],
      ]);
      Route::PUT('/{newsid}',
      [
        'as' => 'news.update',
        'uses' => 'App\Http\Controllers\BackOffice\News\NewsController@update',
        // 'middleware' => ['permission:news_update'],
      ]);
      Route::DELETE('/{newsid}',
      [
        'as' => 'news.delete',
        'uses' => 'App\Http\Controllers\BackOffice\News\NewsController@delete',
        // 'middleware' => ['permission:news_delete'],
      ]);
    });
        
}); 
    