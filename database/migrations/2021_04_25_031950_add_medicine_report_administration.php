<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMedicineReportAdministration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicine_records', function ($table) {
            $table->bigInteger('usr_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('adm1_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('adm2_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('adm3_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('adm4_id')->nullable()->unsigned()->default(null);

            $table->foreign('usr_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('adm1_id')->references('adm1_id')->on('administrative_area_firsts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adm2_id')->references('adm2_id')->on('administrative_area_seconds')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adm3_id')->references('adm3_id')->on('administrative_area_thirds')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adm4_id')->references('adm4_id')->on('administrative_area_fourths')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medicine_records', function ($table) {
        });
    }
}
