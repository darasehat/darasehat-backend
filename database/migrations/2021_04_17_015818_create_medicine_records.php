<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicineRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_records', function (Blueprint $table) {
            $table->bigIncrements('medrec_id');
            $table->bigInteger('createdby')->nullable()->unsigned()->default(null);
            $table->datetime('createdtime')->nullable()->default(null);
            $table->bigInteger('changedby')->nullable()->unsigned()->default(null);
            $table->datetime('changedtime')->nullable()->default(null);
            $table->bigInteger('deletedby')->nullable()->unsigned()->default(null);
            $table->datetime('deletedtime')->nullable()->default(null);
            $table->nullableTimestamps();
            $table->boolean('is_active')->nullable()->unsigned()->default(0);
            $table->bigInteger('usrprf_id')->nullable()->unsigned()->default(null);
            $table->date('medrec_date')->nullable()->default(null);
            $table->time('medrec_time')->nullable()->default(null);
            $table->datetime('medrec_datetime')->nullable()->default(null);

            $table->foreign('usrprf_id')->references('usrprf_id')->on('user_profiles')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicine_records');
    }
}
