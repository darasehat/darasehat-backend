<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('usrprf_id');
            $table->bigInteger('createdby')->nullable()->unsigned()->default(null);
            $table->datetime('createdtime')->nullable()->default(null);
            $table->bigInteger('changedby')->nullable()->unsigned()->default(null);
            $table->datetime('changedtime')->nullable()->default(null);
            $table->bigInteger('deletedby')->nullable()->unsigned()->default(null);
            $table->datetime('deletedtime')->nullable()->default(null);
            $table->nullableTimestamps();
            $table->boolean('is_active')->nullable()->unsigned()->default(0);
            $table->bigInteger('usr_id')->nullable()->unsigned()->default(null);
            $table->string('usrprf_name')->nullable()->default(null);
            $table->text('usrprf_home_address')->nullable()->default(null);
            $table->string('usrprf_phone_no',30)->nullable()->default(null);

            $table->bigInteger('adm1_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('adm2_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('adm3_id')->nullable()->unsigned()->default(null);
            $table->bigInteger('adm4_id')->nullable()->unsigned()->default(null);

            $table->foreign('adm1_id')->references('adm1_id')->on('administrative_area_firsts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adm2_id')->references('adm2_id')->on('administrative_area_seconds')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adm3_id')->references('adm3_id')->on('administrative_area_thirds')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('adm4_id')->references('adm4_id')->on('administrative_area_fourths')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('usr_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
