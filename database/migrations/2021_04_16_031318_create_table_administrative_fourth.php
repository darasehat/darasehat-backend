<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAdministrativeFourth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrative_area_fourths', function (Blueprint $table) {
            $table->bigIncrements('adm4_id');
            $table->bigInteger('createdby')->nullable()->unsigned()->default(null);
            $table->datetime('createdtime')->nullable()->default(null);
            $table->bigInteger('changedby')->nullable()->unsigned()->default(null);
            $table->datetime('changedtime')->nullable()->default(null);
            $table->bigInteger('deletedby')->nullable()->unsigned()->default(null);
            $table->datetime('deletedtime')->nullable()->default(null);
            $table->nullableTimestamps();
            $table->boolean('is_active')->nullable()->unsigned()->default(0);
            $table->string('adm4_name',255)->nullable()->default(null);
            $table->bigInteger('adm3_id')->nullable()->unsigned()->default(null);

            $table->foreign('adm3_id')->references('adm3_id')->on('administrative_area_thirds')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrative_area_fourths');
    }
}
