<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\UserPassport;
use DB;

class OperatorPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        $datas = [
            ['name' => 'end_user_index'],
            ['name' => 'end_user_show'],

            ['name' => 'end_user_edit_password'],
            ['name' => 'end_user_update_password'],

            ['name' => 'medicine_record_index'],
            ['name' => 'medicine_record_store'],
            ['name' => 'medicine_record_show'],
            ['name' => 'medicine_record_edit'],
            ['name' => 'medicine_record_update'],
            ['name' => 'medicine_record_delete'],
            ['name' => 'medicine_record_export'],

            //module News
            ['name' => 'news_index'],
            ['name' => 'news_store'],
            ['name' => 'news_show'],
            ['name' => 'news_edit'],
            ['name' => 'news_update'],
            ['name' => 'news_delete'],
        ];

        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        
        $role = (new Role())->where('name','=','user_operator')->first();
        if(!isset($role))
        {
            $role = Role::create(['guard_name' => 'api', 'name' => 'user_operator']);
        }

        // $user = (new UserPassport())->where('email','=','allmaster@layanesia.com')->first();
        // if($user)
        // {
        //     $user->assignRole('end_user');
        // }

        foreach ($datas as $key => $data) {
            $permission = (new Permission())->where('name','=',$data['name'])->first();
            if($permission)
            {
                print_r($data['name'].' : permission already exist'.PHP_EOL);
            }
            else
            {
                $permission = Permission::create(['guard_name' => 'api', 'name' => $data['name']]);                
                print_r($data['name'].' : permission added'.PHP_EOL);
            }

            $permissionrole = DB::table('role_has_permissions')->select('permission_id','role_id')->where('permission_id','=',$permission->id)->where('role_id','=',$role->id)->first();
            if($permissionrole)
            {
                print_r($data['name'].' : permission already given to : '.$role->name.PHP_EOL);
            }
            else
            {
                $role->givePermissionTo($data['name']);
                print_r($data['name'].', permission give to : '.$role->name.PHP_EOL);
            }
        }
    }
}
