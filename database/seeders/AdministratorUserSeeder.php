<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdministratorUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datetime=date('Y-m-d H:i:s');

        $data = ['name' => 'admin', 'email' => 'master@darasehat.com', 'password' => 'password','superadmin_flag' => 1,'created_at' => $datetime];
        $this->insertUser($data);
    }

    public function insertUser($data)
    {
        $user = (new User())->where('email','=',$data['email'])->first();
        if(!isset($user))
        {
            $user = (new User());
            $user->is_active = 1;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            $user->superadmin_flag = $data['superadmin_flag'];
            $user->created_at = $data['created_at'];
            $user->save();
        }
    }
}
