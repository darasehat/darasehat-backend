<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\UserPassport;
use DB;

class AdministratorRolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        $datas = [
            ['name' => 'dashboard'],
            //kelola user modul umum
            ['name' => 'user_index'],
            ['name' => 'user_store'],
            ['name' => 'user_show'],
            ['name' => 'user_edit'],
            ['name' => 'user_update'],
            ['name' => 'user_delete'],

            ['name' => 'user_edit_password'],
            ['name' => 'user_update_password'],

            ['name' => 'user_operator_index'],
            ['name' => 'user_operator_store'],
            ['name' => 'user_operator_show'],
            ['name' => 'user_operator_edit'],
            ['name' => 'user_operator_update'],
            ['name' => 'user_operator_delete'],

            ['name' => 'user_operator_edit_password'],
            ['name' => 'user_operator_update_password'],

            ['name' => 'end_user_index'],
            ['name' => 'end_user_store'],
            ['name' => 'end_user_show'],
            ['name' => 'end_user_edit'],
            ['name' => 'end_user_update'],
            ['name' => 'end_user_delete'],

            ['name' => 'end_user_edit_password'],
            ['name' => 'end_user_update_password'],

            //kelola role modul umum
            ['name' => 'role_index'],
            ['name' => 'role_store'],
            ['name' => 'role_show'],
            ['name' => 'role_edit'],
            ['name' => 'role_update'],
            ['name' => 'role_delete'],
            //kelola permission modul umum
            ['name' => 'permission_index'],
            ['name' => 'permission_store'],
            ['name' => 'permission_show'],
            ['name' => 'permission_edit'],
            ['name' => 'permission_update'],
            ['name' => 'permission_delete'],
            //kelola provinsi modul umum
            ['name' => 'administrativeareafirst_index'],
            ['name' => 'administrativeareafirst_store'],
            ['name' => 'administrativeareafirst_show'],
            ['name' => 'administrativeareafirst_edit'],
            ['name' => 'administrativeareafirst_update'],
            ['name' => 'administrativeareafirst_delete'],
            //kelola kota/kabupaten modul umum
            ['name' => 'administrativeareasecond_index'],
            ['name' => 'administrativeareasecond_store'],
            ['name' => 'administrativeareasecond_show'],
            ['name' => 'administrativeareasecond_edit'],
            ['name' => 'administrativeareasecond_update'],
            ['name' => 'administrativeareasecond_delete'],
            //kelola kecamatan modul umum
            ['name' => 'administrativeareathird_index'],
            ['name' => 'administrativeareathird_store'],
            ['name' => 'administrativeareathird_show'],
            ['name' => 'administrativeareathird_edit'],
            ['name' => 'administrativeareathird_update'],
            ['name' => 'administrativeareathird_delete'],
            //kelola desa modul umum
            ['name' => 'administrativeareafourth_index'],
            ['name' => 'administrativeareafourth_store'],
            ['name' => 'administrativeareafourth_show'],
            ['name' => 'administrativeareafourth_edit'],
            ['name' => 'administrativeareafourth_update'],
            ['name' => 'administrativeareafourth_delete'],
            //module Medicine record
            ['name' => 'medicine_record_index'],
            ['name' => 'medicine_record_store'],
            ['name' => 'medicine_record_show'],
            ['name' => 'medicine_record_edit'],
            ['name' => 'medicine_record_update'],
            ['name' => 'medicine_record_delete'],
            ['name' => 'medicine_record_export'],
            //module News
            ['name' => 'news_index'],
            ['name' => 'news_store'],
            ['name' => 'news_show'],
            ['name' => 'news_edit'],
            ['name' => 'news_update'],
            ['name' => 'news_delete'],
            
        ];

        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        
        $role = (new Role())->where('name','=','system_administrator')->first();
        if(!isset($role))
        {
            $role = Role::create(['guard_name' => 'api', 'name' => 'system_administrator']);
        }

        $user = (new UserPassport())->where('email','=','master@darasehat.com')->first();
        if($user)
        {
            $user->assignRole('system_administrator');
        }

        foreach ($datas as $key => $data) {
            $permission = (new Permission())->where('name','=',$data['name'])->first();
            if($permission)
            {
                print_r($data['name'].' : permission already exist'.PHP_EOL);
            }
            else
            {
                $permission = Permission::create(['guard_name' => 'api', 'name' => $data['name']]);                
                print_r($data['name'].' : permission added'.PHP_EOL);
            }

            $permissionrole = DB::table('role_has_permissions')->select('permission_id','role_id')->where('permission_id','=',$permission->id)->where('role_id','=',$role->id)->first();
            if($permissionrole)
            {
                print_r($data['name'].' : permission already given to : '.$role->name.PHP_EOL);
            }
            else
            {
                $role->givePermissionTo($data['name']);
                print_r($data['name'].', permission give to : '.$role->name.PHP_EOL);
            }
        }

    }
}
