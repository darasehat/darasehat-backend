<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\UserPassport;
use DB;

class EndUserPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        $datas = [
            ['name' => 'user_edit_password'],
            ['name' => 'user_update_password'],

            ['name' => 'my_medicine_record_index'],
            ['name' => 'my_medicine_record_store'],
            ['name' => 'my_medicine_record_show'],
        ];

        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        
        $role = (new Role())->where('name','=','end_user')->first();
        if(!isset($role))
        {
            $role = Role::create(['guard_name' => 'api', 'name' => 'end_user']);
        }

        // $user = (new UserPassport())->where('email','=','allmaster@layanesia.com')->first();
        // if($user)
        // {
        //     $user->assignRole('end_user');
        // }

        foreach ($datas as $key => $data) {
            $permission = (new Permission())->where('name','=',$data['name'])->first();
            if($permission)
            {
                print_r($data['name'].' : permission already exist'.PHP_EOL);
            }
            else
            {
                $permission = Permission::create(['guard_name' => 'api', 'name' => $data['name']]);                
                print_r($data['name'].' : permission added'.PHP_EOL);
            }

            $permissionrole = DB::table('role_has_permissions')->select('permission_id','role_id')->where('permission_id','=',$permission->id)->where('role_id','=',$role->id)->first();
            if($permissionrole)
            {
                print_r($data['name'].' : permission already given to : '.$role->name.PHP_EOL);
            }
            else
            {
                $role->givePermissionTo($data['name']);
                print_r($data['name'].', permission give to : '.$role->name.PHP_EOL);
            }
        }
    }
}
